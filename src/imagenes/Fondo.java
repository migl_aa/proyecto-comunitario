/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package imagenes;

import java.awt.Dimension;
import java.awt.Graphics;
import javax.swing.ImageIcon;

/**
 *
 * @author Equipo1
 */
//Le heredo todos los atributos del JPanel
public class Fondo extends javax.swing.JPanel{
   
    String nombArch;
// Constructo de la clase
    public Fondo(String nombArch) {
        this.nombArch = nombArch;
    }
    
    @Override
    public void paintComponent(Graphics g){
        Dimension tam = getSize();

        ImageIcon imagen = new ImageIcon(new ImageIcon(getClass().getResource(nombArch)).getImage());
        g.drawImage(imagen.getImage(), 0, 0, tam.width, tam.height, null);

        setOpaque(false);  
        super.paintComponent(g);
    }
}