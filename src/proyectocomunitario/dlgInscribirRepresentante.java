/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proyectocomunitario;

import BD.conexion;
import com.sun.glass.events.KeyEvent;
import com.toedter.calendar.JCalendar;
import java.awt.GraphicsEnvironment;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javafx.scene.input.KeyCode;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;

/**
 *
 * @author Miguel Arroyo
 */
public class dlgInscribirRepresentante extends javax.swing.JDialog {

    /**
     * Creates new form dlgInscribirRepresentante
     */
    public dlgInscribirRepresentante(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
        mostrar();
        iniciar();
        btnModificar.setEnabled(false);
        btnLimpiar.setEnabled(false);
        btnActualizar.setEnabled(false);
        // Hace que el frame aparezca en medio del monitor
        setLocationRelativeTo(null);
        // Evita que se pueda modificar el tamaño del frame
        setResizable(false);
        // Evita que la aplicacion pueda ser cerrada por el boton X
        setDefaultCloseOperation(0);
        // Se le coloca un titulo a la ventata
        setTitle("REGISTRAR REPRESENTANTE");
    }
    void activarCampos(){
        txtNombreRepre.setEnabled(true);
      txtApellidoRepre.setEnabled(true);
      txtFechaNaciRepre.getCalendarButton().setEnabled(true);
      txtOcupacionRepre.setEnabled(true);
      cmbTelefonoRepre.setEnabled(true);
      txtTelefonoRepre.setEnabled(true);
      cmbOtroTelefonoRepre.setEnabled(true);
      txtOtroTelefonoRepre.setEnabled(true);
      txtDireccionRepre.setEnabled(true);
      txtObservacionRepre.setEnabled(true);
    }
    void desactivarCampos(){
        txtNombreRepre.setEnabled(false);
      txtApellidoRepre.setEnabled(false);
      txtFechaNaciRepre.getCalendarButton().setEnabled(false);
      txtOcupacionRepre.setEnabled(false);
      cmbTelefonoRepre.setEnabled(false);
      txtTelefonoRepre.setEnabled(false);
      cmbOtroTelefonoRepre.setEnabled(false);
      txtOtroTelefonoRepre.setEnabled(false);
      txtDireccionRepre.setEnabled(false);
      txtObservacionRepre.setEnabled(false);
    }
    void verificar(){
        lblExiste.setVisible(false);
        lblNoExiste.setVisible(false);
        lblVerificacion.setVisible(true);
        conexion conn=new conexion();
        Connection cn=conn.conectar();
        String cedula="",verificar="",estado="";
        cedula = cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText();
        String query;
        query="SELECT ced_fa,estado FROM familiares WHERE ced_fa='"+cedula+"'";
        try {
            Statement st=cn.createStatement();
            ResultSet rs= st.executeQuery(query);
            while (rs.next()){
                verificar=rs.getString("ced_fa");
                estado=rs.getString("estado");
            }
            if (verificar!="" && estado.equals("1")){
                lblExiste.setVisible(true);
                lblVerificacion.setText("Representate registrado");
                String consulta="SELECT nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,tlf_fa,otroTlf_fa,direccion_fa,observacion_fa FROM familiares WHERE ced_fa='"+cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText()+"'";
                try {
                    Statement stm=cn.createStatement();
            ResultSet rsm= stm.executeQuery(consulta);
           
            while (rsm.next()){
                txtNombreRepre.setText(rsm.getString("nombre_fa"));
                txtApellidoRepre.setText(rsm.getString("apellido_fa"));
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date fechaDate;
                fechaDate = formato.parse(rsm.getString("fechaNacimiento_fa"));
                txtFechaNaciRepre.setDate(fechaDate);
                txtFechaNaciRepre.getCalendarButton().setEnabled(false);
                txtOcupacionRepre.setText(rsm.getString("profesion_fa"));
                String[] temp = {null,null};
                String []temp2 ;
                String tlf,tlf2;
                tlf=rsm.getString("tlf_fa");
                tlf2=rsm.getString("otroTlf_fa");
                
                    temp=tlf.split("-");
                    txtTelefonoRepre.setText(temp[1]);
                     cmbTelefonoRepre.setSelectedItem(temp[0]);
                
                if (!tlf2.equals("")){
                    temp2=tlf2.split("-");
                    txtOtroTelefonoRepre.setText(temp2[1]);
                    cmbOtroTelefonoRepre.setSelectedItem(temp2[0]);
                    txtOtroTelefonoRepre.setEnabled(false);
                    cmbOtroTelefonoRepre.setEnabled(false);
                }
                cmbTelefonoRepre.setSelectedItem(temp[0]);
                
                txtTelefonoRepre.setEnabled(false);
                txtDireccionRepre.setText(rsm.getString("direccion_fa"));
                txtObservacionRepre.setText(rsm.getString("observacion_fa"));
                btnModificar.setEnabled(true);
                btnActualizar.setEnabled(false);
                btnRegistrar.setEnabled(false);
                btnLimpiar.setEnabled(true);
                cmbCedulaRepre.setEnabled(false);
                txtCedulaRepre.setEditable(false);
                
                 Bitacora.bitacora("Inscribir representante", "Se verifico representante ya registrado "+ this.cmbCedulaRepre.getSelectedItem().toString()+"-"+this.txtCedulaRepre.getText());
            }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
            }
            else 
                if (verificar!="" && estado!="0"){
                   lblExiste.setVisible(true);
                lblVerificacion.setText("Representate inactivo");
                String consulta="SELECT nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,tlf_fa,otroTlf_fa,direccion_fa,observacion_fa FROM familiares WHERE ced_fa='"+cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText()+"'";
                    try {
                    Statement stm=cn.createStatement();
            ResultSet rsm= stm.executeQuery(consulta);
           
            while (rsm.next()){
                txtNombreRepre.setText(rsm.getString("nombre_fa"));
                txtApellidoRepre.setText(rsm.getString("apellido_fa"));
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date fechaDate;
                fechaDate = formato.parse(rsm.getString("fechaNacimiento_fa"));
                txtFechaNaciRepre.setDate(fechaDate);
                txtFechaNaciRepre.getCalendarButton().setEnabled(false);
                txtOcupacionRepre.setText(rsm.getString("profesion_fa"));
                String[] temp = {null,null};
                String []temp2 ;
                String tlf,tlf2;
                tlf=rsm.getString("tlf_fa");
                tlf2=rsm.getString("otroTlf_fa");
                
                    temp=tlf.split("-");
                    txtTelefonoRepre.setText(temp[1]);
                     cmbTelefonoRepre.setSelectedItem(temp[0]);
                
                if (!tlf2.equals("")){
                    temp2=tlf2.split("-");
                    txtOtroTelefonoRepre.setText(temp2[1]);
                    cmbOtroTelefonoRepre.setSelectedItem(temp2[0]);
                    txtOtroTelefonoRepre.setEnabled(false);
                    cmbOtroTelefonoRepre.setEnabled(false);
                }
                cmbTelefonoRepre.setSelectedItem(temp[0]);
                
                txtTelefonoRepre.setEnabled(false);
                txtDireccionRepre.setText(rsm.getString("direccion_fa"));
                txtObservacionRepre.setText(rsm.getString("observacion_fa"));
                btnModificar.setEnabled(true);
                btnRegistrar.setEnabled(false);
                btnLimpiar.setEnabled(false);
                btnActualizar.setEnabled(false);
                cmbCedulaRepre.setEnabled(false);
                txtCedulaRepre.setEditable(false);
                btnVerificarRepre.requestFocus();
                dlgRepresentanteInactivo dialog = new dlgRepresentanteInactivo(new javax.swing.JFrame(), true);
                dialog.setVisible(true);
            }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
                }
                }else
                {
               dlgRepresentanteInactivo dialog = new dlgRepresentanteInactivo(new javax.swing.JFrame(), true);
                lblVerificacion.setText("Representante no encontrado");
                activar();
                btnRegistrar.setEnabled(true);
                btnModificar.setEnabled(false);
                btnLimpiar.setEnabled(false);
                btnActualizar.setEnabled(false);
                
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    btnVerificarRepre.requestFocus();   
    }
 void mostrar () {
        //Se ocultan las etiquetas de los iconos 
        lblExiste.setVisible(false);
        lblNoExiste.setVisible(false);
        lblNoExisteNombre.setVisible(false);
        lblNoExisteApellido.setVisible(false);
        lblNoExisteFechaNaci.setVisible(false);
        lblNoExisteProfesion.setVisible(false);
        lblNoExisteTelefono.setVisible(false);
        
        lblNoExisteDireccion.setVisible(false);
        lblVerificacion.setVisible(false);
    }
    
    void iniciar (){
        // se inicializan todos lo campos de textos vacios y parcialmente bloqueados excepto la cedula
        lblNoExiste.setVisible(false);
        lblExiste.setVisible(false);
        lblVerificacion.setVisible(false);
        txtCedulaRepre.setText("");
        cmbCedulaRepre.setSelectedIndex(0);
        txtNombreRepre.setText("");
        txtApellidoRepre.setText("");
        txtOcupacionRepre.setText("");
        txtTelefonoRepre.setText("");
        txtOtroTelefonoRepre.setText("");
        txtDireccionRepre.setText("");
        txtObservacionRepre.setText("");
        txtFechaNaciRepre.setDate(null);
        txtNombreRepre.setEnabled(false);
        txtApellidoRepre.setEnabled(false);
        txtFechaNaciRepre.setEnabled(false);
        txtOcupacionRepre.setEnabled(false);
        txtTelefonoRepre.setEnabled(false);
        txtOtroTelefonoRepre.setEnabled(false);
        txtDireccionRepre.setEnabled(false);
        txtObservacionRepre.setEnabled(false);
        cmbTelefonoRepre.setEnabled(false);
        cmbOtroTelefonoRepre.setEnabled(false);
        btnRegistrar.setEnabled(false);
        cmbTelefonoRepre.setSelectedIndex(0);
        cmbOtroTelefonoRepre.setSelectedIndex(0);
        
    }
        public void activar(){
        //Se desbloquean los campos para poder ser activados
        txtNombreRepre.setEnabled(true);
        txtApellidoRepre.setEnabled(true);
        txtFechaNaciRepre.setEnabled(true);
        txtOcupacionRepre.setEnabled(true);
        
        txtDireccionRepre.setEnabled(true);
        txtObservacionRepre.setEnabled(true);
        cmbTelefonoRepre.setEnabled(true);
        cmbOtroTelefonoRepre.setEnabled(true);
        txtFechaNaciRepre.getDateEditor().setEnabled(false);
        
        
        JCalendar nuevo=txtFechaNaciRepre.getJCalendar();
            JSpinner bloqueo= (JSpinner) nuevo.getYearChooser().getSpinner();
            ((JTextField)bloqueo.getEditor()).setEditable(false);
        }
    
        void limpiar(){
            cmbCedulaRepre.setSelectedIndex(0);
            txtCedulaRepre.setText("");
            txtNombreRepre.setText("");
            txtApellidoRepre.setText("");
            txtFechaNaciRepre.setDate(null);
            txtOcupacionRepre.setText("");
            txtTelefonoRepre.setText("");
            cmbTelefonoRepre.setSelectedIndex(0);
            cmbOtroTelefonoRepre.setSelectedIndex(0);
            txtOtroTelefonoRepre.setText("");
            txtDireccionRepre.setText("");
            txtObservacionRepre.setText("");
        }
        
        public void registrarRepresentante(){
        //Conexion a base de datos
        conexion conn=new conexion();
        Connection cn=conn.conectar();
        // Declaracion de variables
        String cedula="",nombre="",apellido="",fechaNa="",profesion="",telf1="",telf2="",direccion="",observacion="",ano="",mes="",dia="";
        dlgDatosCargadosConExito obj = new dlgDatosCargadosConExito(new javax.swing.JFrame(), true);
         dlgDatosVacios advertencia = new dlgDatosVacios(new javax.swing.JFrame(), true);
        // Captura de parametros
        cedula=cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText();
        nombre=txtNombreRepre.getText();
        apellido=txtApellidoRepre.getText();
        if (txtFechaNaciRepre.getDate()==null)
        {
            fechaNa="";
        }    else{
       
        ano=Integer.toString(txtFechaNaciRepre.getCalendar().get(Calendar.YEAR));
        mes=Integer.toString(txtFechaNaciRepre.getCalendar().get(Calendar.MONTH)+1);
        dia=Integer.toString(txtFechaNaciRepre.getCalendar().get(Calendar.DAY_OF_MONTH));
        fechaNa=dia+"/"+mes+"/"+ano;

        }
        profesion=txtOcupacionRepre.getText();
        telf1= cmbTelefonoRepre.getSelectedItem().toString()+"-"+txtTelefonoRepre.getText();
        if (txtOtroTelefonoRepre.getText().isEmpty()){
            telf2="";
        }else{
        telf2= cmbOtroTelefonoRepre.getSelectedItem().toString()+"-"+txtOtroTelefonoRepre.getText();
        }
        direccion=txtDireccionRepre.getText();
        observacion=txtObservacionRepre.getText();
        // Declaracion de la consulta en SQL
        String consulta="";
        consulta="INSERT INTO familiares (ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,tlf_fa,otroTlf_fa,direccion_fa,observacion_fa,estado) VALUES (?,?,?,?,?,?,?,?,?,?)";
        
//Validar parametros para que no esten vacios
        // "If" que verifica si la caja de texto esta vacia, o el DataChooser contiene alguna fecha
        if ((txtCedulaRepre.getText().isEmpty()) || (txtNombreRepre.getText().isEmpty()) || (txtApellidoRepre.getText().isEmpty()) || (txtFechaNaciRepre.getDate()==null) || (txtOcupacionRepre.getText().isEmpty()) || (txtTelefonoRepre.getText().isEmpty()) || (txtDireccionRepre.getText().isEmpty())){
            advertencia.setVisible(true);
            //Se indica con una X el lugar o campo que esta vacio 
            //Se revisa con un If cada campo nuevamente para comprobar cual esta vacio
            if (txtCedulaRepre.getText().isEmpty()){
                lblNoExiste.setVisible(true);
            }
            if (txtNombreRepre.getText().isEmpty()){
                lblNoExisteNombre.setVisible(true);
            }
            if (txtApellidoRepre.getText().isEmpty()){
                lblNoExisteApellido.setVisible(true);
            }
            if(txtFechaNaciRepre.getDate()==null){
                lblNoExisteFechaNaci.setVisible(true);
            }
            if(txtOcupacionRepre.getText().isEmpty()){
                lblNoExisteProfesion.setVisible(true);
            }
            if(txtTelefonoRepre.getText().isEmpty()){
                lblNoExisteTelefono.setVisible(true);
            }
            if(txtDireccionRepre.getText().isEmpty()){
                lblNoExisteDireccion.setVisible(true);
            }
        }
             else {
        try {
           // Se ejecuta la consulta
            PreparedStatement stm=cn.prepareStatement(consulta);
           // Se insertan los datos  
            stm.setString(1, cedula);
            stm.setString(2, nombre);
            stm.setString(3, apellido);
            stm.setString(4, fechaNa);
            stm.setString(5, profesion);
            stm.setString(6, telf1);
            stm.setString(7, telf2);
            stm.setString(8, direccion);
            stm.setString(9, observacion);
            stm.setString(10,"1" );
           
            
            int n=stm.executeUpdate();
            if(n>0){
                    
// se llama el frame que indica que los datos han sido cargados
                obj.setVisible(true);
                Bitacora.bitacora("Inscribir Representante","Se registro el representate "+cedula);
            }
          iniciar();
          mostrar();
        } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, ex);
        }
         }
    }
        void eliminarRepresentante(){
            conexion conn=new conexion();
        Connection cn=conn.conectar();
        int rep=-1;
        String consulta="UPDATE familiares SET estado='0' WHERE ced_fa='"+this.cmbCedulaRepre.getSelectedItem().toString()+"-"+this.txtCedulaRepre.getText()+"'";
        String consulta2="SELECT Representado FROM estu_fami WHERE ced_fa='"+this.cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText()+"'";
            try {
               Statement pt=cn.createStatement();
               ResultSet rt=pt.executeQuery(consulta2);
                Statement st=cn.createStatement();
                        st.execute(consulta);
                        while (rt.next()){
                            rep=rt.getInt("Representado");
                            txtNombreRepre.setText(Integer.toString(rep));
                        }
                        if ((rep==1) || (rep==0)){
                            dlgNoSePuedeEliminarRepresentante dialog = new dlgNoSePuedeEliminarRepresentante(new javax.swing.JFrame(), true);
                            dialog.setVisible(true);
                            Bitacora.bitacora("Inscribir representante", "Se intento eliminar el representante "+this.cmbCedulaRepre.getSelectedItem().toString()+"-"+this.txtCedulaRepre.getText());
                        }else
                        if (rep==-1) 
                        {
                                st.execute(consulta);
                                Bitacora.bitacora("Inscribir representante", "Se eliminó el representante "+this.cmbCedulaRepre.getSelectedItem().toString()+"-"+this.txtCedulaRepre.getText()); 
                                dlgEliminadosConExito dialog = new dlgEliminadosConExito(new javax.swing.JFrame(), true);
                                 dialog.setVisible(true);
                                 desactivarCampos();
                                 btnLimpiar.setEnabled(false);
                                 limpiar();
                                 lblExiste.setVisible(false);
                                 lblVerificacion.setVisible(false);
                                 btnModificar.setEnabled(false);
                                
                            }else
                            txtNombreRepre.setText(Integer.toString(rep));
            } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            }
        }
        void actualizarRepresentante(){
            //Conexion a base de datos
        conexion conn=new conexion();
        Connection cn=conn.conectar();
        // Declaracion de variables
        String cedula="",nombre="",apellido="",fechaNa="",profesion="",telf1="",telf2="",direccion="",observacion="",ano="",mes="",dia="";
        dlgDatosCargadosConExito obj = new dlgDatosCargadosConExito(new javax.swing.JFrame(), true);
         dlgDatosVacios advertencia = new dlgDatosVacios(new javax.swing.JFrame(), true);
        // Captura de parametros
        cedula=cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText();
        nombre=txtNombreRepre.getText();
        apellido=txtApellidoRepre.getText();
        if (txtFechaNaciRepre.getDate()==null)
        {
            fechaNa="";
        }    else{
       
        ano=Integer.toString(txtFechaNaciRepre.getCalendar().get(Calendar.YEAR));
        mes=Integer.toString(txtFechaNaciRepre.getCalendar().get(Calendar.MONTH)+1);
        dia=Integer.toString(txtFechaNaciRepre.getCalendar().get(Calendar.DAY_OF_MONTH));
        fechaNa=dia+"/"+mes+"/"+ano;

        }
        profesion=txtOcupacionRepre.getText();
        telf1= cmbTelefonoRepre.getSelectedItem().toString()+"-"+txtTelefonoRepre.getText();
        if (txtOtroTelefonoRepre.getText().isEmpty()){
            telf2="";
        }else{
        telf2= cmbOtroTelefonoRepre.getSelectedItem().toString()+"-"+txtOtroTelefonoRepre.getText();
        }
        direccion=txtDireccionRepre.getText();
        observacion=txtObservacionRepre.getText();
        // Declaracion de la consulta en SQL
        String consulta="";
        consulta="UPDATE familiares SET nombre_fa=?,apellido_fa=?,fechaNacimiento_fa=?,profesion_fa=?,tlf_fa=?,otroTlf_fa=?,direccion_fa=?,observacion_fa=?,estado=? WHERE ced_fa='"+cedula+"'";
        
//Validar parametros para que no esten vacios
        // "If" que verifica si la caja de texto esta vacia, o el DataChooser contiene alguna fecha
        if ((txtCedulaRepre.getText().isEmpty()) || (txtNombreRepre.getText().isEmpty()) || (txtApellidoRepre.getText().isEmpty()) || (txtFechaNaciRepre.getDate()==null) || (txtOcupacionRepre.getText().isEmpty()) || (txtTelefonoRepre.getText().isEmpty()) || (txtDireccionRepre.getText().isEmpty())){
            advertencia.setVisible(true);
            //Se indica con una X el lugar o campo que esta vacio 
            //Se revisa con un If cada campo nuevamente para comprobar cual esta vacio
            if (txtCedulaRepre.getText().isEmpty()){
                lblNoExiste.setVisible(true);
            }
            if (txtNombreRepre.getText().isEmpty()){
                lblNoExisteNombre.setVisible(true);
            }
            if (txtApellidoRepre.getText().isEmpty()){
                lblNoExisteApellido.setVisible(true);
            }
            if(txtFechaNaciRepre.getDate()==null){
                lblNoExisteFechaNaci.setVisible(true);
            }
            if(txtOcupacionRepre.getText().isEmpty()){
                lblNoExisteProfesion.setVisible(true);
            }
            if(txtTelefonoRepre.getText().isEmpty()){
                lblNoExisteTelefono.setVisible(true);
            }
            if(txtDireccionRepre.getText().isEmpty()){
                lblNoExisteDireccion.setVisible(true);
            }
        }
             else {
        try {
           // Se ejecuta la consulta
            PreparedStatement stm=cn.prepareStatement(consulta);
           // Se insertan los datos  
            
            stm.setString(1, nombre);
            stm.setString(2, apellido);
            stm.setString(3, fechaNa);
            stm.setString(4, profesion);
            stm.setString(5, telf1);
            stm.setString(6, telf2);
            stm.setString(7, direccion);
            stm.setString(8, observacion);
            stm.setString(9,"1" );
           
            
            int n=stm.executeUpdate();
            if(n>0){
                    
// se llama el frame que indica que los datos han sido cargados
                obj.setVisible(true);
                Bitacora.bitacora("Inscribir Representante","Se actualizo el representate "+cedula);
            }
          iniciar();
          mostrar();
          cmbCedulaRepre.setEnabled(true);
          txtCedulaRepre.setEnabled(true);
          btnRegistrar.setEnabled(false);
        } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, ex);
        }
         }
        }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new imagenes.Fondo("fondo escritorio.jpg");
        inscripcion = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel2 = new javax.swing.JLabel();
        lblCedulaRepre = new javax.swing.JLabel();
        lblNombreRepre = new javax.swing.JLabel();
        lblFechaNaciRepre = new javax.swing.JLabel();
        lblTelefonoRepre = new javax.swing.JLabel();
        lblDireccionRepre = new javax.swing.JLabel();
        lblObservacionRepre = new javax.swing.JLabel();
        lblOtroTelefonoRepre = new javax.swing.JLabel();
        lblOcupacionRepre = new javax.swing.JLabel();
        lblApellidoRepre = new javax.swing.JLabel();
        txtApellidoRepre = new javax.swing.JTextField();
        txtOtroTelefonoRepre = new javax.swing.JTextField();
        txtNombreRepre = new javax.swing.JTextField();
        txtFechaNaciRepre = new com.toedter.calendar.JDateChooser();
        txtOcupacionRepre = new javax.swing.JTextField();
        cmbTelefonoRepre = new javax.swing.JComboBox();
        cmbOtroTelefonoRepre = new javax.swing.JComboBox();
        txtCedulaRepre = new javax.swing.JTextField();
        txtTelefonoRepre = new javax.swing.JTextField();
        cmbCedulaRepre = new javax.swing.JComboBox();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtObservacionRepre = new javax.swing.JTextArea();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDireccionRepre = new javax.swing.JTextArea();
        lblExiste = new javax.swing.JLabel();
        btnVerificarRepre = new javax.swing.JButton();
        lblNoExiste = new javax.swing.JLabel();
        lblVerificacion = new javax.swing.JLabel();
        lblNoExisteFechaNaci = new javax.swing.JLabel();
        lblNoExisteApellido = new javax.swing.JLabel();
        lblNoExisteDireccion = new javax.swing.JLabel();
        lblNoExisteNombre = new javax.swing.JLabel();
        lblNoExisteProfesion = new javax.swing.JLabel();
        lblNoExisteTelefono = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnCerrar = new javax.swing.JButton();
        btnRegistrar = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnLimpiar = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        btnActualizar = new javax.swing.JButton();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        inscripcion.setFont(new java.awt.Font("Comfortaa", 1, 60)); // NOI18N
        inscripcion.setText("Registrar");

        jLabel1.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel1.setText("Representante");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar estudiante.png"))); // NOI18N

        jLabel2.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel2.setText("Datos del Representante");

        lblCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaRepre.setText("Cédula:");

        lblNombreRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblNombreRepre.setText("Nombre: ");

        lblFechaNaciRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblFechaNaciRepre.setText("Fecha de Nacimiento:");

        lblTelefonoRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTelefonoRepre.setText("Teléfono:");

        lblDireccionRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblDireccionRepre.setText("Dirección de Habitación:");

        lblObservacionRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblObservacionRepre.setText("Observación:");

        lblOtroTelefonoRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblOtroTelefonoRepre.setText("Otro Teléfono:");

        lblOcupacionRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblOcupacionRepre.setText("Profesión u Ocupación:");

        lblApellidoRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblApellidoRepre.setText("Apellido:");

        txtApellidoRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtApellidoRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtApellidoRepreActionPerformed(evt);
            }
        });
        txtApellidoRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtApellidoRepreKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoRepreKeyTyped(evt);
            }
        });

        txtOtroTelefonoRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtOtroTelefonoRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOtroTelefonoRepreActionPerformed(evt);
            }
        });
        txtOtroTelefonoRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOtroTelefonoRepreKeyTyped(evt);
            }
        });

        txtNombreRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtNombreRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreRepreActionPerformed(evt);
            }
        });
        txtNombreRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreRepreKeyTyped(evt);
            }
        });

        txtFechaNaciRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtFechaNaciRepre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtFechaNaciRepreMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtFechaNaciRepreMousePressed(evt);
            }
        });
        txtFechaNaciRepre.addInputMethodListener(new java.awt.event.InputMethodListener() {
            public void caretPositionChanged(java.awt.event.InputMethodEvent evt) {
            }
            public void inputMethodTextChanged(java.awt.event.InputMethodEvent evt) {
                txtFechaNaciRepreInputMethodTextChanged(evt);
            }
        });
        txtFechaNaciRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtFechaNaciRepreKeyTyped(evt);
            }
        });

        txtOcupacionRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtOcupacionRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtOcupacionRepreActionPerformed(evt);
            }
        });
        txtOcupacionRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOcupacionRepreKeyTyped(evt);
            }
        });

        cmbTelefonoRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTelefonoRepre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbTelefonoRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTelefonoRepreActionPerformed(evt);
            }
        });

        cmbOtroTelefonoRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbOtroTelefonoRepre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbOtroTelefonoRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbOtroTelefonoRepreActionPerformed(evt);
            }
        });

        txtCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedulaRepre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtCedulaRepreFocusGained(evt);
            }
        });
        txtCedulaRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedulaRepreActionPerformed(evt);
            }
        });
        txtCedulaRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaRepreKeyTyped(evt);
            }
        });

        txtTelefonoRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtTelefonoRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTelefonoRepreActionPerformed(evt);
            }
        });
        txtTelefonoRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoRepreKeyTyped(evt);
            }
        });

        cmbCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbCedulaRepre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "V", "E" }));

        txtObservacionRepre.setColumns(20);
        txtObservacionRepre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtObservacionRepre.setRows(5);
        txtObservacionRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtObservacionRepreKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(txtObservacionRepre);

        txtDireccionRepre.setColumns(20);
        txtDireccionRepre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtDireccionRepre.setRows(5);
        txtDireccionRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDireccionRepreKeyTyped(evt);
            }
        });
        jScrollPane2.setViewportView(txtDireccionRepre);

        lblExiste.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/existe.png"))); // NOI18N

        btnVerificarRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        btnVerificarRepre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Verificar.png"))); // NOI18N
        btnVerificarRepre.setText("Verificar");
        btnVerificarRepre.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/check!.png"))); // NOI18N
        btnVerificarRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarRepreActionPerformed(evt);
            }
        });

        lblNoExiste.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblVerificacion.setFont(new java.awt.Font("Comfortaa", 1, 10)); // NOI18N

        lblNoExisteFechaNaci.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteApellido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteDireccion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteNombre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteProfesion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteTelefono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        jPanel2.setBackground(new java.awt.Color(0, 153, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnCerrar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnCerrar.setForeground(new java.awt.Color(255, 255, 255));
        btnCerrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar (2).png"))); // NOI18N
        btnCerrar.setText("Cerrar");
        btnCerrar.setBorder(null);
        btnCerrar.setContentAreaFilled(false);
        btnCerrar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnCerrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCerrarActionPerformed(evt);
            }
        });

        btnRegistrar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnRegistrar.setForeground(new java.awt.Color(255, 255, 255));
        btnRegistrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Guardar.png"))); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.setBorder(null);
        btnRegistrar.setContentAreaFilled(false);
        btnRegistrar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar 2 icono.png"))); // NOI18N
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        btnModificar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(255, 255, 255));
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pen.png"))); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.setBorder(null);
        btnModificar.setContentAreaFilled(false);
        btnModificar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/modificar icono.png"))); // NOI18N
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnLimpiar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnLimpiar.setForeground(new java.awt.Color(255, 255, 255));
        btnLimpiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar.png"))); // NOI18N
        btnLimpiar.setText("Eliminar");
        btnLimpiar.setBorder(null);
        btnLimpiar.setContentAreaFilled(false);
        btnLimpiar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancell.png"))); // NOI18N
        btnLimpiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnLimpiarActionPerformed(evt);
            }
        });

        btnActualizar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnActualizar.setForeground(new java.awt.Color(255, 255, 255));
        btnActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/actualizarr!!.png"))); // NOI18N
        btnActualizar.setText("Actualizar");
        btnActualizar.setBorder(null);
        btnActualizar.setContentAreaFilled(false);
        btnActualizar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/actualizar!.png"))); // NOI18N
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnCerrar, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addComponent(btnLimpiar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(68, 68, 68)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnRegistrar)
                .addGap(7, 7, 7)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnModificar)
                .addGap(7, 7, 7)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnActualizar)
                .addGap(7, 7, 7)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnLimpiar)
                .addGap(7, 7, 7)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 209, Short.MAX_VALUE)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnCerrar)
                .addGap(7, 7, 7)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41))
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 36, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(255, 255, 255)
                        .addComponent(jLabel1))
                    .addComponent(jLabel2)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblCedulaRepre)
                        .addGap(22, 22, 22)
                        .addComponent(cmbCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(txtCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 128, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNoExiste, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblExiste, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(46, 46, 46)
                        .addComponent(btnVerificarRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 126, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(lblVerificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblNombreRepre)
                        .addGap(4, 4, 4)
                        .addComponent(txtNombreRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 180, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(lblNoExisteNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(102, 102, 102)
                        .addComponent(lblApellidoRepre)
                        .addGap(4, 4, 4)
                        .addComponent(txtApellidoRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 200, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblNoExisteApellido))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblFechaNaciRepre)
                        .addGap(25, 25, 25)
                        .addComponent(txtFechaNaciRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 170, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblNoExisteFechaNaci))
                    .addComponent(lblDireccionRepre)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(lblNoExisteDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblObservacionRepre)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(64, 64, 64)
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 480, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblTelefonoRepre)
                        .addGap(10, 10, 10)
                        .addComponent(cmbTelefonoRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(8, 8, 8)
                        .addComponent(txtTelefonoRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 90, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(lblNoExisteTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(lblOtroTelefonoRepre)
                        .addGap(4, 4, 4)
                        .addComponent(cmbOtroTelefonoRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtOtroTelefonoRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblOcupacionRepre)
                        .addGap(22, 22, 22)
                        .addComponent(txtOcupacionRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 446, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblNoExisteProfesion, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 672, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(inscripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(167, 167, 167)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(28, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 157, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(40, 40, 40)
                        .addComponent(inscripcion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(34, 34, 34)
                                .addComponent(jLabel2)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCedulaRepre)
                    .addComponent(cmbCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExiste, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblExiste, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnVerificarRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblVerificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(25, 25, 25)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtNombreRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtApellidoRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblNombreRepre)
                            .addComponent(lblApellidoRepre))))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblNoExisteFechaNaci, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(txtFechaNaciRepre, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(lblFechaNaciRepre, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 27, Short.MAX_VALUE)))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblOcupacionRepre)
                    .addComponent(txtOcupacionRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteProfesion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblTelefonoRepre))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(cmbTelefonoRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(txtTelefonoRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(lblNoExisteTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(lblOtroTelefonoRepre))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(cmbOtroTelefonoRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtOtroTelefonoRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(20, 20, 20)
                .addComponent(lblDireccionRepre)
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(lblObservacionRepre)
                .addGap(1, 1, 1)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtApellidoRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtApellidoRepreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtApellidoRepreActionPerformed

    private void txtApellidoRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoRepreKeyTyped
        char c=evt.getKeyChar();
        if (txtApellidoRepre.getText().length()>99){
            evt.consume();
        }
        if(!Character.isLetter(c) && !Character.isSpaceChar(c) && (c!=KeyEvent.VK_BACKSPACE)) {
            getToolkit().beep();
            evt.consume(); }
        
        lblNoExisteApellido.setVisible(false);
        // TODO add your handling code here:
    }//GEN-LAST:event_txtApellidoRepreKeyTyped

    private void txtOtroTelefonoRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOtroTelefonoRepreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOtroTelefonoRepreActionPerformed

    private void txtOtroTelefonoRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOtroTelefonoRepreKeyTyped
        int limite=7;
        if(txtOtroTelefonoRepre.getText().length()>=7){
            evt.consume();
        }

        char c=evt.getKeyChar();

        if(!Character.isDigit(c) && (c!=KeyEvent.VK_BACKSPACE)) {
            getToolkit().beep();
            evt.consume(); }  // TODO add your handling code here:
      
    }//GEN-LAST:event_txtOtroTelefonoRepreKeyTyped

    private void txtNombreRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreRepreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreRepreActionPerformed

    private void txtNombreRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreRepreKeyTyped
        char c=evt.getKeyChar();
        if (txtNombreRepre.getText().length()>99){
            evt.consume();
        }
        if((!Character.isLetter(c)) && (!Character.isSpaceChar(c)) && (c!=KeyEvent.VK_BACKSPACE)) {
            getToolkit().beep();
            evt.consume();
        }
      
            
        lblNoExisteNombre.setVisible(false);
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreRepreKeyTyped

    private void txtFechaNaciRepreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtFechaNaciRepreMouseClicked
   // TODO add your handling code here:
    }//GEN-LAST:event_txtFechaNaciRepreMouseClicked

    private void txtFechaNaciRepreMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtFechaNaciRepreMousePressed

        // TODO add your handling code here:
    }//GEN-LAST:event_txtFechaNaciRepreMousePressed

    private void txtFechaNaciRepreInputMethodTextChanged(java.awt.event.InputMethodEvent evt) {//GEN-FIRST:event_txtFechaNaciRepreInputMethodTextChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_txtFechaNaciRepreInputMethodTextChanged

    private void txtFechaNaciRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtFechaNaciRepreKeyTyped

        // TODO add your handling code here:
    }//GEN-LAST:event_txtFechaNaciRepreKeyTyped

    private void txtOcupacionRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtOcupacionRepreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOcupacionRepreActionPerformed

    private void txtOcupacionRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOcupacionRepreKeyTyped
        char c=evt.getKeyChar();
        if (txtOcupacionRepre.getText().length()>99){
            evt.consume();
        }
        
        if(!Character.isLetter(c) && !Character.isSpaceChar(c) && (c!=KeyEvent.VK_BACKSPACE)) {
            getToolkit().beep();
            evt.consume(); }
        lblNoExisteProfesion.setVisible(false);
        // TODO add your handling code here:
    }//GEN-LAST:event_txtOcupacionRepreKeyTyped

    private void cmbTelefonoRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTelefonoRepreActionPerformed

        if (cmbTelefonoRepre.getSelectedIndex()>0){
            txtTelefonoRepre.setEnabled(true);
        }
        else
        {
            txtTelefonoRepre.setText("");
            txtTelefonoRepre.setEnabled(false);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbTelefonoRepreActionPerformed

    private void cmbOtroTelefonoRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbOtroTelefonoRepreActionPerformed
        if (cmbOtroTelefonoRepre.getSelectedIndex()>0){
            txtOtroTelefonoRepre.setEnabled(true);
        }
        else
        {
            txtOtroTelefonoRepre.setText("");
            txtOtroTelefonoRepre.setEnabled(false);
        }// TODO add your handling code here:
    }//GEN-LAST:event_cmbOtroTelefonoRepreActionPerformed

    private void txtCedulaRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedulaRepreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaRepreActionPerformed

    private void txtCedulaRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaRepreKeyTyped
        char c=evt.getKeyChar();
         if(txtCedulaRepre.getText().length()>=9){
            evt.consume();
        }
        if(!Character.isDigit(c) && c!=KeyEvent.VK_ENTER && c!=KeyEvent.VK_BACKSPACE) {
            getToolkit().beep();
            evt.consume();

        }
        if (c==KeyEvent.VK_ENTER){
            verificar();
        }
        
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaRepreKeyTyped

    private void txtTelefonoRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTelefonoRepreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTelefonoRepreActionPerformed

    private void txtTelefonoRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoRepreKeyTyped
        int limite=7;
        if(txtTelefonoRepre.getText().length()>=7){
            evt.consume();
        }
        char c=evt.getKeyChar();

        if(!Character.isDigit(c) && (c!=KeyEvent.VK_BACKSPACE)) {
            getToolkit().beep();
            evt.consume(); }
        lblNoExisteTelefono.setVisible(false);
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTelefonoRepreKeyTyped

    private void txtDireccionRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDireccionRepreKeyTyped
        lblNoExisteDireccion.setVisible(false);
        if (txtDireccionRepre.getText().length()>1999){
            evt.consume();
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_txtDireccionRepreKeyTyped

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed

        registrarRepresentante();

    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void btnCerrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCerrarActionPerformed
        this.dispose();
    }//GEN-LAST:event_btnCerrarActionPerformed

    private void btnVerificarRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarRepreActionPerformed
        verificar();
    }//GEN-LAST:event_btnVerificarRepreActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
      cmbCedulaRepre.setEnabled(false);
      txtCedulaRepre.setEnabled(false);
      activarCampos();
      btnActualizar.setEnabled(true);
      btnModificar.setEnabled(false);
      btnLimpiar.setEnabled(false);
      
      
// TODO add your handling code here:
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
    actualizarRepresentante(); 
    btnActualizar.setEnabled(false);
// TODO add your handling code here:
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnLimpiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnLimpiarActionPerformed
        
        
eliminarRepresentante();


// TODO add your handling code here:
    }//GEN-LAST:event_btnLimpiarActionPerformed

    private void txtObservacionRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtObservacionRepreKeyTyped
    if (txtObservacionRepre.getText().length()>1999){
    evt.consume();
    }   
// TODO add your handling code here:
    }//GEN-LAST:event_txtObservacionRepreKeyTyped

    private void txtCedulaRepreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCedulaRepreFocusGained
     iniciar();
     cmbCedulaRepre.setEnabled(true);
     txtCedulaRepre.setEditable(true);
     btnModificar.setEnabled(false);
     btnLimpiar.setEnabled(false);
// TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaRepreFocusGained

    private void txtApellidoRepreKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoRepreKeyPressed
    char c=evt.getKeyChar();
        if (c==KeyEvent.VK_TAB){
            txtFechaNaciRepre.getCalendarButton().requestFocus();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtApellidoRepreKeyPressed

    /**
     * @param args the command line arguments
     */
   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnCerrar;
    private javax.swing.JButton btnLimpiar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JButton btnVerificarRepre;
    private javax.swing.JComboBox cmbCedulaRepre;
    private javax.swing.JComboBox cmbOtroTelefonoRepre;
    private javax.swing.JComboBox cmbTelefonoRepre;
    private javax.swing.JLabel inscripcion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JLabel lblApellidoRepre;
    private javax.swing.JLabel lblCedulaRepre;
    private javax.swing.JLabel lblDireccionRepre;
    private javax.swing.JLabel lblExiste;
    private javax.swing.JLabel lblFechaNaciRepre;
    private javax.swing.JLabel lblNoExiste;
    private javax.swing.JLabel lblNoExisteApellido;
    private javax.swing.JLabel lblNoExisteDireccion;
    private javax.swing.JLabel lblNoExisteFechaNaci;
    private javax.swing.JLabel lblNoExisteNombre;
    private javax.swing.JLabel lblNoExisteProfesion;
    private javax.swing.JLabel lblNoExisteTelefono;
    private javax.swing.JLabel lblNombreRepre;
    private javax.swing.JLabel lblObservacionRepre;
    private javax.swing.JLabel lblOcupacionRepre;
    private javax.swing.JLabel lblOtroTelefonoRepre;
    private javax.swing.JLabel lblTelefonoRepre;
    private javax.swing.JLabel lblVerificacion;
    private javax.swing.JTextField txtApellidoRepre;
    private javax.swing.JTextField txtCedulaRepre;
    private javax.swing.JTextArea txtDireccionRepre;
    private com.toedter.calendar.JDateChooser txtFechaNaciRepre;
    private javax.swing.JTextField txtNombreRepre;
    private javax.swing.JTextArea txtObservacionRepre;
    private javax.swing.JTextField txtOcupacionRepre;
    private javax.swing.JTextField txtOtroTelefonoRepre;
    private javax.swing.JTextField txtTelefonoRepre;
    // End of variables declaration//GEN-END:variables
}
