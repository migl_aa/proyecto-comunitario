/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocomunitario;

import BD.conexion;
import Reportes.Reporte;
import java.awt.Point;
import java.awt.event.ItemEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author NANY
 */
public class frmAdminMatricula extends javax.swing.JFrame {
DefaultTableModel modelo1;
DefaultTableModel modelo2;
    /**
     * Creates new form frmAdminMatricula
     */
    public frmAdminMatricula() {
        initComponents();
        cargarTabla("");
    }
    public String getNombreProf (String grado, String seccion){
       conexion conn= new conexion();
       Connection cn = conn.conectar();
       String nombre=null, cedula=null,NombreP=null;
       String query = ("SELECT nombreProf, CedulaProf FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
       try {
           Statement st = cn.createStatement();
           ResultSet rs= st.executeQuery(query);
           while (rs.next()){
               nombre=rs.getString("nombreProf");
               cedula=rs.getString("CedulaProf");
               
           }
           NombreP= cedula+" "+nombre;
       }catch (Exception e){
           
       }
       return(NombreP);
     }  
        
     public String getPeriodoEscolar (String grado, String seccion){
         conexion conn=new conexion();
            Connection cn=conn.conectar();
            String periodo=null;
            String query =("SELECT periodo_escolar FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                 Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                    periodo=rs.getString("periodo_escolar");
                }
                
            } catch (Exception e) {
            }
            return (periodo);
     }
     
     public String getTurno (String grado, String seccion){
            conexion conn=new conexion();
            Connection cn=conn.conectar();
            String turno=null;
            String query =("SELECT turno FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                 Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                    turno=rs.getString("turno");
                }
                
            } catch (Exception e) {
            }
            return (turno);
            
        }
        
        public int matriculaSeccion (String grado, String seccion){
            conexion conn=new conexion();
            Connection cn=conn.conectar();
            int matricula = 0;
            String query =("SELECT matricula FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                matricula=rs.getInt("matricula");
                }
            } catch (Exception e) {
            }
            return (matricula);
            
        }
        
        public int matriculaDisponible (String grado, String seccion){
       conexion conn=new conexion();
            Connection cn=conn.conectar();
            int inscritos = 0;
            String query =("SELECT matricula_inscrita FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                inscritos=rs.getInt("matricula_inscrita");
                }
            } catch (Exception e) {
            }
            return inscritos;
            
        }
        
    
    void cargarDatos (){
        if (cmbSeccion.getSelectedIndex()>0){        
        this.txtTurno.setText(getTurno(this.cmbGrado.getSelectedItem().toString(), this.cmbSeccion.getSelectedItem().toString()));
                 this.txtProfesor.setText(getNombreProf(this.cmbGrado.getSelectedItem().toString(), this.cmbSeccion.getSelectedItem().toString()));
                 this.txtPeriodoEscolar.setText(getPeriodoEscolar(this.cmbGrado.getSelectedItem().toString(),this.cmbSeccion.getSelectedItem().toString()));
        this.txtMatriculaAsignada.setText(Integer.toString(this.matriculaSeccion(this.cmbGrado.getSelectedItem().toString(),this.cmbSeccion.getSelectedItem().toString())));
        this.txtMatriculaInscrita.setText(Integer.toString(this.matriculaDisponible(this.cmbGrado.getSelectedItem().toString(),this.cmbSeccion.getSelectedItem().toString())));
        conexion conn=new conexion();
            Connection cn=conn.conectar();
            int f=0,m=0;
            String query="SELECT sexo_estu FROM estudiante WHERE grado_estu='"+cmbGrado.getSelectedItem().toString()+"' AND seccion_estu='"+cmbSeccion.getSelectedItem().toString()+"' AND estado='INSCRITO'";
            try {
                 Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                if ("Masculino".equals(rs.getString("sexo_estu"))){
                    m++;
                }else
                    f++;
                }
                txtVarones.setText(Integer.toString(m));
                txtHembras.setText(Integer.toString(f));
                
            } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            }
        }   
        
    }
    void cargarSeccion(String valor){
        String [] iniciar = {"Selec"};
        cmbSeccion.setModel(new DefaultComboBoxModel(iniciar));
        
            if (cmbGrado.getSelectedIndex()>0){
                //se utiliza el metodo GetSeccion para cargar el modelo de las seccion existentes 
                 conexion conn=new conexion();
        Connection cn=conn.conectar();
            String seccion= null;
           String query="";
           query="SELECT seccion FROM secciongrado WHERE grado='"+valor+"'";
           try {
                
               int i = 0;
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                //Tomo los valores correspondientes de cada seccion segun el grado
                seccion=rs.getString("seccion");
                cmbSeccion.addItem(seccion);
                }
                
                
                
            } catch (Exception e) {
            }
// this.cmbSeccion.setModel(new DefaultComboBoxModel(this.getSeccion(this.cmbGrado.getSelectedItem().toString())) );
            }
       
         
    }
    void cargarTabla(String valor){
        
          String titulos []= {"Grado","Sección", "Turno","Profesor","Periodo","Matricula","Inscritos"};
        String registros[]= new String [7];
        modelo1 = new DefaultTableModel(null, titulos){
            @Override
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            } 
        };
        
        conexion con=new conexion();
        Connection cn=con.conectar();
        
        String query="SELECT seccion,grado,periodo_escolar,nombreProf,CedulaProf,turno,matricula,matricula_inscrita FROM secciongrado WHERE CONCAT (CedulaProf, nombreProf, grado, seccion, turno, matricula, matricula_inscrita) LIKE '%"+valor+"%'";
        
        try {
            Statement st=cn.createStatement();
            ResultSet rs= st.executeQuery(query);
            while(rs.next()){
                registros[0]=rs.getString("grado");
                registros[1]=rs.getString("seccion");
                registros[2]=rs.getString("turno");
                registros[3]=rs.getString("CedulaProf")+" "+rs.getString("nombreProf");
                registros[4]=rs.getString("periodo_escolar");
                registros[5]=Integer.toString(rs.getInt("matricula"));
                registros[6]=Integer.toString(rs.getInt("matricula_inscrita"));
                 modelo1.addRow(registros);
            }
            tblSecciones.setModel(modelo1);
            
            
        } catch (SQLException ex) {
            
            JOptionPane.showMessageDialog(null,ex);
        }
      }
    
    void cargarTablaEstudiantes (String valor){
        String titulos []= {"Cédula","Nombre","Apellido","Sexo","F/N","Teléfono","Código Canaima"};
        String registros[]= new String [7];
        modelo2 = new DefaultTableModel(null, titulos){
            @Override
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            } 
        };
        
        conexion con=new conexion();
        Connection cn=con.conectar();
        
        String query="SELECT ced_estu,primerNombre_estu,segundoNombre_estu,PrimerApellido_estu,segundoApellido_estu,sexo_estu,fn_estu,tlf_estu,cod_Canaima FROM estudiante WHERE (estado='INSCRITO') AND (seccion_estu='"+cmbSeccion.getSelectedItem().toString()+"') AND (grado_estu='"+cmbGrado.getSelectedItem().toString()+"') AND CONCAT (ced_estu,primerNombre_estu,segundoNombre_estu,PrimerApellido_estu,segundoApellido_estu,sexo_estu,fn_estu,tlf_estu,cod_Canaima) LIKE '%"+valor+"%'";
        
        try {
            Statement st=cn.createStatement();
            ResultSet rs= st.executeQuery(query);
            while(rs.next()){
                registros[0]=rs.getString("ced_estu");
                registros[1]=rs.getString("primerNombre_estu")+" "+rs.getString("segundoNombre_estu");
                registros[2]=rs.getString("PrimerApellido_estu")+" "+rs.getString("segundoApellido_estu");
                registros[3]=rs.getString("sexo_estu");
                registros[4]=rs.getString("fn_estu");
                registros[5]=rs.getString("tlf_estu");
                registros[6]=rs.getString("cod_Canaima");
                 modelo2.addRow(registros);
            }
            tblEstudiantes.setModel(modelo2);
            
            
        } catch (SQLException ex) {
            
            JOptionPane.showMessageDialog(null,ex);
        }
      
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        pmnuEstudiantes = new javax.swing.JPopupMenu();
        menuExportar = new javax.swing.JMenuItem();
        jPanel1 = new imagenes.Fondo("fondo escritorio.jpg");
        jPanel2 = new javax.swing.JPanel();
        btnExportar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        btnMenuPrincipal = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        lblGrado = new javax.swing.JLabel();
        lblSeccion = new javax.swing.JLabel();
        cmbGrado = new javax.swing.JComboBox();
        cmbSeccion = new javax.swing.JComboBox();
        lblProfesor = new javax.swing.JLabel();
        lblAñoEscolar = new javax.swing.JLabel();
        txtPeriodoEscolar = new javax.swing.JLabel();
        txtProfesor = new javax.swing.JLabel();
        lblCantidad = new javax.swing.JLabel();
        lblVarones = new javax.swing.JLabel();
        txtVarones = new javax.swing.JLabel();
        lblHembras = new javax.swing.JLabel();
        txtHembras = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblSecciones = new javax.swing.JTable();
        jLabel5 = new javax.swing.JLabel();
        txtMatriculaAsignada = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtMatriculaInscrita = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tblEstudiantes = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        txtBuscaSeccion = new javax.swing.JTextField();
        txtBuscaEstu = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        txtTurno = new javax.swing.JLabel();

        menuExportar.setText("Exportar");
        menuExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                menuExportarActionPerformed(evt);
            }
        });
        pmnuEstudiantes.add(menuExportar);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(0, 153, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnExportar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnExportar.setForeground(new java.awt.Color(255, 255, 255));
        btnExportar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/exportar.png"))); // NOI18N
        btnExportar.setText("Exportar");
        btnExportar.setBorder(null);
        btnExportar.setContentAreaFilled(false);
        btnExportar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/exportar!.png"))); // NOI18N
        btnExportar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnExportarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar (2).png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(null);
        btnCancelar.setContentAreaFilled(false);
        btnCancelar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnMenuPrincipal.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnMenuPrincipal.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnMenuPrincipal.setText("Menú Principal");
        btnMenuPrincipal.setBorder(null);
        btnMenuPrincipal.setContentAreaFilled(false);
        btnMenuPrincipal.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/home.png"))); // NOI18N
        btnMenuPrincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuPrincipalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnMenuPrincipal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 225, Short.MAX_VALUE)
            .addComponent(jSeparator2)
            .addComponent(jSeparator3)
            .addComponent(btnCancelar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnExportar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator4)
            .addComponent(jSeparator5)
            .addComponent(jSeparator6)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(66, 66, 66)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnExportar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 8, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btnMenuPrincipal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(75, 75, 75))
        );

        jLabel1.setFont(new java.awt.Font("Comfortaa", 1, 60)); // NOI18N
        jLabel1.setText("Administración");

        jLabel2.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel2.setText("             de");

        jLabel3.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel3.setText(" Matricula Inscrita");

        lblGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblGrado.setText("Grado:");

        lblSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSeccion.setText("Sección:");

        cmbGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbGrado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "1", "2", "3", "4", "5", "6" }));
        cmbGrado.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbGradoItemStateChanged(evt);
            }
        });
        cmbGrado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbGradoActionPerformed(evt);
            }
        });

        cmbSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbSeccion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec" }));
        cmbSeccion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSeccionItemStateChanged(evt);
            }
        });
        cmbSeccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSeccionActionPerformed(evt);
            }
        });

        lblProfesor.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblProfesor.setText("Profesor:");

        lblAñoEscolar.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblAñoEscolar.setText("Año Escolar:");

        txtPeriodoEscolar.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        txtProfesor.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        lblCantidad.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCantidad.setText("Cantidad:");

        lblVarones.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblVarones.setText("V=");

        txtVarones.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        lblHembras.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblHembras.setText("H=");

        txtHembras.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        tblSecciones.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblSecciones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                tblSeccionesMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(tblSecciones);

        jLabel5.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel5.setText("Matricula Asignada:");

        txtMatriculaAsignada.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel7.setText("Matricula Inscrita:");

        txtMatriculaInscrita.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        jLabel9.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        jLabel9.setText("Estudiantes Inscritos");

        tblEstudiantes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tblEstudiantes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tblEstudiantesMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tblEstudiantes);

        jLabel4.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        jLabel4.setText("Secciones Registradas");

        txtBuscaSeccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBuscaSeccionKeyTyped(evt);
            }
        });

        txtBuscaEstu.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtBuscaEstuKeyTyped(evt);
            }
        });

        jLabel6.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel6.setText("Turno:");

        txtTurno.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(49, 49, 49)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtMatriculaAsignada, javax.swing.GroupLayout.PREFERRED_SIZE, 43, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtMatriculaInscrita, javax.swing.GroupLayout.PREFERRED_SIZE, 336, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING)
                                .addComponent(jSeparator1)
                                .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel2)
                                            .addGap(4, 4, 4)
                                            .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 522, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 197, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(29, 29, 29)
                                            .addComponent(txtBuscaEstu, javax.swing.GroupLayout.PREFERRED_SIZE, 189, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(lblProfesor)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(txtProfesor, javax.swing.GroupLayout.PREFERRED_SIZE, 328, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(lblCantidad)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(lblVarones)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtVarones, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(18, 18, 18)
                                            .addComponent(lblHembras)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtHembras, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGroup(jPanel1Layout.createSequentialGroup()
                                            .addComponent(lblGrado)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(cmbGrado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(26, 26, 26)
                                            .addComponent(lblSeccion)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(cmbSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(6, 6, 6)
                                            .addComponent(lblAñoEscolar)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                            .addComponent(txtPeriodoEscolar, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGap(51, 51, 51)
                                            .addComponent(jLabel6)
                                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                            .addComponent(txtTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGap(0, 0, Short.MAX_VALUE))))
                        .addGap(55, 55, 55))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addGap(28, 28, 28)
                        .addComponent(txtBuscaSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, 190, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addGap(13, 13, 13)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(jLabel3))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cmbSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblSeccion)
                        .addComponent(lblAñoEscolar)
                        .addComponent(txtPeriodoEscolar, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel6))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblGrado)
                        .addComponent(cmbGrado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(24, 24, 24)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblCantidad)
                        .addComponent(lblVarones)
                        .addComponent(txtVarones, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblHembras)
                        .addComponent(txtHembras, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblProfesor)
                    .addComponent(txtProfesor, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel7)
                        .addComponent(txtMatriculaInscrita, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel5)
                        .addComponent(txtMatriculaAsignada, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscaEstu, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addGap(7, 7, 7)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 97, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txtBuscaSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel4))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 127, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(26, 26, 26))
            .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbGradoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbGradoItemStateChanged
      // TODO add your handling code here:
    }//GEN-LAST:event_cmbGradoItemStateChanged

    private void txtBuscaSeccionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscaSeccionKeyTyped
        cargarTabla(txtBuscaSeccion.getText());        // TODO add your handling code here:
    }//GEN-LAST:event_txtBuscaSeccionKeyTyped

    private void cmbSeccionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSeccionItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbSeccionItemStateChanged

    private void cmbSeccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSeccionActionPerformed
    if (cmbSeccion.getSelectedIndex()>0){
        cargarTablaEstudiantes("");
        cargarDatos();
    }
        
// TODO add your handling code here:
    }//GEN-LAST:event_cmbSeccionActionPerformed

    private void cmbGradoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbGradoActionPerformed
        cargarSeccion(cmbGrado.getSelectedItem().toString());        // TODO add your handling code here:
    }//GEN-LAST:event_cmbGradoActionPerformed

    private void txtBuscaEstuKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtBuscaEstuKeyTyped
        cargarTablaEstudiantes(txtBuscaEstu.getText());        // TODO add your handling code here:
    }//GEN-LAST:event_txtBuscaEstuKeyTyped

    private void tblEstudiantesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblEstudiantesMouseClicked
            // TODO add your handling code here:
    }//GEN-LAST:event_tblEstudiantesMouseClicked

    private void menuExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_menuExportarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_menuExportarActionPerformed

    private void tblSeccionesMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tblSeccionesMousePressed
 JTable tabla= (JTable) evt.getSource();
        Point point= evt.getPoint();
        int row= tabla.rowAtPoint(point);
        if((evt.getClickCount() ==2) ){
           String grado = (String) tabla.getValueAt(tabla.getSelectedRow(), 0);
           cmbGrado.setSelectedItem(grado);
           cargarSeccion(grado);
            String seccion = (String) tabla.getValueAt(tabla.getSelectedRow(), 1);
            cmbSeccion.setSelectedItem(seccion);
            cargarDatos();
        }// TODO add your handling code here:
    }//GEN-LAST:event_tblSeccionesMousePressed

    private void btnExportarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnExportarActionPerformed

        // TODO add your handling code here:
         DefaultTableModel modelo = (DefaultTableModel) tblEstudiantes.getModel();
        Map parametros = new HashMap();
        List lista = new ArrayList();
                  
                    parametros.put("grado", cmbGrado.getSelectedItem().toString());
                    parametros.put("seccion", cmbSeccion.getSelectedItem().toString());
                    parametros.put("añoescolar", txtPeriodoEscolar.getText());
                    parametros.put("turno", txtTurno.getText());
                    parametros.put("profesor", txtProfesor.getText());
                    parametros.put("varones",txtVarones.getText());
                    parametros.put("hembras", txtHembras.getText());
                    parametros.put("Asignada", txtMatriculaAsignada.getText());
                    parametros.put("inscrita", txtMatriculaInscrita.getText());
        
        for (int i = 0; i < modelo.getRowCount(); i++)
            {
                frmAdminMatricula datos = new frmAdminMatricula(modelo.getValueAt(i, 0).toString(),modelo.getValueAt(i, 1).toString(), modelo.getValueAt(i, 2).toString(), modelo.getValueAt(i, 3).toString(), modelo.getValueAt(i, 4).toString(),modelo.getValueAt(i, 5).toString(),modelo.getValueAt(i, 6).toString());
                lista.add(datos);
            }
        
        Reporte obj = new Reporte();
        obj.VeReportParametrs("rptAdminMatricula", parametros, lista);
        this.dispose();
    }//GEN-LAST:event_btnExportarActionPerformed

    private void btnMenuPrincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuPrincipalActionPerformed
        // TODO add your handling code here:
        menuInicio menu =new menuInicio();
        menu.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnMenuPrincipalActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmAdminMatricula.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmAdminMatricula.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmAdminMatricula.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmAdminMatricula.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmAdminMatricula().setVisible(true);
            }
        });
    }
    String cedula,nombre,apellido,sexo,fn,telefono,codigocanaima;

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getFn() {
        return fn;
    }

    public void setFn(String fn) {
        this.fn = fn;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCodigocanaima() {
        return codigocanaima;
    }

    public void setCodigocanaima(String codigocanaima) {
        this.codigocanaima = codigocanaima;
    }

    public frmAdminMatricula(String cedula, String nombre, String apellido, String sexo, String fn, String telefono, String codigocanaima) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.apellido = apellido;
        this.sexo = sexo;
        this.fn = fn;
        this.telefono = telefono;
        this.codigocanaima = codigocanaima;
    }
String grado,seccion,turno,profesor,periodo,matricula,inscritos; 

    /**
     *
     * @param grado
     * @param seccion
     * @param turno
     * @param profesor
     * @param periodo
     * @param matricula
     * @param inscritos
     */
    

    public String getGrado() {
        return grado;
    }

    public void setGrado(String grado) {
        this.grado = grado;
    }

    public String getSeccion() {
        return seccion;
    }

    public void setSeccion(String seccion) {
        this.seccion = seccion;
    }

    public String getTurno() {
        return turno;
    }

    public void setTurno(String turno) {
        this.turno = turno;
    }

    public String getProfesor() {
        return profesor;
    }

    public void setProfesor(String profesor) {
        this.profesor = profesor;
    }

    public String getPeriodo() {
        return periodo;
    }

    public void setPeriodo(String periodo) {
        this.periodo = periodo;
    }

    public String getMatricula() {
        return matricula;
    }

    public void setMatricula(String matricula) {
        this.matricula = matricula;
    }

    public String getInscritos() {
        return inscritos;
    }

    public void setInscritos(String inscritos) {
        this.inscritos = inscritos;
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnExportar;
    private javax.swing.JButton btnMenuPrincipal;
    private javax.swing.JComboBox cmbGrado;
    private javax.swing.JComboBox cmbSeccion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JLabel lblAñoEscolar;
    private javax.swing.JLabel lblCantidad;
    private javax.swing.JLabel lblGrado;
    private javax.swing.JLabel lblHembras;
    private javax.swing.JLabel lblProfesor;
    private javax.swing.JLabel lblSeccion;
    private javax.swing.JLabel lblVarones;
    private javax.swing.JMenuItem menuExportar;
    private javax.swing.JPopupMenu pmnuEstudiantes;
    private javax.swing.JTable tblEstudiantes;
    private javax.swing.JTable tblSecciones;
    private javax.swing.JTextField txtBuscaEstu;
    private javax.swing.JTextField txtBuscaSeccion;
    private javax.swing.JLabel txtHembras;
    private javax.swing.JLabel txtMatriculaAsignada;
    private javax.swing.JLabel txtMatriculaInscrita;
    private javax.swing.JLabel txtPeriodoEscolar;
    private javax.swing.JLabel txtProfesor;
    private javax.swing.JLabel txtTurno;
    private javax.swing.JLabel txtVarones;
    // End of variables declaration//GEN-END:variables
}
