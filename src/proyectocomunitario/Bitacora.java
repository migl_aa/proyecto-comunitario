/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocomunitario;

import BD.conexion;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Calendar;
import javax.swing.JOptionPane;

/**
 *
 * @author Miguel Arroyo
 */
public class Bitacora {
    public static void bitacora (String modulo, String accion){
         conexion con=new conexion();
        Connection cn=con.conectar();
        Calendar cal=Calendar.getInstance();
        String usuario="";
        int dia=cal.get(cal.DATE);
        int mes=cal.get(cal.MONTH)+1;
        int ano=cal.get(cal.YEAR);
        int horas=cal.get(cal.HOUR_OF_DAY);
        int minuto=cal.get(cal.MINUTE);
        int segundo=cal.get(cal.SECOND);
        String fecha=Integer.toString(dia)+"/"+Integer.toString(mes)+"/"+Integer.toString(ano);
        String hora=Integer.toString(horas)+":"+Integer.toString(minuto)+":"+Integer.toString(segundo);
        String consulta="INSERT INTO bitacora (usuario,modulo,accion,hora,fecha) VALUES (?,?,?,?,?)";
        String consulta2="SELECT nombre_usu FROM usuarios WHERE estado=1";
        try{
             PreparedStatement ktm=cn.prepareStatement(consulta2);
             Statement st=cn.createStatement();
            ResultSet rs= st.executeQuery(consulta2);
            while (rs.next()){
                usuario=rs.getString("nombre_usu");
            }
             PreparedStatement stm=cn.prepareStatement(consulta);
             stm.setString (1,usuario);
             stm.setString(2, modulo);
             stm.setString(3,accion);
             stm.setString(4,hora);
             stm.setString(5,fecha);
   int n=stm.executeUpdate(); 
               if (n>0){
                    
               }
        
               }catch (SQLException ex){
                JOptionPane.showMessageDialog(null,ex);
            } 
        
    }
}
