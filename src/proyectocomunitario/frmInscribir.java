/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocomunitario;

import BD.conexion;
import java.awt.Color;
import java.awt.GraphicsEnvironment;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

/**
 *
 * @author NANY
 */
public class frmInscribir extends javax.swing.JFrame {

    /**
     * Creates new form frmInscribir
     */
    public frmInscribir() {
        initComponents();
        
          this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
          iniciar();
        lblExisteEstudiante.setVisible(false);
         lblExisteRepresentante.setVisible(false);
       
        
               this.lblDisponible.setVisible(false);
        txtCedulaEstudiante.setEditable(false);
        btnVerificarEstu.setEnabled(false);
        btnGenerarCedulaEstu.setEnabled(false);
        cmbCedulaEstudiante.setEnabled(false);
// Hace que el frame aparezca en medio del monitor
        setLocationRelativeTo(null);
        // Evita que se pueda modificar el tamaño del frame
       setResizable(false);
        
// Evita que la aplicacion pueda ser cerrada por el boton X
        setDefaultCloseOperation(0);
        // Se le coloca un titulo a la ventata
        setTitle("REQUISITOS");
        cmbGrado.setEditable(false);
        cmbSeccion.setEditable(false);
    }
    void verificarRepresentante(){
         lblVerificacion.setVisible(false);
        conexion conn=new conexion();
        Connection cn=conn.conectar();
        String cedula="",verificar="";
        cedula = cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText();
        String query;
        query="SELECT ced_fa FROM familiares WHERE ced_fa='"+cedula+"' AND estado='1'";
        try {
             Statement st=cn.createStatement();
            ResultSet rs= st.executeQuery(query);
            while (rs.next()){
                verificar=rs.getString("ced_fa");
            }
           if (!verificar.equals("")){
                //lblExiste.setVisible(true);
                lblVerificacion.setVisible(true);
                lblVerificacion.setText("Representate registrado");
                txtCedulaEstudiante.setEditable(true);
                btnVerificarEstu.setEnabled(true);
                btnGenerarCedulaEstu.setEnabled(true);
                cmbCedulaEstudiante.setEnabled(true);
                URL url = this.getClass().getResource("/imagenes/existe.png");  
                    ImageIcon icon = new ImageIcon(url);  
                    lblExisteRepresentante.setIcon(icon);
                    lblExisteRepresentante.setVisible(true);
                
           }
           else
           if (verificar.equals("")){
               
               lblVerificacion.setVisible(true);
               lblVerificacion.setText("Representante no encontrado");
               txtCedulaEstudiante.setEditable(false);
               btnVerificarEstu.setEnabled(false);
               btnGenerarCedulaEstu.setEnabled(false);
               cmbCedulaEstudiante.setEnabled(false);
                URL url = this.getClass().getResource("/imagenes/no existe.png");  
                    ImageIcon icon = new ImageIcon(url);  
                    lblExisteRepresentante.setIcon(icon);
                    lblExisteRepresentante.setVisible(true);
           }
        } catch (Exception e) {
        JOptionPane.showMessageDialog(null, e);
        }
        
        
    }
    void verificar (){
        String  ced,verificar="",aux="";
        int estado=0;
        conexion conn=new conexion();
        Connection cn=conn.conectar();
        aux=txtCedulaEstudiante.getText();
        ced=cmbCedulaEstudiante.getSelectedItem().toString()+"-"+txtCedulaEstudiante.getText();
        String query= "SELECT ced_estu,estado_logico, FROM estudiante WHERE ced_estu='"+ced+"'";
        String consulta="SELECT * FROM estudainte WHERE ced_estu='"+ced+"'";
        
       try {
          Statement  st = cn.createStatement();
       ResultSet rs= st.executeQuery(query);
            while (rs.next()){
                verificar=rs.getString("ced_estu");
                estado=rs.getInt("estado_logico");
                
            }
            if ((verificar!="") && (estado==1)){
                dlgYaRegistrado dialog = new dlgYaRegistrado(new javax.swing.JFrame(), true);
                dialog.setVisible(true);
                desactivar();
            }else
                if ((verificar!="") && (estado==0)){
                    dlgRegistradoParaModificar dialog = new dlgRegistradoParaModificar(new javax.swing.JFrame(), true);
                    dialog.setVisible(true);
                    activar();
                    btnSiguienteModificar.setVisible(true);
                     URL url = this.getClass().getResource("/imagenes/existe.png");  
                    ImageIcon icon = new ImageIcon(url);  
                    lblExisteEstudiante.setIcon(icon);
                    lblExisteEstudiante.setVisible(true);
                }else 
                    if (verificar=="" && !aux.equals("")){
                        activar();
                        btnSiguiente.setVisible(true);
                         URL url = this.getClass().getResource("/imagenes/existe.png");  
                    ImageIcon icon = new ImageIcon(url);  
                    lblExisteEstudiante.setIcon(icon);
                    lblExisteEstudiante.setVisible(true);
                    }
       } catch (SQLException ex) {
           JOptionPane.showMessageDialog(null, ex);
       }
    }
    void desactivar (){
        rbtnPartidaNaci.setEnabled(false);
         rbtnCertificadoVacunas.setEnabled(false);
         rbtnFotoEstu.setEnabled(false);
         rbtnCedulaEstu.setEnabled(false);
         rbtnCartaBC.setEnabled(false);
         rbtnCartaRetiro.setEnabled(false);
         rbtnCertificadoPromo.setEnabled(false);
         rbtnFotocopiaCedulaRepre.setEnabled(false);
         rbtnRifRepre.setEnabled(false);
         cmbGrado.setEnabled(false);
         cmbSeccion.setEnabled(false);
         btnSiguiente.setVisible(false);
         txtInstitucion.setEnabled(false);
         rbtnSeleccionarTodo.setEnabled(false);
         btnSiguienteModificar.setVisible(false);
    }
    void activar (){
         rbtnPartidaNaci.setEnabled(true);
         rbtnCertificadoVacunas.setEnabled(true);
         rbtnFotoEstu.setEnabled(true);
         rbtnCedulaEstu.setEnabled(true);
         rbtnCartaBC.setEnabled(true);
         rbtnCartaRetiro.setEnabled(true);
         rbtnCertificadoPromo.setEnabled(true);
         rbtnFotocopiaCedulaRepre.setEnabled(true);
         rbtnRifRepre.setEnabled(true);
         cmbGrado.setEnabled(true);
         cmbSeccion.setEnabled(true);
         
         txtInstitucion.setEnabled(true);
         rbtnSeleccionarTodo.setEnabled(true);
    }
     void iniciar (){
         lblNoExistePartidaNaci.setVisible(false);
         lblNoExisteCertificadoVacunas.setVisible(false);
         lblNoExisteFotoEstu.setVisible(false);
         lblNoExisteCedulaEstu.setVisible(false);
         lblNoExisteCartaBuenaConducta.setVisible(false);
         lblNoExisteCartaDeRetiro.setVisible(false);
         lblNoExisteCertificadoDePromo.setVisible(false);
         lblNoExisteFotocopiaCedulaRepre.setVisible(false);
         lblNoExisteRifRepresentante.setVisible(false);
         lblNoExisteGradoSelecc.setVisible(false);
         lblNoExisteSeccionSelecc.setVisible(false);
         rbtnPartidaNaci.setEnabled(false);
         rbtnCertificadoVacunas.setEnabled(false);
         rbtnFotoEstu.setEnabled(false);
         rbtnCedulaEstu.setEnabled(false);
         rbtnCartaBC.setEnabled(false);
         rbtnCartaRetiro.setEnabled(false);
         rbtnCertificadoPromo.setEnabled(false);
         rbtnFotocopiaCedulaRepre.setEnabled(false);
         rbtnRifRepre.setEnabled(false);
         cmbGrado.setEnabled(false);
         cmbSeccion.setEnabled(false);
         btnSiguiente.setVisible(false);
         txtInstitucion.setEnabled(false);
         rbtnSeleccionarTodo.setEnabled(false);
         btnSiguienteModificar.setVisible(false);
     }
     public String getNombreProf (String grado, String seccion){
       conexion conn= new conexion();
       Connection cn = conn.conectar();
       String nombre=null, cedula=null,NombreP=null;
       String query = ("SELECT nombreProf, CedulaProf FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
       try {
           Statement st = cn.createStatement();
           ResultSet rs= st.executeQuery(query);
           while (rs.next()){
               nombre=rs.getString("nombreProf");
               cedula=rs.getString("CedulaProf");
               
           }
           NombreP= cedula+" "+nombre;
       }catch (Exception e){
           
       }
       return(NombreP);
     }  
        
     public String getPeriodoEscolar (String grado, String seccion){
         conexion conn=new conexion();
            Connection cn=conn.conectar();
            String periodo=null;
            String query =("SELECT periodo_escolar FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                 Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                    periodo=rs.getString("periodo_escolar");
                }
                
            } catch (Exception e) {
            }
            return (periodo);
     }
     
     public String getTurno (String grado, String seccion){
            conexion conn=new conexion();
            Connection cn=conn.conectar();
            String turno=null;
            String query =("SELECT turno FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                 Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                    turno=rs.getString("turno");
                }
                
            } catch (Exception e) {
            }
            return (turno);
            
        }
        
        public int matriculaSeccion (String grado, String seccion){
            conexion conn=new conexion();
            Connection cn=conn.conectar();
            int matricula = 0;
            String query =("SELECT matricula FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                matricula=rs.getInt("matricula");
                }
            } catch (Exception e) {
            }
            return (matricula);
            
        }
        
        public int matriculaDisponible (String grado, String seccion){
       conexion conn=new conexion();
            Connection cn=conn.conectar();
            int inscritos = 0;
            String query =("SELECT matricula_inscrita FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                inscritos=rs.getInt("matricula_inscrita");
                }
            } catch (Exception e) {
            }
            return inscritos;
            
        }
        
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new imagenes.Fondo("fondo color.jpg");
        jPanel2 = new javax.swing.JPanel();
        btnAtras = new javax.swing.JButton();
        btnMenuPrincipal = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        lblInstitucion = new javax.swing.JLabel();
        rbtnSeleccionarTodo = new javax.swing.JRadioButton();
        txtInstitucion = new javax.swing.JTextField();
        txtProfesor = new javax.swing.JLabel();
        lblProfesor = new javax.swing.JLabel();
        lblNoExisteGradoSelecc = new javax.swing.JLabel();
        lblNoExisteSeccionSelecc = new javax.swing.JLabel();
        lblNoExisteRifRepresentante = new javax.swing.JLabel();
        lblNoExisteFotocopiaCedulaRepre = new javax.swing.JLabel();
        lblNoExisteCertificadoDePromo = new javax.swing.JLabel();
        lblNoExisteCartaDeRetiro = new javax.swing.JLabel();
        lblNoExisteCartaBuenaConducta = new javax.swing.JLabel();
        lblNoExisteCedulaEstu = new javax.swing.JLabel();
        lblNoExisteFotoEstu = new javax.swing.JLabel();
        lblNoExisteCertificadoVacunas = new javax.swing.JLabel();
        lblNoExistePartidaNaci = new javax.swing.JLabel();
        lblDisponible = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lblVerificacion = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        btnRegistrarRepre = new javax.swing.JButton();
        btnVerificarRepre = new javax.swing.JButton();
        txtCedulaRepre = new javax.swing.JTextField();
        cmbCedulaRepre = new javax.swing.JComboBox();
        lblCedulaRepre = new javax.swing.JLabel();
        inscripcion = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel1 = new javax.swing.JLabel();
        txtTurno = new javax.swing.JLabel();
        lblTurno = new javax.swing.JLabel();
        cmbSeccion = new javax.swing.JComboBox();
        lblSeccion = new javax.swing.JLabel();
        cmbGrado = new javax.swing.JComboBox();
        lblGrado = new javax.swing.JLabel();
        rbtnCertificadoPromo = new javax.swing.JRadioButton();
        rbtnCartaRetiro = new javax.swing.JRadioButton();
        rbtnCartaBC = new javax.swing.JRadioButton();
        rbtnRifRepre = new javax.swing.JRadioButton();
        rbtnFotocopiaCedulaRepre = new javax.swing.JRadioButton();
        rbtnCedulaEstu = new javax.swing.JRadioButton();
        rbtnFotoEstu = new javax.swing.JRadioButton();
        rbtnCertificadoVacunas = new javax.swing.JRadioButton();
        rbtnPartidaNaci = new javax.swing.JRadioButton();
        jSeparator1 = new javax.swing.JSeparator();
        btnSiguiente = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        cmbCedulaEstudiante = new javax.swing.JComboBox();
        txtCedulaEstudiante = new javax.swing.JTextField();
        btnGenerarCedulaEstu = new javax.swing.JButton();
        lblCedulaEstudiante = new javax.swing.JLabel();
        btnVerificarEstu = new javax.swing.JButton();
        btnSiguienteModificar = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        txtPeriodoEscolar = new javax.swing.JLabel();
        lblExisteRepresentante = new javax.swing.JLabel();
        lblExisteEstudiante = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel2.setBackground(new java.awt.Color(0, 153, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnAtras.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnAtras.setForeground(new java.awt.Color(255, 255, 255));
        btnAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/check.png"))); // NOI18N
        btnAtras.setText("Tipo de\n inscripcion"); // NOI18N
        btnAtras.setActionCommand("Tipo de  Inscripción");
        btnAtras.setBorder(null);
        btnAtras.setContentAreaFilled(false);
        btnAtras.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnAtras.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/inscripcion.png"))); // NOI18N
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });

        btnMenuPrincipal.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnMenuPrincipal.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnMenuPrincipal.setText("Menu Principal");
        btnMenuPrincipal.setBorder(null);
        btnMenuPrincipal.setContentAreaFilled(false);
        btnMenuPrincipal.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/home.png"))); // NOI18N
        btnMenuPrincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuPrincipalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator4)
            .addComponent(jSeparator5)
            .addComponent(jSeparator6)
            .addComponent(jSeparator7)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(btnMenuPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addComponent(btnAtras, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 5, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnAtras)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnMenuPrincipal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(45, 45, 45))
        );

        lblInstitucion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblInstitucion.setText("Institución Proveniente:");

        rbtnSeleccionarTodo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnSeleccionarTodo.setText("Seleccionar Todo");
        rbtnSeleccionarTodo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnSeleccionarTodoActionPerformed(evt);
            }
        });

        txtInstitucion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        txtProfesor.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        lblProfesor.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblProfesor.setText("Profesor:");

        lblNoExisteGradoSelecc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteSeccionSelecc.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteRifRepresentante.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteFotocopiaCedulaRepre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteCertificadoDePromo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteCartaDeRetiro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteCartaBuenaConducta.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteCedulaEstu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteFotoEstu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteCertificadoVacunas.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExistePartidaNaci.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblDisponible.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblDisponible.setForeground(new java.awt.Color(0, 204, 0));

        jLabel9.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel9.setText("Disponibilidad:");

        lblVerificacion.setFont(new java.awt.Font("Comfortaa", 1, 10)); // NOI18N

        jLabel6.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel6.setText("Nuevo");

        btnRegistrarRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        btnRegistrarRepre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add.png"))); // NOI18N
        btnRegistrarRepre.setText("Registrar");
        btnRegistrarRepre.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
        btnRegistrarRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarRepreActionPerformed(evt);
            }
        });

        btnVerificarRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        btnVerificarRepre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Verificar.png"))); // NOI18N
        btnVerificarRepre.setText("Verificar");
        btnVerificarRepre.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/check!.png"))); // NOI18N
        btnVerificarRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarRepreActionPerformed(evt);
            }
        });

        txtCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedulaRepre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtCedulaRepreFocusGained(evt);
            }
        });
        txtCedulaRepre.addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentResized(java.awt.event.ComponentEvent evt) {
                txtCedulaRepreComponentResized(evt);
            }
        });
        txtCedulaRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedulaRepreActionPerformed(evt);
            }
        });
        txtCedulaRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaRepreKeyTyped(evt);
            }
        });

        cmbCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbCedulaRepre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "V", "E" }));
        cmbCedulaRepre.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbCedulaRepreItemStateChanged(evt);
            }
        });

        lblCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaRepre.setText("Cédula Del Representante");

        inscripcion.setFont(new java.awt.Font("Comfortaa", 1, 60)); // NOI18N
        inscripcion.setText("Inscripción");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar estudiante.png"))); // NOI18N

        jLabel3.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel3.setText("INGRESO");

        jLabel5.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel5.setText("Identificación");

        jLabel2.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel2.setText("Sección y Grado donde va a Inscribir");

        jLabel1.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel1.setText("Requisitos");

        txtTurno.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtTurno.setText(" ");

        lblTurno.setBackground(new java.awt.Color(0, 0, 0));
        lblTurno.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTurno.setText("Turno:");

        cmbSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbSeccion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec" }));
        cmbSeccion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSeccionItemStateChanged(evt);
            }
        });
        cmbSeccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSeccionActionPerformed(evt);
            }
        });

        lblSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSeccion.setText("Sección:");

        cmbGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbGrado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "1", "2", "3", "4", "5", "6" }));
        cmbGrado.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbGradoItemStateChanged(evt);
            }
        });
        cmbGrado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbGradoActionPerformed(evt);
            }
        });

        lblGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblGrado.setText("Grado:");

        rbtnCertificadoPromo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnCertificadoPromo.setText("Certificado de Promoción o Informe Descriptivo");
        rbtnCertificadoPromo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnCertificadoPromoActionPerformed(evt);
            }
        });

        rbtnCartaRetiro.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnCartaRetiro.setText("Carta de Retiro");
        rbtnCartaRetiro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnCartaRetiroActionPerformed(evt);
            }
        });

        rbtnCartaBC.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnCartaBC.setText("Carta de Buena Conducta");
        rbtnCartaBC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnCartaBCActionPerformed(evt);
            }
        });

        rbtnRifRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnRifRepre.setText("Rif del Representante");
        rbtnRifRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnRifRepreActionPerformed(evt);
            }
        });

        rbtnFotocopiaCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnFotocopiaCedulaRepre.setText("Fotocopia de la Cédula del Representante");
        rbtnFotocopiaCedulaRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnFotocopiaCedulaRepreActionPerformed(evt);
            }
        });

        rbtnCedulaEstu.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnCedulaEstu.setText("Cédula del Estudiante");
        rbtnCedulaEstu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnCedulaEstuActionPerformed(evt);
            }
        });

        rbtnFotoEstu.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnFotoEstu.setText("Foto del Estudiante");
        rbtnFotoEstu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnFotoEstuActionPerformed(evt);
            }
        });

        rbtnCertificadoVacunas.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnCertificadoVacunas.setText("Certificado de Vacunas");
        rbtnCertificadoVacunas.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnCertificadoVacunasActionPerformed(evt);
            }
        });

        rbtnPartidaNaci.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPartidaNaci.setText("Partida de Nacimiento");
        rbtnPartidaNaci.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnPartidaNaciActionPerformed(evt);
            }
        });

        btnSiguiente.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/siguiente icono.png"))); // NOI18N
        btnSiguiente.setText("Siguiente");
        btnSiguiente.setBorder(null);
        btnSiguiente.setContentAreaFilled(false);
        btnSiguiente.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        btnSiguiente.setPreferredSize(new java.awt.Dimension(90, 60));
        btnSiguiente.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/siguiiente.png"))); // NOI18N
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel7.setText("Cédula del Estudiante");

        cmbCedulaEstudiante.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbCedulaEstudiante.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "V", "E" }));
        cmbCedulaEstudiante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCedulaEstudianteActionPerformed(evt);
            }
        });

        txtCedulaEstudiante.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedulaEstudiante.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtCedulaEstudianteFocusGained(evt);
            }
        });
        txtCedulaEstudiante.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedulaEstudianteActionPerformed(evt);
            }
        });
        txtCedulaEstudiante.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaEstudianteKeyTyped(evt);
            }
        });

        btnGenerarCedulaEstu.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        btnGenerarCedulaEstu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/asterisk4.png"))); // NOI18N
        btnGenerarCedulaEstu.setText("Generar");
        btnGenerarCedulaEstu.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/asterisk4 (1).png"))); // NOI18N
        btnGenerarCedulaEstu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGenerarCedulaEstuActionPerformed(evt);
            }
        });

        btnVerificarEstu.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        btnVerificarEstu.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Verificar.png"))); // NOI18N
        btnVerificarEstu.setText("Verificar");
        btnVerificarEstu.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/check!.png"))); // NOI18N
        btnVerificarEstu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarEstuActionPerformed(evt);
            }
        });

        btnSiguienteModificar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnSiguienteModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/siguiente icono.png"))); // NOI18N
        btnSiguienteModificar.setText("Siguiente");
        btnSiguienteModificar.setBorder(null);
        btnSiguienteModificar.setContentAreaFilled(false);
        btnSiguienteModificar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/siguiiente.png"))); // NOI18N
        btnSiguienteModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteModificarActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel8.setText("Año Escolar:");

        txtPeriodoEscolar.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 40, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(96, 96, 96)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(68, 68, 68)
                                .addComponent(inscripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(179, 179, 179)
                                .addComponent(jLabel6)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3))
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(190, 190, 190)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 900, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(96, 96, 96)
                        .addComponent(jLabel1))
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 904, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(316, 316, 316)
                        .addComponent(lblCedulaEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, 300, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(lblInstitucion)
                        .addGap(10, 10, 10)
                        .addComponent(txtInstitucion, javax.swing.GroupLayout.PREFERRED_SIZE, 528, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(rbtnPartidaNaci)
                        .addGap(6, 6, 6)
                        .addComponent(lblNoExistePartidaNaci)
                        .addGap(185, 185, 185)
                        .addComponent(rbtnCartaRetiro)
                        .addGap(6, 6, 6)
                        .addComponent(lblNoExisteCartaDeRetiro))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(rbtnCertificadoVacunas)
                        .addGap(6, 6, 6)
                        .addComponent(lblNoExisteCertificadoVacunas)
                        .addGap(173, 173, 173)
                        .addComponent(rbtnCertificadoPromo)
                        .addGap(6, 6, 6)
                        .addComponent(lblNoExisteCertificadoDePromo))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(rbtnFotocopiaCedulaRepre)
                        .addGap(6, 6, 6)
                        .addComponent(lblNoExisteFotocopiaCedulaRepre)
                        .addGap(19, 19, 19)
                        .addComponent(rbtnFotoEstu)
                        .addGap(6, 6, 6)
                        .addComponent(lblNoExisteFotoEstu, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(rbtnRifRepre)
                        .addGap(6, 6, 6)
                        .addComponent(lblNoExisteRifRepresentante)
                        .addGap(191, 191, 191)
                        .addComponent(rbtnCedulaEstu)
                        .addGap(6, 6, 6)
                        .addComponent(lblNoExisteCedulaEstu))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(rbtnCartaBC)
                        .addGap(6, 6, 6)
                        .addComponent(lblNoExisteCartaBuenaConducta)
                        .addGap(162, 162, 162)
                        .addComponent(rbtnSeleccionarTodo))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(34, 34, 34)
                        .addComponent(jLabel2))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 904, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addComponent(lblGrado)
                        .addGap(10, 10, 10)
                        .addComponent(cmbGrado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(lblNoExisteGradoSelecc)
                        .addGap(43, 43, 43)
                        .addComponent(lblSeccion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmbSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lblNoExisteSeccionSelecc)
                        .addGap(70, 70, 70)
                        .addComponent(lblTurno)
                        .addGap(10, 10, 10)
                        .addComponent(txtTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(41, 41, 41)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(lblDisponible, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(lblProfesor)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtProfesor, javax.swing.GroupLayout.PREFERRED_SIZE, 406, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel8)
                                .addGap(18, 18, 18)
                                .addComponent(txtPeriodoEscolar, javax.swing.GroupLayout.PREFERRED_SIZE, 131, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(58, 58, 58)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSiguienteModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 140, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(36, 36, 36)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblCedulaRepre)
                                .addGap(17, 17, 17)
                                .addComponent(cmbCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 142, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addGap(51, 51, 51)
                                .addComponent(cmbCedulaEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtCedulaEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(11, 11, 11)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblExisteRepresentante, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblExisteEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, 26, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnVerificarRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnVerificarEstu, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(btnRegistrarRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 135, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lblVerificacion, javax.swing.GroupLayout.DEFAULT_SIZE, 170, Short.MAX_VALUE))
                            .addComponent(btnGenerarCedulaEstu, javax.swing.GroupLayout.PREFERRED_SIZE, 136, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap(43, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(26, 26, 26)
                                .addComponent(inscripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel6)
                                    .addComponent(jLabel3))
                                .addGap(1, 1, 1)
                                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(17, 17, 17)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(lblCedulaRepre)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(cmbCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtCedulaRepre))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(btnRegistrarRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnVerificarRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblExisteRepresentante, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addComponent(lblVerificacion, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addComponent(cmbCedulaEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCedulaEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btnGenerarCedulaEstu, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btnVerificarEstu, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblExisteEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel1))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblCedulaEstudiante, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblInstitucion))
                    .addComponent(txtInstitucion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbtnPartidaNaci)
                    .addComponent(lblNoExistePartidaNaci)
                    .addComponent(rbtnCartaRetiro)
                    .addComponent(lblNoExisteCartaDeRetiro, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(3, 3, 3)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbtnCertificadoVacunas)
                    .addComponent(lblNoExisteCertificadoVacunas)
                    .addComponent(rbtnCertificadoPromo)
                    .addComponent(lblNoExisteCertificadoDePromo))
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbtnFotocopiaCedulaRepre)
                    .addComponent(lblNoExisteFotocopiaCedulaRepre)
                    .addComponent(rbtnFotoEstu)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(lblNoExisteFotoEstu, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(10, 10, 10)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(rbtnRifRepre))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(2, 2, 2)
                        .addComponent(lblNoExisteRifRepresentante))
                    .addComponent(rbtnCedulaEstu)
                    .addComponent(lblNoExisteCedulaEstu))
                .addGap(7, 7, 7)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(rbtnCartaBC)
                    .addComponent(lblNoExisteCartaBuenaConducta)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(rbtnSeleccionarTodo)))
                .addGap(10, 10, 10)
                .addComponent(jLabel2)
                .addGap(6, 6, 6)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblGrado))
                    .addComponent(cmbGrado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteGradoSelecc)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(cmbSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblSeccion))
                    .addComponent(lblNoExisteSeccionSelecc)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(lblTurno))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(txtTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btnSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSiguienteModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblDisponible, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblProfesor)
                            .addComponent(txtProfesor, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(txtPeriodoEscolar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCedulaRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedulaRepreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaRepreActionPerformed

    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
frmCancelarNuevoIngreso obj = new frmCancelarNuevoIngreso();
obj.setVisible(true);
this.dispose();

    // TODO add your handling code here:
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void btnMenuPrincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuPrincipalActionPerformed
   menuInicio obj= new menuInicio();
   obj.setVisible(true);
    this.dispose();// TODO add your handling code here:
    }//GEN-LAST:event_btnMenuPrincipalActionPerformed

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        if ((cmbGrado.getSelectedIndex()==0) || (cmbSeccion.getSelectedIndex()==0) || txtCedulaRepre.getText().isEmpty() || !rbtnPartidaNaci.isSelected() || !rbtnCertificadoVacunas.isSelected() || !rbtnFotoEstu.isSelected() || !rbtnCedulaEstu.isSelected() || !rbtnCartaBC.isSelected() || !rbtnCartaRetiro.isSelected() || !rbtnCertificadoPromo.isSelected() || !rbtnFotocopiaCedulaRepre.isSelected() || !rbtnRifRepre.isSelected()){
            dlgDatosVacios advertencia = new dlgDatosVacios(new javax.swing.JFrame(), true);
            advertencia.setVisible(true);
           if (cmbGrado.getSelectedIndex()==0) {
               lblNoExisteGradoSelecc.setVisible(true);
           }
           if (cmbSeccion.getSelectedIndex()==0) {
               lblNoExisteSeccionSelecc.setVisible(true);
           }

           
           if (!rbtnPartidaNaci.isSelected()) {
               lblNoExistePartidaNaci.setVisible(true);
           }
           if (!rbtnCertificadoVacunas.isSelected()){
               lblNoExisteCertificadoVacunas.setVisible(true);
           }
           if (!rbtnFotoEstu.isSelected()){
               lblNoExisteFotoEstu.setVisible(true);
           }
           if  (!rbtnCedulaEstu.isSelected()){
               lblNoExisteCedulaEstu.setVisible(true);
           }
           
           if (!rbtnCartaBC.isSelected()){
               lblNoExisteCartaBuenaConducta.setVisible(true);
           }
           if (!rbtnCartaRetiro.isSelected()){
               lblNoExisteCartaDeRetiro.setVisible(true);
           }
           if (!rbtnCertificadoPromo.isSelected()){
               lblNoExisteCertificadoDePromo.setVisible(true);
           }
           if (!rbtnFotocopiaCedulaRepre.isSelected()){
               lblNoExisteFotocopiaCedulaRepre.setVisible(true);
           }
           if (!rbtnRifRepre.isSelected()){
               lblNoExisteRifRepresentante.setVisible(true);
           }
        }else{
       frmInscribirNuevo2 obj= new frmInscribirNuevo2();
         
       obj.setVisible(true);
       frmInscribirNuevo2.txtCedula.setText(cmbCedulaEstudiante.getSelectedItem().toString()+"-"+txtCedulaEstudiante.getText());
       frmInscribirNuevo2.txtCedulaRepre.setText(cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText());
       frmInscribirNuevo2.txtGrado.setText(cmbGrado.getSelectedItem().toString());
       frmInscribirNuevo2.txtSeccion.setText(cmbSeccion.getSelectedItem().toString());
       frmInscribirNuevo2.txtTurno.setText(txtTurno.getText());
       frmInscribirNuevo2.txtInstitucion.setText(txtInstitucion.getText());
       frmInscribirNuevo2.txtNombreProf.setText(txtProfesor.getText());
       frmInscribirNuevo2.btnSiguiente.setVisible(true);
       frmInscribirNuevo2.txtPeriodoEscolar.setText(txtPeriodoEscolar.getText());
       frmInscribirNuevo2.btnRegistrar.setVisible(true);
       this.dispose();
       
        }// TODO add your handling code here:
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnRegistrarRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarRepreActionPerformed
    dlgInscribirRepresentante dialog = new dlgInscribirRepresentante(new javax.swing.JFrame(), true);
    dialog.setVisible(true);
    // TODO add your handling code here:
    }//GEN-LAST:event_btnRegistrarRepreActionPerformed

    private void btnVerificarRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarRepreActionPerformed
    verificarRepresentante();        //lblExiste.setVisible(false);
       
       
            
        
    }//GEN-LAST:event_btnVerificarRepreActionPerformed

    private void cmbSeccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSeccionActionPerformed
       if (cmbSeccion.getSelectedIndex()>0){
           lblNoExisteSeccionSelecc.setVisible(false);
       }     // TODO add your handling code here:
    }//GEN-LAST:event_cmbSeccionActionPerformed

    private void txtCedulaRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaRepreKeyTyped
       char c=evt.getKeyChar();

        if ((!Character.isDigit(c)) && (c!=KeyEvent.VK_ENTER) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume(); 
        }
           if(c== KeyEvent.VK_ENTER){
            verificarRepresentante();    
           
        }
           if(txtCedulaRepre.getText().length()>=9){
            evt.consume();
        }
           if (c!= KeyEvent.VK_ENTER){
           lblExisteRepresentante.setVisible(false);
           lblVerificacion.setVisible(false);
           }
    }//GEN-LAST:event_txtCedulaRepreKeyTyped

    private void cmbGradoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbGradoItemStateChanged
        String [] iniciar = {"Selec"};
        cmbSeccion.setModel(new DefaultComboBoxModel(iniciar));
        if (evt.getStateChange() == ItemEvent.SELECTED){
            if (cmbGrado.getSelectedIndex()>0){
                //se utiliza el metodo GetSeccion para cargar el modelo de las seccion existentes 
                 conexion conn=new conexion();
        Connection cn=conn.conectar();
            String seccion= null;
           String query="";
           query="SELECT seccion FROM secciongrado WHERE grado='"+cmbGrado.getSelectedItem().toString()+"'";
           try {
                
               int i = 0;
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                //Tomo los valores correspondientes de cada seccion segun el grado
                seccion=rs.getString("seccion");
                cmbSeccion.addItem(seccion);
                }
                
                
                
            } catch (Exception e) {
            }
// this.cmbSeccion.setModel(new DefaultComboBoxModel(this.getSeccion(this.cmbGrado.getSelectedItem().toString())) );
            }
       
        }
    }//GEN-LAST:event_cmbGradoItemStateChanged

    private void cmbSeccionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSeccionItemStateChanged
         if (evt.getStateChange() == ItemEvent.SELECTED){
             if (this.cmbSeccion.getSelectedIndex()==0){
             txtTurno.setText("");
            //lblNoDisponible.setVisible(false);
            lblDisponible.setVisible(false);
             }else {
                 this.txtTurno.setText(getTurno(this.cmbGrado.getSelectedItem().toString(), this.cmbSeccion.getSelectedItem().toString()));
                 this.txtProfesor.setText(getNombreProf(this.cmbGrado.getSelectedItem().toString(), this.cmbSeccion.getSelectedItem().toString()));
                 this.txtPeriodoEscolar.setText(getPeriodoEscolar(this.cmbGrado.getSelectedItem().toString(),this.cmbSeccion.getSelectedItem().toString()));
                 if (matriculaDisponible(this.cmbGrado.getSelectedItem().toString(),this.cmbSeccion.getSelectedItem().toString() ) < (matriculaSeccion(this.cmbGrado.getSelectedItem().toString(),this.cmbSeccion.getSelectedItem().toString() ))){
                    String path = "/images/myicon.png";  
                    URL url = this.getClass().getResource("/imagenes/existe.png");  
                    ImageIcon icon = new ImageIcon(url);  
  
                   
                    lblDisponible.setIcon(icon); 
                    lblDisponible.setVisible(true);
                    btnSiguiente.setEnabled(true);
                    btnSiguienteModificar.setEnabled(true);
                      //this.lblNoDisponible.setVisible(false); 
                 }else{
                     //this.lblNoDisponible.setVisible(true);
                     
                    URL url = this.getClass().getResource("/imagenes/no existe.png");  
                    ImageIcon icon = new ImageIcon(url); 
                    lblDisponible.setIcon(icon);
                     this.lblDisponible.setVisible(true);
                     btnSiguiente.setEnabled(false);
                    btnSiguienteModificar.setEnabled(false);
                     
                 }
             }
         }
        
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbSeccionItemStateChanged

    private void rbtnPartidaNaciActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnPartidaNaciActionPerformed
        if (rbtnPartidaNaci.isSelected()){
            lblNoExistePartidaNaci.setVisible(false);
        }// TODO add your handling code here:
    }//GEN-LAST:event_rbtnPartidaNaciActionPerformed

    private void rbtnCertificadoVacunasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnCertificadoVacunasActionPerformed
        if (rbtnCertificadoVacunas.isSelected()){
            lblNoExisteCertificadoVacunas.setVisible(false);
        }// TODO add your handling code here:
    }//GEN-LAST:event_rbtnCertificadoVacunasActionPerformed

    private void rbtnFotoEstuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnFotoEstuActionPerformed
        if (rbtnFotoEstu.isSelected()){
            lblNoExisteFotoEstu.setVisible(false);
        }// TODO add your handling code here:
    }//GEN-LAST:event_rbtnFotoEstuActionPerformed

    private void rbtnCedulaEstuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnCedulaEstuActionPerformed
        if (rbtnCedulaEstu.isSelected()){
            lblNoExisteCedulaEstu.setVisible(false);
        }
// TODO add your handling code here:
    }//GEN-LAST:event_rbtnCedulaEstuActionPerformed

    private void rbtnCartaBCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnCartaBCActionPerformed
        if (rbtnCartaBC.isSelected()){
            lblNoExisteCartaBuenaConducta.setVisible(false);
        }            
// TODO add your handling code here:
    }//GEN-LAST:event_rbtnCartaBCActionPerformed

    private void rbtnCartaRetiroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnCartaRetiroActionPerformed
       if (rbtnCartaRetiro.isSelected()){
           lblNoExisteCartaDeRetiro.setVisible(false);
       }
// TODO add your handling code here:
    }//GEN-LAST:event_rbtnCartaRetiroActionPerformed

    private void rbtnCertificadoPromoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnCertificadoPromoActionPerformed
        if (rbtnCertificadoPromo.isSelected()){
            lblNoExisteCertificadoDePromo.setVisible(false);
        }
// TODO add your handling code here:
    }//GEN-LAST:event_rbtnCertificadoPromoActionPerformed

    private void rbtnFotocopiaCedulaRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnFotocopiaCedulaRepreActionPerformed
       if (rbtnFotocopiaCedulaRepre.isSelected()){
           lblNoExisteFotocopiaCedulaRepre.setVisible(false);
       }   // TODO add your handling code here:
    }//GEN-LAST:event_rbtnFotocopiaCedulaRepreActionPerformed

    private void rbtnRifRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnRifRepreActionPerformed
       if (rbtnRifRepre.isSelected()){
           lblNoExisteRifRepresentante.setVisible(false);
       }// TODO add your handling code here:
    }//GEN-LAST:event_rbtnRifRepreActionPerformed

    private void cmbGradoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbGradoActionPerformed
       if (cmbGrado.getSelectedIndex()>0){
           lblNoExisteGradoSelecc.setVisible(false);
       }// TODO add your handling code here:
    }//GEN-LAST:event_cmbGradoActionPerformed

    private void rbtnSeleccionarTodoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnSeleccionarTodoActionPerformed
        // TODO add your handling code here:
        if(rbtnSeleccionarTodo.isSelected()){
         rbtnPartidaNaci.setSelected(true);
         rbtnCertificadoVacunas.setSelected(true);
         rbtnFotoEstu.setSelected(true);
         rbtnCedulaEstu.setSelected(true);
         rbtnCartaBC.setSelected(true);
         rbtnCartaRetiro.setSelected(true);
         rbtnCertificadoPromo.setSelected(true);
         rbtnFotocopiaCedulaRepre.setSelected(true);
         rbtnRifRepre.setSelected(true);
        }
        else{
            rbtnPartidaNaci.setSelected(false);
         rbtnCertificadoVacunas.setSelected(false);
         rbtnFotoEstu.setSelected(false);
         rbtnCedulaEstu.setSelected(false);
         rbtnCartaBC.setSelected(false);
         rbtnCartaRetiro.setSelected(false);
         rbtnCertificadoPromo.setSelected(false);
         rbtnFotocopiaCedulaRepre.setSelected(false);
         rbtnRifRepre.setSelected(false);
            
        }
    }//GEN-LAST:event_rbtnSeleccionarTodoActionPerformed

    private void cmbCedulaEstudianteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCedulaEstudianteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCedulaEstudianteActionPerformed

    private void txtCedulaEstudianteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedulaEstudianteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaEstudianteActionPerformed

    private void btnVerificarEstuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarEstuActionPerformed
        verificar();            // TODO add your handling code here:
    }//GEN-LAST:event_btnVerificarEstuActionPerformed

    private void txtCedulaEstudianteFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCedulaEstudianteFocusGained
        desactivar();   
        lblExisteEstudiante.setVisible(false);// TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaEstudianteFocusGained

    private void txtCedulaEstudianteKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaEstudianteKeyTyped
    char c=evt.getKeyChar();

        if((!Character.isDigit(c))&& (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume(); 
        }
        if(c==KeyEvent.VK_ENTER){
            verificar();
        }
         if(txtCedulaEstudiante.getText().length()>=9){
             
            evt.consume();
         }
         if(c!= KeyEvent.VK_ENTER){
         lblExisteEstudiante.setVisible(false);
         }
    }//GEN-LAST:event_txtCedulaEstudianteKeyTyped

    private void btnSiguienteModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteModificarActionPerformed
        if ((cmbGrado.getSelectedIndex()==0) || (cmbSeccion.getSelectedIndex()==0) || txtCedulaRepre.getText().isEmpty() || !rbtnPartidaNaci.isSelected() || !rbtnCertificadoVacunas.isSelected() || !rbtnFotoEstu.isSelected() || !rbtnCedulaEstu.isSelected() || !rbtnCartaBC.isSelected() || !rbtnCartaRetiro.isSelected() || !rbtnCertificadoPromo.isSelected() || !rbtnFotocopiaCedulaRepre.isSelected() || !rbtnRifRepre.isSelected()){
            dlgDatosVacios advertencia = new dlgDatosVacios(new javax.swing.JFrame(), true);
            advertencia.setVisible(true);
           if (cmbGrado.getSelectedIndex()==0) {
               lblNoExisteGradoSelecc.setVisible(true);
           }
           if (cmbSeccion.getSelectedIndex()==0) {
               lblNoExisteSeccionSelecc.setVisible(true);
           }

           
           if (!rbtnPartidaNaci.isSelected()) {
               lblNoExistePartidaNaci.setVisible(true);
           }
           if (!rbtnCertificadoVacunas.isSelected()){
               lblNoExisteCertificadoVacunas.setVisible(true);
           }
           if (!rbtnFotoEstu.isSelected()){
               lblNoExisteFotoEstu.setVisible(true);
           }
           if  (!rbtnCedulaEstu.isSelected()){
               lblNoExisteCedulaEstu.setVisible(true);
           }
           
           if (!rbtnCartaBC.isSelected()){
               lblNoExisteCartaBuenaConducta.setVisible(true);
           }
           if (!rbtnCartaRetiro.isSelected()){
               lblNoExisteCartaDeRetiro.setVisible(true);
           }
           if (!rbtnCertificadoPromo.isSelected()){
               lblNoExisteCertificadoDePromo.setVisible(true);
           }
           if (!rbtnFotocopiaCedulaRepre.isSelected()){
               lblNoExisteFotocopiaCedulaRepre.setVisible(true);
           }
           if (!rbtnRifRepre.isSelected()){
               lblNoExisteRifRepresentante.setVisible(true);
           }
        }else{
      
        frmInscribirNuevo2 obj= new frmInscribirNuevo2();
        conexion conn=new conexion();
        Connection cn=conn.conectar(); 
       obj.setVisible(true);
       String query2="SELECT f.ced_fa,f.nombre_fa,f.apellido_fa,f.fechaNacimiento_fa,f.profesion_fa,f.direccion_fa,f.tlf_fa,f.otroTlf_fa,f.observacion_fa,d.parentesco FROM (familiares f, estu_fami d,estudiante e) WHERE e.ced_estu='"+this.cmbCedulaEstudiante.getSelectedItem().toString()+"-"+this.txtCedulaEstudiante.getText()+"' AND e.ced_estu=d.ced_estu AND f.ced_fa=d.ced_fa AND d.parentesco='Mamá'";
       String query3="SELECT f.ced_fa,f.nombre_fa,f.apellido_fa,f.fechaNacimiento_fa,f.profesion_fa,f.direccion_fa,f.tlf_fa,f.otroTlf_fa,f.observacion_fa,d.parentesco FROM (familiares f, estu_fami d,estudiante e) WHERE e.ced_estu='"+this.cmbCedulaEstudiante.getSelectedItem().toString()+"-"+this.txtCedulaEstudiante.getText()+"' AND e.ced_estu=d.ced_estu AND f.ced_fa=d.ced_fa AND d.parentesco='Papá'";
       String query1= "SELECT tipo_sangre,v_bcg,v_trivalente,cirugia_s,alergia_s,v_penta,v_polio,v_antineumo,v_hepa_b,v_influenza,v_antiama,v_toxoide,enfermedad,peso,estatura,talla_camisa,talla_pantalon,talla_calzado FROM salud WHERE ced_estu='"+cmbCedulaEstudiante.getSelectedItem().toString()+"-"+txtCedulaEstudiante.getText()+"'";     
       String query="SELECT primerNombre_estu,PrimerApellido_estu,segundoNombre_estu,segundoApellido_estu,sexo_estu,fn_estu,lugarN_estu,direccion_estu,cod_Canaima,tlf_estu,otroTlf_estu FROM estudiante WHERE ced_estu='"+cmbCedulaEstudiante.getSelectedItem().toString()+"-"+txtCedulaEstudiante.getText()+"'";
            try {
       Statement  st = cn.createStatement();
       ResultSet rs= st.executeQuery(query);
       
       Statement rt = cn.createStatement();
       ResultSet dt = rt.executeQuery(query1);
       
       Statement pt=cn.createStatement();
       ResultSet ht= pt.executeQuery(query2);
       
       Statement qt=cn.createStatement();
       ResultSet hs= qt.executeQuery(query3);
       while (rs.next()){
        frmInscribirNuevo2.txtPrimerNombre.setText(rs.getString("primerNombre_estu"));
        frmInscribirNuevo2.txtPrimerApellido.setText(rs.getString("PrimerApellido_estu"));
        frmInscribirNuevo2.txtSegundoNombre.setText(rs.getString("segundoNombre_estu"));
        frmInscribirNuevo2.txtSegundoApellido.setText(rs.getString("segundoApellido_estu"));
        frmInscribirNuevo2.cmbSexo.setSelectedItem(rs.getString("sexo_estu"));
        String fecha2=   rs.getString("fn_estu");
         SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date fechaDate;
                fechaDate = formato.parse(fecha2);
                frmInscribirNuevo2.txtFechaNaci.setDate(fechaDate);
                
        frmInscribirNuevo2.txtLugarNacimiento.setText(rs.getString("lugarN_estu"));
        frmInscribirNuevo2.txtDireccion.setText(rs.getString("direccion_estu"));
        frmInscribirNuevo2.txtCodigoCanaima.setText(rs.getString("cod_Canaima"));
        String [] temp;
        String [] temp2;
        String tlf,tlf2;
        tlf=rs.getString("tlf_estu");
        temp=tlf.split("-");
        tlf2=rs.getString("otroTlf_estu");
        temp2=tlf2.split("-");
        frmInscribirNuevo2.cmbTelefono.setSelectedItem(temp[0]);
        frmInscribirNuevo2.txtTelefono.setText(temp[1]);
        frmInscribirNuevo2.cmbOtroTelefono.setSelectedItem(temp2[0]);
        frmInscribirNuevo2.txtOtroTelefono.setText(temp2[1]);
       }
       while(dt.next()){
           frmInscribirNuevo2.cmbTipoSangre.setSelectedItem(dt.getString("tipo_sangre"));
           frmInscribirNuevo2.txtPeso.setText(dt.getString("peso"));
           frmInscribirNuevo2.txtEstatura.setText(dt.getString("estatura"));
           frmInscribirNuevo2.cmbTallaCamisa.setSelectedItem(dt.getString("talla_camisa"));
           frmInscribirNuevo2.cmbTallaPantalon.setSelectedItem(dt.getString("talla_pantalon"));
           frmInscribirNuevo2.cmbTallaCalzado.setSelectedItem(dt.getString("talla_calzado"));
           String aux="",aux1="",aux2="";
           aux=dt.getString("alergia_s");
           aux1=dt.getString("cirugia_s");
           aux2=dt.getString("enfermedad");
           if (aux.equals("")){
               frmInscribirNuevo2.rbtnAlergiaNo.setSelected(true);
               frmInscribirNuevo2.txtIndiqueTratamiento3.setEnabled(false);
           }
           else{
               frmInscribirNuevo2.rbtnAlergiaSi.setSelected(true);
               frmInscribirNuevo2.txtIndiqueTratamiento3.setEnabled(true);
           }
           
           if(aux1.equals("")){
              frmInscribirNuevo2.rbtnCirugiaNo.setSelected(true);
              frmInscribirNuevo2.txtIndiqueTratamiento.setEnabled(false);
           }
           else{
               frmInscribirNuevo2.rbtnCirugiaSi.setSelected(true);
               frmInscribirNuevo2.txtIndiqueTratamiento.setEnabled(true);
           }
               
           if(aux2.equals("")){
              frmInscribirNuevo2.rbtnEnfermedadNo.setSelected(true);
              frmInscribirNuevo2.txtIndiqueTratamientoEnfermedad.setEnabled(false);
           }
           else{
             frmInscribirNuevo2.rbtnEnfermedadSi.setSelected(true);
             frmInscribirNuevo2.txtIndiqueTratamientoEnfermedad.setEnabled(true);
           }
          if (dt.getInt("v_penta")==1){
             frmInscribirNuevo2.rbtnPentavalenteDosis1.setSelected(true);

          }
          else{
              if(dt.getInt("v_penta")==2){
             frmInscribirNuevo2.rbtnPentavalenteDosis2.setSelected(true);

              }
              else{
                  if(dt.getInt("v_penta")==3){
                      frmInscribirNuevo2.rbtnPentavalenteDosis3.setSelected(true);
                  }
              }
          }
           if(dt.getInt("v_polio")==1){
               frmInscribirNuevo2.rbtnPolioDosis1.setSelected(true);
           }
           else{
               if(dt.getInt("v_polio")==2){
                   frmInscribirNuevo2.rbtnPolioDosis2.setSelected(true);
               }
           }
           if(dt.getInt("v_antineumo")==1){
               frmInscribirNuevo2.rbtnAntineumococoDosis1.setSelected(true);
           }
           
           else{
               if(dt.getInt("v_antineumo")==2){
                   frmInscribirNuevo2.rbtnAntineumococoDosis2.setSelected(true);
               }
           }
           if(dt.getInt("v_hepa_b")==1){
                  frmInscribirNuevo2.rbtnHepatitisDosis1.setSelected(true);
           }
           if(dt.getInt("v_antiama")==1){
               frmInscribirNuevo2.rbtnAntiamarilicaDosis1.setSelected(true);
           }
               
           if(dt.getInt("v_trivalente")==1){
               frmInscribirNuevo2.rbtnTrivalenteDosis1.setSelected(true);
           }
           if(dt.getInt("v_bcg")==1){
               frmInscribirNuevo2.rbtnBCGDosis1.setSelected(true);
           }
           if(dt.getInt("v_influenza")==1){
               frmInscribirNuevo2.rbtnInfluenzaDosis1.setSelected(true);
           }
           else{
               if(dt.getInt("v_influenza")==2){
                   frmInscribirNuevo2.rbtnInfluenzaDosis2.setSelected(true);
               }
           }
           if(dt.getInt("v_toxoide")==1){
               frmInscribirNuevo2.rbtnToxoideDosis1.setSelected(true);
           }
       }
      
       
           obj.txtCedula.setText(this.cmbCedulaEstudiante.getSelectedItem().toString()+"-"+txtCedulaEstudiante.getText());
       obj.txtCedulaRepre.setText(this.cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText());
       obj.txtGrado.setText(this.cmbGrado.getSelectedItem().toString());
       obj.txtSeccion.setText(this.cmbSeccion.getSelectedItem().toString());
       obj.txtTurno.setText(this.txtTurno.getText());
       obj.txtInstitucion.setText(this.txtInstitucion.getText());
       obj.txtNombreProf.setText(this.txtProfesor.getText());
       obj.txtPeriodoEscolar.setText(this.txtPeriodoEscolar.getText()); 
       obj.btnGuardarActualizar.setVisible(true);
       
       this.dispose(); 
            } catch (Exception e) {
            JOptionPane.showMessageDialog(null,e);
            }
       
        }// TODO add your handling code here:
    }//GEN-LAST:event_btnSiguienteModificarActionPerformed

    private void txtCedulaRepreComponentResized(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_txtCedulaRepreComponentResized
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaRepreComponentResized

    private void txtCedulaRepreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCedulaRepreFocusGained
               lblVerificacion.setVisible(false);
               lblExisteRepresentante.setVisible(false);
               txtCedulaEstudiante.setEditable(false);
               btnVerificarEstu.setEnabled(false);
               btnGenerarCedulaEstu.setEnabled(false);
               cmbCedulaEstudiante.setEnabled(false);   
               txtCedulaEstudiante.setText("");// TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaRepreFocusGained

    private void cmbCedulaRepreItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbCedulaRepreItemStateChanged
        if (evt.getStateChange() == ItemEvent.SELECTED){       
               lblVerificacion.setVisible(false);
               txtCedulaEstudiante.setEditable(false);
               btnVerificarEstu.setEnabled(false);
               btnGenerarCedulaEstu.setEnabled(false);
               cmbCedulaEstudiante.setEnabled(false);
               txtCedulaEstudiante.setText("");// TODO add your handling code here:
        }
    }//GEN-LAST:event_cmbCedulaRepreItemStateChanged

    private void btnGenerarCedulaEstuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGenerarCedulaEstuActionPerformed
        // TODO add your handling code here:
        dlgGenerarCedulaEstudiante dialog = new dlgGenerarCedulaEstudiante(new javax.swing.JFrame(), true);
        dialog.setVisible(true);
    }//GEN-LAST:event_btnGenerarCedulaEstuActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmInscribir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmInscribir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmInscribir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmInscribir.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmInscribir().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAtras;
    private javax.swing.JButton btnGenerarCedulaEstu;
    private javax.swing.JButton btnMenuPrincipal;
    private javax.swing.JButton btnRegistrarRepre;
    private javax.swing.JButton btnSiguiente;
    public static javax.swing.JButton btnSiguienteModificar;
    private javax.swing.JButton btnVerificarEstu;
    private javax.swing.JButton btnVerificarRepre;
    public static javax.swing.JComboBox cmbCedulaEstudiante;
    private javax.swing.JComboBox cmbCedulaRepre;
    private javax.swing.JComboBox cmbGrado;
    private javax.swing.JComboBox cmbSeccion;
    private javax.swing.JLabel inscripcion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JLabel lblCedulaEstudiante;
    private javax.swing.JLabel lblCedulaRepre;
    private javax.swing.JLabel lblDisponible;
    private javax.swing.JLabel lblExisteEstudiante;
    private javax.swing.JLabel lblExisteRepresentante;
    private javax.swing.JLabel lblGrado;
    private javax.swing.JLabel lblInstitucion;
    private javax.swing.JLabel lblNoExisteCartaBuenaConducta;
    private javax.swing.JLabel lblNoExisteCartaDeRetiro;
    private javax.swing.JLabel lblNoExisteCedulaEstu;
    private javax.swing.JLabel lblNoExisteCertificadoDePromo;
    private javax.swing.JLabel lblNoExisteCertificadoVacunas;
    private javax.swing.JLabel lblNoExisteFotoEstu;
    private javax.swing.JLabel lblNoExisteFotocopiaCedulaRepre;
    private javax.swing.JLabel lblNoExisteGradoSelecc;
    private javax.swing.JLabel lblNoExistePartidaNaci;
    private javax.swing.JLabel lblNoExisteRifRepresentante;
    private javax.swing.JLabel lblNoExisteSeccionSelecc;
    private javax.swing.JLabel lblProfesor;
    private javax.swing.JLabel lblSeccion;
    private javax.swing.JLabel lblTurno;
    private javax.swing.JLabel lblVerificacion;
    private javax.swing.JRadioButton rbtnCartaBC;
    private javax.swing.JRadioButton rbtnCartaRetiro;
    private javax.swing.JRadioButton rbtnCedulaEstu;
    private javax.swing.JRadioButton rbtnCertificadoPromo;
    private javax.swing.JRadioButton rbtnCertificadoVacunas;
    private javax.swing.JRadioButton rbtnFotoEstu;
    private javax.swing.JRadioButton rbtnFotocopiaCedulaRepre;
    private javax.swing.JRadioButton rbtnPartidaNaci;
    private javax.swing.JRadioButton rbtnRifRepre;
    private javax.swing.JRadioButton rbtnSeleccionarTodo;
    public static javax.swing.JTextField txtCedulaEstudiante;
    private javax.swing.JTextField txtCedulaRepre;
    private javax.swing.JTextField txtInstitucion;
    private javax.swing.JLabel txtPeriodoEscolar;
    public static javax.swing.JLabel txtProfesor;
    private javax.swing.JLabel txtTurno;
    // End of variables declaration//GEN-END:variables
}
