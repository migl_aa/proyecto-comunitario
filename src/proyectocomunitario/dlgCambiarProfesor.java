/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocomunitario;

import BD.conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;

/**
 *
 * @author NANY
 */
public class dlgCambiarProfesor extends javax.swing.JDialog {

    /**
     * Creates new form dlgCambiarProfesor
     */
    public dlgCambiarProfesor(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        cargarProfesor();
    }
    void cambiar(){
        conexion con=new conexion();
        Connection cn=con.conectar();
        String [] temp=txtProfesor.getText().split("\\s+");
        String consulta="UPDATE profesores SET asignado=0 WHERE CedulaProf='"+temp[0]+"'";
        String [] temp2=cmbProfesor.getSelectedItem().toString().split("\\s+");
        String consulta2="UPDATE profesores SET asignado=1 WHERE CedulaProf='"+temp2[0]+"'";
        try {
            Statement st=cn.createStatement();
            st.execute(consulta);
            Statement rs=cn.createStatement();
            rs.execute(consulta2);
            frmAdminSecciones.txtProfesores.setText(this.cmbProfesor.getSelectedItem().toString());
        } catch (Exception e) {
        }
    }
     void cargarProfesor(){
          conexion con=new conexion();
        Connection cn=con.conectar();
        
        String consulta="SELECT CedulaProf, nombreProf,apellidoProf FROM profesores WHERE asignado=0 AND estado_logico=1 ";
        String [] iniciar ={"Selec"};
        cmbProfesor.setModel(new DefaultComboBoxModel(iniciar));
         try {
             Statement st=cn.createStatement();
             ResultSet rs=st.executeQuery(consulta);
             while(rs.next()){
                 cmbProfesor.addItem(rs.getString("CedulaProf")+" "+rs.getString("nombreProf")+" "+rs.getString("apellidoProf"));
             }
         } catch (Exception e) {
         }
     }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new imagenes.Fondo("fondo generar.jpg");
        txtProfesor = new javax.swing.JTextField();
        btnCambiar = new javax.swing.JButton();
        lblProfesor = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        cmbProfesor = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtProfesor.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        txtProfesor.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtProfesorKeyTyped(evt);
            }
        });
        jPanel1.add(txtProfesor, new org.netbeans.lib.awtextra.AbsoluteConstraints(134, 99, 200, -1));

        btnCambiar.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        btnCambiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cambiar.png"))); // NOI18N
        btnCambiar.setText("Cambiar");
        btnCambiar.setBorder(null);
        btnCambiar.setContentAreaFilled(false);
        btnCambiar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cambiar!.png"))); // NOI18N
        btnCambiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCambiarActionPerformed(evt);
            }
        });
        jPanel1.add(btnCambiar, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 210, -1, -1));

        lblProfesor.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblProfesor.setText("Profesor Actual:");
        jPanel1.add(lblProfesor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 102, -1, -1));

        jLabel3.setFont(new java.awt.Font("Comfortaa", 1, 45)); // NOI18N
        jLabel3.setText("Cambiar");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 10, -1, -1));

        jLabel4.setFont(new java.awt.Font("Comfortaa", 0, 26)); // NOI18N
        jLabel4.setText("Profesor");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 50, -1, -1));

        cmbProfesor.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jPanel1.add(cmbProfesor, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 150, 330, -1));

        jLabel1.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jLabel1.setText("Seleccione el Nuevo Profesor");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, -1, -1));

        jButton1.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar (2).png"))); // NOI18N
        jButton1.setText("Cancelar");
        jButton1.setBorder(null);
        jButton1.setContentAreaFilled(false);
        jButton1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(350, 210, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 580, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 253, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtProfesorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtProfesorKeyTyped
        // TODO add your handling code here:
        
    }//GEN-LAST:event_txtProfesorKeyTyped

    private void btnCambiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCambiarActionPerformed
        // TODO add your handling code here:
        
    }//GEN-LAST:event_btnCambiarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
this.dispose();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgCambiarProfesor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgCambiarProfesor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgCambiarProfesor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgCambiarProfesor.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgCambiarProfesor dialog = new dlgCambiarProfesor(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCambiar;
    private javax.swing.JComboBox cmbProfesor;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel lblProfesor;
    public static javax.swing.JTextField txtProfesor;
    // End of variables declaration//GEN-END:variables
}
