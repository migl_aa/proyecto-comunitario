/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocomunitario;

import BD.conexion;
import com.toedter.calendar.JCalendar;
import java.awt.GraphicsEnvironment;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;

/**
 *
 * @author Miguel Arroyo
 */
public class frmInscribirNuevo2 extends javax.swing.JFrame {

    /**
     * Creates new form frmInscribirNuevo2
     */
    public frmInscribirNuevo2() {
        initComponents();
        txtInstitucion.setVisible(false);
        txtTelefono.setEditable(false);
        txtOtroTelefono.setEditable(false);
        btnGuardarActualizar.setVisible(false);
        btnRegistrar.setVisible(false);
        txtPeriodoEscolar.setVisible(false);
        txtNombreProf.setVisible(false);
        txtIndiqueTratamiento.setEnabled(false);
        txtIndiqueTratamiento3.setEnabled(false);
        txtIndiqueTratamientoEnfermedad.setEnabled(false);
        this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
        jPanel3.setVisible(false);
        jPanel5.setVisible(false);
        Mostrar();
        inhabilitar();
        desactivarMadre();
        desactivarPadre();
        
    }
    void limpiarPadre (){
         cmbCedulaPadre.setSelectedIndex(0);
         txtCedulaPadre.setText("");
         txtNombrePadre.setText("");
        txtApellidoPadre.setText("");
        txtFechaNaciPadre.setDate(null);
        txtProfesionPadre.setText("");
        txtDireccionPadre.setText("");
        cmbTelefonoPadre.setSelectedIndex(0);
        txtTelefonoPadre.setText("");
        cmbOtroTlfPadre.setSelectedIndex(0);
        txtOtroTlfPadre.setText("");
        txtObservacionPadre.setText("");
    } 
    void limpiarMadre(){ 
    cmbCedulaMadre.setSelectedIndex(0);
        txtCedulaMadre.setText("");
        txtNombreMadre.setText("");
        txtApellidoMadre.setText("");
        txtFechaNaMadre.setDate(null);
        txtProfesionMadre.setText("");
        txtDireccionMadre.setText("");
        cmbTelefonoMadre.setSelectedIndex(0);
        txtTelefonoMadre.setText("");
        cmbOtroTlfMadre.setSelectedIndex(0);
        txtOtroTlfMadre.setText("");
        txtObservacionMadre.setText("");
    }
    void desactivarMadre (){
        
        txtNombreMadre.setEditable(false);
        txtApellidoMadre.setEditable(false);
        txtFechaNaMadre.getCalendarButton().setEnabled(false);
        txtProfesionMadre.setEditable(false);
        txtDireccionMadre.setEditable(false);
        cmbTelefonoMadre.setEnabled(false);
        txtTelefonoMadre.setEditable(false);
        cmbOtroTlfMadre.setEnabled(false);
        txtOtroTlfMadre.setEditable(false);
        txtObservacionMadre.setEditable(false);
    }
       
     void activarPadre(){
         cmbCedulaPadre.setEnabled(true);
         txtCedulaPadre.setEditable(true);
         txtNombrePadre.setEditable(true);
        txtApellidoPadre.setEditable(true);
        txtFechaNaciPadre.getCalendarButton().setEnabled(true);
        txtProfesionPadre.setEditable(true);
        txtDireccionPadre.setEditable(true);
        cmbTelefonoPadre.setEnabled(true);
        txtTelefonoPadre.setEditable(true);
        cmbOtroTlfPadre.setEnabled(true);
        txtOtroTlfPadre.setEditable(true);
        txtObservacionPadre.setEditable(true);
        
     }
    void desactivarPadre(){
        
        txtNombrePadre.setEditable(false);
        txtApellidoPadre.setEditable(false);
        txtFechaNaciPadre.getCalendarButton().setEnabled(false);
        txtProfesionPadre.setEditable(false);
        txtDireccionPadre.setEditable(false);
        cmbTelefonoPadre.setEnabled(false);
        txtTelefonoPadre.setEditable(false);
        cmbOtroTlfPadre.setEnabled(false);
        txtOtroTlfPadre.setEditable(false);
        txtObservacionPadre.setEditable(false);
    }
    void activarMadre(){
        cmbCedulaMadre.setEnabled(true);
        txtCedulaMadre.setEditable(true);
        txtNombreMadre.setEditable(true);
        txtApellidoMadre.setEditable(true);
        txtFechaNaMadre.getCalendarButton().setEnabled(true);
        txtProfesionMadre.setEditable(true);
        txtDireccionMadre.setEditable(true);
        cmbTelefonoMadre.setEnabled(true);
        txtTelefonoMadre.setEditable(true);
        cmbOtroTlfMadre.setEnabled(true);
        txtOtroTlfMadre.setEditable(true);
        txtObservacionMadre.setEditable(true);
    }
    boolean aux=false;

    public boolean isAux() {
        return aux;
    }

    public void setAux(boolean aux) {
        this.aux = aux;
    }
    boolean aux2=false;

    public boolean isAux2 () {
        return aux2;
    }

    public void setAux2(boolean aux2) {
        this.aux2 = aux2;
    }
    
    boolean auxPadre=false;

    public boolean isAuxPadre() {
        return auxPadre;
    }

    public void setAuxPadre(boolean auxPadre) {
        this.auxPadre = auxPadre;
    }
    boolean auxMadre=false;

    public boolean isAuxMadre() {
        return auxMadre;
    }

    public void setAuxMadre(boolean auxMadre) {
        this.auxMadre = auxMadre;
    }
    boolean editarPadre=false;
    boolean editarMadre=false;

    public boolean isEditarPadre() {
        return editarPadre;
    }

    public void setEditarPadre(boolean editarPadre) {
        this.editarPadre = editarPadre;
    }

    public boolean isEditarMadre() {
        return editarMadre;
    }

    public void setEditarMadre(boolean editarMadre) {
        this.editarMadre = editarMadre;
    }
    void inhabilitar (){
        txtCedula.setEditable(false);
        txtTurno.setEditable(false);
        txtGrado.setEditable(false);
        txtSeccion.setEditable(false);
        txtCedulaRepre.setEditable(false);
        txtCedula1.setEditable(false);
       txtFechaNaci.getDateEditor().setEnabled(false);
        JCalendar nuevo=txtFechaNaciPadre.getJCalendar();
            JSpinner bloqueo= (JSpinner) nuevo.getYearChooser().getSpinner();
            ((JTextField)bloqueo.getEditor()).setEditable(false); 
            txtFechaNaciPadre.getDateEditor().setEnabled(false);
        JCalendar nuevo2=txtFechaNaMadre.getJCalendar();
            JSpinner bloqueo2= (JSpinner) nuevo2.getYearChooser().getSpinner();
            ((JTextField)bloqueo2.getEditor()).setEditable(false);
            txtFechaNaMadre.getDateEditor().setEnabled(false);
        JCalendar nuevo3=txtFechaNaci.getJCalendar();
            JSpinner bloqueo3= (JSpinner) nuevo3.getYearChooser().getSpinner();
            ((JTextField)bloqueo3.getEditor()).setEditable(false);
    }
    void Mostrar (){
        //Se ocultan las etiquetas de los iconos
        
        lblNoExisteDireccion.setVisible(false);
        lblNoExisteFechaNaci.setVisible(false);
        lblNoExisteLugarNaci.setVisible(false);
        lblNoExisteParentesco.setVisible(false);
        lblNoExistePrimerApellido.setVisible(false);
        lblNoExistePrimerNombre.setVisible(false);
        lblNoExisteSexo.setVisible(false);
        lblNoExisteTelefono.setVisible(false);
        lblNoExisteCodigoCanaima.setVisible(false);
    }
    void cargarMadre(String valor){
        conexion conn=new conexion();
        Connection cn=conn.conectar();
        String consulta="SELECT ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,direccion_fa,tlf_fa,otroTlf_fa,observacion_fa FROM familiares WHERE ced_fa='"+valor+"'";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(consulta);
            while (rs.next()){
                txtNombreMadre.setText(rs.getString("nombre_fa"));
                txtApellidoMadre.setText(rs.getString("apellido_fa"));
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date fechaDate;
                fechaDate = formato.parse(rs.getString("fechaNacimiento_fa"));
                txtFechaNaMadre.setDate(fechaDate);
                txtProfesionMadre.setText(rs.getString("profesion_fa"));
                txtDireccionMadre.setText(rs.getString("direccion_fa"));
                String [] temp;
                
                temp=rs.getString("tlf_fa").split("-");
                cmbTelefonoMadre.setSelectedItem(temp[0]);
                txtTelefonoMadre.setText(temp[1]);
                if (!rs.getString("otroTlf_fa").isEmpty()){
                String [] temp2;
                
                temp2=rs.getString("otroTlf_fa").split("-");
                cmbOtroTlfMadre.setSelectedItem(temp2[0]);
                txtOtroTlfMadre.setText(temp2[1]);
                }txtObservacionMadre.setText(rs.getString("observacion_fa"));
                if (rs.getString("ced_fa")!=""){
                desactivarMadre();
                cmbCedulaMadre.setEnabled(false);
                txtCedulaMadre.setEditable(false);
                setAuxMadre(true);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
       
    }
    void cargarPadre(String valor){
        conexion conn=new conexion();
        Connection cn=conn.conectar();
        String consulta="SELECT ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,direccion_fa,tlf_fa,otroTlf_fa,observacion_fa FROM familiares WHERE ced_fa='"+valor+"'";
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(consulta);
            while (rs.next()){
                txtNombrePadre.setText(rs.getString("nombre_fa"));
                txtApellidoPadre.setText(rs.getString("apellido_fa"));
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date fechaDate;
                fechaDate = formato.parse(rs.getString("fechaNacimiento_fa"));
                txtFechaNaciPadre.setDate(fechaDate);
                txtProfesionPadre.setText(rs.getString("profesion_fa"));
                txtDireccionPadre.setText(rs.getString("direccion_fa"));
                String [] temp;
                
                temp=rs.getString("tlf_fa").split("-");
                cmbTelefonoPadre.setSelectedItem(temp[0]);
                txtTelefonoPadre.setText(temp[1]);
                if (!rs.getString("otroTlf_fa").isEmpty()){
                String [] temp2;
                
                temp2=rs.getString("otroTlf_fa").split("-");
                cmbOtroTlfPadre.setSelectedItem(temp2[0]);
                txtOtroTlfPadre.setText(temp2[1]);
                }
                txtObservacionPadre.setText(rs.getString("observacion_fa"));
                if (rs.getString("ced_fa")!=""){
                desactivarPadre();
                cmbCedulaPadre.setEnabled(false);
                txtCedulaPadre.setEditable(false);
                setAuxPadre(true);
                }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
    }
    void registrar(){
        //CAPTURA DE DATOS DE DATOS FAMILIARES
        conexion conn=new conexion();
            Connection cn=conn.conectar();
        String cedulaMadre = "",cedulaPadre = "",nombreMadre = "",nombrePadre = "",apellidoMadre = "",apellidoPadre = "",fechaNaMadre = "",fechaNaPadre = "",observacionMadre = "",observacionPadre = "",direccionMadre = "",direccionPadre = "",telfMadre = "",telfPadre = "",otroTelfMadre = "",otroTelfPadre = "",profesionMadre = "",profesionPadre = "";
        String cedula="",periodo="",profesor="",insti="",primerNombre="",segundoNombre="",primerApellido="",segundoApellido="",cedulaRepre="",grado,seccion,turno,sexo,fechaNaci,lugarNaci="",telefono1="",telefono2="",direccion,canaima,parentesco,nacionalidad;
        if ((isAuxMadre()==false) && (!txtCedulaMadre.getText().isEmpty()) && (isEditarMadre()==false)){
        cedulaMadre=cmbCedulaMadre.getSelectedItem().toString()+"-"+txtCedulaMadre.getText();
        nombreMadre=txtNombreMadre.getText();
        apellidoMadre=txtApellidoMadre.getText();
         String ano,mes,dia; 
                    ano=Integer.toString(txtFechaNaMadre.getCalendar().get(Calendar.YEAR));
                     mes=Integer.toString(txtFechaNaMadre.getCalendar().get(Calendar.MONTH)+1);
                     dia=Integer.toString(txtFechaNaMadre.getCalendar().get(Calendar.DAY_OF_MONTH));
                     fechaNaMadre=dia+"/"+mes+"/"+ano;
       profesionMadre=txtProfesionMadre.getText();
       direccionMadre=txtDireccionMadre.getText();
       telfMadre=cmbTelefonoMadre.getSelectedItem().toString()+"-"+txtTelefonoMadre.getText();
       otroTelfMadre=cmbOtroTlfMadre.getSelectedItem().toString()+"-"+txtTelefonoMadre.getText();
       observacionMadre=txtObservacionMadre.getText();
        }
       if ((isAuxPadre()==false) && (!txtCedulaPadre.getText().isEmpty()) && (isEditarPadre()==false)){
       cedulaPadre=cmbCedulaPadre.getSelectedItem().toString()+"-"+txtCedulaPadre.getText();
       nombrePadre=txtNombrePadre.getText();
       apellidoPadre=txtApellidoPadre.getText();
       String ano1,mes1,dia1; 
                    ano1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.YEAR));
                     mes1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.MONTH)+1);
                     dia1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.DAY_OF_MONTH));
                     fechaNaPadre=dia1+"/"+mes1+"/"+ano1;
        profesionPadre=txtProfesionPadre.getText();
        direccionPadre=txtDireccionMadre.getText();
        telfPadre=cmbTelefonoPadre.getSelectedItem().toString()+"-"+txtTelefonoPadre.getText();
        otroTelfPadre=cmbOtroTlfPadre.getSelectedItem().toString()+"-"+txtOtroTlfPadre.getText();
        observacionPadre=txtObservacionPadre.getText();
       }
//CAPTURA DE DATOS DE DATOS PERSONALES
                cedula=txtCedula.getText();
                primerNombre=txtPrimerNombre.getText();
                segundoNombre=txtSegundoNombre.getText();
                primerApellido=txtPrimerApellido.getText();
                segundoApellido=txtSegundoApellido.getText();
                cedulaRepre=txtCedulaRepre.getText();
                grado=txtGrado.getText();
                seccion=txtSeccion.getText();
                turno=txtTurno.getText();
                parentesco=cmbParentesco.getSelectedItem().toString();
                sexo=cmbSexo.getSelectedItem().toString();
                profesor=txtNombreProf.getText();
                periodo=txtPeriodoEscolar.getText();
                if (txtFechaNaci.getDate()==null){
                    fechaNaci="";
                }else{
                    String ano,mes,dia;
                    ano=Integer.toString(txtFechaNaci.getCalendar().get(Calendar.YEAR));
                     mes=Integer.toString(txtFechaNaci.getCalendar().get(Calendar.MONTH)+1);
                     dia=Integer.toString(txtFechaNaci.getCalendar().get(Calendar.DAY_OF_MONTH));
                     fechaNaci=dia+"/"+mes+"/"+ano;
                        }
                lugarNaci=txtLugarNacimiento.getText();
                insti=txtInstitucion.getText();
                telefono1=cmbTelefono.getSelectedItem().toString()+"-"+txtTelefono.getText();
                if (txtOtroTelefono.getText().isEmpty()){
                    telefono2="";
                }else{
                telefono2=cmbOtroTelefono.getSelectedItem().toString()+"-"+txtOtroTelefono.getText();
                }
                direccion=txtDireccion.getText();
                canaima=txtCodigoCanaima.getText();
                String [] temp;
                temp=cedula.split("-");
                if(temp[0].equals("V")){
                    nacionalidad="VENEZOLANO";
                   
                }
                else{
                    nacionalidad="EXTRANJERO";
                } 
                //CAPTURA DE DATOS SALUD
                int dosisPenta,dosisPolio,dosisAntineu,dosisBCG,dosisHepa,dosisInflu,dosisAntiama,dosisTriva,dosisToxoide;
            String estatura="",peso="",enfermedad = "",cirugia = "",alergia = "",tipoSangre = "",tallaCamisa="",tallaPantalon="",tallaZapato="";
            
            
            if (rbtnPentavalenteDosis1.isSelected()){
                dosisPenta=1;
            }else 
                if (rbtnPentavalenteDosis2.isSelected()){
                    dosisPenta=2;
                }else
                    if (rbtnPentavalenteDosis3.isSelected()){
                        dosisPenta=3;
                    }else 
                        dosisPenta=0;
            
            if (rbtnPolioDosis1.isSelected()){
                    dosisPolio=1;
            }else
                if (rbtnPolioDosis2.isSelected()){
                    dosisPolio=2;
                }else
                    dosisPolio=0;
            if (rbtnAntineumococoDosis1.isSelected()){
                    dosisAntineu=1;
            }else
                if (rbtnAntineumococoDosis2.isSelected()){
                    dosisAntineu=2;
                }else
                    dosisAntineu=0;
            if (rbtnBCGDosis1.isSelected()){
                dosisBCG=1;
            }else
                dosisBCG=0;
            if (rbtnHepatitisDosis1.isSelected()){
                dosisHepa=1;
            }else
                dosisHepa=0;
            if (rbtnInfluenzaDosis1.isSelected()){
                dosisInflu=1;
            }else
                if (rbtnInfluenzaDosis2.isSelected()){
                    dosisInflu=2;
                }else 
                    dosisInflu=0;
            if (rbtnAntiamarilicaDosis1.isSelected()){
                dosisAntiama=1;
            }else
                dosisAntiama=0;
            if (rbtnTrivalenteDosis1.isSelected()){
                dosisTriva=1;
            }else
                dosisTriva=0;
            if (rbtnToxoideDosis1.isSelected()){
                dosisToxoide=1;
            }else
                dosisToxoide=0;
            if (!txtPeso.getText().isEmpty()){
            peso=txtPeso.getText();
            }else
                peso="";
            if (!txtEstatura.getText().isEmpty()){
            estatura=txtEstatura.getText();
            }else
                estatura="";
            if (cmbTallaCamisa.getSelectedIndex()>0){
            tallaCamisa=cmbTallaCamisa.getSelectedItem().toString();
            }else
                tallaCamisa="";
            if (cmbTallaPantalon.getSelectedIndex()>0){
            tallaPantalon=cmbTallaPantalon.getSelectedItem().toString();
            }
            if (cmbTallaCalzado.getSelectedIndex()>0){
            tallaZapato=cmbTallaCalzado.getSelectedItem().toString();
            }
            if (rbtnAlergiaSi.isSelected()){
            alergia=txtIndiqueTratamiento3.getText();
            }else
                if (rbtnAlergiaNo.isSelected()){
                    alergia="";
                }
            if (rbtnCirugiaSi.isSelected()){
            cirugia=txtIndiqueTratamiento.getText();
            }else
                if (rbtnCirugiaNo.isSelected()){
                    cirugia="";
                }
            if (rbtnEnfermedadSi.isSelected()){
            enfermedad=txtIndiqueTratamientoEnfermedad.getText();
            }else
                if (rbtnEnfermedadNo.isSelected()){
                    enfermedad="";
                }
            if (cmbTipoSangre.getSelectedIndex()>0){
                tipoSangre=cmbTipoSangre.getSelectedItem().toString();
            }else
                tipoSangre="";
            
            
             String consulta,consulta2,consulta3,consulta1,consulta5,consulta7,consultaR,consultaM,consultaP,consultaIM,consultaIP;
                //Consultas Insertar alumno
                consulta="INSERT INTO estudiante (ced_estu,primerNombre_estu,segundoNombre_estu,PrimerApellido_estu,segundoApellido_estu,sexo_estu,fn_estu,lugarN_estu,nacionalidad_estu,direccion_estu,tlf_estu,cod_Canaima,seccion_estu,grado_estu,otroTlf_estu,turno,estado_logico,estado,institucion,periodo) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)" ;
                consulta2="INSERT INTO estu_año (ced_estu,seccion,grado,periodo_escolar,veces_cursante,condicion,turno,Profesor) VALUES (?,?,?,?,?,?,?,? )";    
                
                //Consulta actualizar datos
                String consulta6="UPDATE secciongrado SET matricula_inscrita=matricula_inscrita+1 WHERE seccion='"+seccion+"' AND grado='"+grado+"'";
                //Consultas Insertar familiares
                consulta5=("INSERT INTO familiares (ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,direccion_fa,tlf_fa,otroTlf_fa,observacion_fa,estado) VALUES (?,?,?,?,?,?,?,?,?,?)");
        
                consulta1=("INSERT INTO estu_fami (ced_estu,ced_fa,parentesco,Representado) VALUES (?,?,?,?)");
                 consultaR=("INSERT INTO estu_fami (ced_estu,ced_fa,parentesco,Representado) VALUES ('"+cedula+"','"+cedulaRepre+"','"+parentesco+"',1)");
                 consultaM=("INSERT INTO estu_fami (ced_estu,ced_fa,parentesco,Representado) VALUES ('"+cedula+"','"+this.cmbCedulaMadre.getSelectedItem().toString()+"-"+this.txtCedulaMadre.getText()+"','Mamá',0)");
                 consultaP=("INSERT INTO estu_fami (ced_estu,ced_fa,parentesco,Representado) VALUES ('"+cedula+"','"+this.cmbCedulaPadre.getSelectedItem().toString()+"-"+this.txtCedulaPadre.getText()+"','Papá',0)");
                 consultaIM=("INSERT INTO familiares (ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,direccion_fa,tlf_fa,otroTlf_fa,observacion_fa,estado) VALUES ('"+cedulaMadre+"','"+nombreMadre+"','"+apellidoMadre+"','"+fechaNaMadre+"','"+profesionMadre+"','"+direccionMadre+"','"+telfMadre+"','"+otroTelfMadre+"','"+observacionMadre+"',1)");
                 consultaIP=("INSERT INTO familiares (ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,direccion_fa,tlf_fa,otroTlf_fa,observacion_fa,estado) VALUES ('"+cedulaPadre+"','"+nombrePadre+"','"+apellidoPadre+"','"+fechaNaPadre+"','"+profesionPadre+"','"+direccionPadre+"','"+telfPadre+"','"+otroTelfPadre+"','"+observacionPadre+"',1)");
//Datos de salud
           
                consulta7="INSERT INTO salud (ced_estu,tipo_sangre,v_bcg,v_trivalente,cirugia_s,alergia_s,v_penta,v_polio,v_antineumo,v_hepa_b,v_influenza,v_antiama,v_toxoide,enfermedad,peso,estatura,talla_camisa,talla_pantalon,talla_calzado) VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
                try {
                       
                        
                        
                        
                        Statement st=cn.createStatement();
                        st.execute(consulta6);
                        PreparedStatement stm=cn.prepareStatement(consulta);
                        stm.setString(1, cedula);
                        stm.setString(2, primerNombre);
                        stm.setString(3, segundoNombre);
                        stm.setString(4, primerApellido);
                        stm.setString(5, segundoApellido);
                        stm.setString(6, sexo);
                        stm.setString(7, fechaNaci);
                        stm.setString(8, lugarNaci);
                        stm.setString(9, nacionalidad);
                        stm.setString(10, direccion);
                        stm.setString(11, telefono1);
                        stm.setString(12, canaima);
                        stm.setString(13, seccion);
                        stm.setString(14, grado);
                        stm.setString(15, telefono2);
                        stm.setString(16, turno);
                        stm.setInt(17, 1);
                        stm.setString(18, "INSCRITO");
                        stm.setString(19, insti);
                        stm.setString(20, periodo);
                        PreparedStatement rtm=cn.prepareStatement(consulta2);
                        rtm.setString(1, cedula);
                        rtm.setString(2, seccion);
                        rtm.setString(3, grado);
                        rtm.setString(4, txtPeriodoEscolar.getText());
                        rtm.setInt(5,1);
                        rtm.setString(6, "REGULAR");
                        rtm.setString(7, turno);
                        rtm.setString(8, profesor);
                        
                       
                         Statement pt=cn.createStatement();
                       pt.execute(consultaR);
                        
                   
                        
                        PreparedStatement etm=cn.prepareStatement(consulta7);
                    etm.setString(1, cedula);
                    etm.setString(2, tipoSangre);
                    etm.setInt(3, dosisBCG);
                    etm.setInt(4, dosisTriva);
                    etm.setString(5,cirugia);
                    etm.setString(6, alergia);
                    etm.setInt(7, dosisPenta);
                    etm.setInt(8, dosisPolio);
                    etm.setInt(9, dosisAntineu);
                    etm.setInt(10, dosisHepa);
                    etm.setInt(11, dosisInflu);
                    etm.setInt(12, dosisAntiama);
                    etm.setInt(13, dosisToxoide);
                    etm.setString(14, enfermedad);
                    etm.setString(15, peso);
                    etm.setString(16, estatura);
                    etm.setString(17, tallaCamisa);
                    etm.setString(18, tallaPantalon);
                    etm.setString(19, tallaZapato);
               //Inscribir Datos familiares
              PreparedStatement ytm=cn.prepareStatement(consulta5);
                   Statement mt=cn.createStatement();
                         Statement mmt=cn.createStatement();
                    if ((!this.txtCedulaMadre.getText().isEmpty()) && (this.isEditarMadre()==false) && (this.isAuxMadre()==false)){
            
            mmt.execute(consultaIM);
            
            mt.execute(consultaM);
            }else
                   if (isAuxMadre()==true && isEditarMadre()==false){
                     
            mt.execute(consultaM);
                   }
                
                Statement ppt=cn.createStatement();
               
                Statement ipt=cn.createStatement();
            if ((!this.txtCedulaPadre.getText().isEmpty())&& (this.isEditarPadre()==false) && (this.isAuxPadre()==false)){
                
                        
            ipt.execute(consultaIP);
           
            ppt.execute(consultaP);
            }else
                if (this.isAuxPadre()==true && this.isEditarPadre()==false){
                 
            ppt.execute(consultaP);
                }
//Ingresar datos salud
             int n=stm.executeUpdate();
             int l=rtm.executeUpdate();
             int e=etm.executeUpdate();
             
             if (n>0 && l>0 && e>0){
                 Bitacora obj = new Bitacora();
            obj.bitacora("Inscribir Nuevo Ingreso", "Se inscribio el estudiante "+cedula+" "+primerNombre+" "+primerApellido+" En "+grado+seccion);
               dlgDatosCargadoInscribir dialog = new dlgDatosCargadoInscribir(new javax.swing.JFrame(), true);
               dialog.setVisible(true);
               this.dispose();
             }
                    
                    
                    
            
                            
            
                } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                
                
                
    }
    
    void actualizar(){
        //CAPTURA DE DATOS DE DATOS FAMILIARES
         conexion conn=new conexion();
            Connection cn=conn.conectar();
        String cedulaMadre = "",cedulaPadre = "",nombreMadre = "",nombrePadre = "",apellidoMadre = "",apellidoPadre = "",fechaNaMadre = "",fechaNaPadre = "",observacionMadre = "",observacionPadre = "",direccionMadre = "",direccionPadre = "",telfMadre = "",telfPadre = "",otroTelfMadre = "",otroTelfPadre = "",profesionMadre = "",profesionPadre = "";
        String cedula="",periodo="",profesor="",insti="",primerNombre="",segundoNombre="",primerApellido="",segundoApellido="",cedulaRepre="",grado,seccion,turno,sexo,fechaNaci,lugarNaci="",telefono1="",telefono2="",direccion,canaima,parentesco,nacionalidad;
        if ((isAuxMadre()==false) && (!txtCedulaMadre.getText().isEmpty()) && (isEditarMadre()==false)){
        cedulaMadre=cmbCedulaMadre.getSelectedItem().toString()+"-"+txtCedulaMadre.getText();
        nombreMadre=txtNombreMadre.getText();
        apellidoMadre=txtApellidoMadre.getText();
         String ano,mes,dia; 
                    ano=Integer.toString(txtFechaNaMadre.getCalendar().get(Calendar.YEAR));
                     mes=Integer.toString(txtFechaNaMadre.getCalendar().get(Calendar.MONTH)+1);
                     dia=Integer.toString(txtFechaNaMadre.getCalendar().get(Calendar.DAY_OF_MONTH));
                     fechaNaMadre=dia+"/"+mes+"/"+ano;
       profesionMadre=txtProfesionMadre.getText();
       direccionMadre=txtDireccionMadre.getText();
       telfMadre=cmbTelefonoMadre.getSelectedItem().toString()+"-"+txtTelefonoMadre.getText();
       otroTelfMadre=cmbOtroTlfMadre.getSelectedItem().toString()+"-"+txtTelefonoMadre.getText();
       observacionMadre=txtObservacionMadre.getText();
        }
       if ((isAuxPadre()==false) && (!txtCedulaPadre.getText().isEmpty()) && (isEditarPadre()==false)){
       cedulaPadre=cmbCedulaPadre.getSelectedItem().toString()+"-"+txtCedulaPadre.getText();
       nombrePadre=txtNombrePadre.getText();
       apellidoPadre=txtApellidoPadre.getText();
       String ano1,mes1,dia1; 
                    ano1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.YEAR));
                     mes1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.MONTH)+1);
                     dia1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.DAY_OF_MONTH));
                     fechaNaPadre=dia1+"/"+mes1+"/"+ano1;
        profesionPadre=txtProfesionPadre.getText();
        direccionPadre=txtDireccionMadre.getText();
        telfPadre=cmbTelefonoPadre.getSelectedItem().toString()+"-"+txtTelefonoPadre.getText();
        otroTelfPadre=cmbOtroTlfPadre.getSelectedItem().toString()+"-"+txtOtroTlfPadre.getText();
        observacionPadre=txtObservacionPadre.getText();
       }
//CAPTURA DE DATOS DE DATOS PERSONALES
                cedula=txtCedula.getText();
                primerNombre=txtPrimerNombre.getText();
                segundoNombre=txtSegundoNombre.getText();
                primerApellido=txtPrimerApellido.getText();
                segundoApellido=txtSegundoApellido.getText();
                cedulaRepre=txtCedulaRepre.getText();
                grado=txtGrado.getText();
                seccion=txtSeccion.getText();
                turno=txtTurno.getText();
                parentesco=cmbParentesco.getSelectedItem().toString();
                sexo=cmbSexo.getSelectedItem().toString();
                profesor=txtNombreProf.getText();
                periodo=txtPeriodoEscolar.getText();
                if (txtFechaNaci.getDate()==null){
                    fechaNaci="";
                }else{
                    String ano,mes,dia;
                    ano=Integer.toString(txtFechaNaci.getCalendar().get(Calendar.YEAR));
                     mes=Integer.toString(txtFechaNaci.getCalendar().get(Calendar.MONTH)+1);
                     dia=Integer.toString(txtFechaNaci.getCalendar().get(Calendar.DAY_OF_MONTH));
                     fechaNaci=dia+"/"+mes+"/"+ano;
                        }
                lugarNaci=txtLugarNacimiento.getText();
                insti=txtInstitucion.getText();
                telefono1=cmbTelefono.getSelectedItem().toString()+"-"+txtTelefono.getText();
                if (txtOtroTelefono.getText().isEmpty()){
                    telefono2="";
                }else{
                telefono2=cmbOtroTelefono.getSelectedItem().toString()+"-"+txtOtroTelefono.getText();
                }
                direccion=txtDireccion.getText();
                canaima=txtCodigoCanaima.getText();
                String [] temp;
                temp=cedula.split("-");
                if(temp[0].equals("V")){
                    nacionalidad="VENEZOLANO";
                   
                }
                else{
                    nacionalidad="EXTRANJERO";
                } 
                //CAPTURA DE DATOS SALUD
                int dosisPenta,dosisPolio,dosisAntineu,dosisBCG,dosisHepa,dosisInflu,dosisAntiama,dosisTriva,dosisToxoide;
            String estatura="",peso="",enfermedad = "",cirugia = "",alergia = "",tipoSangre = "",tallaCamisa="",tallaPantalon="",tallaZapato="";
            
            
            if (rbtnPentavalenteDosis1.isSelected()){
                dosisPenta=1;
            }else 
                if (rbtnPentavalenteDosis2.isSelected()){
                    dosisPenta=2;
                }else
                    if (rbtnPentavalenteDosis3.isSelected()){
                        dosisPenta=3;
                    }else 
                        dosisPenta=0;
            
            if (rbtnPolioDosis1.isSelected()){
                    dosisPolio=1;
            }else
                if (rbtnPolioDosis2.isSelected()){
                    dosisPolio=2;
                }else
                    dosisPolio=0;
            if (rbtnAntineumococoDosis1.isSelected()){
                    dosisAntineu=1;
            }else
                if (rbtnAntineumococoDosis2.isSelected()){
                    dosisAntineu=2;
                }else
                    dosisAntineu=0;
            if (rbtnBCGDosis1.isSelected()){
                dosisBCG=1;
            }else
                dosisBCG=0;
            if (rbtnHepatitisDosis1.isSelected()){
                dosisHepa=1;
            }else
                dosisHepa=0;
            if (rbtnInfluenzaDosis1.isSelected()){
                dosisInflu=1;
            }else
                if (rbtnInfluenzaDosis2.isSelected()){
                    dosisInflu=2;
                }else 
                    dosisInflu=0;
            if (rbtnAntiamarilicaDosis1.isSelected()){
                dosisAntiama=1;
            }else
                dosisAntiama=0;
            if (rbtnTrivalenteDosis1.isSelected()){
                dosisTriva=1;
            }else
                dosisTriva=0;
            if (rbtnToxoideDosis1.isSelected()){
                dosisToxoide=1;
            }else
                dosisToxoide=0;
            if (!txtPeso.getText().isEmpty()){
            peso=txtPeso.getText();
            }else
                peso="";
            if (!txtEstatura.getText().isEmpty()){
            estatura=txtEstatura.getText();
            }else
                estatura="";
            if (cmbTallaCamisa.getSelectedIndex()>0){
            tallaCamisa=cmbTallaCamisa.getSelectedItem().toString();
            }else
                tallaCamisa="";
            if (cmbTallaPantalon.getSelectedIndex()>0){
            tallaPantalon=cmbTallaPantalon.getSelectedItem().toString();
            }
            if (cmbTallaCalzado.getSelectedIndex()>0){
            tallaZapato=cmbTallaCalzado.getSelectedItem().toString();
            }
            if (rbtnAlergiaSi.isSelected()){
            alergia=txtIndiqueTratamiento3.getText();
            }else
                if (rbtnAlergiaNo.isSelected()){
                    alergia="";
                }
            if (rbtnCirugiaSi.isSelected()){
            cirugia=txtIndiqueTratamiento.getText();
            }else
                if (rbtnCirugiaNo.isSelected()){
                    cirugia="";
                }
            if (rbtnEnfermedadSi.isSelected()){
            enfermedad=txtIndiqueTratamientoEnfermedad.getText();
            }else
                if (rbtnEnfermedadNo.isSelected()){
                    enfermedad="";
                }
            if (cmbTipoSangre.getSelectedIndex()>0){
                tipoSangre=cmbTipoSangre.getSelectedItem().toString();
            }else
                tipoSangre="";
            
            
             String consulta,consulta2,consulta3,consulta1,consulta5,consulta7,consultaR,consultaM,consultaP,consultaIM,consultaIP;
                //Consultas Insertar alumno
                consulta="UPDATE estudiante SET primerNombre_estu=?,segundoNombre_estu=?,PrimerApellido_estu=?,segundoApellido_estu=?,sexo_estu=?,fn_estu=?,lugarN_estu=?,nacionalidad_estu=?,direccion_estu=?,tlf_estu=?,cod_Canaima=?,seccion_estu=?,grado_estu=?,otroTlf_estu=?,turno=?,estado_logico=?,estado=?,institucion=?,periodo=? WHERE ced_estu='"+cedula+"'" ;
                consulta2="INSERT INTO estu_año (ced_estu,seccion,grado,periodo_escolar,veces_cursante,condicion,turno,Profesor) VALUES (?,?,?,?,?,?,?,? )";    
                
                //Consulta actualizar datos
                String consulta6="UPDATE secciongrado SET matricula_inscrita=matricula_inscrita+1 WHERE seccion='"+seccion+"' AND grado='"+grado+"'";
                //Consultas Insertar familiares
                consulta5=("INSERT INTO familiares (ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,direccion_fa,tlf_fa,otroTlf_fa,observacion_fa,estado) VALUES (?,?,?,?,?,?,?,?,?,?)");
        
                consulta1=("INSERT INTO estu_fami (ced_estu,ced_fa,parentesco,Representado) VALUES (?,?,?,?)");
                 consultaR=("INSERT INTO estu_fami (ced_estu,ced_fa,parentesco,Representado) VALUES ('"+cedula+"','"+cedulaRepre+"','"+parentesco+"',1)");
                 consultaM=("INSERT INTO estu_fami (ced_estu,ced_fa,parentesco,Representado) VALUES ('"+cedula+"','"+this.cmbCedulaMadre.getSelectedItem().toString()+"-"+this.txtCedulaMadre.getText()+"','Mamá',0)");
                 consultaP=("INSERT INTO estu_fami (ced_estu,ced_fa,parentesco,Representado) VALUES ('"+cedula+"','"+this.cmbCedulaPadre.getSelectedItem().toString()+"-"+this.txtCedulaPadre.getText()+"','Papá',0)");
                 consultaIM=("INSERT INTO familiares (ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,direccion_fa,tlf_fa,otroTlf_fa,observacion_fa,estado) VALUES ('"+cedulaMadre+"','"+nombreMadre+"','"+apellidoMadre+"','"+fechaNaMadre+"','"+profesionMadre+"','"+direccionMadre+"','"+telfMadre+"','"+otroTelfMadre+"','"+observacionMadre+"',1)");
                 consultaIP=("INSERT INTO familiares (ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,direccion_fa,tlf_fa,otroTlf_fa,observacion_fa,estado) VALUES ('"+cedulaPadre+"','"+nombrePadre+"','"+apellidoPadre+"','"+fechaNaPadre+"','"+profesionPadre+"','"+direccionPadre+"','"+telfPadre+"','"+otroTelfPadre+"','"+observacionPadre+"',1)");
//Datos de salud
           
                consulta7="UPDATE salud SET tipo_sangre=?,v_bcg=?,v_trivalente=?,cirugia_s=?,alergia_s=?,v_penta=?,v_polio=?,v_antineumo=?,v_hepa_b=?,v_influenza=?,v_antiama=?,v_toxoide=?,enfermedad=?,peso=?,estatura=?,talla_camisa=?,talla_pantalon=?,talla_calzado=? WHERE ced_estu='"+cedula+"'";
                try {
                       
                        
                        
                        
                        Statement st=cn.createStatement();
                        st.execute(consulta6);
                        PreparedStatement stm=cn.prepareStatement(consulta);
                        
                        stm.setString(1, primerNombre);
                        stm.setString(2, segundoNombre);
                        stm.setString(3, primerApellido);
                        stm.setString(4, segundoApellido);
                        stm.setString(5, sexo);
                        stm.setString(6, fechaNaci);
                        stm.setString(7, lugarNaci);
                        stm.setString(8, nacionalidad);
                        stm.setString(9, direccion);
                        stm.setString(10, telefono1);
                        stm.setString(11, canaima);
                        stm.setString(12, seccion);
                        stm.setString(13, grado);
                        stm.setString(14, telefono2);
                        stm.setString(15, turno);
                        stm.setInt(16, 1);
                        stm.setString(17, "INSCRITO");
                        stm.setString(18, insti);
                        stm.setString(19, periodo);
                        PreparedStatement rtm=cn.prepareStatement(consulta2);
                        rtm.setString(1, cedula);
                        rtm.setString(2, seccion);
                        rtm.setString(3, grado);
                        rtm.setString(4, txtPeriodoEscolar.getText());
                        rtm.setInt(5,1);
                        rtm.setString(6, "REGULAR");
                        rtm.setString(7, turno);
                        rtm.setString(8, profesor);
                        
                       
                         Statement pt=cn.createStatement();
                       pt.execute(consultaR);
                        
                   
                        
                        PreparedStatement etm=cn.prepareStatement(consulta7);
                    
                    etm.setString(1, tipoSangre);
                    etm.setInt(2, dosisBCG);
                    etm.setInt(3, dosisTriva);
                    etm.setString(4,cirugia);
                    etm.setString(5, alergia);
                    etm.setInt(6, dosisPenta);
                    etm.setInt(7, dosisPolio);
                    etm.setInt(8, dosisAntineu);
                    etm.setInt(9, dosisHepa);
                    etm.setInt(10, dosisInflu);
                    etm.setInt(11, dosisAntiama);
                    etm.setInt(12, dosisToxoide);
                    etm.setString(13, enfermedad);
                    etm.setString(14, peso);
                    etm.setString(15, estatura);
                    etm.setString(16, tallaCamisa);
                    etm.setString(17, tallaPantalon);
                    etm.setString(18, tallaZapato);
               //Inscribir Datos familiares
              PreparedStatement ytm=cn.prepareStatement(consulta5);
                   Statement mt=cn.createStatement();
                         Statement mmt=cn.createStatement();
                    if ((!this.txtCedulaMadre.getText().isEmpty()) && (this.isEditarMadre()==false) && (this.isAuxMadre()==false)){
            
            mmt.execute(consultaIM);
            
            mt.execute(consultaM);
            }else
                   if (isAuxMadre()==true && isEditarMadre()==false){
                     
            mt.execute(consultaM);
                   }
                
                Statement ppt=cn.createStatement();
               
                Statement ipt=cn.createStatement();
            if ((!this.txtCedulaPadre.getText().isEmpty())&& (this.isEditarPadre()==false) && (this.isAuxPadre()==false)){
                
                        
            ipt.execute(consultaIP);
           
            ppt.execute(consultaP);
            }else
                if (this.isAuxPadre()==true && this.isEditarPadre()==false){
                 
            ppt.execute(consultaP);
                }
//Ingresar datos salud
             int n=stm.executeUpdate();
             int l=rtm.executeUpdate();
             int e=etm.executeUpdate();
             
             if (n>0 && l>0 && e>0){
                 Bitacora obj = new Bitacora();
            obj.bitacora("Inscribir Nuevo Ingreso", "Se inscribio el estudiante "+cedula+" "+primerNombre+" "+primerApellido+" En "+grado+seccion);
               dlgDatosCargadoInscribir dialog = new dlgDatosCargadoInscribir(new javax.swing.JFrame(), true);
               dialog.setVisible(true);
               this.dispose();
             }
                    
                    
                    
            
                            
            
                } catch (Exception e) {
                        JOptionPane.showMessageDialog(null, e);
                    }
                
                
                
                
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grtbAlergias = new javax.swing.ButtonGroup();
        grtbnCirugia = new javax.swing.ButtonGroup();
        grbtnEnfermedad = new javax.swing.ButtonGroup();
        grbtnPenta = new javax.swing.ButtonGroup();
        grbtnPolio = new javax.swing.ButtonGroup();
        grbtnAntineumo = new javax.swing.ButtonGroup();
        grbtnInfluenza = new javax.swing.ButtonGroup();
        jPanel1 = new imagenes.Fondo("fondo color.jpg");
        inscripcion = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        lblCedula = new javax.swing.JLabel();
        txtCedula = new javax.swing.JTextField();
        lblSegundoNombre = new javax.swing.JLabel();
        txtPrimerNombre = new javax.swing.JTextField();
        txtSegundoNombre = new javax.swing.JTextField();
        lblPrimerApellido = new javax.swing.JLabel();
        txtPrimerApellido = new javax.swing.JTextField();
        lblSegundoApellido = new javax.swing.JLabel();
        txtSegundoApellido = new javax.swing.JTextField();
        lblSexo = new javax.swing.JLabel();
        cmbSexo = new javax.swing.JComboBox();
        lblFechaNac = new javax.swing.JLabel();
        txtFechaNaci = new com.toedter.calendar.JDateChooser();
        lblLugarNacimiento = new javax.swing.JLabel();
        lblCedulaRepre = new javax.swing.JLabel();
        txtCedulaRepre = new javax.swing.JTextField();
        txtLugarNacimiento = new javax.swing.JTextField();
        lblDireccion = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtDireccion = new javax.swing.JTextArea();
        lblTelefono = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JTextField();
        cmbTelefono = new javax.swing.JComboBox();
        lblOtroTelefono = new javax.swing.JLabel();
        cmbOtroTelefono = new javax.swing.JComboBox();
        txtOtroTelefono = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        btnSiguiente = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtGrado = new javax.swing.JTextField();
        txtSeccion = new javax.swing.JTextField();
        txtTurno = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        cmbParentesco = new javax.swing.JComboBox();
        lblNoExistePrimerNombre = new javax.swing.JLabel();
        lblNoExisteParentesco = new javax.swing.JLabel();
        lblNoExisteFechaNaci = new javax.swing.JLabel();
        lblNoExistePrimerApellido = new javax.swing.JLabel();
        lblNoExisteSexo = new javax.swing.JLabel();
        lblNoExisteLugarNaci = new javax.swing.JLabel();
        lblNoExisteTelefono = new javax.swing.JLabel();
        lblNoExisteDireccion = new javax.swing.JLabel();
        txtInstitucion = new javax.swing.JLabel();
        txtCodigoCanaima = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        btnMenuPrincipal = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        lblPrimerNombre = new javax.swing.JLabel();
        lblNoExisteCodigoCanaima = new javax.swing.JLabel();
        txtNombreProf = new javax.swing.JLabel();
        txtPeriodoEscolar = new javax.swing.JLabel();
        jPanel3 = new imagenes.Fondo("fondo color.jpg");
        jScrollPane1 = new javax.swing.JScrollPane();
        txtIndiqueTratamiento = new javax.swing.JTextArea();
        lblTallaPantalon = new javax.swing.JLabel();
        lblAntiamarilica1 = new javax.swing.JLabel();
        rbtnAlergiaSi = new javax.swing.JRadioButton();
        cmbTallaCamisa = new javax.swing.JComboBox();
        rbtnPentavalenteDosis3 = new javax.swing.JRadioButton();
        rbtnAntiamarilicaDosis1 = new javax.swing.JRadioButton();
        lblPolio = new javax.swing.JLabel();
        rbtnAlergiaNo = new javax.swing.JRadioButton();
        jLabel10 = new javax.swing.JLabel();
        lblToxoide1 = new javax.swing.JLabel();
        lblPeso = new javax.swing.JLabel();
        lblIndiqueTratamiento1 = new javax.swing.JLabel();
        rbtnHepatitisDosis1 = new javax.swing.JRadioButton();
        lblIndiqueTratamiento3 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        txtEstatura = new javax.swing.JTextField();
        lblAntineumococo = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtIndiqueTratamiento3 = new javax.swing.JTextArea();
        jSeparator7 = new javax.swing.JSeparator();
        lblPentavalente = new javax.swing.JLabel();
        rbtnCirugiaSi = new javax.swing.JRadioButton();
        lblTallaCalzado = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        rbtnCirugiaNo = new javax.swing.JRadioButton();
        rbtnBCGDosis1 = new javax.swing.JRadioButton();
        lblTallaCamisa = new javax.swing.JLabel();
        rbtnPentavalenteDosis1 = new javax.swing.JRadioButton();
        lblHepatitis1 = new javax.swing.JLabel();
        cmbTallaPantalon = new javax.swing.JComboBox();
        rbtnAntineumococoDosis1 = new javax.swing.JRadioButton();
        cmbTallaCalzado = new javax.swing.JComboBox();
        jSeparator8 = new javax.swing.JSeparator();
        lblSufreCirugia = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        rbtnInfluenzaDosis1 = new javax.swing.JRadioButton();
        lblTrivalente = new javax.swing.JLabel();
        lblBCG1 = new javax.swing.JLabel();
        lblEstatura = new javax.swing.JLabel();
        rbtnInfluenzaDosis2 = new javax.swing.JRadioButton();
        lblInfluenza1 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        txtPeso = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        rbtnToxoideDosis1 = new javax.swing.JRadioButton();
        rbtnPentavalenteDosis2 = new javax.swing.JRadioButton();
        rbtnPolioDosis2 = new javax.swing.JRadioButton();
        jLabel15 = new javax.swing.JLabel();
        txtCedula1 = new javax.swing.JTextField();
        rbtnPolioDosis1 = new javax.swing.JRadioButton();
        cmbTipoSangre = new javax.swing.JComboBox();
        jLabel16 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        lblTipoSangre1 = new javax.swing.JLabel();
        rbtnAntineumococoDosis2 = new javax.swing.JRadioButton();
        jSeparator9 = new javax.swing.JSeparator();
        lblAlergia = new javax.swing.JLabel();
        inscripcion1 = new javax.swing.JLabel();
        rbtnTrivalenteDosis1 = new javax.swing.JRadioButton();
        jLabel18 = new javax.swing.JLabel();
        lblCedulaEstu1 = new javax.swing.JLabel();
        lblSufreEfermedad = new javax.swing.JLabel();
        btnSiguiente1 = new javax.swing.JButton();
        rbtnEnfermedadSi = new javax.swing.JRadioButton();
        rbtnEnfermedadNo = new javax.swing.JRadioButton();
        lblIndiqueTratamientoEnfermedad = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtIndiqueTratamientoEnfermedad = new javax.swing.JTextArea();
        jPanel4 = new javax.swing.JPanel();
        btnCancelar1 = new javax.swing.JButton();
        jSeparator10 = new javax.swing.JSeparator();
        jSeparator11 = new javax.swing.JSeparator();
        jSeparator18 = new javax.swing.JSeparator();
        jButton2 = new javax.swing.JButton();
        jSeparator19 = new javax.swing.JSeparator();
        jLabel19 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jPanel5 = new imagenes.Fondo("fondo color.jpg");
        jLabel20 = new javax.swing.JLabel();
        jSeparator12 = new javax.swing.JSeparator();
        jLabel21 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jSeparator13 = new javax.swing.JSeparator();
        jLabel24 = new javax.swing.JLabel();
        lblCedulaMadre = new javax.swing.JLabel();
        txtCedulaMadre = new javax.swing.JTextField();
        btnCedulaMadre = new javax.swing.JButton();
        lblNombreMadre = new javax.swing.JLabel();
        lblApellidoMadre = new javax.swing.JLabel();
        txtNombreMadre = new javax.swing.JTextField();
        txtApellidoMadre = new javax.swing.JTextField();
        lblFechaNaciMadre = new javax.swing.JLabel();
        lblProfesionMadre = new javax.swing.JLabel();
        txtProfesionMadre = new javax.swing.JTextField();
        lblTelefonoMadre = new javax.swing.JLabel();
        cmbTelefonoMadre = new javax.swing.JComboBox();
        txtTelefonoMadre = new javax.swing.JTextField();
        cmbCedulaMadre = new javax.swing.JComboBox();
        lblDireccionMadre = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDireccionMadre = new javax.swing.JTextArea();
        lblOtroTlfMadre = new javax.swing.JLabel();
        cmbOtroTlfMadre = new javax.swing.JComboBox();
        txtOtroTlfMadre = new javax.swing.JTextField();
        jSeparator14 = new javax.swing.JSeparator();
        jLabel25 = new javax.swing.JLabel();
        lblCedulaPadre = new javax.swing.JLabel();
        cmbCedulaPadre = new javax.swing.JComboBox();
        txtCedulaPadre = new javax.swing.JTextField();
        btnCedulaPadre = new javax.swing.JButton();
        lblNombrePadre = new javax.swing.JLabel();
        txtNombrePadre = new javax.swing.JTextField();
        lblApellidoPadre = new javax.swing.JLabel();
        txtApellidoPadre = new javax.swing.JTextField();
        lblFechaNaciPadre = new javax.swing.JLabel();
        lblProfesionPadre = new javax.swing.JLabel();
        txtProfesionPadre = new javax.swing.JTextField();
        lblDireccionPadre = new javax.swing.JLabel();
        jScrollPane6 = new javax.swing.JScrollPane();
        txtDireccionPadre = new javax.swing.JTextArea();
        lblTelefonoPAdre = new javax.swing.JLabel();
        cmbTelefonoPadre = new javax.swing.JComboBox();
        txtTelefonoPadre = new javax.swing.JTextField();
        lblOtroTlfPadre = new javax.swing.JLabel();
        cmbOtroTlfPadre = new javax.swing.JComboBox();
        txtOtroTlfPadre = new javax.swing.JTextField();
        jLabel26 = new javax.swing.JLabel();
        txtFechaNaMadre = new com.toedter.calendar.JDateChooser();
        txtFechaNaciPadre = new com.toedter.calendar.JDateChooser();
        jPanel6 = new javax.swing.JPanel();
        btnCancelar2 = new javax.swing.JButton();
        jSeparator15 = new javax.swing.JSeparator();
        jSeparator16 = new javax.swing.JSeparator();
        btnMenuFamiliares = new javax.swing.JButton();
        jSeparator20 = new javax.swing.JSeparator();
        jSeparator21 = new javax.swing.JSeparator();
        jLabel27 = new javax.swing.JLabel();
        jScrollPane7 = new javax.swing.JScrollPane();
        txtObservacionMadre = new javax.swing.JTextArea();
        jLabel28 = new javax.swing.JLabel();
        jScrollPane8 = new javax.swing.JScrollPane();
        txtObservacionPadre = new javax.swing.JTextArea();
        btnRegistrar = new javax.swing.JButton();
        btnAtrasFamiliares = new javax.swing.JButton();
        btnGuardarActualizar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        inscripcion.setFont(new java.awt.Font("Comfortaa", 1, 60)); // NOI18N
        inscripcion.setText("Inscripción");

        jLabel1.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel1.setText("Nuevo");

        jLabel3.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel3.setText("INGRESO");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar estudiante.png"))); // NOI18N

        jLabel5.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel5.setText("Datos Personales");

        lblCedula.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedula.setText("Cédula");

        txtCedula.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        txtCedula.setToolTipText("");
        txtCedula.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCedulaFocusLost(evt);
            }
        });
        txtCedula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedulaActionPerformed(evt);
            }
        });
        txtCedula.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaKeyTyped(evt);
            }
        });

        lblSegundoNombre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSegundoNombre.setText("Segundo Nombre");

        txtPrimerNombre.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        txtPrimerNombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrimerNombreActionPerformed(evt);
            }
        });
        txtPrimerNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrimerNombreKeyTyped(evt);
            }
        });

        txtSegundoNombre.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        txtSegundoNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSegundoNombreKeyTyped(evt);
            }
        });

        lblPrimerApellido.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblPrimerApellido.setText("Primer Apellido");

        txtPrimerApellido.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        txtPrimerApellido.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPrimerApellidoActionPerformed(evt);
            }
        });
        txtPrimerApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPrimerApellidoKeyTyped(evt);
            }
        });

        lblSegundoApellido.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSegundoApellido.setText("Segundo Apellido");

        txtSegundoApellido.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        txtSegundoApellido.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtSegundoApellidoKeyTyped(evt);
            }
        });

        lblSexo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSexo.setText("Sexo");

        cmbSexo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbSexo.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "Masculino", "Femenino" }));
        cmbSexo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSexoActionPerformed(evt);
            }
        });

        lblFechaNac.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblFechaNac.setText("Fecha de Nacimiento");

        txtFechaNaci.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        lblLugarNacimiento.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblLugarNacimiento.setText("Lugar de Nacimiento");

        lblCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaRepre.setText("Cédula del Representante");

        txtCedulaRepre.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N

        txtLugarNacimiento.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        txtLugarNacimiento.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtLugarNacimientoKeyTyped(evt);
            }
        });

        lblDireccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblDireccion.setText("Dirección de Habitación");

        txtDireccion.setColumns(20);
        txtDireccion.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtDireccion.setRows(5);
        txtDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtDireccionKeyTyped(evt);
            }
        });
        jScrollPane3.setViewportView(txtDireccion);

        lblTelefono.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTelefono.setText("Teléfono");

        txtTelefono.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTelefonoActionPerformed(evt);
            }
        });
        txtTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoKeyTyped(evt);
            }
        });

        cmbTelefono.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTelefono.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTelefonoActionPerformed(evt);
            }
        });

        lblOtroTelefono.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblOtroTelefono.setText("Otro Teléfono");

        cmbOtroTelefono.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbOtroTelefono.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbOtroTelefono.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbOtroTelefonoActionPerformed(evt);
            }
        });

        txtOtroTelefono.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtOtroTelefono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOtroTelefonoKeyTyped(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel2.setText("Código Canaima");

        btnSiguiente.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/siguiente icono.png"))); // NOI18N
        btnSiguiente.setText("Siguiente");
        btnSiguiente.setBorder(null);
        btnSiguiente.setContentAreaFilled(false);
        btnSiguiente.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/siguiiente.png"))); // NOI18N
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel7.setText("Sección");

        jLabel8.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel8.setText("Grado");

        jLabel6.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel6.setText("Turno");

        txtGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtGrado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGradoActionPerformed(evt);
            }
        });

        txtSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtSeccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtSeccionActionPerformed(evt);
            }
        });

        txtTurno.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtTurno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTurnoActionPerformed(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel9.setText("Parentesco");

        cmbParentesco.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbParentesco.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec Parentesco", "Mamá", "Papá", "Abuelo", "Abuela", "Tio", "Tia", "Primo", "Prima", "Hermano", "Hermana", "Padrino", "Madrina", "Otro" }));
        cmbParentesco.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbParentescoItemStateChanged(evt);
            }
        });
        cmbParentesco.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbParentescoActionPerformed(evt);
            }
        });

        lblNoExistePrimerNombre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteParentesco.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteFechaNaci.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExistePrimerApellido.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteSexo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteLugarNaci.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteTelefono.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        lblNoExisteDireccion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        txtCodigoCanaima.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCodigoCanaima.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCodigoCanaimaActionPerformed(evt);
            }
        });
        txtCodigoCanaima.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtCodigoCanaimaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCodigoCanaimaKeyTyped(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnCancelar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar (2).png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(null);
        btnCancelar.setContentAreaFilled(false);
        btnCancelar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnMenuPrincipal.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnMenuPrincipal.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnMenuPrincipal.setText("Menú Principal");
        btnMenuPrincipal.setBorder(null);
        btnMenuPrincipal.setContentAreaFilled(false);
        btnMenuPrincipal.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/home.png"))); // NOI18N
        btnMenuPrincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuPrincipalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator2)
            .addComponent(jSeparator3)
            .addComponent(jSeparator4)
            .addComponent(jSeparator5)
            .addComponent(btnCancelar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(btnMenuPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 210, Short.MAX_VALUE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btnMenuPrincipal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(46, 46, 46))
        );

        lblPrimerNombre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblPrimerNombre.setText("Primer Nombre");

        lblNoExisteCodigoCanaima.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/no existe.png"))); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED, 192, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(inscripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(232, 232, 232)
                                .addComponent(jLabel1)
                                .addGap(6, 6, 6)
                                .addComponent(jLabel3))
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(86, 86, 86)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 741, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblCedula)
                        .addGap(10, 10, 10)
                        .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 146, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(142, 142, 142)
                        .addComponent(jLabel8)
                        .addGap(4, 4, 4)
                        .addComponent(txtGrado, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel7)
                        .addGap(10, 10, 10)
                        .addComponent(txtSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(jLabel6)
                        .addGap(4, 4, 4)
                        .addComponent(txtTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblPrimerApellido)
                        .addGap(10, 10, 10)
                        .addComponent(txtPrimerApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(lblNoExistePrimerApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29)
                        .addComponent(lblSegundoApellido)
                        .addGap(10, 10, 10)
                        .addComponent(txtSegundoApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblSexo)
                        .addGap(4, 4, 4)
                        .addComponent(cmbSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(lblNoExisteSexo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(157, 157, 157)
                        .addComponent(lblFechaNac)
                        .addGap(18, 18, 18)
                        .addComponent(txtFechaNaci, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(lblNoExisteFechaNaci, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblLugarNacimiento)
                        .addGap(24, 24, 24)
                        .addComponent(txtLugarNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, 280, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(lblNoExisteLugarNaci, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblDireccion)
                        .addGap(10, 10, 10)
                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 410, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(lblNoExisteDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblCedulaRepre)
                                .addGap(4, 4, 4)
                                .addComponent(txtCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel9)
                                .addGap(10, 10, 10)
                                .addComponent(cmbParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(lblNoExisteParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblPrimerNombre)
                                .addGap(10, 10, 10)
                                .addComponent(txtPrimerNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(4, 4, 4)
                                .addComponent(lblNoExistePrimerNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(29, 29, 29)
                                .addComponent(lblSegundoNombre)
                                .addGap(10, 10, 10)
                                .addComponent(txtSegundoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 175, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addComponent(lblTelefono)
                            .addGap(16, 16, 16)
                            .addComponent(cmbTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(6, 6, 6)
                            .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(6, 6, 6)
                            .addComponent(lblNoExisteTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(43, 43, 43)
                            .addComponent(lblOtroTelefono)
                            .addGap(4, 4, 4)
                            .addComponent(cmbOtroTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(10, 10, 10)
                            .addComponent(txtOtroTelefono))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(txtInstitucion, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(4, 4, 4)
                                    .addComponent(txtCodigoCanaima, javax.swing.GroupLayout.PREFERRED_SIZE, 226, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(lblNoExisteCodigoCanaima))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                    .addGap(33, 33, 33)
                                    .addComponent(txtNombreProf, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(51, 51, 51)
                                    .addComponent(txtPeriodoEscolar)))
                            .addGap(210, 210, 210)
                            .addComponent(btnSiguiente))))
                .addContainerGap(214, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(28, 28, 28)
                        .addComponent(inscripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addComponent(jLabel3))
                        .addGap(1, 1, 1)
                        .addComponent(jLabel5))
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 158, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCedula)
                    .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtGrado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTurno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addComponent(jLabel7)
                            .addComponent(jLabel6))))
                .addGap(20, 20, 20)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteParentesco, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblCedulaRepre)
                            .addComponent(txtCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPrimerNombre)
                    .addComponent(txtPrimerNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExistePrimerNombre, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSegundoNombre)
                    .addComponent(txtSegundoNombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblPrimerApellido)
                    .addComponent(txtPrimerApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExistePrimerApellido, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblSegundoApellido)
                    .addComponent(txtSegundoApellido, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblSexo))
                    .addComponent(cmbSexo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteSexo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblFechaNac)
                    .addComponent(txtFechaNaci, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteFechaNaci, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblLugarNacimiento)
                    .addComponent(txtLugarNacimiento, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteLugarNaci, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(22, 22, 22)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbOtroTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtOtroTelefono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTelefono)
                            .addComponent(lblOtroTelefono))))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblDireccion)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblNoExisteDireccion, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(jLabel2)
                        .addGap(22, 22, 22)
                        .addComponent(txtInstitucion, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(txtCodigoCanaima, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtNombreProf, javax.swing.GroupLayout.PREFERRED_SIZE, 12, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtPeriodoEscolar)))
                    .addComponent(lblNoExisteCodigoCanaima, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(btnSiguiente, javax.swing.GroupLayout.PREFERRED_SIZE, 68, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(0, 0, Short.MAX_VALUE))
        );

        txtIndiqueTratamiento.setColumns(20);
        txtIndiqueTratamiento.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtIndiqueTratamiento.setRows(5);
        jScrollPane1.setViewportView(txtIndiqueTratamiento);

        lblTallaPantalon.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTallaPantalon.setText(" Pantalón");

        lblAntiamarilica1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblAntiamarilica1.setText("Antiamarílica");

        grtbAlergias.add(rbtnAlergiaSi);
        rbtnAlergiaSi.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAlergiaSi.setText("Si");
        rbtnAlergiaSi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnAlergiaSiActionPerformed(evt);
            }
        });

        cmbTallaCamisa.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTallaCamisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "2", "4", "6", "8", "10", "12", "14", "16", "18", "S", "M", "L", "XL" }));

        grbtnPenta.add(rbtnPentavalenteDosis3);
        rbtnPentavalenteDosis3.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPentavalenteDosis3.setText("3");

        rbtnAntiamarilicaDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAntiamarilicaDosis1.setText("1");

        lblPolio.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblPolio.setText("Polio");

        grtbAlergias.add(rbtnAlergiaNo);
        rbtnAlergiaNo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAlergiaNo.setText("No");
        rbtnAlergiaNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnAlergiaNoActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel10.setText("Nuevo");

        lblToxoide1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblToxoide1.setText("Toxoide");

        lblPeso.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblPeso.setText("Peso");

        lblIndiqueTratamiento1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblIndiqueTratamiento1.setText("Indique Cúal y Tratamiento");

        rbtnHepatitisDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnHepatitisDosis1.setText("1");

        lblIndiqueTratamiento3.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblIndiqueTratamiento3.setText("Indique Cúal y Tratamiento");

        txtEstatura.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtEstatura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtEstaturaActionPerformed(evt);
            }
        });
        txtEstatura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEstaturaKeyTyped(evt);
            }
        });

        lblAntineumococo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblAntineumococo.setText("Antineumococo");

        txtIndiqueTratamiento3.setColumns(20);
        txtIndiqueTratamiento3.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtIndiqueTratamiento3.setRows(5);
        jScrollPane5.setViewportView(txtIndiqueTratamiento3);

        lblPentavalente.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblPentavalente.setText("Pentavalente");

        grtbnCirugia.add(rbtnCirugiaSi);
        rbtnCirugiaSi.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnCirugiaSi.setText("Si");
        rbtnCirugiaSi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnCirugiaSiActionPerformed(evt);
            }
        });

        lblTallaCalzado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTallaCalzado.setText("Calzado");

        jLabel11.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel11.setText("INGRESO");

        grtbnCirugia.add(rbtnCirugiaNo);
        rbtnCirugiaNo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnCirugiaNo.setText("No");
        rbtnCirugiaNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnCirugiaNoActionPerformed(evt);
            }
        });

        rbtnBCGDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnBCGDosis1.setText("1");

        lblTallaCamisa.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTallaCamisa.setText("Camisa");

        grbtnPenta.add(rbtnPentavalenteDosis1);
        rbtnPentavalenteDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPentavalenteDosis1.setText("1");

        lblHepatitis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblHepatitis1.setText("Hepatitis B");

        cmbTallaPantalon.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTallaPantalon.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "4", "6", "8", "10", "12", "14", "16", "18", "26", "28", "30", "32", "34" }));

        grbtnAntineumo.add(rbtnAntineumococoDosis1);
        rbtnAntineumococoDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAntineumococoDosis1.setText("1");

        cmbTallaCalzado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTallaCalzado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44" }));

        lblSufreCirugia.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSufreCirugia.setText("¿Ha sufrido de alguna operación o cirugia?");

        jLabel12.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel12.setText("Datos sobre la Salud del Estudiante");

        grbtnInfluenza.add(rbtnInfluenzaDosis1);
        rbtnInfluenzaDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnInfluenzaDosis1.setText("1");

        lblTrivalente.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTrivalente.setText("Trivalente");

        lblBCG1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblBCG1.setText("BCG");

        lblEstatura.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblEstatura.setText("Estatura");

        grbtnInfluenza.add(rbtnInfluenzaDosis2);
        rbtnInfluenzaDosis2.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnInfluenzaDosis2.setText("2");
        rbtnInfluenzaDosis2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnInfluenzaDosis2ActionPerformed(evt);
            }
        });

        lblInfluenza1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblInfluenza1.setText("Influenza");

        jLabel13.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel13.setText("Otros Datos");

        txtPeso.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtPeso.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtPesoActionPerformed(evt);
            }
        });
        txtPeso.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPesoKeyTyped(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel14.setText("Metros");

        rbtnToxoideDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnToxoideDosis1.setText("1");

        grbtnPenta.add(rbtnPentavalenteDosis2);
        rbtnPentavalenteDosis2.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPentavalenteDosis2.setText("2");
        rbtnPentavalenteDosis2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnPentavalenteDosis2ActionPerformed(evt);
            }
        });

        grbtnPolio.add(rbtnPolioDosis2);
        rbtnPolioDosis2.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPolioDosis2.setText("2");

        jLabel15.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        jLabel15.setText("Dosis");

        txtCedula1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedula1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedula1ActionPerformed(evt);
            }
        });

        grbtnPolio.add(rbtnPolioDosis1);
        rbtnPolioDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPolioDosis1.setText("1");
        rbtnPolioDosis1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnPolioDosis1ActionPerformed(evt);
            }
        });

        cmbTipoSangre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTipoSangre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "O-", "O+", "A+", "A-", "B-", "B+", "AB-", "AB+" }));

        jLabel16.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel16.setText("KG");

        jLabel17.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        jLabel17.setText("Vacunas");

        lblTipoSangre1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTipoSangre1.setText("Tipo de Sangre");

        grbtnAntineumo.add(rbtnAntineumococoDosis2);
        rbtnAntineumococoDosis2.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAntineumococoDosis2.setText("2");
        rbtnAntineumococoDosis2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnAntineumococoDosis2ActionPerformed(evt);
            }
        });

        jSeparator9.setOrientation(javax.swing.SwingConstants.VERTICAL);

        lblAlergia.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblAlergia.setText("¿Sufre de algún tipo de Alergia?");

        inscripcion1.setFont(new java.awt.Font("Comfortaa", 1, 60)); // NOI18N
        inscripcion1.setText("Inscripción");

        rbtnTrivalenteDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnTrivalenteDosis1.setText("1");
        rbtnTrivalenteDosis1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnTrivalenteDosis1ActionPerformed(evt);
            }
        });

        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/datos medicos.png"))); // NOI18N

        lblCedulaEstu1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaEstu1.setText("Cédula Estudiante");

        lblSufreEfermedad.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSufreEfermedad.setText("¿Padece algun tipo de enfermedad?");

        btnSiguiente1.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnSiguiente1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/siguiente icono.png"))); // NOI18N
        btnSiguiente1.setText("Siguiente");
        btnSiguiente1.setBorder(null);
        btnSiguiente1.setContentAreaFilled(false);
        btnSiguiente1.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        btnSiguiente1.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnSiguiente1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/siguiiente.png"))); // NOI18N
        btnSiguiente1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguiente1ActionPerformed(evt);
            }
        });

        grbtnEnfermedad.add(rbtnEnfermedadSi);
        rbtnEnfermedadSi.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnEnfermedadSi.setText("Si");
        rbtnEnfermedadSi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnEnfermedadSiActionPerformed(evt);
            }
        });

        grbtnEnfermedad.add(rbtnEnfermedadNo);
        rbtnEnfermedadNo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnEnfermedadNo.setText("No");
        rbtnEnfermedadNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnEnfermedadNoActionPerformed(evt);
            }
        });

        lblIndiqueTratamientoEnfermedad.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblIndiqueTratamientoEnfermedad.setText("Indique Cúal y Tratamiento");

        txtIndiqueTratamientoEnfermedad.setColumns(20);
        txtIndiqueTratamientoEnfermedad.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtIndiqueTratamientoEnfermedad.setRows(5);
        jScrollPane4.setViewportView(txtIndiqueTratamientoEnfermedad);

        jPanel4.setBackground(new java.awt.Color(0, 153, 102));
        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnCancelar1.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnCancelar1.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar (2).png"))); // NOI18N
        btnCancelar1.setText("Cancelar");
        btnCancelar1.setBorder(null);
        btnCancelar1.setContentAreaFilled(false);
        btnCancelar1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnCancelar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelar1ActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        jButton2.setText("Menú Principal");
        jButton2.setBorder(null);
        jButton2.setContentAreaFilled(false);
        jButton2.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/home.png"))); // NOI18N

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator19, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCancelar1, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator18, javax.swing.GroupLayout.PREFERRED_SIZE, 212, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btnCancelar1)
                .addGap(11, 11, 11)
                .addComponent(jSeparator11, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jSeparator19, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(jButton2)
                .addGap(7, 7, 7)
                .addComponent(jSeparator18, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(56, 56, 56))
        );

        jLabel19.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel19.setText("TALLAS");

        jButton1.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/atras azul.png"))); // NOI18N
        jButton1.setText("Atrás");
        jButton1.setBorder(null);
        jButton1.setContentAreaFilled(false);
        jButton1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/atras negro.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 110, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(lblCedulaEstu1)
                        .addGap(16, 16, 16)
                        .addComponent(txtCedula1, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(lblTipoSangre1)
                        .addGap(14, 14, 14)
                        .addComponent(cmbTipoSangre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(241, 241, 241)
                        .addComponent(jLabel13)
                        .addGap(331, 331, 331)
                        .addComponent(jLabel17)
                        .addGap(102, 102, 102)
                        .addComponent(jLabel15))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(53, 53, 53)
                                .addComponent(inscripcion1, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(307, 307, 307)
                                .addComponent(jLabel10)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel11))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabel12)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(lblPeso)
                                .addGap(22, 22, 22)
                                .addComponent(txtPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(jLabel16)
                                .addGap(49, 49, 49)
                                .addComponent(lblEstatura)
                                .addGap(10, 10, 10)
                                .addComponent(txtEstatura, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(jLabel14))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(lblTallaCamisa)
                                .addGap(20, 20, 20)
                                .addComponent(cmbTallaCamisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(50, 50, 50)
                                        .addComponent(jLabel19))
                                    .addComponent(lblTallaPantalon)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(100, 100, 100)
                                        .addComponent(cmbTallaPantalon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(16, 16, 16)
                                .addComponent(lblTallaCalzado)
                                .addGap(21, 21, 21)
                                .addComponent(cmbTallaCalzado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblAlergia)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(30, 30, 30)
                                        .addComponent(lblIndiqueTratamiento3)))
                                .addGap(25, 25, 25)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(rbtnAlergiaSi)
                                        .addGap(34, 34, 34)
                                        .addComponent(rbtnAlergiaNo))
                                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(lblSufreCirugia)
                                .addGap(21, 21, 21)
                                .addComponent(rbtnCirugiaSi)
                                .addGap(31, 31, 31)
                                .addComponent(rbtnCirugiaNo))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(lblIndiqueTratamiento1)
                                .addGap(37, 37, 37)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(lblSufreEfermedad)
                                .addGap(18, 18, 18)
                                .addComponent(rbtnEnfermedadSi)
                                .addGap(36, 36, 36)
                                .addComponent(rbtnEnfermedadNo))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(lblIndiqueTratamientoEnfermedad)
                                .addGap(37, 37, 37)
                                .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(30, 30, 30)
                        .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(90, 90, 90)
                                        .addComponent(lblPolio)
                                        .addGap(1, 1, 1)
                                        .addComponent(rbtnPolioDosis1)
                                        .addGap(39, 39, 39)
                                        .addComponent(rbtnPolioDosis2))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(lblAntineumococo)
                                        .addGap(4, 4, 4)
                                        .addComponent(rbtnAntineumococoDosis1)
                                        .addGap(39, 39, 39)
                                        .addComponent(rbtnAntineumococoDosis2))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(40, 40, 40)
                                        .addComponent(lblHepatitis1)
                                        .addGap(4, 4, 4)
                                        .addComponent(rbtnHepatitisDosis1))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(20, 20, 20)
                                        .addComponent(lblAntiamarilica1)
                                        .addGap(3, 3, 3)
                                        .addComponent(rbtnAntiamarilicaDosis1))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(50, 50, 50)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addGap(80, 80, 80)
                                                .addComponent(rbtnTrivalenteDosis1))
                                            .addComponent(lblTrivalente)))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(90, 90, 90)
                                        .addComponent(lblBCG1)
                                        .addGap(6, 6, 6)
                                        .addComponent(rbtnBCGDosis1))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(50, 50, 50)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addComponent(lblInfluenza1)
                                                .addGap(6, 6, 6)
                                                .addComponent(rbtnInfluenzaDosis1)
                                                .addGap(39, 39, 39)
                                                .addComponent(rbtnInfluenzaDosis2))
                                            .addGroup(jPanel3Layout.createSequentialGroup()
                                                .addGap(10, 10, 10)
                                                .addComponent(lblToxoide1)
                                                .addGap(3, 3, 3)
                                                .addComponent(rbtnToxoideDosis1))))
                                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(74, 74, 74)))
                                .addGap(57, 57, 57))
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(lblPentavalente)
                                .addGap(4, 4, 4)
                                .addComponent(rbtnPentavalenteDosis1)
                                .addGap(39, 39, 39)
                                .addComponent(rbtnPentavalenteDosis2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(rbtnPentavalenteDosis3))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(btnSiguiente1))))
                    .addComponent(jSeparator6)
                    .addComponent(jSeparator7))
                .addContainerGap(110, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(inscripcion1, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(7, 7, 7)
                                .addComponent(jLabel10))
                            .addComponent(jLabel11))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel12)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel18, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCedulaEstu1)
                    .addComponent(txtCedula1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTipoSangre1)
                    .addComponent(cmbTipoSangre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel13)
                    .addComponent(jLabel17)
                    .addComponent(jLabel15, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPentavalente)
                            .addComponent(rbtnPentavalenteDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(rbtnPentavalenteDosis2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(rbtnPentavalenteDosis3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPolio)
                            .addComponent(rbtnPolioDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rbtnPolioDosis2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblAntineumococo)
                            .addComponent(rbtnAntineumococoDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rbtnAntineumococoDosis2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblHepatitis1)
                            .addComponent(rbtnHepatitisDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblAntiamarilica1)
                            .addComponent(rbtnAntiamarilicaDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbtnTrivalenteDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTrivalente))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblBCG1)
                            .addComponent(rbtnBCGDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblInfluenza1)
                            .addComponent(rbtnInfluenzaDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rbtnInfluenzaDosis2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblToxoide1)
                            .addComponent(rbtnToxoideDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnSiguiente1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGap(58, 58, 58))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblPeso)
                                    .addComponent(txtPeso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel16)
                                    .addComponent(lblEstatura)
                                    .addComponent(txtEstatura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel14))
                                .addGap(13, 13, 13)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel19)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(20, 20, 20)
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblTallaCamisa)
                                            .addComponent(cmbTallaCamisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(lblTallaPantalon)
                                            .addComponent(cmbTallaPantalon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(lblTallaCalzado)
                                            .addComponent(cmbTallaCalzado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(13, 13, 13)
                                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addComponent(lblAlergia)
                                        .addGap(19, 19, 19)
                                        .addComponent(lblIndiqueTratamiento3))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(rbtnAlergiaSi, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(rbtnAlergiaNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(19, 19, 19)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblSufreCirugia)
                                    .addComponent(rbtnCirugiaSi)
                                    .addComponent(rbtnCirugiaNo))
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(11, 11, 11)
                                        .addComponent(lblIndiqueTratamiento1))
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(1, 1, 1)
                                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 52, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(5, 5, 5)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(lblSufreEfermedad)
                                    .addComponent(rbtnEnfermedadSi)
                                    .addComponent(rbtnEnfermedadNo))
                                .addGap(13, 13, 13)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel3Layout.createSequentialGroup()
                                        .addGap(21, 21, 21)
                                        .addComponent(lblIndiqueTratamientoEnfermedad))
                                    .addComponent(jScrollPane4, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );

        jLabel20.setFont(new java.awt.Font("Comfortaa", 1, 60)); // NOI18N
        jLabel20.setText("Inscripción");

        jLabel21.setBackground(new java.awt.Color(255, 255, 255));
        jLabel21.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel21.setText("Nuevo");

        jLabel22.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel22.setText("INGRESO");

        jLabel23.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel23.setText("Datos Familiares");

        jLabel24.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel24.setText("Datos de la Madre");

        lblCedulaMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaMadre.setText("Cédula");

        txtCedulaMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedulaMadre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtCedulaMadreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCedulaMadreFocusLost(evt);
            }
        });
        txtCedulaMadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaMadreKeyTyped(evt);
            }
        });

        btnCedulaMadre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/lens.png"))); // NOI18N
        btnCedulaMadre.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar icono.png"))); // NOI18N
        btnCedulaMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCedulaMadreActionPerformed(evt);
            }
        });

        lblNombreMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblNombreMadre.setText("Nombre");

        lblApellidoMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblApellidoMadre.setText("Apellido");

        txtNombreMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtNombreMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreMadreActionPerformed(evt);
            }
        });
        txtNombreMadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreMadreKeyTyped(evt);
            }
        });

        txtApellidoMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtApellidoMadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoMadreKeyTyped(evt);
            }
        });

        lblFechaNaciMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblFechaNaciMadre.setText("F/N");

        lblProfesionMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblProfesionMadre.setText("Profesión u Ocupación");

        txtProfesionMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtProfesionMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtProfesionMadreActionPerformed(evt);
            }
        });

        lblTelefonoMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTelefonoMadre.setText("Teléfono");

        cmbTelefonoMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTelefonoMadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbTelefonoMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTelefonoMadreActionPerformed(evt);
            }
        });

        txtTelefonoMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtTelefonoMadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoMadreKeyTyped(evt);
            }
        });

        cmbCedulaMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbCedulaMadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "V", "E" }));

        lblDireccionMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblDireccionMadre.setText("Dirección de Habitación");

        txtDireccionMadre.setColumns(20);
        txtDireccionMadre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtDireccionMadre.setRows(5);
        jScrollPane2.setViewportView(txtDireccionMadre);

        lblOtroTlfMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblOtroTlfMadre.setText("Otro Teléfono");

        cmbOtroTlfMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbOtroTlfMadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbOtroTlfMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbOtroTlfMadreActionPerformed(evt);
            }
        });

        txtOtroTlfMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtOtroTlfMadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOtroTlfMadreKeyTyped(evt);
            }
        });

        jLabel25.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel25.setText("Datos del Padre");

        lblCedulaPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaPadre.setText("Cédula");

        cmbCedulaPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbCedulaPadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "V", "E" }));

        txtCedulaPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedulaPadre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtCedulaPadreFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txtCedulaPadreFocusLost(evt);
            }
        });
        txtCedulaPadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaPadreKeyTyped(evt);
            }
        });

        btnCedulaPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        btnCedulaPadre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/lens.png"))); // NOI18N
        btnCedulaPadre.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar icono.png"))); // NOI18N
        btnCedulaPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCedulaPadreActionPerformed(evt);
            }
        });

        lblNombrePadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblNombrePadre.setText("Nombre");

        txtNombrePadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtNombrePadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombrePadreKeyTyped(evt);
            }
        });

        lblApellidoPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblApellidoPadre.setText("Apellido");

        txtApellidoPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtApellidoPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtApellidoPadreActionPerformed(evt);
            }
        });
        txtApellidoPadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoPadreKeyTyped(evt);
            }
        });

        lblFechaNaciPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblFechaNaciPadre.setText("F/N");

        lblProfesionPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblProfesionPadre.setText("Profesión u Ocupación");

        txtProfesionPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N

        lblDireccionPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblDireccionPadre.setText("Dirección de Habitación");

        txtDireccionPadre.setColumns(20);
        txtDireccionPadre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtDireccionPadre.setRows(5);
        jScrollPane6.setViewportView(txtDireccionPadre);

        lblTelefonoPAdre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTelefonoPAdre.setText("Teléfono");

        cmbTelefonoPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTelefonoPadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbTelefonoPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTelefonoPadreActionPerformed(evt);
            }
        });

        txtTelefonoPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtTelefonoPadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoPadreKeyTyped(evt);
            }
        });

        lblOtroTlfPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblOtroTlfPadre.setText("Otro Teléfono");

        cmbOtroTlfPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbOtroTlfPadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbOtroTlfPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbOtroTlfPadreActionPerformed(evt);
            }
        });

        txtOtroTlfPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtOtroTlfPadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOtroTlfPadreKeyTyped(evt);
            }
        });

        jLabel26.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/family-icon.png"))); // NOI18N

        txtFechaNaMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        txtFechaNaciPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        jPanel6.setBackground(new java.awt.Color(0, 153, 102));
        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        btnCancelar2.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnCancelar2.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar (2).png"))); // NOI18N
        btnCancelar2.setText("Cancelar");
        btnCancelar2.setBorder(null);
        btnCancelar2.setContentAreaFilled(false);
        btnCancelar2.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        jPanel6.add(btnCancelar2, new org.netbeans.lib.awtextra.AbsoluteConstraints(7, 90, 200, -1));
        jPanel6.add(jSeparator15, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 80, 220, -1));
        jPanel6.add(jSeparator16, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 130, 220, -1));

        btnMenuFamiliares.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnMenuFamiliares.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuFamiliares.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnMenuFamiliares.setText("Menú Principal");
        btnMenuFamiliares.setBorder(null);
        btnMenuFamiliares.setContentAreaFilled(false);
        btnMenuFamiliares.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/home.png"))); // NOI18N
        btnMenuFamiliares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuFamiliaresActionPerformed(evt);
            }
        });
        jPanel6.add(btnMenuFamiliares, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 560, 180, 30));
        jPanel6.add(jSeparator20, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 552, 220, 10));
        jPanel6.add(jSeparator21, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 600, 220, -1));

        jLabel27.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel27.setText("Observación");

        jScrollPane7.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        txtObservacionMadre.setColumns(20);
        txtObservacionMadre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtObservacionMadre.setRows(5);
        jScrollPane7.setViewportView(txtObservacionMadre);

        jLabel28.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel28.setText("Observación");

        txtObservacionPadre.setColumns(20);
        txtObservacionPadre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtObservacionPadre.setRows(5);
        jScrollPane8.setViewportView(txtObservacionPadre);

        btnRegistrar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnRegistrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Guardar.png"))); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.setBorder(null);
        btnRegistrar.setContentAreaFilled(false);
        btnRegistrar.setHorizontalAlignment(javax.swing.SwingConstants.TRAILING);
        btnRegistrar.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnRegistrar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar 2 icono.png"))); // NOI18N
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        btnAtrasFamiliares.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnAtrasFamiliares.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/atras azul.png"))); // NOI18N
        btnAtrasFamiliares.setText("Atrás");
        btnAtrasFamiliares.setBorder(null);
        btnAtrasFamiliares.setContentAreaFilled(false);
        btnAtrasFamiliares.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/atras negro.png"))); // NOI18N
        btnAtrasFamiliares.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasFamiliaresActionPerformed(evt);
            }
        });

        btnGuardarActualizar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnGuardarActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Guardar.png"))); // NOI18N
        btnGuardarActualizar.setText("Registrar");
        btnGuardarActualizar.setBorder(null);
        btnGuardarActualizar.setContentAreaFilled(false);
        btnGuardarActualizar.setHorizontalTextPosition(javax.swing.SwingConstants.LEFT);
        btnGuardarActualizar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar 2 icono.png"))); // NOI18N
        btnGuardarActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGuardarActualizarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(21, 21, 21)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(42, 42, 42)
                                .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(258, 258, 258)
                                .addComponent(jLabel21))
                            .addComponent(jLabel23))
                        .addGap(6, 6, 6)
                        .addComponent(jLabel22)
                        .addGap(102, 102, 102)
                        .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(362, 362, 362)
                        .addComponent(jLabel24))
                    .addComponent(jSeparator12, javax.swing.GroupLayout.PREFERRED_SIZE, 1060, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator13, javax.swing.GroupLayout.PREFERRED_SIZE, 1060, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(19, 19, 19)
                        .addComponent(lblCedulaMadre)
                        .addGap(4, 4, 4)
                        .addComponent(cmbCedulaMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(txtCedulaMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 108, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(6, 6, 6)
                        .addComponent(btnCedulaMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(15, 15, 15)
                        .addComponent(lblNombreMadre)
                        .addGap(2, 2, 2)
                        .addComponent(txtNombreMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(lblApellidoMadre)
                        .addGap(4, 4, 4)
                        .addComponent(txtApellidoMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 222, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(12, 12, 12)
                        .addComponent(lblFechaNaciMadre)
                        .addGap(10, 10, 10)
                        .addComponent(txtFechaNaMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(lblProfesionMadre)
                        .addGap(10, 10, 10)
                        .addComponent(txtProfesionMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 236, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(77, 77, 77)
                        .addComponent(lblDireccionMadre)
                        .addGap(23, 23, 23)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 319, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addComponent(lblTelefonoMadre)
                        .addGap(10, 10, 10)
                        .addComponent(cmbTelefonoMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(txtTelefonoMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblOtroTlfMadre)
                        .addGap(4, 4, 4)
                        .addComponent(cmbOtroTlfMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(txtOtroTlfMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(jLabel28)
                        .addGap(4, 4, 4)
                        .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(380, 380, 380)
                        .addComponent(jLabel25))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jSeparator14, javax.swing.GroupLayout.PREFERRED_SIZE, 1054, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addComponent(lblCedulaPadre)
                        .addGap(4, 4, 4)
                        .addComponent(cmbCedulaPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addComponent(txtCedulaPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(7, 7, 7)
                        .addComponent(btnCedulaPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblNombrePadre)
                        .addGap(8, 8, 8)
                        .addComponent(txtNombrePadre, javax.swing.GroupLayout.PREFERRED_SIZE, 184, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(4, 4, 4)
                        .addComponent(lblApellidoPadre)
                        .addGap(4, 4, 4)
                        .addComponent(txtApellidoPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 224, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(10, 10, 10)
                        .addComponent(lblFechaNaciPadre)
                        .addGap(10, 10, 10)
                        .addComponent(txtFechaNaciPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(lblProfesionPadre)
                                .addGap(15, 15, 15)
                                .addComponent(txtProfesionPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addComponent(lblTelefonoPAdre)
                                .addGap(18, 18, 18)
                                .addComponent(cmbTelefonoPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(6, 6, 6)
                                .addComponent(txtTelefonoPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(16, 16, 16)
                                .addComponent(lblOtroTlfPadre)
                                .addGap(4, 4, 4)
                                .addComponent(cmbOtroTlfPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(6, 6, 6)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(lblDireccionPadre))
                            .addComponent(txtOtroTlfPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(34, 34, 34)
                        .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(jLabel27)
                        .addGap(4, 4, 4)
                        .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 340, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(129, 129, 129)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(160, 160, 160)
                                .addComponent(btnGuardarActualizar))
                            .addComponent(btnAtrasFamiliares, javax.swing.GroupLayout.PREFERRED_SIZE, 134, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(130, 130, 130)
                                .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE))))))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 696, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jLabel20, javax.swing.GroupLayout.PREFERRED_SIZE, 78, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(3, 3, 3)
                        .addComponent(jLabel21)
                        .addGap(14, 14, 14)
                        .addComponent(jLabel23))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(74, 74, 74)
                        .addComponent(jLabel22))
                    .addComponent(jLabel26, javax.swing.GroupLayout.PREFERRED_SIZE, 143, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel24, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator12, javax.swing.GroupLayout.PREFERRED_SIZE, 9, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addComponent(jSeparator13, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(7, 7, 7)
                        .addComponent(lblCedulaMadre))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(cmbCedulaMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(txtCedulaMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnCedulaMadre, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(lblNombreMadre))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(txtNombreMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(lblApellidoMadre))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(txtApellidoMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(9, 9, 9)
                        .addComponent(lblFechaNaciMadre))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(txtFechaNaMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(23, 23, 23)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(lblProfesionMadre))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(txtProfesionMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(13, 13, 13)
                        .addComponent(lblDireccionMadre))
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(cmbTelefonoMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtTelefonoMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(cmbOtroTlfMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtOtroTlfMadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblTelefonoMadre)
                            .addComponent(lblOtroTlfMadre))))
                .addGap(13, 13, 13)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel28)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(10, 10, 10)
                .addComponent(jLabel25)
                .addGap(5, 5, 5)
                .addComponent(jSeparator14, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblCedulaPadre))
                    .addComponent(cmbCedulaPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(txtCedulaPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnCedulaPadre, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(lblNombrePadre))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(txtNombrePadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(lblApellidoPadre))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(txtApellidoPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblFechaNaciPadre)
                    .addComponent(txtFechaNaciPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(8, 8, 8)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblProfesionPadre)
                            .addComponent(txtProfesionPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(cmbTelefonoPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtTelefonoPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(cmbOtroTlfPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addGap(3, 3, 3)
                                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblTelefonoPAdre)
                                    .addComponent(lblOtroTlfPadre)))))
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(15, 15, 15)
                        .addComponent(lblDireccionPadre)
                        .addGap(11, 11, 11)
                        .addComponent(txtOtroTlfPadre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane6, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel27)
                    .addComponent(jScrollPane8, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnGuardarActualizar)
                            .addComponent(btnAtrasFamiliares)
                            .addComponent(btnRegistrar)))))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCedulaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCedulaFocusLost
        // TODO add your handling code here:

    }//GEN-LAST:event_txtCedulaFocusLost

    private void txtCedulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedulaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaActionPerformed

    private void txtCedulaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaKeyTyped
        // TODO add your handling code here:

    }//GEN-LAST:event_txtCedulaKeyTyped

    private void txtPrimerNombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrimerNombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrimerNombreActionPerformed

    private void txtPrimerNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrimerNombreKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();

        if((!Character.isLetter(c)) && (!Character.isSpaceChar(c)) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume();
        }
        lblNoExistePrimerNombre.setVisible(false);
    }//GEN-LAST:event_txtPrimerNombreKeyTyped

    private void txtPrimerApellidoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPrimerApellidoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPrimerApellidoActionPerformed

    private void txtPrimerApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPrimerApellidoKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();

        if(!Character.isLetter(c) && !Character.isSpaceChar(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume();
        }
        lblNoExistePrimerApellido.setVisible(false);
    }//GEN-LAST:event_txtPrimerApellidoKeyTyped

    private void cmbSexoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSexoActionPerformed
        // TODO add your handling code here:
        if(cmbSexo.getSelectedIndex()>0){
            lblNoExisteSexo.setVisible(false);
        }
    }//GEN-LAST:event_cmbSexoActionPerformed

    private void txtLugarNacimientoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtLugarNacimientoKeyTyped
        // TODO add your handling code here:
        
        lblNoExisteLugarNaci.setVisible(false);

    }//GEN-LAST:event_txtLugarNacimientoKeyTyped

    private void txtDireccionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtDireccionKeyTyped
        // TODO add your handling code here:
        lblNoExisteDireccion.setVisible(false);
    }//GEN-LAST:event_txtDireccionKeyTyped

    private void txtTelefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoKeyTyped
        // TODO add your handling code here:
        int limite=7;
        if(txtTelefono.getText().length()>=7){
            evt.consume();
        }

        char c=evt.getKeyChar();

        if(!Character.isDigit(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume(); }
        lblNoExisteTelefono.setVisible(false);
    }//GEN-LAST:event_txtTelefonoKeyTyped

    private void cmbTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTelefonoActionPerformed
        // TODO add your handling code here:
        if(cmbTelefono.getSelectedIndex()>0){
            txtTelefono.setEditable(true);
        }
        else{
            txtTelefono.setEditable(false);
            txtTelefono.setText("");
        }
    }//GEN-LAST:event_cmbTelefonoActionPerformed

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
         dlgDatosVacios advertencia = new dlgDatosVacios(new javax.swing.JFrame(), true);    
        if ((txtCedula.getText().isEmpty()) || (txtPrimerNombre.getText().isEmpty()) || (txtPrimerApellido.getText().isEmpty()) || (cmbParentesco.getSelectedIndex()==0) || (cmbSexo.getSelectedIndex()==0) || (txtFechaNaci.getDate()==null) || (txtLugarNacimiento.getText().isEmpty()) || (txtTelefono.getText().isEmpty()) || (txtDireccion.getText().isEmpty()) || (txtCodigoCanaima.getText().isEmpty())){
                    advertencia.setVisible(true);
                    //Se indica con una X el lugar o campo que esta vacio 
                    //Se revisa con un If cada campo nuevamente para comprobar cual esta vacio
                    
                    if(txtPrimerNombre.getText().isEmpty()){
                        lblNoExistePrimerNombre.setVisible(true);
                    }
                    if(txtPrimerApellido.getText().isEmpty()){
                        lblNoExistePrimerApellido.setVisible(true);
                    }
                    if(cmbParentesco.getSelectedIndex()==0){
                        lblNoExisteParentesco.setVisible(true);
                    }
                    if(cmbSexo.getSelectedIndex()==0){
                        lblNoExisteSexo.setVisible(true);
                    }
                    if(txtFechaNaci.getDate()==null){
                        lblNoExisteFechaNaci.setVisible(true);
                    }
                    if(txtLugarNacimiento.getText().isEmpty()){
                        lblNoExisteLugarNaci.setVisible(true);
                    }
                    if(txtTelefono.getText().isEmpty()){
                        lblNoExisteTelefono.setVisible(true);
                    }
                    if(txtDireccion.getText().isEmpty()){
                        lblNoExisteDireccion.setVisible(true);
                    }
                    if (txtCodigoCanaima.getText().isEmpty()){
                        lblNoExisteCodigoCanaima.setVisible(true);
                    }
                            }else{
        jPanel3.setVisible(true);
        jPanel1.setVisible(false);
        txtCedula1.setText(txtCedula.getText());
        }
        // TODO add your handling code here:
      //  Registrar();
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void txtGradoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGradoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtGradoActionPerformed

    private void txtSeccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtSeccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtSeccionActionPerformed

    private void txtTurnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTurnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTurnoActionPerformed

    private void cmbParentescoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbParentescoActionPerformed
        // TODO add your handling code here:
        if(cmbParentesco.getSelectedIndex()>0){
            lblNoExisteParentesco.setVisible(false);
        }

    }//GEN-LAST:event_cmbParentescoActionPerformed

    private void txtCodigoCanaimaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCodigoCanaimaActionPerformed
        lblNoExisteCodigoCanaima.setVisible(false);
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoCanaimaActionPerformed

    private void txtCodigoCanaimaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoCanaimaKeyTyped
        char c=evt.getKeyChar();

        if ((!Character.isLetter(c)) && (!Character.isSpaceChar(c)) && (c!=KeyEvent.VK_COMMA) && (c!=46) && (c!=KeyEvent.VK_MINUS) && (!Character.isDigit(c))) {
            getToolkit().beep();
            evt.consume();
        }
        lblNoExisteCodigoCanaima.setVisible(false); // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoCanaimaKeyTyped

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        int valor;

        valor=JOptionPane.showConfirmDialog(null,"\t¿DESEA SALIR?\n\nSe Borraran todos los datos no guardados","Confirmar salida",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
        if (valor==JOptionPane.YES_OPTION){
            this.dispose();
            frmTipoDeInscripcion obj= new frmTipoDeInscripcion();
            obj.setVisible(true);
        }

    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnMenuPrincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuPrincipalActionPerformed
        int valor;

        valor=JOptionPane.showConfirmDialog(null,"\t\t¿DESEA SALIR?\nSe Borraran todos los datos no guardados","Confirmar salida",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
        if (valor==JOptionPane.YES_OPTION){
            this.dispose();
            menuInicio obj= new menuInicio();
            obj.setVisible(true);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_btnMenuPrincipalActionPerformed

    private void rbtnAlergiaSiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnAlergiaSiActionPerformed
        // TODO add your handling code here:
        if(rbtnAlergiaSi.isSelected()){
            txtIndiqueTratamiento3.setEnabled(true);

        }
        else
        { txtIndiqueTratamiento3.setEnabled(false);
            txtIndiqueTratamiento3.setText("");
        }
    }//GEN-LAST:event_rbtnAlergiaSiActionPerformed

    private void rbtnAlergiaNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnAlergiaNoActionPerformed
       if(rbtnAlergiaNo.isSelected()){
            txtIndiqueTratamiento3.setEnabled(false);
            txtIndiqueTratamiento3.setText("");
        }
        else{
            txtIndiqueTratamiento3.setEnabled(true);
            txtIndiqueTratamiento3.setText("");
        }     // TODO add your handling code here:
    }//GEN-LAST:event_rbtnAlergiaNoActionPerformed

    private void rbtnCirugiaSiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnCirugiaSiActionPerformed
        // TODO add your handling code here:
        if(rbtnCirugiaSi.isSelected()){
            txtIndiqueTratamiento.setEnabled(true);
        }
        else{
            txtIndiqueTratamiento.setEnabled(false);
            txtIndiqueTratamiento.setText("");
        }
    }//GEN-LAST:event_rbtnCirugiaSiActionPerformed

    private void rbtnCirugiaNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnCirugiaNoActionPerformed
     if(rbtnCirugiaNo.isSelected()){
            txtIndiqueTratamiento.setEnabled(false);
            txtIndiqueTratamiento.setText("");
        }
        else{
            txtIndiqueTratamiento.setEnabled(true);
            txtIndiqueTratamiento.setText("");
        }        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnCirugiaNoActionPerformed

    private void rbtnInfluenzaDosis2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnInfluenzaDosis2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnInfluenzaDosis2ActionPerformed

    private void txtPesoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtPesoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtPesoActionPerformed

    private void rbtnPentavalenteDosis2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnPentavalenteDosis2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnPentavalenteDosis2ActionPerformed

    private void txtCedula1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedula1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedula1ActionPerformed

    private void rbtnPolioDosis1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnPolioDosis1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnPolioDosis1ActionPerformed

    private void rbtnAntineumococoDosis2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnAntineumococoDosis2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnAntineumococoDosis2ActionPerformed

    private void rbtnTrivalenteDosis1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnTrivalenteDosis1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnTrivalenteDosis1ActionPerformed

    private void btnSiguiente1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguiente1ActionPerformed
    jPanel3.setVisible(false);
    jPanel5.setVisible(true);//   Registrar();                // TODO add your handling code here:
    }//GEN-LAST:event_btnSiguiente1ActionPerformed

    private void rbtnEnfermedadSiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnEnfermedadSiActionPerformed
        // TODO add your handling code here:
        if(rbtnEnfermedadSi.isSelected()){
            txtIndiqueTratamientoEnfermedad.setEnabled(true);
        }
        else{
            txtIndiqueTratamientoEnfermedad.setEnabled(false);
            txtIndiqueTratamientoEnfermedad.setText("");
        }
    }//GEN-LAST:event_rbtnEnfermedadSiActionPerformed

    private void rbtnEnfermedadNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnEnfermedadNoActionPerformed
     if(rbtnEnfermedadNo.isSelected()){
            txtIndiqueTratamientoEnfermedad.setEnabled(false);
            txtIndiqueTratamientoEnfermedad.setText("");
        }
        else{
            txtIndiqueTratamientoEnfermedad.setEnabled(true);
            txtIndiqueTratamientoEnfermedad.setText("");
        }        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnEnfermedadNoActionPerformed

    private void btnCancelar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelar1ActionPerformed
        int valor;

        valor=JOptionPane.showConfirmDialog(null,"\t¿DESEA SALIR?\n\nSe Borraran todos los datos no guardados","Confirmar salida",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
        if (valor==JOptionPane.YES_OPTION){
            this.dispose();
            frmTipoDeInscripcion obj= new frmTipoDeInscripcion();
            obj.setVisible(true);
          //  cancelar();
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelar1ActionPerformed

    private void txtNombreMadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreMadreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreMadreActionPerformed

    private void txtProfesionMadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtProfesionMadreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtProfesionMadreActionPerformed

    private void txtApellidoPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtApellidoPadreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtApellidoPadreActionPerformed

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
        if (txtCedulaMadre.getText().isEmpty() && txtCedulaPadre.getText().isEmpty()){
            dlgDebeAgregarPorLoMenosAUnPadre dialog = new dlgDebeAgregarPorLoMenosAUnPadre(new javax.swing.JFrame(), true);
            dialog.setVisible(true);
        }else
        registrar();        // TODO add your handling code here:
    }//GEN-LAST:event_btnRegistrarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        jPanel3.setVisible(false);
        jPanel1.setVisible(true);// TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void btnAtrasFamiliaresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasFamiliaresActionPerformed
        jPanel5.setVisible(false);
        jPanel3.setVisible(true);// TODO add your handling code here:
    }//GEN-LAST:event_btnAtrasFamiliaresActionPerformed

    private void cmbParentescoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbParentescoItemStateChanged
     if (evt.getStateChange() == ItemEvent.SELECTED){
        conexion conn=new conexion();
        Connection cn=conn.conectar();     
        String cedula = null,nombre=null,apellido=null,fecha=null,profesion=null,direccion=null,tlf=null,otroTlf="",observacion=null;
        String consulta="SELECT ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,direccion_fa,tlf_fa,otroTlf_fa,observacion_fa FROM familiares WHERE ced_fa='"+txtCedulaRepre.getText()+"'";   
              try {
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(consulta);
                while (rs.next()){
                cedula=rs.getString("ced_fa");
                nombre=rs.getString("nombre_fa");
                apellido=rs.getString("apellido_fa");
                fecha=rs.getString("fechaNacimiento_fa");
                profesion=rs.getString("profesion_fa");
                direccion=rs.getString("direccion_fa");
                tlf=rs.getString("tlf_fa");
                otroTlf=rs.getString("otroTlf_fa");
                observacion=rs.getString("observacion_fa");
                
                }
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, e);
            }
        if (cmbParentesco.getSelectedItem().toString().equals("Mamá")){
              
              
            try {
                String[] temp3=cedula.split("-");
                cmbCedulaMadre.setSelectedItem(temp3[0]);
                txtCedulaMadre.setText(temp3[1]);
                txtCedulaMadre.setEditable(false);
                cmbCedulaMadre.setEnabled(false);
                txtNombreMadre.setText(nombre);
                txtNombreMadre.setEditable(false);
                txtApellidoMadre.setText(apellido);
                txtApellidoMadre.setEditable(false);
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date fechaDate;
                fechaDate = formato.parse(fecha);
                txtFechaNaMadre.setDate(fechaDate);
                txtFechaNaMadre.getCalendarButton().setEnabled(false);
                txtProfesionMadre.setText(profesion);
                txtProfesionMadre.setEditable(false);
                txtDireccionMadre.setText(direccion);
                txtDireccionMadre.setEditable(false);
                String [] temp;          
                temp=tlf.split("-");
                cmbTelefonoMadre.setSelectedItem(temp[0]);
                cmbTelefonoMadre.setEnabled(false);
                txtTelefonoMadre.setText(temp[1]);
                txtTelefonoMadre.setEditable(false);
                if (!otroTlf.isEmpty()){
                String [] temp2;
                temp2=otroTlf.split("-");
                cmbOtroTlfMadre.setSelectedItem(temp2[0]);
                
                txtOtroTlfMadre.setText(temp2[1]);
                
                }
                cmbOtroTlfMadre.setEnabled(false);
                txtOtroTlfMadre.setEditable(false);
                txtObservacionMadre.setText(observacion);
                txtObservacionMadre.setEditable(false);
                txtFechaNaciPadre.getCalendarButton().setEnabled(true);
                activarPadre();
                limpiarPadre();
                setEditarMadre(true);
                setEditarPadre(false); 
                       
                
            } catch (ParseException ex) {
               JOptionPane.showMessageDialog(null, ex);
            }
              
              
  
        }else 
            if (cmbParentesco.getSelectedItem().toString().equals("Papá")){
              
              
            try {
                String[] temp3=cedula.split("-");
                cmbCedulaPadre.setSelectedItem(temp3[0]);
                txtCedulaPadre.setText(temp3[1]);
                cmbCedulaPadre.setEnabled(false);
                txtCedulaPadre.setEditable(false);
                txtNombrePadre.setText(nombre);
                txtNombrePadre.setEditable(false);
                txtApellidoPadre.setText(apellido);
                txtApellidoPadre.setEditable(false);
                SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                Date fechaDate;
                fechaDate = formato.parse(fecha);
                txtFechaNaciPadre.setDate(fechaDate);
                txtFechaNaciPadre.getCalendarButton().setEnabled(false);
                txtProfesionPadre.setText(profesion);
                txtProfesionPadre.setEditable(false);
                txtDireccionPadre.setText(direccion);
                txtDireccionPadre.setEditable(false);
                if (!tlf.isEmpty()){
                String [] temp;
                temp=tlf.split("-");
                cmbTelefonoPadre.setSelectedItem(temp[0]);
                cmbTelefonoPadre.setEnabled(false);
                txtTelefonoPadre.setText(temp[1]);
                txtTelefonoPadre.setEditable(false);
                }
                if (!otroTlf.isEmpty()){
                String [] temp2;
                temp2=otroTlf.split("-");
                cmbOtroTlfPadre.setSelectedItem(temp2[0]);
                cmbOtroTlfPadre.setEnabled(false);
                txtOtroTlfPadre.setText(temp2[1]);
                txtOtroTlfPadre.setEditable(false);
                }
                txtObservacionPadre.setText(observacion);
                txtObservacionPadre.setEditable(false);
                txtFechaNaMadre.getCalendarButton().setEnabled(true);
                  activarMadre();
                  limpiarMadre();
                  setEditarPadre(true);
                  setEditarMadre(false);
                
            } catch (ParseException ex) {
               JOptionPane.showMessageDialog(null, ex);
            }
              
              
  
        }else{
                limpiarMadre();
                limpiarPadre();
                activarMadre();
                activarPadre();
                setEditarMadre(false);
                setEditarPadre(false);
            } 
            
        
     }       // TODO add your handling code here:
    }//GEN-LAST:event_cmbParentescoItemStateChanged

    private void btnMenuFamiliaresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuFamiliaresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnMenuFamiliaresActionPerformed

    private void cmbOtroTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbOtroTelefonoActionPerformed
     if (cmbOtroTelefono.getSelectedIndex()>0){
            txtOtroTelefono.setEditable(true);
        }
        else
        {
            txtOtroTelefono.setText("");
            txtOtroTelefono.setEditable(false);
        }        // TODO add your handling code here:
    }//GEN-LAST:event_cmbOtroTelefonoActionPerformed

    private void txtTelefonoMadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoMadreKeyTyped
        // TODO add your handling code here:
         int limite=7;
        if(txtTelefonoMadre.getText().length()>=7){
            evt.consume();
        }

        char c=evt.getKeyChar();

        if(!Character.isDigit(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume(); }
        
    }//GEN-LAST:event_txtTelefonoMadreKeyTyped

    private void cmbTelefonoMadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTelefonoMadreActionPerformed
        // TODO add your handling code here:
        if(cmbTelefonoMadre.getSelectedIndex()>0){
            txtTelefonoMadre.setEnabled(true);
        }
        else{
            txtTelefonoMadre.setEnabled(false);
            txtTelefonoMadre.setText("");
        }
    }//GEN-LAST:event_cmbTelefonoMadreActionPerformed

    private void txtOtroTlfMadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOtroTlfMadreKeyTyped
   
         int limite=7;
        if(txtOtroTlfMadre.getText().length()>=7){
            evt.consume();
        }

        char c=evt.getKeyChar();

        if(!Character.isDigit(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume(); }
    }//GEN-LAST:event_txtOtroTlfMadreKeyTyped

    private void cmbOtroTlfMadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbOtroTlfMadreActionPerformed
        // TODO add your handling code here:
        if(cmbOtroTlfMadre.getSelectedIndex()>0){
            txtOtroTlfMadre.setEnabled(true);
        }
        else{
            txtOtroTlfMadre.setEnabled(false);
            txtOtroTlfMadre.setText("");
        }
    }//GEN-LAST:event_cmbOtroTlfMadreActionPerformed

    private void cmbTelefonoPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTelefonoPadreActionPerformed
        // TODO add your handling code here:
        if(cmbTelefonoPadre.getSelectedIndex()>0){
            txtTelefonoPadre.setEnabled(true);
        }
        else{
            txtTelefonoPadre.setEnabled(false);
            txtTelefonoPadre.setText("");
        }
       
    }//GEN-LAST:event_cmbTelefonoPadreActionPerformed

    private void txtTelefonoPadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoPadreKeyTyped
        // TODO add your handling code here:
         int limite=7;
        if(txtTelefonoPadre.getText().length()>=7){
            evt.consume();
        }

        char c=evt.getKeyChar();

        if(!Character.isDigit(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume(); }
    }//GEN-LAST:event_txtTelefonoPadreKeyTyped

    private void txtOtroTlfPadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOtroTlfPadreKeyTyped
        // TODO add your handling code here:
         int limite=7;
        if(txtOtroTlfPadre.getText().length()>=7){
            evt.consume();
        }

        char c=evt.getKeyChar() ;

        if(!Character.isDigit(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume(); }
    }//GEN-LAST:event_txtOtroTlfPadreKeyTyped

    private void cmbOtroTlfPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbOtroTlfPadreActionPerformed
        // TODO add your handling code here:
         if(cmbOtroTlfPadre.getSelectedIndex()>0){
            txtOtroTlfPadre.setEnabled(true);
        }
        else{
            txtOtroTlfPadre.setEnabled(false);
            txtOtroTlfPadre.setText("");
        }
       
    }//GEN-LAST:event_cmbOtroTlfPadreActionPerformed

    private void txtNombreMadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreMadreKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();

        if(!Character.isLetter(c) && !Character.isSpaceChar(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume();
        }
        
    }//GEN-LAST:event_txtNombreMadreKeyTyped

    private void txtApellidoMadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoMadreKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();

        if(!Character.isLetter(c) && !Character.isSpaceChar(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume();
        }
        
    }//GEN-LAST:event_txtApellidoMadreKeyTyped

    private void txtNombrePadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombrePadreKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();

        if(!Character.isLetter(c) && !Character.isSpaceChar(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume();
        }
        
    }//GEN-LAST:event_txtNombrePadreKeyTyped

    private void txtApellidoPadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoPadreKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();

        if(!Character.isLetter(c) && !Character.isSpaceChar(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume();
        }
        
    }//GEN-LAST:event_txtApellidoPadreKeyTyped

    private void txtOtroTelefonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOtroTelefonoKeyTyped
        // TODO add your handling code here:
        int limite=7;
        if(txtOtroTelefono.getText().length()>=7){
            evt.consume();
        }

        char c=evt.getKeyChar();

        if(!Character.isDigit(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume(); }
    }//GEN-LAST:event_txtOtroTelefonoKeyTyped

    private void txtPesoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesoKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
      
        if(!Character.isDigit(c) && (c!= KeyEvent.VK_COMMA) && (c!=KeyEvent.VK_BACK_SPACE) ){
            getToolkit().beep();
            evt.consume();
   
        }else
        if (c==KeyEvent.VK_COMMA ){
            if (c==KeyEvent.VK_COMMA && isAux()==true){
            getToolkit().beep();
            evt.consume();
        }else
            setAux(true);
        }
        
        int ayu=-1;
        if (c==KeyEvent.VK_BACK_SPACE){
        ayu=txtPeso.getText().indexOf(KeyEvent.VK_COMMA);
        }
        if ((c==KeyEvent.VK_BACK_SPACE) && (ayu==-1)){
            setAux(false);
        }
    }//GEN-LAST:event_txtPesoKeyTyped

    private void txtEstaturaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEstaturaKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
        
        if(!Character.isDigit(c) && (c!= KeyEvent.VK_COMMA)&& (c!=KeyEvent.VK_BACK_SPACE)  ){
            getToolkit().beep();
            evt.consume();
        }else
        if (c==KeyEvent.VK_COMMA ){
            if (c==KeyEvent.VK_COMMA && isAux2()==true){
            getToolkit().beep();
            evt.consume();
        }else
            setAux2(true);
        }
        
      int ayu=-1;
        if (c==KeyEvent.VK_BACK_SPACE){
        ayu=txtEstatura.getText().indexOf(KeyEvent.VK_COMMA);
        }
        if ((c==KeyEvent.VK_BACK_SPACE) && (ayu==-1)){
            setAux2(false);
        }
 
        
    }//GEN-LAST:event_txtEstaturaKeyTyped

    private void btnGuardarActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnGuardarActualizarActionPerformed
        // TODO add your handling code here:
        actualizar();
    }//GEN-LAST:event_btnGuardarActualizarActionPerformed

    private void txtSegundoNombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSegundoNombreKeyTyped
        char c=evt.getKeyChar();

        if((!Character.isLetter(c)) && (!Character.isSpaceChar(c)) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtSegundoNombreKeyTyped

    private void txtSegundoApellidoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtSegundoApellidoKeyTyped
        char c=evt.getKeyChar();

        if(!Character.isLetter(c) && !Character.isSpaceChar(c) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtSegundoApellidoKeyTyped

    private void txtCodigoCanaimaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCodigoCanaimaKeyReleased
    txtCodigoCanaima.setText((txtCodigoCanaima.getText()).toUpperCase());         // TODO add your handling code here:
    }//GEN-LAST:event_txtCodigoCanaimaKeyReleased

    private void txtCedulaMadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaMadreKeyTyped
     char c=evt.getKeyChar();

        if ((!Character.isDigit(c)) && (c!=KeyEvent.VK_ENTER) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume(); 
        }
        if(txtCedulaMadre.getText().length()>=9){
            evt.consume();
            getToolkit().beep();
           
        }
        if (c==KeyEvent.VK_ENTER){
            cargarMadre(cmbCedulaMadre.getSelectedItem().toString()+"-"+txtCedulaMadre.getText());
        }
// TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaMadreKeyTyped

    private void txtCedulaPadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaPadreKeyTyped
        char c=evt.getKeyChar();

        if ((!Character.isDigit(c)) && (c!=KeyEvent.VK_ENTER) && (c!=KeyEvent.VK_BACK_SPACE)) {
            getToolkit().beep();
            evt.consume(); 
        }
        if(txtCedulaPadre.getText().length()>=9){
            evt.consume();
            getToolkit().beep();
        }      
        if (c==KeyEvent.VK_ENTER){
            cargarPadre(cmbCedulaPadre.getSelectedItem().toString()+"-"+txtCedulaPadre.getText());
        }

// TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaPadreKeyTyped

    private void txtCedulaMadreFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCedulaMadreFocusLost
        cargarMadre(cmbCedulaMadre.getSelectedItem().toString()+"-"+txtCedulaMadre.getText());        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaMadreFocusLost

    private void btnCedulaMadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCedulaMadreActionPerformed
        cargarMadre(cmbCedulaMadre.getSelectedItem().toString()+"-"+txtCedulaMadre.getText());  // TODO add your handling code here:
    }//GEN-LAST:event_btnCedulaMadreActionPerformed

    private void txtCedulaPadreFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCedulaPadreFocusLost
       cargarPadre(cmbCedulaPadre.getSelectedItem().toString()+"-"+txtCedulaPadre.getText()); // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaPadreFocusLost

    private void btnCedulaPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCedulaPadreActionPerformed
      cargarPadre(cmbCedulaPadre.getSelectedItem().toString()+"-"+txtCedulaPadre.getText());  // TODO add your handling code here:
    }//GEN-LAST:event_btnCedulaPadreActionPerformed

    private void txtTelefonoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTelefonoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTelefonoActionPerformed

    private void txtEstaturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtEstaturaActionPerformed
         // TODO add your handling code here:
    }//GEN-LAST:event_txtEstaturaActionPerformed

    private void txtCedulaMadreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCedulaMadreFocusGained
    limpiarMadre();        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaMadreFocusGained

    private void txtCedulaPadreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCedulaPadreFocusGained
    limpiarPadre();        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaPadreFocusGained

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmInscribirNuevo2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmInscribirNuevo2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmInscribirNuevo2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmInscribirNuevo2.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmInscribirNuevo2().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAtrasFamiliares;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnCancelar1;
    private javax.swing.JButton btnCancelar2;
    private javax.swing.JButton btnCedulaMadre;
    private javax.swing.JButton btnCedulaPadre;
    public static javax.swing.JButton btnGuardarActualizar;
    private javax.swing.JButton btnMenuFamiliares;
    private javax.swing.JButton btnMenuPrincipal;
    public static javax.swing.JButton btnRegistrar;
    public static javax.swing.JButton btnSiguiente;
    private javax.swing.JButton btnSiguiente1;
    public static javax.swing.JComboBox cmbCedulaMadre;
    public static javax.swing.JComboBox cmbCedulaPadre;
    public static javax.swing.JComboBox cmbOtroTelefono;
    public static javax.swing.JComboBox cmbOtroTlfMadre;
    public static javax.swing.JComboBox cmbOtroTlfPadre;
    public static javax.swing.JComboBox cmbParentesco;
    public static javax.swing.JComboBox cmbSexo;
    public static javax.swing.JComboBox cmbTallaCalzado;
    public static javax.swing.JComboBox cmbTallaCamisa;
    public static javax.swing.JComboBox cmbTallaPantalon;
    public static javax.swing.JComboBox cmbTelefono;
    public static javax.swing.JComboBox cmbTelefonoMadre;
    public static javax.swing.JComboBox cmbTelefonoPadre;
    public static javax.swing.JComboBox cmbTipoSangre;
    public static javax.swing.ButtonGroup grbtnAntineumo;
    public static javax.swing.ButtonGroup grbtnEnfermedad;
    public static javax.swing.ButtonGroup grbtnInfluenza;
    public static javax.swing.ButtonGroup grbtnPenta;
    public static javax.swing.ButtonGroup grbtnPolio;
    public static javax.swing.ButtonGroup grtbAlergias;
    public static javax.swing.ButtonGroup grtbnCirugia;
    private javax.swing.JLabel inscripcion;
    private javax.swing.JLabel inscripcion1;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel26;
    private javax.swing.JLabel jLabel27;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JScrollPane jScrollPane6;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator18;
    private javax.swing.JSeparator jSeparator19;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator20;
    private javax.swing.JSeparator jSeparator21;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lblAlergia;
    private javax.swing.JLabel lblAntiamarilica1;
    private javax.swing.JLabel lblAntineumococo;
    private javax.swing.JLabel lblApellidoMadre;
    private javax.swing.JLabel lblApellidoPadre;
    private javax.swing.JLabel lblBCG1;
    private javax.swing.JLabel lblCedula;
    private javax.swing.JLabel lblCedulaEstu1;
    private javax.swing.JLabel lblCedulaMadre;
    private javax.swing.JLabel lblCedulaPadre;
    private javax.swing.JLabel lblCedulaRepre;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblDireccionMadre;
    private javax.swing.JLabel lblDireccionPadre;
    private javax.swing.JLabel lblEstatura;
    private javax.swing.JLabel lblFechaNac;
    private javax.swing.JLabel lblFechaNaciMadre;
    private javax.swing.JLabel lblFechaNaciPadre;
    private javax.swing.JLabel lblHepatitis1;
    private javax.swing.JLabel lblIndiqueTratamiento1;
    private javax.swing.JLabel lblIndiqueTratamiento3;
    private javax.swing.JLabel lblIndiqueTratamientoEnfermedad;
    private javax.swing.JLabel lblInfluenza1;
    private javax.swing.JLabel lblLugarNacimiento;
    private javax.swing.JLabel lblNoExisteCodigoCanaima;
    private javax.swing.JLabel lblNoExisteDireccion;
    private javax.swing.JLabel lblNoExisteFechaNaci;
    private javax.swing.JLabel lblNoExisteLugarNaci;
    private javax.swing.JLabel lblNoExisteParentesco;
    private javax.swing.JLabel lblNoExistePrimerApellido;
    private javax.swing.JLabel lblNoExistePrimerNombre;
    private javax.swing.JLabel lblNoExisteSexo;
    private javax.swing.JLabel lblNoExisteTelefono;
    private javax.swing.JLabel lblNombreMadre;
    private javax.swing.JLabel lblNombrePadre;
    private javax.swing.JLabel lblOtroTelefono;
    private javax.swing.JLabel lblOtroTlfMadre;
    private javax.swing.JLabel lblOtroTlfPadre;
    private javax.swing.JLabel lblPentavalente;
    private javax.swing.JLabel lblPeso;
    private javax.swing.JLabel lblPolio;
    private javax.swing.JLabel lblPrimerApellido;
    private javax.swing.JLabel lblPrimerNombre;
    private javax.swing.JLabel lblProfesionMadre;
    private javax.swing.JLabel lblProfesionPadre;
    private javax.swing.JLabel lblSegundoApellido;
    private javax.swing.JLabel lblSegundoNombre;
    private javax.swing.JLabel lblSexo;
    private javax.swing.JLabel lblSufreCirugia;
    private javax.swing.JLabel lblSufreEfermedad;
    private javax.swing.JLabel lblTallaCalzado;
    private javax.swing.JLabel lblTallaCamisa;
    private javax.swing.JLabel lblTallaPantalon;
    private javax.swing.JLabel lblTelefono;
    private javax.swing.JLabel lblTelefonoMadre;
    private javax.swing.JLabel lblTelefonoPAdre;
    private javax.swing.JLabel lblTipoSangre1;
    private javax.swing.JLabel lblToxoide1;
    private javax.swing.JLabel lblTrivalente;
    public static javax.swing.JRadioButton rbtnAlergiaNo;
    public static javax.swing.JRadioButton rbtnAlergiaSi;
    public static javax.swing.JRadioButton rbtnAntiamarilicaDosis1;
    public static javax.swing.JRadioButton rbtnAntineumococoDosis1;
    public static javax.swing.JRadioButton rbtnAntineumococoDosis2;
    public static javax.swing.JRadioButton rbtnBCGDosis1;
    public static javax.swing.JRadioButton rbtnCirugiaNo;
    public static javax.swing.JRadioButton rbtnCirugiaSi;
    public static javax.swing.JRadioButton rbtnEnfermedadNo;
    public static javax.swing.JRadioButton rbtnEnfermedadSi;
    public static javax.swing.JRadioButton rbtnHepatitisDosis1;
    public static javax.swing.JRadioButton rbtnInfluenzaDosis1;
    public static javax.swing.JRadioButton rbtnInfluenzaDosis2;
    public static javax.swing.JRadioButton rbtnPentavalenteDosis1;
    public static javax.swing.JRadioButton rbtnPentavalenteDosis2;
    public static javax.swing.JRadioButton rbtnPentavalenteDosis3;
    public static javax.swing.JRadioButton rbtnPolioDosis1;
    public static javax.swing.JRadioButton rbtnPolioDosis2;
    public static javax.swing.JRadioButton rbtnToxoideDosis1;
    public static javax.swing.JRadioButton rbtnTrivalenteDosis1;
    public static javax.swing.JTextField txtApellidoMadre;
    public static javax.swing.JTextField txtApellidoPadre;
    public static javax.swing.JTextField txtCedula;
    public static javax.swing.JTextField txtCedula1;
    public static javax.swing.JTextField txtCedulaMadre;
    public static javax.swing.JTextField txtCedulaPadre;
    public static javax.swing.JTextField txtCedulaRepre;
    public static javax.swing.JTextField txtCodigoCanaima;
    public static javax.swing.JTextArea txtDireccion;
    public static javax.swing.JTextArea txtDireccionMadre;
    public static javax.swing.JTextArea txtDireccionPadre;
    public static javax.swing.JTextField txtEstatura;
    public static com.toedter.calendar.JDateChooser txtFechaNaMadre;
    public static com.toedter.calendar.JDateChooser txtFechaNaci;
    public static com.toedter.calendar.JDateChooser txtFechaNaciPadre;
    public static javax.swing.JTextField txtGrado;
    public static javax.swing.JTextArea txtIndiqueTratamiento;
    public static javax.swing.JTextArea txtIndiqueTratamiento3;
    public static javax.swing.JTextArea txtIndiqueTratamientoEnfermedad;
    public static javax.swing.JLabel txtInstitucion;
    public static javax.swing.JTextField txtLugarNacimiento;
    public static javax.swing.JTextField txtNombreMadre;
    public static javax.swing.JTextField txtNombrePadre;
    public static javax.swing.JLabel txtNombreProf;
    public static javax.swing.JTextArea txtObservacionMadre;
    public static javax.swing.JTextArea txtObservacionPadre;
    public static javax.swing.JTextField txtOtroTelefono;
    public static javax.swing.JTextField txtOtroTlfMadre;
    public static javax.swing.JTextField txtOtroTlfPadre;
    public static javax.swing.JLabel txtPeriodoEscolar;
    public static javax.swing.JTextField txtPeso;
    public static javax.swing.JTextField txtPrimerApellido;
    public static javax.swing.JTextField txtPrimerNombre;
    public static javax.swing.JTextField txtProfesionMadre;
    public static javax.swing.JTextField txtProfesionPadre;
    public static javax.swing.JTextField txtSeccion;
    public static javax.swing.JTextField txtSegundoApellido;
    public static javax.swing.JTextField txtSegundoNombre;
    public static javax.swing.JTextField txtTelefono;
    public static javax.swing.JTextField txtTelefonoMadre;
    public static javax.swing.JTextField txtTelefonoPadre;
    public static javax.swing.JTextField txtTurno;
    // End of variables declaration//GEN-END:variables
}
