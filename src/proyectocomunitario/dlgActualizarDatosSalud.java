/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocomunitario;

import BD.conexion;
import java.awt.GraphicsEnvironment;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;

/**
 *
 * @author NANY
 */
public class dlgActualizarDatosSalud extends javax.swing.JDialog {

    /**
     * Creates new form dlgActualizarDatosSalud
     */
    public dlgActualizarDatosSalud(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        txtCedula.setEnabled(false);
        this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
    }
    boolean aux=false;
    boolean aux2=false;

    public boolean isAux() {
        return aux;
    }

    public void setAux(boolean aux) {
        this.aux = aux;
    }

    public boolean isAux2() {
        return aux2;
    }

    public void setAux2(boolean aux2) {
        this.aux2 = aux2;
    }
    void Cargar(String valor){
        conexion con=new conexion();
        Connection cn=con.conectar();
        String query1= "SELECT tipo_sangre,v_bcg,v_trivalente,cirugia_s,alergia_s,v_penta,v_polio,v_antineumo,v_hepa_b,v_influenza,v_antiama,v_toxoide,enfermedad,peso,estatura,talla_camisa,talla_pantalon,talla_calzado FROM salud WHERE ced_estu='"+valor+"'";
        try {
            Statement er=cn.createStatement();
            ResultSet dt=er.executeQuery(query1);
             while(dt.next()){
           cmbTipoSangre.setSelectedItem(dt.getString("tipo_sangre"));
           txtPeso.setText(dt.getString("peso"));
           txtEstatura.setText(dt.getString("estatura"));
           cmbTallaCamisa.setSelectedItem(dt.getString("talla_camisa"));
           cmbTallaPantalon.setSelectedItem(dt.getString("talla_pantalon"));
           cmbTallaCalzado.setSelectedItem(dt.getString("talla_calzado"));
           String aux="",aux1="",aux2="";
           aux=dt.getString("alergia_s");
           aux1=dt.getString("cirugia_s");
           aux2=dt.getString("enfermedad");
           if (aux.equals("")){
               rbtnAlergiaNo.setSelected(true);
               txtIndiqueTratamiento3.setEnabled(false);
           }
           else{
               rbtnAlergiaSi.setSelected(true);
               txtIndiqueTratamiento3.setEnabled(true);
           }
           
           if(aux1.equals("")){
              rbtnCirugiaNo.setSelected(true);
              txtIndiqueTratamiento.setEnabled(false);
           }
           else{
               rbtnCirugiaSi.setSelected(true);
               txtIndiqueTratamiento.setEnabled(true);
           }
               
           if(aux2.equals("")){
              rbtnEnfermedadNo.setSelected(true);
              txtIndiqueTratamientoEnfermedad.setEnabled(false);
           }
           else{
             rbtnEnfermedadSi.setSelected(true);
             txtIndiqueTratamientoEnfermedad.setEnabled(true);
           }
          if (dt.getInt("v_penta")==1){
             rbtnPentavalenteDosis1.setSelected(true);

          }
          else{
              if(dt.getInt("v_penta")==2){
             rbtnPentavalenteDosis2.setSelected(true);

              }
              else{
                  if(dt.getInt("v_penta")==3){
                      rbtnPentavalenteDosis3.setSelected(true);
                  }
              }
          }
           if(dt.getInt("v_polio")==1){
               rbtnPolioDosis1.setSelected(true);
           }
           else{
               if(dt.getInt("v_polio")==2){
                   rbtnPolioDosis2.setSelected(true);
               }
           }
           if(dt.getInt("v_antineumo")==1){
               rbtnAntineumococoDosis1.setSelected(true);
           }
           
           else{
               if(dt.getInt("v_antineumo")==2){
                   rbtnAntineumococoDosis2.setSelected(true);
               }
           }
           if(dt.getInt("v_hepa_b")==1){
                  rbtnHepatitisDosis1.setSelected(true);
           }
           if(dt.getInt("v_antiama")==1){
               rbtnAntiamarilicaDosis1.setSelected(true);
           }
               
           if(dt.getInt("v_trivalente")==1){
               rbtnTrivalenteDosis1.setSelected(true);
           }
           if(dt.getInt("v_bcg")==1){
               rbtnBCGDosis1.setSelected(true);
           }
           if(dt.getInt("v_influenza")==1){
               rbtnInfluenzaDosis1.setSelected(true);
           }
           else{
               if(dt.getInt("v_influenza")==2){
                   rbtnInfluenzaDosis2.setSelected(true);
               }
           }
           if(dt.getInt("v_toxoide")==1){
               rbtnToxoideDosis1.setSelected(true);
           }
       }
        } catch (Exception e) {
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grtbAlergias = new javax.swing.ButtonGroup();
        grtbnCirugia = new javax.swing.ButtonGroup();
        grbtnEnfermedad = new javax.swing.ButtonGroup();
        grbtnPenta = new javax.swing.ButtonGroup();
        grbtnPolio = new javax.swing.ButtonGroup();
        grbtnAntineumo = new javax.swing.ButtonGroup();
        grbtnInfluenza = new javax.swing.ButtonGroup();
        jPanel1 = new imagenes.Fondo("fondo escritorio.jpg");
        jScrollPane1 = new javax.swing.JScrollPane();
        txtIndiqueTratamiento = new javax.swing.JTextArea();
        lblTallaPantalon = new javax.swing.JLabel();
        lblAntiamarilica1 = new javax.swing.JLabel();
        rbtnAlergiaSi = new javax.swing.JRadioButton();
        cmbTallaCamisa = new javax.swing.JComboBox();
        rbtnPentavalenteDosis3 = new javax.swing.JRadioButton();
        rbtnAntiamarilicaDosis1 = new javax.swing.JRadioButton();
        lblPolio = new javax.swing.JLabel();
        rbtnAlergiaNo = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        lblToxoide1 = new javax.swing.JLabel();
        lblPeso = new javax.swing.JLabel();
        lblIndiqueTratamiento1 = new javax.swing.JLabel();
        rbtnHepatitisDosis1 = new javax.swing.JRadioButton();
        lblIndiqueTratamiento3 = new javax.swing.JLabel();
        jSeparator6 = new javax.swing.JSeparator();
        txtEstatura = new javax.swing.JTextField();
        lblAntineumococo = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtIndiqueTratamiento3 = new javax.swing.JTextArea();
        jSeparator5 = new javax.swing.JSeparator();
        lblPentavalente = new javax.swing.JLabel();
        rbtnCirugiaSi = new javax.swing.JRadioButton();
        lblTallaCalzado = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        rbtnCirugiaNo = new javax.swing.JRadioButton();
        rbtnBCGDosis1 = new javax.swing.JRadioButton();
        lblTallaCamisa = new javax.swing.JLabel();
        rbtnPentavalenteDosis1 = new javax.swing.JRadioButton();
        lblHepatitis1 = new javax.swing.JLabel();
        cmbTallaPantalon = new javax.swing.JComboBox();
        rbtnAntineumococoDosis1 = new javax.swing.JRadioButton();
        cmbTallaCalzado = new javax.swing.JComboBox();
        jSeparator3 = new javax.swing.JSeparator();
        lblSufreCirugia = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        rbtnInfluenzaDosis1 = new javax.swing.JRadioButton();
        lblTrivalente = new javax.swing.JLabel();
        lblBCG1 = new javax.swing.JLabel();
        lblEstatura = new javax.swing.JLabel();
        rbtnInfluenzaDosis2 = new javax.swing.JRadioButton();
        lblInfluenza1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        txtPeso = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        rbtnToxoideDosis1 = new javax.swing.JRadioButton();
        rbtnPentavalenteDosis2 = new javax.swing.JRadioButton();
        rbtnPolioDosis2 = new javax.swing.JRadioButton();
        jLabel12 = new javax.swing.JLabel();
        txtCedula = new javax.swing.JTextField();
        rbtnPolioDosis1 = new javax.swing.JRadioButton();
        cmbTipoSangre = new javax.swing.JComboBox();
        jLabel13 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lblTipoSangre1 = new javax.swing.JLabel();
        rbtnAntineumococoDosis2 = new javax.swing.JRadioButton();
        jSeparator8 = new javax.swing.JSeparator();
        lblAlergia = new javax.swing.JLabel();
        inscripcion = new javax.swing.JLabel();
        rbtnTrivalenteDosis1 = new javax.swing.JRadioButton();
        jLabel10 = new javax.swing.JLabel();
        lblCedulaEstu1 = new javax.swing.JLabel();
        lblSufreEfermedad = new javax.swing.JLabel();
        rbtnEnfermedadSi = new javax.swing.JRadioButton();
        rbtnEnfermedadNo = new javax.swing.JRadioButton();
        lblIndiqueTratamientoEnfermedad = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        txtIndiqueTratamientoEnfermedad = new javax.swing.JTextArea();
        jPanel2 = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        jSeparator1 = new javax.swing.JSeparator();
        jSeparator2 = new javax.swing.JSeparator();
        jButton1 = new javax.swing.JButton();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        txtIndiqueTratamiento.setColumns(20);
        txtIndiqueTratamiento.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtIndiqueTratamiento.setRows(5);
        jScrollPane1.setViewportView(txtIndiqueTratamiento);

        lblTallaPantalon.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTallaPantalon.setText(" Pantalón");

        lblAntiamarilica1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblAntiamarilica1.setText("Antiamarílica");

        grtbAlergias.add(rbtnAlergiaSi);
        rbtnAlergiaSi.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAlergiaSi.setText("Si");
        rbtnAlergiaSi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnAlergiaSiActionPerformed(evt);
            }
        });

        cmbTallaCamisa.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTallaCamisa.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "2", "4", "6", "8", "10", "12", "14", "16", "18", "S", "M", "L", "XL" }));

        grbtnPenta.add(rbtnPentavalenteDosis3);
        rbtnPentavalenteDosis3.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPentavalenteDosis3.setText("3");

        rbtnAntiamarilicaDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAntiamarilicaDosis1.setText("1");

        lblPolio.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblPolio.setText("Polio");

        grtbAlergias.add(rbtnAlergiaNo);
        rbtnAlergiaNo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAlergiaNo.setText("No");
        rbtnAlergiaNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnAlergiaNoActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel1.setText("Datos de");

        lblToxoide1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblToxoide1.setText("Toxoide");

        lblPeso.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblPeso.setText("Peso");

        lblIndiqueTratamiento1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblIndiqueTratamiento1.setText("Indique Cúal y Tratamiento");

        rbtnHepatitisDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnHepatitisDosis1.setText("1");

        lblIndiqueTratamiento3.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblIndiqueTratamiento3.setText("Indique Cúal y Tratamiento");

        txtEstatura.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtEstatura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtEstaturaKeyTyped(evt);
            }
        });

        lblAntineumococo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblAntineumococo.setText("Antineumococo");

        txtIndiqueTratamiento3.setColumns(20);
        txtIndiqueTratamiento3.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtIndiqueTratamiento3.setRows(5);
        jScrollPane5.setViewportView(txtIndiqueTratamiento3);

        lblPentavalente.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblPentavalente.setText("Pentavalente");

        grtbnCirugia.add(rbtnCirugiaSi);
        rbtnCirugiaSi.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnCirugiaSi.setText("Si");
        rbtnCirugiaSi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnCirugiaSiActionPerformed(evt);
            }
        });

        lblTallaCalzado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTallaCalzado.setText("Calzado");

        jLabel3.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel3.setText("SALUD");

        grtbnCirugia.add(rbtnCirugiaNo);
        rbtnCirugiaNo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnCirugiaNo.setText("No");
        rbtnCirugiaNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnCirugiaNoActionPerformed(evt);
            }
        });

        rbtnBCGDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnBCGDosis1.setText("1");

        lblTallaCamisa.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTallaCamisa.setText("Camisa");

        grbtnPenta.add(rbtnPentavalenteDosis1);
        rbtnPentavalenteDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPentavalenteDosis1.setText("1");

        lblHepatitis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblHepatitis1.setText("Hepatitis B");

        cmbTallaPantalon.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTallaPantalon.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "4", "6", "8", "10", "12", "14", "16", "18", "26", "28", "30", "32", "34" }));

        grbtnAntineumo.add(rbtnAntineumococoDosis1);
        rbtnAntineumococoDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAntineumococoDosis1.setText("1");

        cmbTallaCalzado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTallaCalzado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec" }));

        lblSufreCirugia.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSufreCirugia.setText("¿Ha sufrido de alguna operación o cirugia?");

        jLabel11.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel11.setText("Datos sobre la Salud del Estudiante");

        grbtnInfluenza.add(rbtnInfluenzaDosis1);
        rbtnInfluenzaDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnInfluenzaDosis1.setText("1");

        lblTrivalente.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTrivalente.setText("Trivalente");

        lblBCG1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblBCG1.setText("BCG");

        lblEstatura.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblEstatura.setText("Estatura");

        grbtnInfluenza.add(rbtnInfluenzaDosis2);
        rbtnInfluenzaDosis2.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnInfluenzaDosis2.setText("2");
        rbtnInfluenzaDosis2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnInfluenzaDosis2ActionPerformed(evt);
            }
        });

        lblInfluenza1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblInfluenza1.setText("Influenza");

        jLabel2.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        jLabel2.setText("Otros Datos");

        txtPeso.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtPeso.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtPesoKeyTyped(evt);
            }
        });

        jLabel14.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel14.setText("Centimetros");

        rbtnToxoideDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnToxoideDosis1.setText("1");

        grbtnPenta.add(rbtnPentavalenteDosis2);
        rbtnPentavalenteDosis2.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPentavalenteDosis2.setText("2");
        rbtnPentavalenteDosis2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnPentavalenteDosis2ActionPerformed(evt);
            }
        });

        grbtnPolio.add(rbtnPolioDosis2);
        rbtnPolioDosis2.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPolioDosis2.setText("2");

        jLabel12.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        jLabel12.setText("Dosis");

        txtCedula.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedula.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedulaActionPerformed(evt);
            }
        });

        grbtnPolio.add(rbtnPolioDosis1);
        rbtnPolioDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnPolioDosis1.setText("1");
        rbtnPolioDosis1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnPolioDosis1ActionPerformed(evt);
            }
        });

        cmbTipoSangre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTipoSangre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "O-", "O+", "A+", "A-", "B-", "B+", "AB-", "AB+" }));

        jLabel13.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel13.setText("KG");

        jLabel7.setFont(new java.awt.Font("Comfortaa", 0, 16)); // NOI18N
        jLabel7.setText("Vacunas");

        lblTipoSangre1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTipoSangre1.setText("Tipo de Sangre");

        grbtnAntineumo.add(rbtnAntineumococoDosis2);
        rbtnAntineumococoDosis2.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAntineumococoDosis2.setText("2");
        rbtnAntineumococoDosis2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnAntineumococoDosis2ActionPerformed(evt);
            }
        });

        jSeparator8.setOrientation(javax.swing.SwingConstants.VERTICAL);

        lblAlergia.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblAlergia.setText("¿Sufre de algún tipo de Alergia?");

        inscripcion.setFont(new java.awt.Font("Comfortaa", 1, 60)); // NOI18N
        inscripcion.setText("Actualización");

        rbtnTrivalenteDosis1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnTrivalenteDosis1.setText("1");
        rbtnTrivalenteDosis1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnTrivalenteDosis1ActionPerformed(evt);
            }
        });

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/datos medicos.png"))); // NOI18N

        lblCedulaEstu1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaEstu1.setText("Cédula Estudiante");

        lblSufreEfermedad.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSufreEfermedad.setText("¿Padece algún tipo de enfermedad?");

        grbtnEnfermedad.add(rbtnEnfermedadSi);
        rbtnEnfermedadSi.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnEnfermedadSi.setText("Si");
        rbtnEnfermedadSi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnEnfermedadSiActionPerformed(evt);
            }
        });

        grbtnEnfermedad.add(rbtnEnfermedadNo);
        rbtnEnfermedadNo.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnEnfermedadNo.setText("No");
        rbtnEnfermedadNo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnEnfermedadNoActionPerformed(evt);
            }
        });

        lblIndiqueTratamientoEnfermedad.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblIndiqueTratamientoEnfermedad.setText("Indique Cúal y Tratamiento");

        txtIndiqueTratamientoEnfermedad.setColumns(20);
        txtIndiqueTratamientoEnfermedad.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        txtIndiqueTratamientoEnfermedad.setRows(5);
        jScrollPane3.setViewportView(txtIndiqueTratamientoEnfermedad);

        jPanel2.setBackground(new java.awt.Color(0, 153, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnCancelar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar (2).png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(null);
        btnCancelar.setContentAreaFilled(false);
        btnCancelar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        jButton1.setForeground(new java.awt.Color(255, 255, 255));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pen.png"))); // NOI18N
        jButton1.setText("Modificar");
        jButton1.setBorder(null);
        jButton1.setContentAreaFilled(false);
        jButton1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/modificar icono.png"))); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator1)
            .addComponent(jSeparator2)
            .addComponent(btnCancelar, javax.swing.GroupLayout.DEFAULT_SIZE, 215, Short.MAX_VALUE)
            .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jSeparator4, javax.swing.GroupLayout.Alignment.TRAILING)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(jButton1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btnCancelar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jLabel4.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel4.setText("TALLAS");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 34, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(247, 247, 247)
                        .addComponent(lblCedulaEstu1)
                        .addGap(16, 16, 16)
                        .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(lblTipoSangre1)
                        .addGap(14, 14, 14)
                        .addComponent(cmbTipoSangre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(241, 241, 241)
                        .addComponent(jLabel2)
                        .addGap(331, 331, 331)
                        .addComponent(jLabel7)
                        .addGap(102, 102, 102)
                        .addComponent(jLabel12))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(252, 252, 252)
                                .addComponent(jLabel1)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel3))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(12, 12, 12)
                                .addComponent(jLabel11))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(53, 53, 53)
                                .addComponent(inscripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(lblPeso)
                                .addGap(22, 22, 22)
                                .addComponent(txtPeso, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(jLabel13)
                                .addGap(49, 49, 49)
                                .addComponent(lblEstatura)
                                .addGap(10, 10, 10)
                                .addComponent(txtEstatura, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(10, 10, 10)
                                .addComponent(jLabel14))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(lblTallaCamisa)
                                .addGap(20, 20, 20)
                                .addComponent(cmbTallaCamisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(50, 50, 50)
                                        .addComponent(jLabel4))
                                    .addComponent(lblTallaPantalon)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(100, 100, 100)
                                        .addComponent(cmbTallaPantalon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(16, 16, 16)
                                .addComponent(lblTallaCalzado)
                                .addGap(21, 21, 21)
                                .addComponent(cmbTallaCalzado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 580, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(10, 10, 10)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblAlergia)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(30, 30, 30)
                                        .addComponent(lblIndiqueTratamiento3)))
                                .addGap(25, 25, 25)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(rbtnAlergiaSi)
                                        .addGap(11, 11, 11)
                                        .addComponent(rbtnAlergiaNo))
                                    .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblSufreCirugia)
                                .addGap(21, 21, 21)
                                .addComponent(rbtnCirugiaSi)
                                .addGap(31, 31, 31)
                                .addComponent(rbtnCirugiaNo))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(lblIndiqueTratamiento1)
                                .addGap(37, 37, 37)
                                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblSufreEfermedad)
                                .addGap(52, 52, 52)
                                .addComponent(rbtnEnfermedadSi)
                                .addGap(31, 31, 31)
                                .addComponent(rbtnEnfermedadNo))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(40, 40, 40)
                                .addComponent(lblIndiqueTratamientoEnfermedad)
                                .addGap(37, 37, 37)
                                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(30, 30, 30)
                        .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(90, 90, 90)
                                        .addComponent(lblPolio)
                                        .addGap(1, 1, 1)
                                        .addComponent(rbtnPolioDosis1)
                                        .addGap(39, 39, 39)
                                        .addComponent(rbtnPolioDosis2))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblAntineumococo)
                                        .addGap(4, 4, 4)
                                        .addComponent(rbtnAntineumococoDosis1)
                                        .addGap(39, 39, 39)
                                        .addComponent(rbtnAntineumococoDosis2))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(40, 40, 40)
                                        .addComponent(lblHepatitis1)
                                        .addGap(4, 4, 4)
                                        .addComponent(rbtnHepatitisDosis1))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(20, 20, 20)
                                        .addComponent(lblAntiamarilica1)
                                        .addGap(3, 3, 3)
                                        .addComponent(rbtnAntiamarilicaDosis1))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(50, 50, 50)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(80, 80, 80)
                                                .addComponent(rbtnTrivalenteDosis1))
                                            .addComponent(lblTrivalente)))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(90, 90, 90)
                                        .addComponent(lblBCG1)
                                        .addGap(6, 6, 6)
                                        .addComponent(rbtnBCGDosis1))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(50, 50, 50)
                                        .addComponent(lblInfluenza1)
                                        .addGap(6, 6, 6)
                                        .addComponent(rbtnInfluenzaDosis1)
                                        .addGap(39, 39, 39)
                                        .addComponent(rbtnInfluenzaDosis2))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(60, 60, 60)
                                        .addComponent(lblToxoide1)
                                        .addGap(3, 3, 3)
                                        .addComponent(rbtnToxoideDosis1)))
                                .addGap(60, 60, 60))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(20, 20, 20)
                                .addComponent(lblPentavalente)
                                .addGap(4, 4, 4)
                                .addComponent(rbtnPentavalenteDosis1)
                                .addGap(39, 39, 39)
                                .addComponent(rbtnPentavalenteDosis2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(rbtnPentavalenteDosis3))))
                    .addComponent(jSeparator6)
                    .addComponent(jSeparator5))
                .addContainerGap(59, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(inscripcion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel10, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)))
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lblCedulaEstu1)
                    .addComponent(txtCedula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblTipoSangre1)
                    .addComponent(cmbTipoSangre, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(21, 21, 21)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel7)
                    .addComponent(jLabel12, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(4, 4, 4)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPentavalente)
                            .addComponent(rbtnPentavalenteDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(rbtnPentavalenteDosis2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(rbtnPentavalenteDosis3, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblPolio)
                            .addComponent(rbtnPolioDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rbtnPolioDosis2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblAntineumococo)
                            .addComponent(rbtnAntineumococoDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rbtnAntineumococoDosis2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblHepatitis1)
                            .addComponent(rbtnHepatitisDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblAntiamarilica1)
                            .addComponent(rbtnAntiamarilicaDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(rbtnTrivalenteDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTrivalente))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblBCG1)
                            .addComponent(rbtnBCGDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblInfluenza1)
                            .addComponent(rbtnInfluenzaDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(rbtnInfluenzaDosis2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(9, 9, 9)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblToxoide1)
                            .addComponent(rbtnToxoideDosis1, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 390, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblPeso)
                                    .addComponent(txtPeso, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel13)
                                    .addComponent(lblEstatura)
                                    .addComponent(txtEstatura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jLabel14))
                                .addGap(13, 13, 13)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel4)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(20, 20, 20)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(lblTallaCamisa)
                                            .addComponent(cmbTallaCamisa, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(lblTallaPantalon)
                                            .addComponent(cmbTallaPantalon, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(lblTallaCalzado)
                                            .addComponent(cmbTallaCalzado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGap(13, 13, 13)
                                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblAlergia)
                                        .addGap(19, 19, 19)
                                        .addComponent(lblIndiqueTratamiento3))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(rbtnAlergiaSi, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(rbtnAlergiaNo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addComponent(jScrollPane5, javax.swing.GroupLayout.PREFERRED_SIZE, 66, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(4, 4, 4)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblSufreCirugia)
                                    .addComponent(rbtnCirugiaSi)
                                    .addComponent(rbtnCirugiaNo))
                                .addGap(1, 1, 1)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(10, 10, 10)
                                        .addComponent(lblIndiqueTratamiento1))
                                    .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(6, 6, 6)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(lblSufreEfermedad)
                                    .addComponent(rbtnEnfermedadSi)
                                    .addComponent(rbtnEnfermedadNo))
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(21, 21, 21)
                                        .addComponent(lblIndiqueTratamientoEnfermedad)
                                        .addGap(0, 0, Short.MAX_VALUE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(11, 11, 11)
                                        .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))))
                        .addContainerGap())))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void rbtnAlergiaSiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnAlergiaSiActionPerformed
        // TODO add your handling code here:
        if(rbtnAlergiaSi.isSelected()){
            txtIndiqueTratamiento3.setEnabled(true);

        }
        else
        { txtIndiqueTratamiento3.setEnabled(false);
            txtIndiqueTratamiento3.setText("");
        }
    }//GEN-LAST:event_rbtnAlergiaSiActionPerformed

    private void rbtnAlergiaNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnAlergiaNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnAlergiaNoActionPerformed

    private void rbtnCirugiaSiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnCirugiaSiActionPerformed
        // TODO add your handling code here:
        if(rbtnCirugiaSi.isSelected()){
            txtIndiqueTratamiento.setEnabled(true);
        }
        else{
            txtIndiqueTratamiento.setEnabled(false);
            txtIndiqueTratamiento.setText("");
        }
    }//GEN-LAST:event_rbtnCirugiaSiActionPerformed

    private void rbtnCirugiaNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnCirugiaNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnCirugiaNoActionPerformed

    private void rbtnInfluenzaDosis2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnInfluenzaDosis2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnInfluenzaDosis2ActionPerformed

    private void rbtnPentavalenteDosis2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnPentavalenteDosis2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnPentavalenteDosis2ActionPerformed

    private void txtCedulaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedulaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaActionPerformed

    private void rbtnPolioDosis1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnPolioDosis1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnPolioDosis1ActionPerformed

    private void rbtnAntineumococoDosis2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnAntineumococoDosis2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnAntineumococoDosis2ActionPerformed

    private void rbtnTrivalenteDosis1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnTrivalenteDosis1ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnTrivalenteDosis1ActionPerformed

    private void rbtnEnfermedadSiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnEnfermedadSiActionPerformed
        // TODO add your handling code here:
        if(rbtnEnfermedadSi.isSelected()){
            txtIndiqueTratamientoEnfermedad.setEnabled(true);
        }
        else{
            txtIndiqueTratamientoEnfermedad.setEnabled(false);
            txtIndiqueTratamientoEnfermedad.setText("");
        }
    }//GEN-LAST:event_rbtnEnfermedadSiActionPerformed

    private void rbtnEnfermedadNoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnEnfermedadNoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnEnfermedadNoActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void txtPesoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtPesoKeyTyped
        // TODO add your handling code here:
         char c=evt.getKeyChar();
      
        if(!Character.isDigit(c) && (c!= KeyEvent.VK_COMMA) && (c!=KeyEvent.VK_BACK_SPACE) ){
            getToolkit().beep();
            evt.consume();
   
        }else
        if (c==KeyEvent.VK_COMMA ){
            if (c==KeyEvent.VK_COMMA && isAux()==true){
            getToolkit().beep();
            evt.consume();
        }else
            setAux(true);
        }
        
        int ayu=-1;
        if (c==KeyEvent.VK_BACK_SPACE){
        ayu=txtPeso.getText().indexOf(KeyEvent.VK_COMMA);
        }
        if ((c==KeyEvent.VK_BACK_SPACE) && (ayu==-1)){
            setAux(false);
        }
    }//GEN-LAST:event_txtPesoKeyTyped

    private void txtEstaturaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtEstaturaKeyTyped
        // TODO add your handling code here:
         char c=evt.getKeyChar();
        
        if(!Character.isDigit(c) && (c!= KeyEvent.VK_COMMA)&& (c!=KeyEvent.VK_BACK_SPACE)  ){
            getToolkit().beep();
            evt.consume();
        }else
        if (c==KeyEvent.VK_COMMA ){
            if (c==KeyEvent.VK_COMMA && isAux2()==true){
            getToolkit().beep();
            evt.consume();
        }else
            setAux2(true);
        }
        
      int ayu=-1;
        if (c==KeyEvent.VK_BACK_SPACE){
        ayu=txtEstatura.getText().indexOf(KeyEvent.VK_COMMA);
        }
        if ((c==KeyEvent.VK_BACK_SPACE) && (ayu==-1)){
            setAux2(false);
        }
 
    }//GEN-LAST:event_txtEstaturaKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgActualizarDatosSalud.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgActualizarDatosSalud.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgActualizarDatosSalud.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgActualizarDatosSalud.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgActualizarDatosSalud dialog = new dlgActualizarDatosSalud(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JComboBox cmbTallaCalzado;
    private javax.swing.JComboBox cmbTallaCamisa;
    private javax.swing.JComboBox cmbTallaPantalon;
    private javax.swing.JComboBox cmbTipoSangre;
    public static javax.swing.ButtonGroup grbtnAntineumo;
    public static javax.swing.ButtonGroup grbtnEnfermedad;
    public static javax.swing.ButtonGroup grbtnInfluenza;
    public static javax.swing.ButtonGroup grbtnPenta;
    public static javax.swing.ButtonGroup grbtnPolio;
    public static javax.swing.ButtonGroup grtbAlergias;
    public static javax.swing.ButtonGroup grtbnCirugia;
    private javax.swing.JLabel inscripcion;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JLabel lblAlergia;
    private javax.swing.JLabel lblAntiamarilica1;
    private javax.swing.JLabel lblAntineumococo;
    private javax.swing.JLabel lblBCG1;
    private javax.swing.JLabel lblCedulaEstu1;
    private javax.swing.JLabel lblEstatura;
    private javax.swing.JLabel lblHepatitis1;
    private javax.swing.JLabel lblIndiqueTratamiento1;
    private javax.swing.JLabel lblIndiqueTratamiento3;
    private javax.swing.JLabel lblIndiqueTratamientoEnfermedad;
    private javax.swing.JLabel lblInfluenza1;
    private javax.swing.JLabel lblPentavalente;
    private javax.swing.JLabel lblPeso;
    private javax.swing.JLabel lblPolio;
    private javax.swing.JLabel lblSufreCirugia;
    private javax.swing.JLabel lblSufreEfermedad;
    private javax.swing.JLabel lblTallaCalzado;
    private javax.swing.JLabel lblTallaCamisa;
    private javax.swing.JLabel lblTallaPantalon;
    private javax.swing.JLabel lblTipoSangre1;
    private javax.swing.JLabel lblToxoide1;
    private javax.swing.JLabel lblTrivalente;
    private javax.swing.JRadioButton rbtnAlergiaNo;
    private javax.swing.JRadioButton rbtnAlergiaSi;
    private javax.swing.JRadioButton rbtnAntiamarilicaDosis1;
    private javax.swing.JRadioButton rbtnAntineumococoDosis1;
    private javax.swing.JRadioButton rbtnAntineumococoDosis2;
    private javax.swing.JRadioButton rbtnBCGDosis1;
    private javax.swing.JRadioButton rbtnCirugiaNo;
    private javax.swing.JRadioButton rbtnCirugiaSi;
    private javax.swing.JRadioButton rbtnEnfermedadNo;
    private javax.swing.JRadioButton rbtnEnfermedadSi;
    private javax.swing.JRadioButton rbtnHepatitisDosis1;
    private javax.swing.JRadioButton rbtnInfluenzaDosis1;
    private javax.swing.JRadioButton rbtnInfluenzaDosis2;
    private javax.swing.JRadioButton rbtnPentavalenteDosis1;
    private javax.swing.JRadioButton rbtnPentavalenteDosis2;
    private javax.swing.JRadioButton rbtnPentavalenteDosis3;
    private javax.swing.JRadioButton rbtnPolioDosis1;
    private javax.swing.JRadioButton rbtnPolioDosis2;
    private javax.swing.JRadioButton rbtnToxoideDosis1;
    private javax.swing.JRadioButton rbtnTrivalenteDosis1;
    public static javax.swing.JTextField txtCedula;
    private javax.swing.JTextField txtEstatura;
    private javax.swing.JTextArea txtIndiqueTratamiento;
    private javax.swing.JTextArea txtIndiqueTratamiento3;
    private javax.swing.JTextArea txtIndiqueTratamientoEnfermedad;
    private javax.swing.JTextField txtPeso;
    // End of variables declaration//GEN-END:variables
}
