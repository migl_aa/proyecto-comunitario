/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocomunitario;

import BD.conexion;
import com.sun.glass.events.KeyEvent;
import com.toedter.calendar.JCalendar;
import java.awt.GraphicsEnvironment;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.swing.JOptionPane;
import javax.swing.JSpinner;
import javax.swing.JTextField;

/**
 *
 * @author NANY
 */
public class dlgActulizarDatosFamiliares extends javax.swing.JDialog {

    /**
     * Creates new form dlgActulizarDatosFamiliares
     */
    public dlgActulizarDatosFamiliares(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        JCalendar nuevo3=txtFechaNaciPadre.getJCalendar();
                      JSpinner bloqueo3= (JSpinner) nuevo3.getYearChooser().getSpinner();
                      ((JTextField)bloqueo3.getEditor()).setEditable(false);
        txtCedulaEstu.setVisible(false);
        JCalendar nuevo2=txtFechaNaciMadre.getJCalendar();
                      JSpinner bloqueo2= (JSpinner) nuevo2.getYearChooser().getSpinner();
                      ((JTextField)bloqueo2.getEditor()).setEditable(false);
        this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
    }
    void actualizar(){
        conexion con=new conexion();
        Connection cn=con.conectar();
        String fechaNa,fechaNa2;
        String telfMa,telf2Ma,telfPa,telf2Pa;
        telfMa=cmbTelefonoMadre.getSelectedItem().toString()+"-"+txtTelefonoMadre.getText();
        telfPa=cmbTelefonoPadre.getSelectedItem().toString()+"-"+txtTelefonoPadre.getText();
        telf2Ma=cmbOtroTlfMadre.getSelectedItem().toString()+"-"+txtOtroTlfMadre.getText();
        telf2Pa=cmbOtroTlfPadre.getSelectedItem().toString()+"-"+txtOtroTlfPadre.getText();
        if ("Selec-".equals(telfMa)){
            telfMa="";
        }
        if ("Selec-".equals(telfPa)){
        
            telfPa="";
        }
        if ("Selec-".equals(telf2Pa)){
            telf2Pa="";
        }
        if ("Selec-".equals(telf2Ma)){
            telf2Ma="";
        }
        txtNombreMadre.setText(telfMa+"-"+telf2Ma+"-"+telfPa+"-"+telf2Pa);
        if (txtFechaNaciMadre.getDate()==null)
        {
            fechaNa="";
        }    else{
       String ano,mes,dia;
        ano=Integer.toString(txtFechaNaciMadre.getCalendar().get(Calendar.YEAR));
        mes=Integer.toString(txtFechaNaciMadre.getCalendar().get(Calendar.MONTH)+1);
        dia=Integer.toString(txtFechaNaciMadre.getCalendar().get(Calendar.DAY_OF_MONTH));
        fechaNa=dia+"/"+mes+"/"+ano;

        }
        if (txtFechaNaciPadre.getDate()==null)
        {
            fechaNa2="";
        }    else{
       String ano1,mes1,dia1;
        ano1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.YEAR));
        mes1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.MONTH)+1);
        dia1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.DAY_OF_MONTH));
        fechaNa2=dia1+"/"+mes1+"/"+ano1;

        }
        String consulta="UPDATE familiares SET nombre_fa='"+txtNombreMadre.getText()+"',apellido_fa='"+txtApellidoMadre.getText()+"',fechaNacimiento_fa='"+fechaNa+"',profesion_fa='"+txtProfesionMadre.getText()+"',tlf_fa='"+telfMa+"',otroTlf_fa='"+telf2Ma+"',direccion_fa='"+txtDireccionMadre.getText()+"',observacion_fa='"+txtObservacion.getText()+"' WHERE ced_fa='"+cmbCedulaMadre.getSelectedItem().toString()+"-"+txtCedulaMadre.getText()+"'";
        String consulta2="UPDATE familiares SET nombre_fa='"+txtNombrePadre.getText()+"',apellido_fa='"+txtApellidoPadre.getText()+"',fechaNacimiento_fa='"+fechaNa2+"',profesion_fa='"+txtProfesionPadre.getText()+"',tlf_fa='"+telfPa+"',otroTlf_fa='"+telf2Pa+"',direccion_fa='"+txtDireccionPadre.getText()+"',observacion_fa='"+txtObservacionPadre.getText()+"' WHERE ced_fa='"+cmbCedulaPadre.getSelectedItem().toString()+"-"+txtCedulaPadre.getText()+"'";
        String consultaPadre= "INSERT INTO familiares (ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,tlf_fa,otroTlf_fa,direccion_fa,observacion_fa ) VALUES ('"+cmbCedulaPadre.getSelectedItem().toString()+"-"+txtCedulaPadre.getText()+"','"+txtNombrePadre.getText()+"','"+txtApellidoPadre.getText()+"','"+fechaNa2+"','"+txtProfesionPadre.getText()+"','"+telfPa+"','"+telf2Pa+"','"+txtDireccionPadre.getText()+"','"+txtObservacionPadre.getText()+"')";
        String consultaMadre="INSERT INTO familiares (ced_fa,nombre_fa,apellido_fa,fechaNacimiento_fa,profesion_fa,tlf_fa,otroTlf_fa,direccion_fa,observacion_fa ) VALUES ('"+cmbCedulaMadre.getSelectedItem().toString()+"-"+txtCedulaMadre.getText()+"','"+txtNombreMadre.getText()+"','"+txtApellidoMadre.getText()+"','"+fechaNa2+"','"+txtProfesionMadre.getText()+"','"+telfPa+"','"+telf2Pa+"-"+txtOtroTlfMadre.getText()+"','"+txtDireccionMadre.getText()+"','"+txtObservacion.getText()+"')";
        String consultaRegistroPapa="INSERT INTO estu_fami (ced_estu,ced_fa,parentesco,Representado) VALUES ('"+txtCedulaEstu.getText()+"','"+txtCedulaPadre.getText()+"','Papá',0)";
        String consultaRegistroMama="INSERT INTO estu_fami (ced_estu,ced_fa,parentesco,Representado) VALUES ('"+txtCedulaEstu.getText()+"','"+txtCedulaMadre.getText()+"','Mamá',0)";
        try {
            if (txtCedulaMadre.getText()!=""){
            Statement st=cn.createStatement();
            st.execute(consulta);
            }else
            {
                Statement pt=cn.createStatement();
                pt.execute(consultaMadre);
                Statement mt=cn.createStatement();
                mt.execute(consultaRegistroMama);
            }
                    
            if (txtCedulaPadre.getText()!=""){
            Statement rs=cn.createStatement();
            rs.execute(consulta2);
            }else
            {
               Statement qs=cn.createStatement(); 
               qs.execute(consultaPadre);
              Statement ys=cn.createStatement(); 
              ys.execute(consultaRegistroPapa);
            }
            //Datos de la madre
         
            if (this.txtCedulaMadre.getText()!=""){
            frmDatosEstuActualizar.txtCedulaMadre.setText(cmbCedulaMadre.getSelectedItem().toString()+"-"+txtCedulaMadre.getText());
                   
            frmDatosEstuActualizar.txtNombresMadre.setText(this.txtNombreMadre.getText()+" "+this.txtApellidoMadre.getText());
                        if (txtFechaNaciMadre.getDate()==null)
        {
            fechaNa="";
        }    else{
       String ano,mes,dia;
        ano=Integer.toString(txtFechaNaciMadre.getCalendar().get(Calendar.YEAR));
        mes=Integer.toString(txtFechaNaciMadre.getCalendar().get(Calendar.MONTH)+1);
        dia=Integer.toString(txtFechaNaciMadre.getCalendar().get(Calendar.DAY_OF_MONTH));
        fechaNa=dia+"/"+mes+"/"+ano;

        }
                        
            frmDatosEstuActualizar.txtFechaNaciMadre.setText(fechaNa);
             frmDatosEstuActualizar.txtOcupacionMadre.setText(txtProfesionMadre.getText());
           
                     frmDatosEstuActualizar.txtTelefonoMadre.setText(telfMa);        
             frmDatosEstuActualizar.txtOtroTelefonoMadre.setText(telf2Ma);
             frmDatosEstuActualizar.txtDireccionMadre.setText(txtDireccionMadre.getText());
            
            }
//Datos padre
            if(this.txtCedulaPadre.getText()!=""){
                        frmDatosEstuActualizar.txtCedulaPadre.setText(cmbCedulaPadre.getSelectedItem().toString()+" "+txtCedulaPadre.getText());
                    frmDatosEstuActualizar.txtNombresPadre.setText(this.txtNombrePadre.getText()+" "+this.txtApellidoPadre.getText());
                    if(txtFechaNaciPadre.getDate()==null)
                    {
                        fechaNa2="";
                    }    else{
                        String ano1,mes1,dia1;
                        ano1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.YEAR));
                        mes1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.MONTH)+1);
                        dia1=Integer.toString(txtFechaNaciPadre.getCalendar().get(Calendar.DAY_OF_MONTH));
                        fechaNa2=dia1+"/"+mes1+"/"+ano1;
                    }
                     frmDatosEstuActualizar.txtFechaNaciPadre.setText(fechaNa2);
                     frmDatosEstuActualizar.txtOcupacionPadre.setText(txtProfesionPadre.getText());
                     frmDatosEstuActualizar.txtTelefonoPadre.setText(telfPa);
                     frmDatosEstuActualizar.txtOtroTelefonoPadre.setText(telf2Pa);
                     frmDatosEstuActualizar.txtDireccionPadre.setText(txtDireccionPadre.getText());
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
            }
    }
    void cargarDatos(String valor){
        conexion con=new conexion();
        Connection cn=con.conectar();
        String parentesco;
        String consulta="SELECT f.ced_fa,f.nombre_fa,f.apellido_fa,f.fechaNacimiento_fa,f.profesion_fa,f.direccion_fa,f.tlf_fa,f.otroTlf_fa,f.observacion_fa,d.parentesco FROM (familiares f, estu_fami d, estudiante e) WHERE f.ced_fa=d.ced_fa AND d.ced_estu='"+valor+"'";
        try {
            Statement st= cn.createStatement();
            ResultSet rs= st.executeQuery(consulta);
            while (rs.next()){
                parentesco=rs.getString("d.parentesco");
                txtNombreMadre.setText(parentesco);
                if (parentesco.equals("Mamá")){
                   String[]temp;
                   temp=rs.getString("f.ced_fa").split("-");
                   
                   cmbCedulaMadre.setSelectedItem(temp[0]);
                   txtCedulaMadre.setText(temp[1]);
                   cmbCedulaMadre.setEnabled(false);
                   txtCedulaMadre.setEnabled(false);
                   txtNombreMadre.setText(rs.getString("f.nombre_fa"));
                   txtApellidoMadre.setText(rs.getString("f.apellido_fa"));
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                     Date fechaDate;
                     fechaDate = formato.parse(rs.getString("f.fechaNacimiento_fa"));
                   txtFechaNaciMadre.setDate(fechaDate);
                     txtFechaNaciMadre.getDateEditor().setEnabled(false);
                     
                      txtProfesionMadre.setText(rs.getString("f.profesion_fa"));
                      String [] temp1={null,null};
                      String [] temp2;
                      String tlf,tlf2;
                tlf=rs.getString("f.tlf_fa");
                tlf2=rs.getString("f.otroTlf_fa");
                   temp1=tlf.split("-");
                   txtTelefonoMadre.setText(temp1[1]);
                     cmbTelefonoMadre.setSelectedItem(temp1[0]);
                      if (!tlf2.equals("")){
                    temp2=tlf2.split("-");
                    txtOtroTlfMadre.setText(temp2[1]);
                    cmbOtroTlfMadre.setSelectedItem(temp2[0]);
                    }
                      txtDireccionMadre.setText(rs.getString("f.direccion_fa"));
                      txtObservacion.setText(rs.getString("f.observacion_fa"));
                
                }else
                    if (parentesco.equals("Papá")){
                         String[]temp;
                   temp=rs.getString("f.ced_fa").split("-");
                   cmbCedulaPadre.setSelectedItem(temp[0]);
                   txtCedulaPadre.setText(temp[1]);
                   cmbCedulaPadre.setEnabled(false);
                   txtCedulaPadre.setEnabled(false);
                   txtNombrePadre.setText(rs.getString("f.nombre_fa"));
                   txtApellidoPadre.setText(rs.getString("f.apellido_fa"));
                    SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
                     Date fechaDate;
                     fechaDate = formato.parse(rs.getString("f.fechaNacimiento_fa"));
                   txtFechaNaciPadre.setDate(fechaDate);
                     txtFechaNaciPadre.getDateEditor().setEnabled(false);
                     
                      txtProfesionPadre.setText(rs.getString("f.profesion_fa"));
                      String [] temp1;
                      String [] temp2;
                      String tlf,tlf2;
                tlf=rs.getString("f.tlf_fa");
                tlf2=rs.getString("f.otroTlf_fa");
                   temp1=tlf.split("-");
                   txtTelefonoPadre.setText(temp1[1]);
                     cmbTelefonoPadre.setSelectedItem(temp1[0]);
                      if (!tlf2.equals("")){
                    temp2=tlf2.split("-");
                    txtOtroTlfPadre.setText(temp2[1]);
                    cmbOtroTlfPadre.setSelectedItem(temp2[0]);
                    }
                      txtDireccionPadre.setText(rs.getString("f.direccion_fa"));
                      txtObservacionPadre.setText(rs.getString("f.observacion_fa"));
                        
                    }
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
 
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new imagenes.Fondo("fondo escritorio.jpg");
        cmbOtroTlfPadre = new javax.swing.JComboBox();
        txtApellidoMadre = new javax.swing.JTextField();
        cmbOtroTlfMadre = new javax.swing.JComboBox();
        lblTelefonoPAdre = new javax.swing.JLabel();
        lblCedulaPadre = new javax.swing.JLabel();
        txtProfesionMadre = new javax.swing.JTextField();
        lblDireccionPadre = new javax.swing.JLabel();
        cmbTelefonoMadre = new javax.swing.JComboBox();
        txtOtroTlfPadre = new javax.swing.JTextField();
        cmbCedulaPadre = new javax.swing.JComboBox();
        cmbCedulaMadre = new javax.swing.JComboBox();
        lblDireccionMadre = new javax.swing.JLabel();
        lblOtroTlfMadre = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtFechaNaciMadre = new com.toedter.calendar.JDateChooser();
        lblNombrePadre = new javax.swing.JLabel();
        lblApellidoPadre = new javax.swing.JLabel();
        txtOtroTlfMadre = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtDireccionMadre = new javax.swing.JTextArea();
        txtCedulaMadre = new javax.swing.JTextField();
        txtProfesionPadre = new javax.swing.JTextField();
        txtApellidoPadre = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        txtNombrePadre = new javax.swing.JTextField();
        txtFechaNaciPadre = new com.toedter.calendar.JDateChooser();
        txtTelefonoPadre = new javax.swing.JTextField();
        txtNombreMadre = new javax.swing.JTextField();
        lblProfesionMadre = new javax.swing.JLabel();
        inscripcion = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtDireccionPadre = new javax.swing.JTextArea();
        lblCedulaMadre = new javax.swing.JLabel();
        lblNombreMadre = new javax.swing.JLabel();
        cmbTelefonoPadre = new javax.swing.JComboBox();
        lblOtroTlfPadre = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lblFechaNaciPadre = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        lblApellidoMadre = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lblTelefonoMadre = new javax.swing.JLabel();
        txtCedulaPadre = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        lblFechaNaciMadre = new javax.swing.JLabel();
        lblProfesionPadre = new javax.swing.JLabel();
        txtTelefonoMadre = new javax.swing.JTextField();
        jPanel2 = new javax.swing.JPanel();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        btnModificar = new javax.swing.JButton();
        btnCancelar = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        txtObservacionPadre = new javax.swing.JTextField();
        jLabel7 = new javax.swing.JLabel();
        txtObservacion = new javax.swing.JTextField();
        txtCedulaEstu = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cmbOtroTlfPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbOtroTlfPadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbOtroTlfPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbOtroTlfPadreActionPerformed(evt);
            }
        });
        jPanel1.add(cmbOtroTlfPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 560, -1, -1));

        txtApellidoMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtApellidoMadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoMadreKeyTyped(evt);
            }
        });
        jPanel1.add(txtApellidoMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(700, 200, 160, -1));

        cmbOtroTlfMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbOtroTlfMadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbOtroTlfMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbOtroTlfMadreActionPerformed(evt);
            }
        });
        jPanel1.add(cmbOtroTlfMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 280, -1, -1));

        lblTelefonoPAdre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTelefonoPAdre.setText("Teléfono");
        jPanel1.add(lblTelefonoPAdre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 560, -1, -1));

        lblCedulaPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaPadre.setText("Cédula");
        jPanel1.add(lblCedulaPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 440, -1, -1));

        txtProfesionMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtProfesionMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtProfesionMadreActionPerformed(evt);
            }
        });
        jPanel1.add(txtProfesionMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 240, 180, -1));

        lblDireccionPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblDireccionPadre.setText("Dirección de Habitación");
        jPanel1.add(lblDireccionPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 600, -1, -1));

        cmbTelefonoMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTelefonoMadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbTelefonoMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTelefonoMadreActionPerformed(evt);
            }
        });
        jPanel1.add(cmbTelefonoMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 280, -1, -1));

        txtOtroTlfPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtOtroTlfPadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOtroTlfPadreKeyTyped(evt);
            }
        });
        jPanel1.add(txtOtroTlfPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 560, 90, -1));

        cmbCedulaPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbCedulaPadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "V", "E" }));
        jPanel1.add(cmbCedulaPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 440, -1, -1));

        cmbCedulaMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbCedulaMadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "V", "E" }));
        jPanel1.add(cmbCedulaMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 160, -1, -1));

        lblDireccionMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblDireccionMadre.setText("Dirección de Habitación");
        jPanel1.add(lblDireccionMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 330, -1, -1));

        lblOtroTlfMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblOtroTlfMadre.setText("Otro Teléfono");
        jPanel1.add(lblOtroTlfMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 280, -1, -1));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar estudiante.png"))); // NOI18N
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 10, 150, 140));

        txtFechaNaciMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(txtFechaNaciMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 240, 140, 30));

        lblNombrePadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblNombrePadre.setText("Nombre");
        jPanel1.add(lblNombrePadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 480, -1, 20));

        lblApellidoPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblApellidoPadre.setText("Apellido");
        jPanel1.add(lblApellidoPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 480, -1, -1));

        txtOtroTlfMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtOtroTlfMadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtOtroTlfMadreKeyTyped(evt);
            }
        });
        jPanel1.add(txtOtroTlfMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 280, 100, -1));

        txtDireccionMadre.setColumns(20);
        txtDireccionMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        txtDireccionMadre.setRows(5);
        jScrollPane1.setViewportView(txtDireccionMadre);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 320, 500, 48));

        txtCedulaMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedulaMadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaMadreKeyTyped(evt);
            }
        });
        jPanel1.add(txtCedulaMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 160, 100, -1));

        txtProfesionPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(txtProfesionPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 520, 190, -1));

        txtApellidoPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtApellidoPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtApellidoPadreActionPerformed(evt);
            }
        });
        txtApellidoPadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtApellidoPadreKeyTyped(evt);
            }
        });
        jPanel1.add(txtApellidoPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 480, 190, -1));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 430, 760, 10));

        txtNombrePadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtNombrePadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombrePadreKeyTyped(evt);
            }
        });
        jPanel1.add(txtNombrePadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 480, 170, -1));

        txtFechaNaciPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(txtFechaNaciPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 520, 140, 30));

        txtTelefonoPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtTelefonoPadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoPadreKeyTyped(evt);
            }
        });
        jPanel1.add(txtTelefonoPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 560, 100, -1));

        txtNombreMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtNombreMadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtNombreMadreActionPerformed(evt);
            }
        });
        txtNombreMadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtNombreMadreKeyTyped(evt);
            }
        });
        jPanel1.add(txtNombreMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 200, 160, -1));

        lblProfesionMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblProfesionMadre.setText("Profesión u Ocupación");
        jPanel1.add(lblProfesionMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 240, -1, 20));

        inscripcion.setFont(new java.awt.Font("Comfortaa", 1, 60)); // NOI18N
        inscripcion.setText("Actualización");
        jPanel1.add(inscripcion, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 10, -1, 80));

        txtDireccionPadre.setColumns(20);
        txtDireccionPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        txtDireccionPadre.setRows(5);
        jScrollPane2.setViewportView(txtDireccionPadre);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 590, 500, 50));

        lblCedulaMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaMadre.setText("Cédula");
        jPanel1.add(lblCedulaMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 160, -1, -1));

        lblNombreMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblNombreMadre.setText("Nombre");
        jPanel1.add(lblNombreMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 200, -1, -1));

        cmbTelefonoPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTelefonoPadre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0251", "0414", "0424", "0416", "0426", "0412" }));
        cmbTelefonoPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbTelefonoPadreActionPerformed(evt);
            }
        });
        jPanel1.add(cmbTelefonoPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 560, -1, 30));

        lblOtroTlfPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblOtroTlfPadre.setText("Otro Teléfono");
        jPanel1.add(lblOtroTlfPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 560, -1, -1));

        jLabel3.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel3.setText("Datos");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 90, -1, -1));

        lblFechaNaciPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblFechaNaciPadre.setText("Fecha de Nacimiento");
        jPanel1.add(lblFechaNaciPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 520, -1, -1));

        jLabel2.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel2.setText("Datos del Padre");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 410, -1, 20));

        lblApellidoMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblApellidoMadre.setText("Apellido");
        jPanel1.add(lblApellidoMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 200, -1, -1));

        jLabel6.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel6.setText("Datos de la Madre");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 130, -1, -1));

        lblTelefonoMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTelefonoMadre.setText("Teléfono");
        jPanel1.add(lblTelefonoMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 280, -1, -1));

        txtCedulaPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedulaPadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaPadreKeyTyped(evt);
            }
        });
        jPanel1.add(txtCedulaPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 440, 100, -1));

        jLabel5.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel5.setText("FAMILIARES");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 96, -1, -1));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 150, 760, 10));

        lblFechaNaciMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblFechaNaciMadre.setText("Fecha de Nacimiento");
        jPanel1.add(lblFechaNaciMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 240, -1, -1));

        lblProfesionPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblProfesionPadre.setText("Profesión u Ocupación");
        jPanel1.add(lblProfesionPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 520, -1, -1));

        txtTelefonoMadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtTelefonoMadre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtTelefonoMadreKeyTyped(evt);
            }
        });
        jPanel1.add(txtTelefonoMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 280, 100, -1));

        jPanel2.setBackground(new java.awt.Color(0, 153, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnModificar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(255, 255, 255));
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pen.png"))); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.setBorder(null);
        btnModificar.setContentAreaFilled(false);
        btnModificar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/modificar icono.png"))); // NOI18N
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar (2).png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(null);
        btnCancelar.setContentAreaFilled(false);
        btnCancelar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(88, 88, 88)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnModificar)
                .addGap(7, 7, 7)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnCancelar)
                .addGap(7, 7, 7)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(518, Short.MAX_VALUE))
        );

        jPanel1.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 220, 720));

        jLabel1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel1.setText("Observación");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 650, -1, -1));

        txtObservacionPadre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtObservacionPadre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtObservacionPadreActionPerformed(evt);
            }
        });
        jPanel1.add(txtObservacionPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 650, 540, -1));

        jLabel7.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel7.setText("Observación");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 380, -1, -1));

        txtObservacion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtObservacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtObservacionActionPerformed(evt);
            }
        });
        jPanel1.add(txtObservacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 380, 570, -1));
        jPanel1.add(txtCedulaEstu, new org.netbeans.lib.awtextra.AbsoluteConstraints(807, 188, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtProfesionMadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtProfesionMadreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtProfesionMadreActionPerformed

    private void txtApellidoPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtApellidoPadreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtApellidoPadreActionPerformed

    private void txtNombreMadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtNombreMadreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtNombreMadreActionPerformed

    private void txtObservacionPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtObservacionPadreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtObservacionPadreActionPerformed

    private void cmbTelefonoMadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTelefonoMadreActionPerformed
        // TODO add your handling code here:
        if (cmbTelefonoMadre.getSelectedIndex()>0){
            txtTelefonoMadre.setEnabled(true);
        }
        else
        {
            txtTelefonoMadre.setText("");
            txtTelefonoMadre.setEnabled(false);
        }
    }//GEN-LAST:event_cmbTelefonoMadreActionPerformed

    private void cmbOtroTlfMadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbOtroTlfMadreActionPerformed
        // TODO add your handling code here:
        if (cmbOtroTlfMadre.getSelectedIndex()>0){
            txtOtroTlfMadre.setEnabled(true);
        }
        else
        {
            txtOtroTlfMadre.setText("");
            txtOtroTlfMadre.setEnabled(false);
        }
    }//GEN-LAST:event_cmbOtroTlfMadreActionPerformed

    private void cmbOtroTlfPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbOtroTlfPadreActionPerformed
        // TODO add your handling code here:
        if (cmbOtroTlfPadre.getSelectedIndex()>0){
            txtOtroTlfPadre.setEnabled(true);
        }
        else
        {
            txtOtroTlfPadre.setText("");
            txtOtroTlfPadre.setEnabled(false);
        }
    }//GEN-LAST:event_cmbOtroTlfPadreActionPerformed

    private void cmbTelefonoPadreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbTelefonoPadreActionPerformed
        // TODO add your handling code here:
        if (cmbTelefonoPadre.getSelectedIndex()>0){
            txtTelefonoPadre.setEnabled(true);
        }
        else
        {
            txtTelefonoPadre.setText("");
            txtTelefonoPadre.setEnabled(false);
        }
    }//GEN-LAST:event_cmbTelefonoPadreActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed

        // TODO add your handling code here:
        this.dispose();
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        // TODO add your handling code here:
        if((((txtCedulaMadre.getText()!="") && (txtNombreMadre.getText()!="") && (txtApellidoMadre.getText()!="")) || (txtCedulaMadre.getText()=="")) && ((txtCedulaPadre.getText()!="" && txtNombrePadre.getText()!="" && txtApellidoPadre.getText()!="") || (txtCedulaPadre.getText()==""))){
            actualizar();
            this.dispose();
        }else{
            dlgDatosVacios dialog = new dlgDatosVacios(new javax.swing.JFrame(), true);
            dialog.setVisible(true);
        } 
        
            
        
    }//GEN-LAST:event_btnModificarActionPerformed

    private void txtCedulaMadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaMadreKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
         if(txtCedulaMadre.getText().length()>=9){
            evt.consume();
        }
        if(!Character.isDigit(c) && c!=KeyEvent.VK_ENTER && c!=KeyEvent.VK_BACKSPACE) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_txtCedulaMadreKeyTyped

    private void txtCedulaPadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaPadreKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
         if(txtCedulaPadre.getText().length()>=9){
            evt.consume();
        }
        if(!Character.isDigit(c) && c!=KeyEvent.VK_ENTER && c!=KeyEvent.VK_BACKSPACE) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_txtCedulaPadreKeyTyped

    private void txtNombreMadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombreMadreKeyTyped
        // TODO add your handling code here:
        char c=evt.getKeyChar();
        if(!Character.isLetter(c) && c!=KeyEvent.VK_ENTER && c!=KeyEvent.VK_BACKSPACE) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_txtNombreMadreKeyTyped

    private void txtApellidoMadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoMadreKeyTyped
        // TODO add your handling code here:
         char c=evt.getKeyChar();
        if(!Character.isLetter(c) && c!=KeyEvent.VK_ENTER && c!=KeyEvent.VK_BACKSPACE) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_txtApellidoMadreKeyTyped

    private void txtTelefonoMadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoMadreKeyTyped
        // TODO add your handling code here:
        if(txtTelefonoMadre.getText().length()>=7){
            evt.consume();
        }
        
    }//GEN-LAST:event_txtTelefonoMadreKeyTyped

    private void txtOtroTlfMadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOtroTlfMadreKeyTyped
        // TODO add your handling code here:
        if(txtOtroTlfMadre.getText().length()>=7){
            evt.consume();
        }
    }//GEN-LAST:event_txtOtroTlfMadreKeyTyped

    private void txtTelefonoPadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtTelefonoPadreKeyTyped
         // TODO add your handling code here:
        if(txtTelefonoPadre.getText().length()>=7){
            evt.consume();
        }
    }//GEN-LAST:event_txtTelefonoPadreKeyTyped

    private void txtOtroTlfPadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtOtroTlfPadreKeyTyped
        // TODO add your handling code here:
        if(txtOtroTlfPadre.getText().length()>=7){
            evt.consume();
        }
    }//GEN-LAST:event_txtOtroTlfPadreKeyTyped

    private void txtNombrePadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtNombrePadreKeyTyped
        // TODO add your handling code here:
         char c=evt.getKeyChar();
        if(!Character.isLetter(c) && c!=KeyEvent.VK_ENTER && c!=KeyEvent.VK_BACKSPACE) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_txtNombrePadreKeyTyped

    private void txtApellidoPadreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtApellidoPadreKeyTyped
         // TODO add your handling code here:
         char c=evt.getKeyChar();
        if(!Character.isLetter(c) && c!=KeyEvent.VK_ENTER && c!=KeyEvent.VK_BACKSPACE) {
            getToolkit().beep();
            evt.consume();
        }
    }//GEN-LAST:event_txtApellidoPadreKeyTyped

    private void txtObservacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtObservacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtObservacionActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(dlgActulizarDatosFamiliares.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(dlgActulizarDatosFamiliares.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(dlgActulizarDatosFamiliares.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(dlgActulizarDatosFamiliares.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                dlgActulizarDatosFamiliares dialog = new dlgActulizarDatosFamiliares(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JComboBox cmbCedulaMadre;
    private javax.swing.JComboBox cmbCedulaPadre;
    private javax.swing.JComboBox cmbOtroTlfMadre;
    private javax.swing.JComboBox cmbOtroTlfPadre;
    private javax.swing.JComboBox cmbTelefonoMadre;
    private javax.swing.JComboBox cmbTelefonoPadre;
    private javax.swing.JLabel inscripcion;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JLabel lblApellidoMadre;
    private javax.swing.JLabel lblApellidoPadre;
    private javax.swing.JLabel lblCedulaMadre;
    private javax.swing.JLabel lblCedulaPadre;
    private javax.swing.JLabel lblDireccionMadre;
    private javax.swing.JLabel lblDireccionPadre;
    private javax.swing.JLabel lblFechaNaciMadre;
    private javax.swing.JLabel lblFechaNaciPadre;
    private javax.swing.JLabel lblNombreMadre;
    private javax.swing.JLabel lblNombrePadre;
    private javax.swing.JLabel lblOtroTlfMadre;
    private javax.swing.JLabel lblOtroTlfPadre;
    private javax.swing.JLabel lblProfesionMadre;
    private javax.swing.JLabel lblProfesionPadre;
    private javax.swing.JLabel lblTelefonoMadre;
    private javax.swing.JLabel lblTelefonoPAdre;
    private javax.swing.JTextField txtApellidoMadre;
    private javax.swing.JTextField txtApellidoPadre;
    public static javax.swing.JLabel txtCedulaEstu;
    private javax.swing.JTextField txtCedulaMadre;
    private javax.swing.JTextField txtCedulaPadre;
    private javax.swing.JTextArea txtDireccionMadre;
    private javax.swing.JTextArea txtDireccionPadre;
    private com.toedter.calendar.JDateChooser txtFechaNaciMadre;
    private com.toedter.calendar.JDateChooser txtFechaNaciPadre;
    private javax.swing.JTextField txtNombreMadre;
    private javax.swing.JTextField txtNombrePadre;
    private javax.swing.JTextField txtObservacion;
    private javax.swing.JTextField txtObservacionPadre;
    private javax.swing.JTextField txtOtroTlfMadre;
    private javax.swing.JTextField txtOtroTlfPadre;
    private javax.swing.JTextField txtProfesionMadre;
    private javax.swing.JTextField txtProfesionPadre;
    private javax.swing.JTextField txtTelefonoMadre;
    private javax.swing.JTextField txtTelefonoPadre;
    // End of variables declaration//GEN-END:variables
}
