/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proyectocomunitario;

import BD.conexion;
import java.awt.GraphicsEnvironment;
import java.awt.event.ItemEvent;
import java.awt.event.KeyEvent;
import java.net.URL;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


/**
 *
 * @author Miguel Arroyo
 */
public class frmInscribirReingreso extends javax.swing.JFrame {
 DefaultTableModel modelo1;
    /**
     * Creates new form frmInscribirReingreso
     */
    public frmInscribirReingreso() {
        initComponents();
       grupoBotones.add(rbtnAprobado);
       grupoBotones.add(rbtnRepitiente);
       this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
       txtCedulaRepre.setText("");
       cmbRepresentado.setEnabled(false);
       jButton2.setEnabled(false);
       rbtnAprobado.setEnabled(false);
       rbtnRepitiente.setEnabled(false);
       txtGrado.setEnabled(false);
       cmbSeccion.setEnabled(false);
       txtCondicion.setVisible(false);
       txtCedulaEstu.setVisible(false);
       txtGradoEstu.setVisible(false);
       btnSiguiente.setEnabled(false);
       txtParentesco.setVisible(false);
    }
    public static boolean aux=false;

    public static boolean isAux() {
        return aux;
    }

    public static void setAux(boolean aux) {
        frmInscribirReingreso.aux = aux;
    }
    
    
     public int matriculaSeccion (String grado, String seccion){
            conexion conn=new conexion();
            Connection cn=conn.conectar();
            int matricula = 0;
            String query =("SELECT matricula FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                matricula=rs.getInt("matricula");
                }
            } catch (Exception e) {
            }
            return (matricula);
            
        }
        
        public int matriculaDisponible (String grado, String seccion){
       conexion conn=new conexion();
            Connection cn=conn.conectar();
            int inscritos = 0;
            String query =("SELECT matricula_inscrita FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                inscritos=rs.getInt("matricula_inscrita");
                }
            } catch (Exception e) {
            }
            return inscritos;
            
        }
        
     public String getPeriodoEscolar (String grado, String seccion){
         conexion conn=new conexion();
            Connection cn=conn.conectar();
            String periodo=null;
            String query =("SELECT periodo_escolar FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                 Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                    periodo=rs.getString("periodo_escolar");
                }
                
            } catch (Exception e) {
            }
            return (periodo);
     }
    public String getNombreProf (String grado, String seccion){
       conexion conn= new conexion();
       Connection cn = conn.conectar();
       String nombre=null, cedula=null,NombreP=null;
       String query = ("SELECT nombreProf, CedulaProf FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
       try {
           Statement st = cn.createStatement();
           ResultSet rs= st.executeQuery(query);
           while (rs.next()){
               nombre=rs.getString("nombreProf");
               cedula=rs.getString("CedulaProf");
               
           }
           NombreP= cedula+" "+nombre;
       }catch (Exception e){
           
       }
       return(NombreP);
     }  
    public String getTurno (String grado, String seccion){
            conexion conn=new conexion();
            Connection cn=conn.conectar();
            String turno=null;
            String query =("SELECT turno FROM secciongrado WHERE grado='"+grado+"' AND seccion='"+seccion+"'");
            try {
                 Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                    turno=rs.getString("turno");
                }
                
            } catch (Exception e) {
            }
            return (turno);
            
        }
    void cargarRepresentado (){
         
        conexion conn=new conexion();
        Connection cn=conn.conectar();
        String cedula="",verificar="";
        cedula = cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText();
        String query2;
        query2="SELECT ced_fa FROM familiares WHERE ced_fa='"+cedula+"'";
        try {
             Statement st=cn.createStatement();
            ResultSet rs= st.executeQuery(query2);
            while (rs.next()){
                verificar=rs.getString("ced_fa");
            }
           if (!verificar.equals("")){
                //lblExiste.setVisible(true);
               cmbRepresentado.setEnabled(true);
               jButton2.setEnabled(true);
               cargarCombo();
         
              URL url = this.getClass().getResource("/imagenes/existe.png");  
                    ImageIcon icon = new ImageIcon(url);
                    lblVerificar.setIcon(icon);
           }
           else
           if (verificar.equals("")){
               
               cmbRepresentado.setEnabled(false);
               jButton2.setEnabled(false);
               btnSiguiente.setEnabled(false);
               
                URL url = this.getClass().getResource("/imagenes/no existe.png");  
                    ImageIcon icon = new ImageIcon(url);
                    lblVerificar.setIcon(icon);
           }
        } catch (Exception e) {
        JOptionPane.showMessageDialog(null, e);
        }  
        
         
      }
      void cargarCombo () {
          conexion con=new conexion();
        Connection cn=con.conectar();
          String [] iniciar = {"Selec Representado"};
        cmbRepresentado.setModel(new DefaultComboBoxModel(iniciar));
        
         
                //se utiliza el metodo GetSeccion para cargar el modelo de las seccion existentes 
                 
        
           String query="";
           query="SELECT e.ced_estu,e.primerNombre_estu,e.PrimerApellido_estu,e.grado_estu,e.estado FROM (estudiante e,familiares f,estu_fami d) WHERE (f.ced_fa='"+cmbCedulaRepre.getSelectedItem().toString()+"-"+txtCedulaRepre.getText()+"') AND (d.ced_fa=f.ced_fa) AND (e.ced_estu=d.ced_estu) AND (d.Representado=1) AND (e.estado_logico=1)";
           try {
              
          
                
                Statement stm=cn.createStatement();
                ResultSet rst= stm.executeQuery(query);
                while (rst.next()){
                //Tomo lo   s valores correspondientes de cada seccion segun el grado
              
                cmbRepresentado.addItem(rst.getString("e.estado")+" Grado-"+rst.getString("e.grado_estu")+" "+rst.getString("e.ced_estu")+" "+rst.getString("e.primerNombre_estu")+" "+rst.getString("e.PrimerApellido_estu"));
                   
                }
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(null, e);
          }
      
      }
      void cargarTabla(String valor){
          String titulos []= {"Periodo Escolar","Grado", "Sección","Turno","Profesor","Condicion"};
        String registros[]= new String [6];
        modelo1 = new DefaultTableModel(null, titulos){
            @Override
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            } 
        };
        
        conexion con=new conexion();
        Connection cn=con.conectar();
        
        String query="SELECT periodo_escolar,seccion,grado,turno,condicion,Profesor FROM estu_año WHERE ced_estu='"+valor+"'";
        
        try {
            Statement st=cn.createStatement();
            ResultSet rs= st.executeQuery(query);
            while(rs.next()){
                registros[0]=rs.getString("periodo_escolar");
                registros[1]=rs.getString("grado");
                registros[2]=rs.getString("seccion");
                registros[3]=rs.getString("turno");
                registros[4]=rs.getString("Profesor");
                registros[5]=rs.getString("condicion");
                 modelo1.addRow(registros);
            }
            tblHistorial.setModel(modelo1);
            
            
        } catch (SQLException ex) {
            
            JOptionPane.showMessageDialog(null,ex);
        }
      }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        grupoBotones = new javax.swing.ButtonGroup();
        jPanel1 = new imagenes.Fondo("fondo color.jpg");
        inscripcion = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblCedulaEstudiante = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        txtCedulaRepre = new javax.swing.JTextField();
        cmbCedulaRepre = new javax.swing.JComboBox();
        lblCedulaRepre = new javax.swing.JLabel();
        btnVerificarRepre = new javax.swing.JButton();
        lblGrado = new javax.swing.JLabel();
        txtGrado = new javax.swing.JTextField();
        lblCondicion = new javax.swing.JLabel();
        rbtnAprobado = new javax.swing.JRadioButton();
        rbtnRepitiente = new javax.swing.JRadioButton();
        lblSeccion = new javax.swing.JLabel();
        lblTurno = new javax.swing.JLabel();
        cmbSeccion = new javax.swing.JComboBox();
        txtTurno = new javax.swing.JLabel();
        lblHistorial = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblHistorial = new javax.swing.JTable();
        btnSiguiente = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        cmbRepresentado = new javax.swing.JComboBox();
        jPanel2 = new javax.swing.JPanel();
        btnAtras = new javax.swing.JButton();
        btnMenuPrincipal = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        btnRegistrarRepre = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();
        txtGradoEstu = new javax.swing.JLabel();
        txtCedulaEstu = new javax.swing.JLabel();
        txtCondicion = new javax.swing.JLabel();
        lblProfesor = new javax.swing.JLabel();
        txtProfesor = new javax.swing.JLabel();
        lblAñoEscolar = new javax.swing.JLabel();
        txtAñoEscolar = new javax.swing.JLabel();
        lblDisponibilidad = new javax.swing.JLabel();
        lblIconoDisponibilidad = new javax.swing.JLabel();
        lblVerificar = new javax.swing.JLabel();
        txtParentesco = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        inscripcion.setFont(new java.awt.Font("Comfortaa", 1, 60)); // NOI18N
        inscripcion.setText("Inscripción");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar estudiante.png"))); // NOI18N

        jLabel3.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel3.setText("INGRESO");

        jLabel1.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel1.setText("Re");

        lblCedulaEstudiante.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaEstudiante.setText("Cédula del Estudiante");

        txtCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedulaRepre.setText(" ");
        txtCedulaRepre.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txtCedulaRepreFocusGained(evt);
            }
        });
        txtCedulaRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedulaRepreActionPerformed(evt);
            }
        });
        txtCedulaRepre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtCedulaRepreKeyTyped(evt);
            }
        });

        cmbCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbCedulaRepre.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "V", "E" }));
        cmbCedulaRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbCedulaRepreActionPerformed(evt);
            }
        });

        lblCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaRepre.setText("Cédula del Representante");

        btnVerificarRepre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        btnVerificarRepre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Verificar.png"))); // NOI18N
        btnVerificarRepre.setText("Verificar");
        btnVerificarRepre.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar icono.png"))); // NOI18N
        btnVerificarRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnVerificarRepreActionPerformed(evt);
            }
        });

        lblGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblGrado.setText("Grado:");

        txtGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtGrado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtGradoActionPerformed(evt);
            }
        });

        lblCondicion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCondicion.setText("Condición:");

        grupoBotones.add(rbtnAprobado);
        rbtnAprobado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnAprobado.setText("Aprobado");
        rbtnAprobado.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rbtnAprobadoItemStateChanged(evt);
            }
        });
        rbtnAprobado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnAprobadoActionPerformed(evt);
            }
        });

        grupoBotones.add(rbtnRepitiente);
        rbtnRepitiente.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        rbtnRepitiente.setText("Repitiente");
        rbtnRepitiente.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                rbtnRepitienteItemStateChanged(evt);
            }
        });
        rbtnRepitiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                rbtnRepitienteActionPerformed(evt);
            }
        });

        lblSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSeccion.setText("Sección:");

        lblTurno.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTurno.setText("Turno:");

        cmbSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbSeccion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "" }));
        cmbSeccion.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbSeccionItemStateChanged(evt);
            }
        });
        cmbSeccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbSeccionActionPerformed(evt);
            }
        });

        txtTurno.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        lblHistorial.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        lblHistorial.setText("Historial");

        tblHistorial.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        tblHistorial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblHistorial);

        btnSiguiente.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnSiguiente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/siguiente icono.png"))); // NOI18N
        btnSiguiente.setText("Siguiente");
        btnSiguiente.setBorder(null);
        btnSiguiente.setContentAreaFilled(false);
        btnSiguiente.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/siguiiente.png"))); // NOI18N
        btnSiguiente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSiguienteActionPerformed(evt);
            }
        });

        jLabel2.setFont(new java.awt.Font("Comfortaa", 0, 20)); // NOI18N
        jLabel2.setText("Datos para la inscripción");

        cmbRepresentado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbRepresentado.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                cmbRepresentadoItemStateChanged(evt);
            }
        });
        cmbRepresentado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbRepresentadoActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(0, 153, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnAtras.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnAtras.setForeground(new java.awt.Color(255, 255, 255));
        btnAtras.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/check.png"))); // NOI18N
        btnAtras.setText("Tipo de Inscripción");
        btnAtras.setBorder(null);
        btnAtras.setContentAreaFilled(false);
        btnAtras.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/inscripcion.png"))); // NOI18N
        btnAtras.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAtrasActionPerformed(evt);
            }
        });

        btnMenuPrincipal.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnMenuPrincipal.setForeground(new java.awt.Color(255, 255, 255));
        btnMenuPrincipal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnMenuPrincipal.setText(" Menú Principal");
        btnMenuPrincipal.setBorder(null);
        btnMenuPrincipal.setContentAreaFilled(false);
        btnMenuPrincipal.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/home.png"))); // NOI18N
        btnMenuPrincipal.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnMenuPrincipalActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(0, 0, Short.MAX_VALUE))
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(btnAtras, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnMenuPrincipal, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(20, 20, 20))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(80, 80, 80)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(btnAtras))
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 411, Short.MAX_VALUE)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addComponent(btnMenuPrincipal)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(67, 67, 67))
        );

        btnRegistrarRepre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        btnRegistrarRepre.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add.png"))); // NOI18N
        btnRegistrarRepre.setText("Registrar");
        btnRegistrarRepre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarRepreActionPerformed(evt);
            }
        });

        jButton2.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        jButton2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo pequeño.png"))); // NOI18N
        jButton2.setText("Agregar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        lblProfesor.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblProfesor.setText("Profesor");

        txtProfesor.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        lblAñoEscolar.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblAñoEscolar.setText("Año Escolar:");

        txtAñoEscolar.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        lblDisponibilidad.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblDisponibilidad.setText("Disponibilidad:");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 89, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(272, 272, 272)
                                .addComponent(jLabel1)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel3))
                            .addComponent(inscripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jSeparator1)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblCondicion)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rbtnAprobado)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(rbtnRepitiente)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtParentesco)
                                .addGap(91, 91, 91)
                                .addComponent(txtGradoEstu)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(txtCedulaEstu))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(151, 151, 151)
                                .addComponent(txtCondicion))))
                    .addComponent(lblHistorial)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(lblGrado)
                            .addComponent(lblProfesor))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(txtGrado, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(lblSeccion)
                                .addGap(18, 18, 18)
                                .addComponent(cmbSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblTurno)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(txtProfesor, javax.swing.GroupLayout.PREFERRED_SIZE, 425, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblAñoEscolar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txtAñoEscolar, javax.swing.GroupLayout.PREFERRED_SIZE, 79, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(lblDisponibilidad)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lblIconoDisponibilidad, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblCedulaEstudiante)
                        .addGap(18, 18, 18)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(btnSiguiente, javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addComponent(cmbRepresentado, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE))))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(lblCedulaRepre)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(cmbCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lblVerificar, javax.swing.GroupLayout.DEFAULT_SIZE, 34, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btnVerificarRepre)
                        .addGap(18, 18, 18)
                        .addComponent(btnRegistrarRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 123, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(79, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(inscripcion, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(65, 65, 65)
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel1)
                                .addComponent(jLabel3)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(lblCedulaRepre)
                                .addComponent(cmbCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(txtCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnVerificarRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(btnRegistrarRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(lblVerificar, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(24, 24, 24)
                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 162, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(26, 26, 26)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(cmbRepresentado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCedulaEstudiante)
                    .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(rbtnAprobado)
                            .addComponent(lblCondicion)
                            .addComponent(rbtnRepitiente)
                            .addComponent(txtGradoEstu)
                            .addComponent(txtCedulaEstu)
                            .addComponent(txtParentesco)))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtCondicion)))
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(txtGrado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblGrado)
                            .addComponent(lblSeccion)
                            .addComponent(cmbSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(lblTurno))
                        .addComponent(txtTurno, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblAñoEscolar)
                        .addComponent(txtAñoEscolar, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(lblProfesor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(txtProfesor, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblDisponibilidad)
                        .addComponent(lblIconoDisponibilidad, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(27, 27, 27)
                .addComponent(lblHistorial)
                .addGap(1, 1, 1)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btnSiguiente)
                .addGap(39, 39, 39))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbCedulaRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbCedulaRepreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbCedulaRepreActionPerformed

    private void txtGradoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtGradoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtGradoActionPerformed

    private void cmbSeccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbSeccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbSeccionActionPerformed

    private void rbtnRepitienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnRepitienteActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnRepitienteActionPerformed

    private void btnSiguienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSiguienteActionPerformed
        // TODO add your handling code here:
        frmDatosEstuActualizar datos= new frmDatosEstuActualizar();
        datos.setVisible(true);
        datos.cargar(txtCedulaEstu.getText());
        datos.cargarTabla(txtCedulaEstu.getText());
        frmDatosEstuActualizar.txtCedulaEstu.setText(txtCedulaEstu.getText());
        frmDatosEstuActualizar.txtCedulaRepre.setText(txtCedulaRepre.getText());
        frmDatosEstuActualizar.txtCondicion.setText(txtCondicion.getText());
        frmDatosEstuActualizar.txtGrado.setText(txtGrado.getText());
        frmDatosEstuActualizar.txtSeccion.setText(cmbSeccion.getSelectedItem().toString());
        frmDatosEstuActualizar.txtTurno.setText(txtTurno.getText());
        frmDatosEstuActualizar.txtProfesor.setText(txtProfesor.getText());
        frmDatosEstuActualizar.txtAñoEscolar.setText(this.txtAñoEscolar.getText());
        this.dispose();
        
    }//GEN-LAST:event_btnSiguienteActionPerformed

    private void btnMenuPrincipalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnMenuPrincipalActionPerformed
        // TODO add your handling code here:
        menuInicio obj= new menuInicio();
       obj.setVisible(true);
       this.dispose();
    }//GEN-LAST:event_btnMenuPrincipalActionPerformed

    private void btnAtrasActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAtrasActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAtrasActionPerformed

    private void btnVerificarRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnVerificarRepreActionPerformed
      cargarRepresentado();  // TODO add your handling code here:
    }//GEN-LAST:event_btnVerificarRepreActionPerformed

    private void btnRegistrarRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarRepreActionPerformed
 dlgInscribirRepresentante dialog = new dlgInscribirRepresentante(new javax.swing.JFrame(), true);
    dialog.setVisible(true);        // TODO add your handling code here:
    }//GEN-LAST:event_btnRegistrarRepreActionPerformed

    private void cmbRepresentadoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbRepresentadoItemStateChanged
   
         // TODO add your handling code here:
    }//GEN-LAST:event_cmbRepresentadoItemStateChanged

    private void rbtnAprobadoItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbtnAprobadoItemStateChanged
        String [] iniciar = {"Selec"};
        cmbSeccion.setModel(new DefaultComboBoxModel(iniciar));
        if (evt.getStateChange() == ItemEvent.SELECTED){
        if (rbtnAprobado.isSelected()){
            int aux=Integer.parseInt(txtGradoEstu.getText());
            aux++;
            String grado=Integer.toString(aux);
            txtGrado.setText(grado);
            txtCondicion.setText("REGULAR");
            conexion conn=new conexion();
        Connection cn=conn.conectar();
            String seccion= null;
           String query="";
           query="SELECT seccion FROM secciongrado WHERE grado='"+grado+"'";
           try {
                
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                //Tomo los valores correspondientes de cada seccion segun el grado
                seccion=rs.getString("seccion");
                cmbSeccion.addItem(seccion);
                }
                
                
                
            } catch (Exception e) {
            }
            
        }
        }// TODO add your handling code here:
    }//GEN-LAST:event_rbtnAprobadoItemStateChanged

    private void rbtnRepitienteItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_rbtnRepitienteItemStateChanged
    String [] iniciar = {"Selec"};
        cmbSeccion.setModel(new DefaultComboBoxModel(iniciar));
        if (evt.getStateChange() == ItemEvent.SELECTED){
        if (rbtnRepitiente.isSelected()){
            int aux=Integer.parseInt(txtGradoEstu.getText());
            String grado=Integer.toString(aux);
            txtGrado.setText(grado);
            txtCondicion.setText("REPITIENTE");
            conexion conn=new conexion();
        Connection cn=conn.conectar();
            String seccion= null;
           String query="";
           query="SELECT seccion FROM secciongrado WHERE grado='"+grado+"'";
           try {
                
                Statement st=cn.createStatement();
                ResultSet rs= st.executeQuery(query);
                while (rs.next()){
                //Tomo los valores correspondientes de cada seccion segun el grado
                seccion=rs.getString("seccion");
                cmbSeccion.addItem(seccion);
                }
                
                
                
            } catch (Exception e) {
            }
            
        }
        }        // TODO add your handling code here:
    }//GEN-LAST:event_rbtnRepitienteItemStateChanged

    private void cmbSeccionItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_cmbSeccionItemStateChanged
    if (evt.getStateChange() == ItemEvent.SELECTED){
             if (this.cmbSeccion.getSelectedIndex()==0){
             txtTurno.setText("");
            //lblNoDisponible.setVisible(false);
           
             }else {
             this.txtTurno.setText(getTurno(this.txtGrado.getText(), this.cmbSeccion.getSelectedItem().toString()));
             this.txtProfesor.setText(getNombreProf(this.txtGrado.getText(),this.cmbSeccion.getSelectedItem().toString() ));
              this.txtAñoEscolar.setText(getPeriodoEscolar(this.txtGrado.getText(),this.cmbSeccion.getSelectedItem().toString()));
             if (matriculaDisponible(this.txtGrado.getText(),this.cmbSeccion.getSelectedItem().toString() ) < (matriculaSeccion(this.txtGrado.getText(),this.cmbSeccion.getSelectedItem().toString() ))){
                    String path = "/images/myicon.png";  
                    URL url = this.getClass().getResource("/imagenes/existe.png");  
                    ImageIcon icon = new ImageIcon(url);  
  
                   btnSiguiente.setEnabled(true);
                    lblIconoDisponibilidad.setIcon(icon); 
                    lblIconoDisponibilidad.setVisible(true);
                   btnSiguiente.setEnabled(true);
                   
                      //this.lblNoDisponible.setVisible(false); 
                 }else{
                     //this.lblNoDisponible.setVisible(true);
                     
                    URL url = this.getClass().getResource("/imagenes/no existe.png");  
                    ImageIcon icon = new ImageIcon(url); 
                    lblIconoDisponibilidad.setIcon(icon);
                     this.lblIconoDisponibilidad.setVisible(true);
                    btnSiguiente.setEnabled(false);
                 }
             }
             
    }// TODO add your handling code here:
    }//GEN-LAST:event_cmbSeccionItemStateChanged

    private void txtCedulaRepreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtCedulaRepreKeyTyped
 char c=evt.getKeyChar();
 if(c== KeyEvent.VK_ENTER){
        cargarRepresentado();
 }// TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaRepreKeyTyped

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
             dlgAgregarEstudiante dialog = new dlgAgregarEstudiante(new javax.swing.JFrame(), true);  // TODO add your handling code here:
             dlgAgregarEstudiante.txtCedulaRepre.setText(this.cmbCedulaRepre.getSelectedItem().toString()+"-"+this.txtCedulaRepre.getText());
             
             dialog.setVisible(true);
             
    }//GEN-LAST:event_jButton2ActionPerformed

    private void rbtnAprobadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_rbtnAprobadoActionPerformed
         // TODO add your handling code here:
    }//GEN-LAST:event_rbtnAprobadoActionPerformed

    private void txtCedulaRepreFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txtCedulaRepreFocusGained
     String [] iniciar = {"Selec"};
     String [] iniciar2={""};
        cmbRepresentado.setEnabled(false);
    jButton2.setEnabled(false);
     cmbSeccion.setModel(new DefaultComboBoxModel(iniciar));
     cmbRepresentado.setModel(new DefaultComboBoxModel(iniciar2));
     grupoBotones.clearSelection();
     lblVerificar.setIcon(null);
     lblIconoDisponibilidad.setIcon(null);
     txtGrado.setText("");
     txtGrado.setEnabled(false);
     rbtnAprobado.setEnabled(false);
     rbtnRepitiente.setEnabled(false);
     cmbSeccion.setEnabled(false);
     cmbSeccion.setSelectedIndex(0);
     txtTurno.setText("");
     txtAñoEscolar.setText("");
     txtProfesor.setText("");
     
     cargarTabla("");
// TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaRepreFocusGained

    private void cmbRepresentadoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbRepresentadoActionPerformed
    String aux=cmbRepresentado.getSelectedItem().toString();
         String [] temp;
         String [] temp2;
         temp=aux.split("\\s+");
         temp2=temp[1].split("-");
         if ((cmbRepresentado.getSelectedIndex()!=0) && (temp[0].equals("POR-INSCRIBIR"))){
         rbtnAprobado.setSelected(false);
     rbtnRepitiente.setSelected(false);
             grupoBotones.clearSelection();
         temp=aux.split("\\s+");
         temp2=temp[1].split("-");
         txtGradoEstu.setText(temp2[1]);
         txtCedulaEstu.setText(temp[2]);
         cargarTabla(temp[2]);
          rbtnAprobado.setEnabled(true);
                 rbtnRepitiente.setEnabled(true);
                 txtGrado.setEnabled(true);
                 cmbSeccion.setEnabled(true);
                 txtGrado.setEditable(false);
                 txtGrado.setText("");
                 cmbSeccion.setSelectedIndex(0);
                 txtTurno.setText("");
                
     }else 
             if (temp[0].equals("INSCRITO")){
                 cargarTabla("");
                 rbtnAprobado.setSelected(false);
     grupoBotones.clearSelection();
                 rbtnAprobado.setEnabled(false);
                 rbtnRepitiente.setEnabled(false);
                 txtGrado.setEnabled(false);
                 cmbSeccion.setEnabled(false);
                 txtGrado.setText("");
                 cmbSeccion.setSelectedIndex(0);
                 txtTurno.setText("");
                 txtProfesor.setText("");
                 txtAñoEscolar.setText("");
                 lblIconoDisponibilidad.setIcon(null);
                 dlgEstudianteYaInscrito dialog = new dlgEstudianteYaInscrito(new javax.swing.JFrame(), true);
                 dialog.setVisible(true);
             }
             // TODO add your handling code here:
    }//GEN-LAST:event_cmbRepresentadoActionPerformed

    private void txtCedulaRepreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedulaRepreActionPerformed
         // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaRepreActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmInscribirReingreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmInscribirReingreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmInscribirReingreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmInscribirReingreso.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmInscribirReingreso().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAtras;
    private javax.swing.JButton btnMenuPrincipal;
    private javax.swing.JButton btnRegistrarRepre;
    private javax.swing.JButton btnSiguiente;
    private javax.swing.JButton btnVerificarRepre;
    private javax.swing.JComboBox cmbCedulaRepre;
    public static javax.swing.JComboBox cmbRepresentado;
    private javax.swing.JComboBox cmbSeccion;
    private javax.swing.ButtonGroup grupoBotones;
    private javax.swing.JLabel inscripcion;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JLabel lblAñoEscolar;
    private javax.swing.JLabel lblCedulaEstudiante;
    private javax.swing.JLabel lblCedulaRepre;
    private javax.swing.JLabel lblCondicion;
    private javax.swing.JLabel lblDisponibilidad;
    private javax.swing.JLabel lblGrado;
    private javax.swing.JLabel lblHistorial;
    private javax.swing.JLabel lblIconoDisponibilidad;
    private javax.swing.JLabel lblProfesor;
    private javax.swing.JLabel lblSeccion;
    private javax.swing.JLabel lblTurno;
    private javax.swing.JLabel lblVerificar;
    private javax.swing.JRadioButton rbtnAprobado;
    private javax.swing.JRadioButton rbtnRepitiente;
    private javax.swing.JTable tblHistorial;
    private javax.swing.JLabel txtAñoEscolar;
    private javax.swing.JLabel txtCedulaEstu;
    private javax.swing.JTextField txtCedulaRepre;
    private javax.swing.JLabel txtCondicion;
    private javax.swing.JTextField txtGrado;
    private javax.swing.JLabel txtGradoEstu;
    public static javax.swing.JLabel txtParentesco;
    private javax.swing.JLabel txtProfesor;
    private javax.swing.JLabel txtTurno;
    // End of variables declaration//GEN-END:variables
}
