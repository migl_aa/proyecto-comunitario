/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package proyectocomunitario;

import BD.conexion;
import java.awt.GraphicsEnvironment;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Miguel Arroyo
 */
public class frmDatosEstuActualizar extends javax.swing.JFrame {
   DefaultTableModel modelo1;
    /**
     * Creates new form frmDatosEstu
     */
    public frmDatosEstuActualizar() {
        initComponents();
        this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
        txtCedulaEstu.setEnabled(false);
        txtCedulaRepre.setEnabled(false);
        txtCondicion.setEnabled(false);
        txtGrado.setEnabled(false);
        txtSeccion.setEnabled(false);
        txtTurno.setEnabled(false);
        txtProfesor.setEnabled(false);
    }
    
                      
    
    void cargarTabla(String valor){
          String titulos []= {"Periodo Escolar","Grado", "Sección","Turno","Profesor","Condicion"};
        String registros[]= new String [6];
        modelo1 = new DefaultTableModel(null, titulos){
            @Override
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            } 
        };
        
        conexion con=new conexion();
        Connection cn=con.conectar();
        
        String query="SELECT periodo_escolar,seccion,grado,turno,veces_cursante,condicion,Profesor FROM estu_año WHERE ced_estu='"+valor+"'";
        
        try {
            Statement st=cn.createStatement();
            ResultSet rs= st.executeQuery(query);
            while(rs.next()){
                registros[0]=rs.getString("periodo_escolar");
                registros[1]=rs.getString("grado");
                registros[2]=rs.getString("seccion");
                registros[3]=rs.getString("turno");
                registros[4]=rs.getString("Profesor");
                registros[5]=rs.getString("condicion");
                 modelo1.addRow(registros);
            }
            tblHistorial.setModel(modelo1);
            
            
        } catch (SQLException ex) {
            
            JOptionPane.showMessageDialog(null,ex);
        }
      }
    
     void cargar(String valor ){
         conexion conn=new conexion();
         Connection cn=conn.conectar();
         String consulta1,consulta2,consulta3,consulta4,consulta5;
         //Datos personales
        consulta1="SELECT ced_estu,primerNombre_estu,segundoNombre_estu,PrimerApellido_estu,segundoApellido_estu,sexo_estu,fn_estu,lugarN_estu,direccion_estu,tlf_estu,cod_Canaima,otroTlf_estu FROM estudiante WHERE ced_estu='"+valor+"'" ;
        //Datos de salud
        consulta2="SELECT ced_estu,tipo_sangre,v_bcg,v_trivalente,cirugia_s,alergia_s,v_penta,v_polio,v_antineumo,v_hepa_b,v_influenza,v_antiama,v_toxoide,enfermedad,peso,estatura,talla_camisa,talla_pantalon,talla_calzado FROM salud WHERE ced_estu='"+valor+"'";
         //Datos Familiares
        consulta3="SELECT f.ced_fa,f.nombre_fa,f.apellido_fa,f.fechaNacimiento_fa,f.profesion_fa,f.direccion_fa,f.tlf_fa,f.otroTlf_fa,f.observacion_fa FROM familiares f, estu_fami d, estudiante e WHERE e.ced_estu='"+valor+"' AND d.ced_estu='"+valor+"' AND f.ced_fa=d.ced_fa AND d.parentesco='Mamá'";
         consulta4="SELECT f.ced_fa,f.nombre_fa,f.apellido_fa,f.fechaNacimiento_fa,f.profesion_fa,f.direccion_fa,f.tlf_fa,f.otroTlf_fa,f.observacion_fa FROM familiares f, estu_fami d, estudiante e WHERE e.ced_estu='"+valor+"' AND d.ced_estu='"+valor+"' AND f.ced_fa=d.ced_fa AND d.parentesco='Papá'";
         //Datos del Representante
         consulta5="SELECT f.ced_fa,f.nombre_fa,f.apellido_fa,f.fechaNacimiento_fa,f.profesion_fa,f.direccion_fa,f.tlf_fa,f.otroTlf_fa,f.observacion_fa,d.parentesco FROM familiares f, estu_fami d, estudiante e WHERE e.ced_estu='"+valor+"' AND d.ced_estu='"+valor+"' AND f.ced_fa=d.ced_fa AND d.Representado=1";
         try {
             Statement st=cn.createStatement();
             ResultSet rt=st.executeQuery(consulta1);
             while(rt.next()){
             //Datos personales
             txtCedula.setText(rt.getString("ced_estu"));
             txtNombres.setText(rt.getString("primerNombre_estu")+" "+rt.getString("segundoNombre_estu")+" "+rt.getString("PrimerApellido_estu")+" "+rt.getString("segundoApellido_estu"));
             txtSexo.setText(rt.getString("sexo_estu"));
             txtFechaNaci.setText(rt.getString("fn_estu"));
             txtLugarNaci.setText(rt.getString("lugarN_estu"));
             txtTelefono.setText(rt.getString("tlf_estu"));
             txtOtroTelefono.setText(rt.getString("otroTlf_estu"));
             txtDireccion.setText(rt.getString("direccion_estu"));
             txtCodigoCanaima.setText(rt.getString("cod_Canaima"));
             }
             //Datos Salud
             
             Statement dt=cn.createStatement();
             ResultSet td=dt.executeQuery(consulta2);
             while(td.next()){
             txtTipoSangre.setText(td.getString("tipo_sangre"));
             String aux="";
                     aux=td.getString("alergia_s");
             if (aux.equals("")){
                 txtAlergiaTratamiento.setText("No tiene");
             }else
                 if (!aux.equals(""))
             txtAlergiaTratamiento.setText(td.getString("alergia_s"));
            
             String aux1="";
                    aux1=td.getString("cirugia_s");
             if(aux1.equals("")){
                 txtCirugiaTratamiento.setText("No tiene");
             }else
                 if(!aux1.equals(""))
                     txtCirugiaTratamiento.setText(td.getString("cirugia_s"));
             
             String aux2="";
              aux2=td.getString("enfermedad");
              if(aux2.equals("")){
                  txtEnfermedadTratamiento.setText("No tiene");
              }else
                  if(!aux2.equals(""))
                      txtEnfermedadTratamiento.setText(td.getString("enfermedad"));
              
              txtDosisPenta.setText(Integer.toString(td.getInt("v_penta")));
              txtDosisPolio.setText(Integer.toString(td.getInt("v_polio")));
              txtDosisAntineumo.setText(Integer.toString(td.getInt("v_antineumo")));
              txtDosisBcg.setText(Integer.toString(td.getInt("v_bcg")));
              txtDosisHepa.setText(Integer.toString(td.getInt("v_hepa_b")));
              txtDosisInfluenza.setText(Integer.toString(td.getInt("v_influenza")));
              txtDosisAntiama.setText(Integer.toString(td.getInt("v_antiama")));
              txtDosisTrivalente.setText(Integer.toString(td.getInt("v_trivalente")));
              txtDosisToxoide.setText(Integer.toString(td.getInt("v_toxoide")));
              txtPeso.setText(Integer.toString(td.getInt("peso")));
              txtEstatura.setText(Integer.toString(td.getInt("estatura")));
              txtTallaCamisa.setText(Integer.toString(td.getInt("talla_camisa")));
              txtTallaPantalon.setText(Integer.toString(td.getInt("talla_pantalon")));
              txtTallaCalzado.setText(Integer.toString(td.getInt("talla_calzado")));
              }
             //Datos Familiares
             Statement df=cn.createStatement();
             ResultSet er=df.executeQuery(consulta3);
             while(er.next()){
                 String mama="";
                    mama=er.getString("ced_fa");
                    if (!mama.equals("")){
                        txtCedulaMadre.setText(er.getString("ced_fa"));
                        txtNombresMadre.setText(er.getString("nombre_fa")+" "+er.getString("apellido_fa"));
                        txtFechaNaciMadre.setText(er.getString("fechaNacimiento_fa"));
                        txtOcupacionMadre.setText(er.getString("profesion_fa"));
                        txtTelefonoMadre.setText(er.getString("tlf_fa"));
                        txtOtroTelefonoMadre.setText(er.getString("otroTlf_fa"));
                        txtDireccionMadre.setText(er.getString("direccion_fa"));
                        }
                 
             }
             Statement gt=cn.createStatement();
             ResultSet op=gt.executeQuery(consulta4);
             while(op.next()){
                 String papa="";
                 papa=op.getString("ced_fa");
                 if(!papa.equals("")){
                     txtCedulaPadre.setText(op.getString("ced_fa"));
                     txtNombresPadre.setText(op.getString("nombre_fa")+" "+op.getString("apellido_fa"));
                     txtFechaNaciPadre.setText(op.getString("fechaNacimiento_fa"));
                     txtOcupacionPadre.setText(op.getString("profesion_fa"));
                     txtTelefonoPadre.setText(op.getString("tlf_fa"));
                     txtOtroTelefonoPadre.setText(op.getString("otroTlf_fa"));
                     txtDireccionPadre.setText(op.getString("direccion_fa"));
                 }
             }
             //Datos del Representante
             Statement th=cn.createStatement();
             ResultSet bn=th.executeQuery(consulta5);
             while(bn.next()){
                 txtRepre.setText(bn.getString("f.ced_fa"));
                 txtNombresRepre.setText(bn.getString("nombre_fa")+" "+bn.getString("apellido_fa"));
                 txtFechaNaciRepre.setText(bn.getString("fechaNacimiento_fa"));
                 txtOcupacionRepre.setText(bn.getString("profesion_fa"));
                 txtTelefonoRepre.setText(bn.getString("tlf_fa"));
                 txtOtroTlfRepre.setText(bn.getString("otroTlf_fa"));
                 txtDireccionRepre.setText(bn.getString("direccion_fa"));
                 txtParentescoRepre.setText(bn.getString("d.parentesco"));
             }
         } catch (Exception e) {
             JOptionPane.showMessageDialog(null, e);
         }
     }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel2 = new imagenes.Fondo("fondo color.jpg");
        jLabel13 = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        jLabel12 = new javax.swing.JLabel();
        lblCedulaRepre = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        lblCedulaEstu = new javax.swing.JLabel();
        txtCedulaEstu = new javax.swing.JTextField();
        jLabel6 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblHistorial = new javax.swing.JTable();
        jTabbedPane1 = new javax.swing.JTabbedPane();
        pnlDatosPersonales = new javax.swing.JPanel();
        lblCedula = new javax.swing.JLabel();
        txtCedula = new javax.swing.JLabel();
        lblPrimerNombre = new javax.swing.JLabel();
        txtNombres = new javax.swing.JLabel();
        lblSexo = new javax.swing.JLabel();
        txtSexo = new javax.swing.JLabel();
        lblFechaNac = new javax.swing.JLabel();
        txtFechaNaci = new javax.swing.JLabel();
        lblLugarNacimiento = new javax.swing.JLabel();
        txtLugarNaci = new javax.swing.JLabel();
        lblTelefono = new javax.swing.JLabel();
        txtTelefono = new javax.swing.JLabel();
        lblOtroTelefono = new javax.swing.JLabel();
        txtOtroTelefono = new javax.swing.JLabel();
        lblCodigoCanaima = new javax.swing.JLabel();
        txtDireccion = new javax.swing.JLabel();
        lblDireccion = new javax.swing.JLabel();
        txtCodigoCanaima = new javax.swing.JLabel();
        btnModificar = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        pnlDatosSalud = new javax.swing.JPanel();
        lblTipoSangre = new javax.swing.JLabel();
        txtTipoSangre = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        txtCirugiaTratamiento = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        txtEnfermedadTratamiento = new javax.swing.JLabel();
        txtAlergiaTratamiento = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        lblPentavalente = new javax.swing.JLabel();
        lblPolio = new javax.swing.JLabel();
        lblAntineumococo = new javax.swing.JLabel();
        lblBCG = new javax.swing.JLabel();
        lblHepatitis = new javax.swing.JLabel();
        lblInfluenza = new javax.swing.JLabel();
        lblAntiamarilica = new javax.swing.JLabel();
        lblTrivalente = new javax.swing.JLabel();
        lblToxoide = new javax.swing.JLabel();
        btnModificar3 = new javax.swing.JButton();
        txtDosisPenta = new javax.swing.JLabel();
        txtDosisPolio = new javax.swing.JLabel();
        txtDosisAntineumo = new javax.swing.JLabel();
        txtDosisBcg = new javax.swing.JLabel();
        txtDosisHepa = new javax.swing.JLabel();
        txtDosisInfluenza = new javax.swing.JLabel();
        txtDosisAntiama = new javax.swing.JLabel();
        txtDosisTrivalente = new javax.swing.JLabel();
        txtDosisToxoide = new javax.swing.JLabel();
        lblPeso = new javax.swing.JLabel();
        txtPeso = new javax.swing.JLabel();
        lblEstatura = new javax.swing.JLabel();
        txtEstatura = new javax.swing.JLabel();
        lblTallaCamisa = new javax.swing.JLabel();
        txtTallaCamisa = new javax.swing.JLabel();
        lblTallaPantalon = new javax.swing.JLabel();
        txtTallaPantalon = new javax.swing.JLabel();
        lblTallaCalzado = new javax.swing.JLabel();
        txtTallaCalzado = new javax.swing.JLabel();
        lblFondo = new javax.swing.JLabel();
        pnlDatosFamiliares = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        lblCedulaMadre = new javax.swing.JLabel();
        txtCedulaMadre = new javax.swing.JLabel();
        txtNombresMadre = new javax.swing.JLabel();
        txtFechaNaciMadre = new javax.swing.JLabel();
        lblOcupacionMadre = new javax.swing.JLabel();
        txtOcupacionMadre = new javax.swing.JLabel();
        lblTelefonoMadre = new javax.swing.JLabel();
        txtTelefonoMadre = new javax.swing.JLabel();
        lblOtroTelefonoMadre = new javax.swing.JLabel();
        txtOtroTelefonoMadre = new javax.swing.JLabel();
        lblDireccionMadre = new javax.swing.JLabel();
        txtDireccionMadre = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        jLabel11 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        lblCedulaPadre = new javax.swing.JLabel();
        txtCedulaPadre = new javax.swing.JLabel();
        lblNombresMadre = new javax.swing.JLabel();
        lblFechaNaciMadre1 = new javax.swing.JLabel();
        txtNombresPadre = new javax.swing.JLabel();
        txtFechaNaciPadre = new javax.swing.JLabel();
        lblFechaNaciPadre = new javax.swing.JLabel();
        lblNombresPadre = new javax.swing.JLabel();
        lblOtroTelefonoPadre = new javax.swing.JLabel();
        lblOcupacionPadre = new javax.swing.JLabel();
        txtOcupacionPadre = new javax.swing.JLabel();
        lblTelefonoPadre = new javax.swing.JLabel();
        txtTelefonoPadre = new javax.swing.JLabel();
        txtOtroTelefonoPadre = new javax.swing.JLabel();
        lblDireccionPadre = new javax.swing.JLabel();
        txtDireccionPadre = new javax.swing.JLabel();
        btnModificar1 = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lblRepre = new javax.swing.JLabel();
        txtRepre = new javax.swing.JLabel();
        lblNombreRepre = new javax.swing.JLabel();
        txtNombresRepre = new javax.swing.JLabel();
        lblParentescoRepre = new javax.swing.JLabel();
        txtParentescoRepre = new javax.swing.JLabel();
        lblFechaNaciRepre = new javax.swing.JLabel();
        txtFechaNaciRepre = new javax.swing.JLabel();
        lblOcupacionRepre = new javax.swing.JLabel();
        txtOcupacionRepre = new javax.swing.JLabel();
        lblTelefonoRepre = new javax.swing.JLabel();
        txtTelefonoRepre = new javax.swing.JLabel();
        lblOtroTelefonoRepre = new javax.swing.JLabel();
        txtOtroTlfRepre = new javax.swing.JLabel();
        lblDireccionRepre = new javax.swing.JLabel();
        txtDireccionRepre = new javax.swing.JLabel();
        lblObservacionRepre = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        txtCedulaRepre = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        btnCancelar = new javax.swing.JButton();
        btnRegistrar = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        jSeparator9 = new javax.swing.JSeparator();
        lblGrado = new javax.swing.JLabel();
        txtGrado = new javax.swing.JTextField();
        lblSeccion = new javax.swing.JLabel();
        txtSeccion = new javax.swing.JTextField();
        lblTurno = new javax.swing.JLabel();
        txtTurno = new javax.swing.JTextField();
        lblProfesor = new javax.swing.JLabel();
        txtProfesor = new javax.swing.JTextField();
        lblCondicion = new javax.swing.JLabel();
        txtCondicion = new javax.swing.JTextField();
        txtAñoEscolar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jLabel13.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel13.setText("Datos");

        jLabel12.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel12.setText("de");

        lblCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaRepre.setText("Cédula del Representante:");

        jLabel1.setFont(new java.awt.Font("Comfortaa", 0, 60)); // NOI18N
        jLabel1.setText("Actualización ");

        lblCedulaEstu.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCedulaEstu.setText("Cédula del Estudiante:");

        txtCedulaEstu.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtCedulaEstu.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtCedulaEstuActionPerformed(evt);
            }
        });

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/refrescar.png"))); // NOI18N

        tblHistorial.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        tblHistorial.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblHistorial);

        jTabbedPane1.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        pnlDatosPersonales.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblCedula.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblCedula.setForeground(new java.awt.Color(0, 102, 102));
        lblCedula.setText("Cédula:");
        pnlDatosPersonales.add(lblCedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        txtCedula.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosPersonales.add(txtCedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 10, 92, 19));

        lblPrimerNombre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblPrimerNombre.setForeground(new java.awt.Color(0, 102, 102));
        lblPrimerNombre.setText(" Nombres y Apellidos:");
        pnlDatosPersonales.add(lblPrimerNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 10, -1, -1));

        txtNombres.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosPersonales.add(txtNombres, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 10, 450, 20));

        lblSexo.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblSexo.setForeground(new java.awt.Color(0, 102, 102));
        lblSexo.setText("Sexo:");
        pnlDatosPersonales.add(lblSexo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, -1));

        txtSexo.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosPersonales.add(txtSexo, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 40, 100, 20));

        lblFechaNac.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblFechaNac.setForeground(new java.awt.Color(0, 102, 102));
        lblFechaNac.setText("Fecha de Nacimiento:");
        pnlDatosPersonales.add(lblFechaNac, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 40, -1, -1));

        txtFechaNaci.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosPersonales.add(txtFechaNaci, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 40, 110, 20));

        lblLugarNacimiento.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblLugarNacimiento.setForeground(new java.awt.Color(0, 102, 102));
        lblLugarNacimiento.setText("Lugar de Nacimiento:");
        pnlDatosPersonales.add(lblLugarNacimiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, -1));

        txtLugarNaci.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosPersonales.add(txtLugarNaci, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 70, 710, 20));

        lblTelefono.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblTelefono.setForeground(new java.awt.Color(0, 102, 102));
        lblTelefono.setText("Teléfono:");
        pnlDatosPersonales.add(lblTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, -1, -1));

        txtTelefono.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosPersonales.add(txtTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 100, 110, 20));

        lblOtroTelefono.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblOtroTelefono.setForeground(new java.awt.Color(0, 102, 102));
        lblOtroTelefono.setText("Otro Teléfono:");
        pnlDatosPersonales.add(lblOtroTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(270, 100, -1, -1));

        txtOtroTelefono.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosPersonales.add(txtOtroTelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 100, 100, 20));

        lblCodigoCanaima.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblCodigoCanaima.setForeground(new java.awt.Color(0, 102, 102));
        lblCodigoCanaima.setText("Código Canaima:");
        pnlDatosPersonales.add(lblCodigoCanaima, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, -1, -1));

        txtDireccion.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        txtDireccion.setVerifyInputWhenFocusTarget(false);
        pnlDatosPersonales.add(txtDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 890, 30));

        lblDireccion.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblDireccion.setForeground(new java.awt.Color(0, 102, 102));
        lblDireccion.setText("Dirección de Habitación:");
        pnlDatosPersonales.add(lblDireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 130, -1, -1));

        txtCodigoCanaima.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosPersonales.add(txtCodigoCanaima, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 180, 140, 20));

        btnModificar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pen.png"))); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.setBorder(null);
        btnModificar.setContentAreaFilled(false);
        btnModificar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/modificar icono.png"))); // NOI18N
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });
        pnlDatosPersonales.add(btnModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 230, -1, -1));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondo panel2.jpg"))); // NOI18N
        pnlDatosPersonales.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 940, 280));

        jTabbedPane1.addTab("Datos Personales", pnlDatosPersonales);

        pnlDatosSalud.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblTipoSangre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblTipoSangre.setForeground(new java.awt.Color(0, 102, 102));
        lblTipoSangre.setText("Tipo de Sangre:");
        pnlDatosSalud.add(lblTipoSangre, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 10, -1, -1));

        txtTipoSangre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtTipoSangre, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 10, 30, 20));

        jLabel17.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(0, 102, 102));
        jLabel17.setText("Cirugia y tratamiento:");
        pnlDatosSalud.add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        txtCirugiaTratamiento.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtCirugiaTratamiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 60, 740, 20));

        jLabel19.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jLabel19.setText("Dosis de Vacunas");
        pnlDatosSalud.add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        txtEnfermedadTratamiento.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtEnfermedadTratamiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 90, 690, 20));

        txtAlergiaTratamiento.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtAlergiaTratamiento, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 30, 740, 20));

        jLabel20.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(0, 102, 102));
        jLabel20.setText("Alergia y Tratamiento:");
        pnlDatosSalud.add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 30, -1, -1));

        jLabel23.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(0, 102, 102));
        jLabel23.setText("Enfermedad y Tratamiento:");
        pnlDatosSalud.add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, -1, -1));
        pnlDatosSalud.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 910, 10));

        lblPentavalente.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblPentavalente.setForeground(new java.awt.Color(0, 102, 102));
        lblPentavalente.setText("Pentavalente:");
        pnlDatosSalud.add(lblPentavalente, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 150, -1, -1));

        lblPolio.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblPolio.setForeground(new java.awt.Color(0, 102, 102));
        lblPolio.setText("Polio:");
        pnlDatosSalud.add(lblPolio, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 150, -1, -1));

        lblAntineumococo.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblAntineumococo.setForeground(new java.awt.Color(0, 102, 102));
        lblAntineumococo.setText("Antineumococo:");
        pnlDatosSalud.add(lblAntineumococo, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 150, -1, -1));

        lblBCG.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblBCG.setForeground(new java.awt.Color(0, 102, 102));
        lblBCG.setText("BCG:");
        pnlDatosSalud.add(lblBCG, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 150, -1, -1));

        lblHepatitis.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblHepatitis.setForeground(new java.awt.Color(0, 102, 102));
        lblHepatitis.setText("Hepatitis B:");
        pnlDatosSalud.add(lblHepatitis, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 150, -1, -1));

        lblInfluenza.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblInfluenza.setForeground(new java.awt.Color(0, 102, 102));
        lblInfluenza.setText("Influenza:");
        pnlDatosSalud.add(lblInfluenza, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 190, -1, -1));

        lblAntiamarilica.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblAntiamarilica.setForeground(new java.awt.Color(0, 102, 102));
        lblAntiamarilica.setText("Antiamarílica:");
        pnlDatosSalud.add(lblAntiamarilica, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 190, -1, -1));

        lblTrivalente.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblTrivalente.setForeground(new java.awt.Color(0, 102, 102));
        lblTrivalente.setText("Trivalente:");
        pnlDatosSalud.add(lblTrivalente, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 190, -1, -1));

        lblToxoide.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblToxoide.setForeground(new java.awt.Color(0, 102, 102));
        lblToxoide.setText("Toxoide:");
        pnlDatosSalud.add(lblToxoide, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 190, -1, -1));

        btnModificar3.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnModificar3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pen.png"))); // NOI18N
        btnModificar3.setText("Modificar");
        btnModificar3.setBorder(null);
        btnModificar3.setContentAreaFilled(false);
        btnModificar3.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/modificar icono.png"))); // NOI18N
        btnModificar3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificar3ActionPerformed(evt);
            }
        });
        pnlDatosSalud.add(btnModificar3, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 220, -1, -1));

        txtDosisPenta.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtDosisPenta, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 150, 40, 20));

        txtDosisPolio.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtDosisPolio, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 150, 40, 20));

        txtDosisAntineumo.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtDosisAntineumo, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 150, 30, 20));

        txtDosisBcg.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtDosisBcg, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 150, 40, 20));

        txtDosisHepa.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtDosisHepa, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 150, 30, 20));

        txtDosisInfluenza.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtDosisInfluenza, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 190, 30, 20));

        txtDosisAntiama.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtDosisAntiama, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 190, 30, 20));

        txtDosisTrivalente.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtDosisTrivalente, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 190, 30, 20));

        txtDosisToxoide.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtDosisToxoide, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 190, 30, 20));

        lblPeso.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblPeso.setForeground(new java.awt.Color(0, 102, 102));
        lblPeso.setText("Peso:");
        pnlDatosSalud.add(lblPeso, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 230, -1, -1));

        txtPeso.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtPeso, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 230, 60, 30));

        lblEstatura.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblEstatura.setForeground(new java.awt.Color(0, 102, 102));
        lblEstatura.setText("Estatura:");
        pnlDatosSalud.add(lblEstatura, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 230, -1, -1));

        txtEstatura.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtEstatura, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 230, 50, 20));

        lblTallaCamisa.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblTallaCamisa.setForeground(new java.awt.Color(0, 102, 102));
        lblTallaCamisa.setText("Talla Camisa:");
        pnlDatosSalud.add(lblTallaCamisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 230, -1, -1));

        txtTallaCamisa.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtTallaCamisa, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 230, 30, 20));

        lblTallaPantalon.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblTallaPantalon.setForeground(new java.awt.Color(0, 102, 102));
        lblTallaPantalon.setText("Talla Pantalón:");
        pnlDatosSalud.add(lblTallaPantalon, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 230, -1, -1));

        txtTallaPantalon.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtTallaPantalon, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 230, 30, 20));

        lblTallaCalzado.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblTallaCalzado.setForeground(new java.awt.Color(0, 102, 102));
        lblTallaCalzado.setText("Talla Calzado:");
        pnlDatosSalud.add(lblTallaCalzado, new org.netbeans.lib.awtextra.AbsoluteConstraints(630, 230, -1, -1));

        txtTallaCalzado.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosSalud.add(txtTallaCalzado, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 230, 30, 20));

        lblFondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondo panel2.jpg"))); // NOI18N
        pnlDatosSalud.add(lblFondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(-4, -10, 990, 290));

        jTabbedPane1.addTab("Datos de Salud", pnlDatosSalud);

        pnlDatosFamiliares.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel2.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jLabel2.setText("Datos de la Madre");
        pnlDatosFamiliares.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 0, -1, -1));

        lblCedulaMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblCedulaMadre.setForeground(new java.awt.Color(0, 102, 102));
        lblCedulaMadre.setText("Cédula:");
        pnlDatosFamiliares.add(lblCedulaMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, -1));

        txtCedulaMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtCedulaMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 30, 80, 20));

        txtNombresMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtNombresMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(320, 30, 280, 20));

        txtFechaNaciMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtFechaNaciMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(780, 30, 130, 20));

        lblOcupacionMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblOcupacionMadre.setForeground(new java.awt.Color(0, 102, 102));
        lblOcupacionMadre.setText("Profesión u Ocupación:");
        pnlDatosFamiliares.add(lblOcupacionMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 60, -1, -1));

        txtOcupacionMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtOcupacionMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 60, 240, 20));

        lblTelefonoMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblTelefonoMadre.setForeground(new java.awt.Color(0, 102, 102));
        lblTelefonoMadre.setText("Teléfono:");
        pnlDatosFamiliares.add(lblTelefonoMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 60, -1, -1));

        txtTelefonoMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtTelefonoMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 60, 110, 20));

        lblOtroTelefonoMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblOtroTelefonoMadre.setForeground(new java.awt.Color(0, 102, 102));
        lblOtroTelefonoMadre.setText("Otro Teléfono:");
        pnlDatosFamiliares.add(lblOtroTelefonoMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 60, -1, -1));

        txtOtroTelefonoMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtOtroTelefonoMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 60, 110, 20));

        lblDireccionMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblDireccionMadre.setForeground(new java.awt.Color(0, 102, 102));
        lblDireccionMadre.setText("Dirección de Habitación:");
        pnlDatosFamiliares.add(lblDireccionMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, -1));

        txtDireccionMadre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        pnlDatosFamiliares.add(txtDireccionMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 90, 740, 20));
        pnlDatosFamiliares.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 910, 10));

        jLabel11.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jLabel11.setText("Datos del Padre");
        pnlDatosFamiliares.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, -1, -1));
        pnlDatosFamiliares.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 910, 10));

        lblCedulaPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblCedulaPadre.setForeground(new java.awt.Color(0, 102, 102));
        lblCedulaPadre.setText("Cédula:");
        pnlDatosFamiliares.add(lblCedulaPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, -1, -1));

        txtCedulaPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtCedulaPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 150, 80, 20));

        lblNombresMadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblNombresMadre.setForeground(new java.awt.Color(0, 102, 102));
        lblNombresMadre.setText(" Nombre y Apellido:");
        pnlDatosFamiliares.add(lblNombresMadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 30, -1, -1));

        lblFechaNaciMadre1.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblFechaNaciMadre1.setForeground(new java.awt.Color(0, 102, 102));
        lblFechaNaciMadre1.setText("Fecha de Nacimiento:");
        pnlDatosFamiliares.add(lblFechaNaciMadre1, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 30, -1, -1));

        txtNombresPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtNombresPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 150, 250, 20));

        txtFechaNaciPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtFechaNaciPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 150, 130, 20));

        lblFechaNaciPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblFechaNaciPadre.setForeground(new java.awt.Color(0, 102, 102));
        lblFechaNaciPadre.setText("Fecha de Nacimiento:");
        pnlDatosFamiliares.add(lblFechaNaciPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 150, -1, -1));

        lblNombresPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblNombresPadre.setForeground(new java.awt.Color(0, 102, 102));
        lblNombresPadre.setText(" Nombre y Apellido:");
        pnlDatosFamiliares.add(lblNombresPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 150, -1, -1));

        lblOtroTelefonoPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblOtroTelefonoPadre.setForeground(new java.awt.Color(0, 102, 102));
        lblOtroTelefonoPadre.setText("Otro Teléfono:");
        pnlDatosFamiliares.add(lblOtroTelefonoPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 180, -1, -1));

        lblOcupacionPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblOcupacionPadre.setForeground(new java.awt.Color(0, 102, 102));
        lblOcupacionPadre.setText("Profesión u Ocupación:");
        pnlDatosFamiliares.add(lblOcupacionPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 180, -1, -1));

        txtOcupacionPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtOcupacionPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 180, 240, 20));

        lblTelefonoPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblTelefonoPadre.setForeground(new java.awt.Color(0, 102, 102));
        lblTelefonoPadre.setText("Teléfono:");
        pnlDatosFamiliares.add(lblTelefonoPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 180, -1, -1));

        txtTelefonoPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtTelefonoPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 180, 110, 20));

        txtOtroTelefonoPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        pnlDatosFamiliares.add(txtOtroTelefonoPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 180, 100, 20));

        lblDireccionPadre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblDireccionPadre.setForeground(new java.awt.Color(0, 102, 102));
        lblDireccionPadre.setText("Dirección de Habitación:");
        pnlDatosFamiliares.add(lblDireccionPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 210, -1, -1));

        txtDireccionPadre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        pnlDatosFamiliares.add(txtDireccionPadre, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 210, 740, 20));

        btnModificar1.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnModificar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pen.png"))); // NOI18N
        btnModificar1.setText("Modificar");
        btnModificar1.setBorder(null);
        btnModificar1.setContentAreaFilled(false);
        btnModificar1.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/modificar icono.png"))); // NOI18N
        btnModificar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificar1ActionPerformed(evt);
            }
        });
        pnlDatosFamiliares.add(btnModificar1, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 230, -1, -1));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondo panel2.jpg"))); // NOI18N
        pnlDatosFamiliares.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 940, 280));

        jTabbedPane1.addTab("Datos Familiares", pnlDatosFamiliares);

        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lblRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblRepre.setForeground(new java.awt.Color(0, 102, 102));
        lblRepre.setText("Cédula:");
        jPanel1.add(lblRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 11, -1, -1));

        txtRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(txtRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 10, 80, 20));

        lblNombreRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblNombreRepre.setForeground(new java.awt.Color(0, 102, 102));
        lblNombreRepre.setText("Nombre y Apellido:");
        jPanel1.add(lblNombreRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 10, -1, -1));

        txtNombresRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(txtNombresRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 10, 200, 20));

        lblParentescoRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblParentescoRepre.setForeground(new java.awt.Color(0, 102, 102));
        lblParentescoRepre.setText("Parentesco:");
        jPanel1.add(lblParentescoRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(590, 10, -1, -1));

        txtParentescoRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(txtParentescoRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 10, 90, 20));

        lblFechaNaciRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblFechaNaciRepre.setForeground(new java.awt.Color(0, 102, 102));
        lblFechaNaciRepre.setText("Fecha de Nacimiento:");
        jPanel1.add(lblFechaNaciRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, -1));

        txtFechaNaciRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(txtFechaNaciRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 50, 90, 20));

        lblOcupacionRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblOcupacionRepre.setForeground(new java.awt.Color(0, 102, 102));
        lblOcupacionRepre.setText("Profesión u Ocupación:");
        jPanel1.add(lblOcupacionRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 50, -1, -1));

        txtOcupacionRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(txtOcupacionRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 50, 410, 20));

        lblTelefonoRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblTelefonoRepre.setForeground(new java.awt.Color(0, 102, 102));
        lblTelefonoRepre.setText("Teléfono:");
        jPanel1.add(lblTelefonoRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, -1));

        txtTelefonoRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(txtTelefonoRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 90, 110, 20));

        lblOtroTelefonoRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblOtroTelefonoRepre.setForeground(new java.awt.Color(0, 102, 102));
        lblOtroTelefonoRepre.setText("Otro Teléfono:");
        jPanel1.add(lblOtroTelefonoRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 90, -1, -1));

        txtOtroTlfRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(txtOtroTlfRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 90, 100, 20));

        lblDireccionRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblDireccionRepre.setForeground(new java.awt.Color(0, 102, 102));
        lblDireccionRepre.setText("Dirección de Habitación:");
        jPanel1.add(lblDireccionRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, -1, -1));

        txtDireccionRepre.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        jPanel1.add(txtDireccionRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 130, 720, 20));

        lblObservacionRepre.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        lblObservacionRepre.setForeground(new java.awt.Color(0, 102, 102));
        lblObservacionRepre.setText("Observación:");
        jPanel1.add(lblObservacionRepre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, -1, -1));

        jLabel4.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 170, 810, 20));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/fondo panel2.jpg"))); // NOI18N
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, 280));

        jTabbedPane1.addTab("Datos del Representante", jPanel1);

        txtCedulaRepre.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        jPanel3.setBackground(new java.awt.Color(0, 153, 102));
        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnCancelar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar (2).png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(null);
        btnCancelar.setContentAreaFilled(false);
        btnCancelar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        btnRegistrar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnRegistrar.setForeground(new java.awt.Color(255, 255, 255));
        btnRegistrar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Guardar.png"))); // NOI18N
        btnRegistrar.setText("Registrar");
        btnRegistrar.setBorder(null);
        btnRegistrar.setContentAreaFilled(false);
        btnRegistrar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar 2 icono.png"))); // NOI18N
        btnRegistrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrarActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnSalir.setForeground(new java.awt.Color(255, 255, 255));
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnSalir.setText("Menú Principal");
        btnSalir.setBorder(null);
        btnSalir.setContentAreaFilled(false);
        btnSalir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/home.png"))); // NOI18N
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnRegistrar, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(90, 90, 90)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(btnRegistrar)))
                .addGap(7, 7, 7)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(8, 8, 8)
                .addComponent(btnCancelar)
                .addGap(9, 9, 9)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(348, 348, 348)
                .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnSalir)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(64, 64, 64))
        );

        lblGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblGrado.setText("Grado:");

        txtGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        lblSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSeccion.setText("Sección:");

        txtSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        lblTurno.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTurno.setText("Turno:");

        txtTurno.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtTurno.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtTurnoActionPerformed(evt);
            }
        });

        lblProfesor.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblProfesor.setText("Profesor:");

        txtProfesor.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        lblCondicion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblCondicion.setText("Condición:");

        txtCondicion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, 216, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 33, Short.MAX_VALUE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 940, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 940, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(jScrollPane1)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(lblCedulaEstu)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtCedulaEstu, javax.swing.GroupLayout.PREFERRED_SIZE, 110, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(lblCedulaRepre)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(48, 48, 48)
                                        .addComponent(lblCondicion)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(txtCondicion, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel2Layout.createSequentialGroup()
                                        .addComponent(lblGrado)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addComponent(txtGrado, javax.swing.GroupLayout.PREFERRED_SIZE, 40, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(20, 20, 20)
                                        .addComponent(lblSeccion)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(txtAñoEscolar)
                                            .addGroup(jPanel2Layout.createSequentialGroup()
                                                .addComponent(txtSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, 50, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(67, 67, 67)
                                                .addComponent(lblTurno)
                                                .addGap(7, 7, 7)
                                                .addComponent(txtTurno, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                .addGap(50, 50, 50)
                                                .addComponent(lblProfesor)
                                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(txtProfesor)))))))
                        .addContainerGap(20, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(138, 138, 138)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(340, 340, 340)
                                .addComponent(jLabel12))
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(400, 400, 400)
                                .addComponent(jLabel13))
                            .addComponent(jLabel1))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(126, 126, 126))))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(jPanel2Layout.createSequentialGroup()
                                .addGap(60, 60, 60)
                                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel12)
                                    .addComponent(jLabel13))))
                        .addGap(4, 4, 4))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 117, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(4, 4, 4)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lblCedulaEstu)
                    .addComponent(txtCedulaEstu, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCedulaRepre)
                    .addComponent(txtCedulaRepre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblCondicion)
                    .addComponent(txtCondicion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(txtAñoEscolar)
                .addGap(16, 16, 16)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txtGrado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(lblGrado))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblSeccion)
                        .addComponent(txtSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(lblTurno)
                    .addComponent(txtTurno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(lblProfesor)
                        .addComponent(txtProfesor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 100, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jTabbedPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 310, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(29, Short.MAX_VALUE))
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        jTabbedPane1.getAccessibleContext().setAccessibleName("Datos Personales");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtCedulaEstuActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtCedulaEstuActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtCedulaEstuActionPerformed

    private void txtTurnoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtTurnoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtTurnoActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
        // TODO add your handling code here:
        dlgActualizarDatosPersonales dialog = new dlgActualizarDatosPersonales(new javax.swing.JFrame(), true);
        dialog.setVisible(true);
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnModificar3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificar3ActionPerformed
        // TODO add your handling code here:
        dlgActualizarDatosSalud dialog = new dlgActualizarDatosSalud(new javax.swing.JFrame(), true);
        dialog.setVisible(true);
    }//GEN-LAST:event_btnModificar3ActionPerformed

    private void btnModificar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificar1ActionPerformed
        // TODO add your handling code here:
         dlgActulizarDatosFamiliares dialog = new dlgActulizarDatosFamiliares(new javax.swing.JFrame(), true);
         dialog.cargarDatos(txtCedula.getText());
         dlgActulizarDatosFamiliares.txtCedulaEstu.setText(this.txtCedula.getText());
         dialog.setVisible(true);
         
         
         
    }//GEN-LAST:event_btnModificar1ActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        menuInicio obj= new menuInicio();
        obj.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        int valor;

        valor=JOptionPane.showConfirmDialog(null,"\t¿DESEA SALIR?\n\nSe Borraran todos los datos no guardados","Confirmar salida",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
        if (valor==JOptionPane.YES_OPTION){
            this.dispose();
            frmTipoDeInscripcion obj= new frmTipoDeInscripcion();
            obj.setVisible(true);
          //  cancelar();
        }
    }//GEN-LAST:event_btnCancelarActionPerformed

    private void btnRegistrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrarActionPerformed
conexion con=new conexion();
        Connection cn=con.conectar();
String consulta="UPDATE estudiante SET seccion_estu='"+txtSeccion.getText()+"',grado_estu='"+txtGrado.getText()+"',turno='"+txtTurno.getText()+"', periodo='"+txtAñoEscolar.getText()+"',estado='INSCRITO' WHERE ced_estu='"+txtCedulaEstu.getText()+"'";    
String consulta2="INSERT INTO estu_año (ced_estu,seccion,grado,periodo_escolar,veces_cursante,condicion,turno,Profesor) VALUES ('"+txtCedulaEstu.getText()+"','"+txtSeccion.getText()+"','"+txtGrado.getText()+"','"+txtAñoEscolar.getText()+"',1,'"+txtCondicion.getText()+"','"+txtTurno.getText()+"','"+txtProfesor.getText()+"')";
        try {
           Statement th=cn.createStatement();
           th.execute(consulta);
           Statement ph=cn.createStatement();
           ph.execute(consulta2);
           dlgDatosCargadoInscribir dialog = new dlgDatosCargadoInscribir(new javax.swing.JFrame(), true);
           this.dispose();
           dialog.setVisible(true);
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
// TODO add your handling code here:
    }//GEN-LAST:event_btnRegistrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmDatosEstuActualizar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmDatosEstuActualizar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmDatosEstuActualizar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmDatosEstuActualizar.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmDatosEstuActualizar().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnModificar1;
    private javax.swing.JButton btnModificar3;
    private javax.swing.JButton btnRegistrar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel23;
    public static javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTabbedPane jTabbedPane1;
    private javax.swing.JLabel lblAntiamarilica;
    private javax.swing.JLabel lblAntineumococo;
    private javax.swing.JLabel lblBCG;
    private javax.swing.JLabel lblCedula;
    private javax.swing.JLabel lblCedulaEstu;
    private javax.swing.JLabel lblCedulaMadre;
    private javax.swing.JLabel lblCedulaPadre;
    private javax.swing.JLabel lblCedulaRepre;
    private javax.swing.JLabel lblCodigoCanaima;
    private javax.swing.JLabel lblCondicion;
    private javax.swing.JLabel lblDireccion;
    private javax.swing.JLabel lblDireccionMadre;
    private javax.swing.JLabel lblDireccionPadre;
    private javax.swing.JLabel lblDireccionRepre;
    private javax.swing.JLabel lblEstatura;
    private javax.swing.JLabel lblFechaNac;
    private javax.swing.JLabel lblFechaNaciMadre1;
    private javax.swing.JLabel lblFechaNaciPadre;
    private javax.swing.JLabel lblFechaNaciRepre;
    private javax.swing.JLabel lblFondo;
    private javax.swing.JLabel lblGrado;
    private javax.swing.JLabel lblHepatitis;
    private javax.swing.JLabel lblInfluenza;
    private javax.swing.JLabel lblLugarNacimiento;
    private javax.swing.JLabel lblNombreRepre;
    private javax.swing.JLabel lblNombresMadre;
    private javax.swing.JLabel lblNombresPadre;
    private javax.swing.JLabel lblObservacionRepre;
    private javax.swing.JLabel lblOcupacionMadre;
    private javax.swing.JLabel lblOcupacionPadre;
    private javax.swing.JLabel lblOcupacionRepre;
    private javax.swing.JLabel lblOtroTelefono;
    private javax.swing.JLabel lblOtroTelefonoMadre;
    private javax.swing.JLabel lblOtroTelefonoPadre;
    private javax.swing.JLabel lblOtroTelefonoRepre;
    private javax.swing.JLabel lblParentescoRepre;
    private javax.swing.JLabel lblPentavalente;
    private javax.swing.JLabel lblPeso;
    private javax.swing.JLabel lblPolio;
    private javax.swing.JLabel lblPrimerNombre;
    private javax.swing.JLabel lblProfesor;
    private javax.swing.JLabel lblRepre;
    private javax.swing.JLabel lblSeccion;
    private javax.swing.JLabel lblSexo;
    private javax.swing.JLabel lblTallaCalzado;
    private javax.swing.JLabel lblTallaCamisa;
    private javax.swing.JLabel lblTallaPantalon;
    private javax.swing.JLabel lblTelefono;
    private javax.swing.JLabel lblTelefonoMadre;
    private javax.swing.JLabel lblTelefonoPadre;
    private javax.swing.JLabel lblTelefonoRepre;
    private javax.swing.JLabel lblTipoSangre;
    private javax.swing.JLabel lblToxoide;
    private javax.swing.JLabel lblTrivalente;
    private javax.swing.JLabel lblTurno;
    private javax.swing.JPanel pnlDatosFamiliares;
    private javax.swing.JPanel pnlDatosPersonales;
    private javax.swing.JPanel pnlDatosSalud;
    private javax.swing.JTable tblHistorial;
    private javax.swing.JLabel txtAlergiaTratamiento;
    public static javax.swing.JLabel txtAñoEscolar;
    private javax.swing.JLabel txtCedula;
    public static javax.swing.JTextField txtCedulaEstu;
    public static javax.swing.JLabel txtCedulaMadre;
    public static javax.swing.JLabel txtCedulaPadre;
    public static javax.swing.JTextField txtCedulaRepre;
    private javax.swing.JLabel txtCirugiaTratamiento;
    private javax.swing.JLabel txtCodigoCanaima;
    public static javax.swing.JTextField txtCondicion;
    private javax.swing.JLabel txtDireccion;
    public static javax.swing.JLabel txtDireccionMadre;
    public static javax.swing.JLabel txtDireccionPadre;
    public static javax.swing.JLabel txtDireccionRepre;
    private javax.swing.JLabel txtDosisAntiama;
    private javax.swing.JLabel txtDosisAntineumo;
    private javax.swing.JLabel txtDosisBcg;
    private javax.swing.JLabel txtDosisHepa;
    private javax.swing.JLabel txtDosisInfluenza;
    private javax.swing.JLabel txtDosisPenta;
    private javax.swing.JLabel txtDosisPolio;
    private javax.swing.JLabel txtDosisToxoide;
    private javax.swing.JLabel txtDosisTrivalente;
    private javax.swing.JLabel txtEnfermedadTratamiento;
    private javax.swing.JLabel txtEstatura;
    private javax.swing.JLabel txtFechaNaci;
    public static javax.swing.JLabel txtFechaNaciMadre;
    public static javax.swing.JLabel txtFechaNaciPadre;
    public static javax.swing.JLabel txtFechaNaciRepre;
    public static javax.swing.JTextField txtGrado;
    private javax.swing.JLabel txtLugarNaci;
    private javax.swing.JLabel txtNombres;
    public static javax.swing.JLabel txtNombresMadre;
    public static javax.swing.JLabel txtNombresPadre;
    public static javax.swing.JLabel txtNombresRepre;
    public static javax.swing.JLabel txtOcupacionMadre;
    public static javax.swing.JLabel txtOcupacionPadre;
    public static javax.swing.JLabel txtOcupacionRepre;
    private javax.swing.JLabel txtOtroTelefono;
    public static javax.swing.JLabel txtOtroTelefonoMadre;
    public static javax.swing.JLabel txtOtroTelefonoPadre;
    public static javax.swing.JLabel txtOtroTlfRepre;
    public static javax.swing.JLabel txtParentescoRepre;
    private javax.swing.JLabel txtPeso;
    public static javax.swing.JTextField txtProfesor;
    public static javax.swing.JLabel txtRepre;
    public static javax.swing.JTextField txtSeccion;
    private javax.swing.JLabel txtSexo;
    private javax.swing.JLabel txtTallaCalzado;
    private javax.swing.JLabel txtTallaCamisa;
    private javax.swing.JLabel txtTallaPantalon;
    private javax.swing.JLabel txtTelefono;
    public static javax.swing.JLabel txtTelefonoMadre;
    public static javax.swing.JLabel txtTelefonoPadre;
    public static javax.swing.JLabel txtTelefonoRepre;
    private javax.swing.JLabel txtTipoSangre;
    public static javax.swing.JTextField txtTurno;
    // End of variables declaration//GEN-END:variables
}
