/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package proyectocomunitario;

import BD.conexion;
import java.awt.GraphicsEnvironment;
import java.awt.Point;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author NANY
 */
public class frmAdminSecciones extends javax.swing.JFrame {
DefaultTableModel modelo1;
    /**
     * Creates new form frmAdminSecciones
     */
    public frmAdminSecciones() {
        initComponents();
        cargarTabla("");
        this.setBounds(GraphicsEnvironment.getLocalGraphicsEnvironment().getMaximumWindowBounds());
         btnActualizar.setEnabled(false);
         btnCancelar.setEnabled(false);
         btnEliminar.setEnabled(false);
         btnModificar.setEnabled(false);
         btnRegistrr.setEnabled(false);
         cmbGrado.setEnabled(false);
         cmbSeccion.setEnabled(false);
         txtProfesores.setEnabled(false);
         btnCambiar.setVisible(false);
         cmbTurno.setEnabled(false);
         btnAgregar.setVisible(false);
         cmbMatricula.setEnabled(false);
         tblTablaSeccion.addMouseListener(new MouseAdapter(){
           public void mousePressed(MouseEvent Mouse_evt){
              JTable tabla= (JTable) Mouse_evt.getSource();
              Point point= Mouse_evt.getPoint();
              int row= tabla.rowAtPoint(point);
              if(Mouse_evt.getClickCount() ==2){
                  String seccion=(String) tabla.getValueAt(tabla.getSelectedRow(), 2);
                  String grado=(String) tabla.getValueAt(tabla.getSelectedRow(), 1);
                  cargar(seccion, grado);
                  btnNuevo.setEnabled(false);
                  btnModificar.setEnabled(true);
                  btnCancelar.setEnabled(true);
                  
              }
           }  
         });
       
    }
    void limpiar(){
        cmbGrado.setSelectedIndex(0);
        cmbSeccion.setSelectedIndex(0);
        cmbMatricula.setSelectedIndex(0);
        cmbTurno.setSelectedIndex(0);
        txtProfesores.setText("");
        
    }
    void eliminar(){
        conexion con=new conexion();
        Connection cn=con.conectar();
        String consulta="DELETE * FROM secciongrado WHERE seccion='"+cmbSeccion.getSelectedItem().toString()+"' AND grado='"+cmbGrado.getSelectedItem().toString()+"'";
        try {
            Statement st=cn.createStatement();
            st.execute(consulta);
            
        } catch (Exception e) {
        }
    }
    void actualizar(){
         String seccion="",grado="";
        conexion con=new conexion();
        Connection cn=con.conectar();
      
        String consulta2=" UPDATE  secciongrado SET grado=?,seccion=?,periodo_escolar=?,nombreProf=?,CedulaProf=?,turno=?,matricula=?,matricula_inscrita=?";
        String nombre = null;
        try {
            
            
               PreparedStatement stm=cn.prepareStatement(consulta2);
               String [] temp=txtProfesores.getText().split("\\s+");
               for (int i=1;i<=temp.length;i++){
                  if (i<temp.length){
                   nombre+=temp[i]+" ";
                  }else
                      if(i==temp.length){
                          nombre+=temp[i];
                      }
               }
               stm.setString(1, cmbGrado.getSelectedItem().toString());
               stm.setString(2, cmbSeccion.getSelectedItem().toString());
               stm.setString(3, txtAñoEscolar.getText());
               stm.setString(4, nombre);
               stm.setString(5, temp[0]);
               stm.setString(6, cmbTurno.getSelectedItem().toString());
               stm.setInt(7, Integer.parseInt(cmbMatricula.getSelectedItem().toString()));
               stm.setInt(8, 0);
               
            
        } catch (Exception e) {
        }
    }
    void registrar(){
        String seccion="",grado="";
        conexion con=new conexion();
        Connection cn=con.conectar();
        String consulta=" SELECT seccion, grado FROM secciongrado WHERE seccion='"+cmbSeccion.getSelectedItem().toString()+"' AND grado='"+cmbGrado.getSelectedItem().toString()+"'";
        String consulta2=" INSERT INTO secciongrado (grado,seccion,periodo_escolar,nombreProf,CedulaProf,turno,matricula,matricula_inscrita) VALUES (?,?,?,?,?,?,?,?)";
        String nombre = null;
        try {
            Statement st=cn.createStatement();
            ResultSet rs=st.executeQuery(consulta);
            while(rs.next()){
                seccion=rs.getString("seccion");
                grado=rs.getString("grado");
            }
            if(!seccion.equals("") && !grado.equals("")){
                 dlgYaexisteSeccion dialog = new dlgYaexisteSeccion(new javax.swing.JFrame(), true);
                 dialog.setVisible(true);
            }
            else{
               PreparedStatement stm=cn.prepareStatement(consulta2);
               String [] temp=txtProfesores.getText().split("\\s+");
               for (int i=1;i<=temp.length;i++){
                  if (i<temp.length){
                   nombre+=temp[i]+" ";
                  }else
                      if(i==temp.length){
                          nombre+=temp[i];
                      }
               }
               stm.setString(1, cmbGrado.getSelectedItem().toString());
               stm.setString(2, cmbSeccion.getSelectedItem().toString());
               stm.setString(3, txtAñoEscolar.getText());
               stm.setString(4, nombre);
               stm.setString(5, temp[0]);
               stm.setString(6, cmbTurno.getSelectedItem().toString());
               stm.setInt(7, Integer.parseInt(cmbMatricula.getSelectedItem().toString()));
               stm.setInt(8, 0);
               
            }
        } catch (Exception e) {
        }
        
    }
    void desactivar(){
        cmbGrado.setEnabled(false);
         cmbSeccion.setEnabled(false);
         txtProfesores.setEnabled(false);
         cmbTurno.setEnabled(false);
         cmbMatricula.setEnabled(false);
         
    }
    void activar(){
         cmbGrado.setEnabled(true);
         cmbSeccion.setEnabled(true);
         txtProfesores.setEnabled(true);
         cmbTurno.setEnabled(true);
         cmbMatricula.setEnabled(true);
    }
    void cargar(String seccion, String grado){
        conexion con=new conexion();
        Connection cn=con.conectar();
        String consulta="SELECT seccion,grado,nombreProf,CedulaProf,turno,matricula,matricula_inscrita FROM secciongrado WHERE seccion='"+seccion+"' AND grado='"+grado+"'";
        try {
            Statement st=cn.createStatement();
            ResultSet rs= st.executeQuery(consulta);
            while(rs.next()){
                cmbSeccion.setSelectedItem(rs.getString("seccion"));
                cmbGrado.setSelectedItem(rs.getString("grado"));
                txtProfesores.setText(rs.getString("CedulaProf")+" "+rs.getString("nombreProf"));
                cmbTurno.setSelectedItem(rs.getString("turno"));
                cmbMatricula.setSelectedItem(rs.getString("matricula"));
                txtMatriculaInscrita.setText(rs.getString("matricula_inscrita"));
            }
            
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, e);
        }
        
    }
 void cargarTabla(String valor){
        String titulos []= {"Profesor","Grado", "Sección", "Turno","Matricula de la Sección","Matricula Inscrita"};
        String registros[]= new String [6];
        modelo1 = new DefaultTableModel(null, titulos){
            @Override
            public boolean isCellEditable(int rowIndex,int columnIndex){
                return false;
            } 
        };
        
        conexion con=new conexion();
        Connection cn=con.conectar();
        
        String query="SELECT CedulaProf, nombreProf, grado, seccion, turno, matricula, matricula_inscrita FROM (secciongrado)  WHERE   CONCAT (CedulaProf, nombreProf, grado, seccion, turno, matricula, matricula_inscrita) LIKE '%"+valor+"%'";
        
        try {
            Statement st=cn.createStatement();
            ResultSet rs= st.executeQuery(query);
            while(rs.next()){
                registros[0]=rs.getString("CedulaProf")+" "+rs.getString("nombreProf");
                registros[1]=rs.getString("grado");
                registros[2]=rs.getString("seccion");
                registros[3]=rs.getString("turno");
                registros[4]=rs.getString("matricula");
                registros[5]=rs.getString("matricula_inscrita");
                modelo1.addRow(registros);
            }
            tblTablaSeccion.setModel(modelo1);
            
            
        } catch (SQLException ex) {
            
            JOptionPane.showMessageDialog(null,ex);
        }
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new imagenes.Fondo("fondo escritorio.jpg");
        cmbTurno = new javax.swing.JComboBox();
        lblSeccion = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        cmbMatricula = new javax.swing.JComboBox();
        lblTurno = new javax.swing.JLabel();
        cmbGrado = new javax.swing.JComboBox();
        jLabel1 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblTablaSeccion = new javax.swing.JTable();
        lblGrado = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        cmbSeccion = new javax.swing.JComboBox();
        lblMatricula = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        btnNuevo = new javax.swing.JButton();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        btnRegistrr = new javax.swing.JButton();
        btnSalir = new javax.swing.JButton();
        jSeparator2 = new javax.swing.JSeparator();
        jSeparator3 = new javax.swing.JSeparator();
        jSeparator4 = new javax.swing.JSeparator();
        jSeparator5 = new javax.swing.JSeparator();
        jSeparator6 = new javax.swing.JSeparator();
        jSeparator7 = new javax.swing.JSeparator();
        jSeparator8 = new javax.swing.JSeparator();
        btnActualizar = new javax.swing.JButton();
        jSeparator9 = new javax.swing.JSeparator();
        btnCancelar = new javax.swing.JButton();
        jSeparator10 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        txtAñoEscolar = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        txtMatriculaInscrita = new javax.swing.JLabel();
        txtbusqueda = new javax.swing.JTextField();
        btnbuscar = new javax.swing.JButton();
        txtProfesores = new javax.swing.JTextField();
        btnCambiar = new javax.swing.JButton();
        btnAgregar = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        cmbTurno.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbTurno.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "Mañana", "Tarde" }));

        lblSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblSeccion.setText("Sección");

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/administracion.png"))); // NOI18N

        cmbMatricula.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbMatricula.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40" }));

        lblTurno.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblTurno.setText("Turno");

        cmbGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbGrado.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "1", "2", "3", "4", "5", "6" }));
        cmbGrado.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cmbGradoActionPerformed(evt);
            }
        });

        jLabel1.setFont(new java.awt.Font("Comfortaa", 1, 60)); // NOI18N
        jLabel1.setText("Administración");

        jScrollPane1.setFont(new java.awt.Font("Comfortaa", 1, 14)); // NOI18N

        tblTablaSeccion.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        tblTablaSeccion.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jScrollPane1.setViewportView(tblTablaSeccion);

        lblGrado.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblGrado.setText("Grado");

        jLabel3.setFont(new java.awt.Font("Comfortaa", 1, 36)); // NOI18N
        jLabel3.setText("Secciones");

        cmbSeccion.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        cmbSeccion.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Selec", "A", "B", "C", "D", "E", "F" }));

        lblMatricula.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        lblMatricula.setText("Matricula de la Sección");

        jLabel2.setFont(new java.awt.Font("Comfortaa", 0, 36)); // NOI18N
        jLabel2.setText("de");

        jPanel2.setBackground(new java.awt.Color(0, 153, 102));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        btnNuevo.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnNuevo.setForeground(new java.awt.Color(255, 255, 255));
        btnNuevo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/agregar2.png"))); // NOI18N
        btnNuevo.setText("Nuevo");
        btnNuevo.setBorder(null);
        btnNuevo.setContentAreaFilled(false);
        btnNuevo.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/new.png"))); // NOI18N
        btnNuevo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnNuevoActionPerformed(evt);
            }
        });

        btnModificar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnModificar.setForeground(new java.awt.Color(255, 255, 255));
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/pen.png"))); // NOI18N
        btnModificar.setText("Modificar");
        btnModificar.setBorder(null);
        btnModificar.setContentAreaFilled(false);
        btnModificar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/modi.png"))); // NOI18N
        btnModificar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnModificarActionPerformed(evt);
            }
        });

        btnEliminar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnEliminar.setForeground(new java.awt.Color(255, 255, 255));
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/eliminar.png"))); // NOI18N
        btnEliminar.setText("Eliminar");
        btnEliminar.setBorder(null);
        btnEliminar.setContentAreaFilled(false);
        btnEliminar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancell.png"))); // NOI18N
        btnEliminar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEliminarActionPerformed(evt);
            }
        });

        btnRegistrr.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnRegistrr.setForeground(new java.awt.Color(255, 255, 255));
        btnRegistrr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Guardar.png"))); // NOI18N
        btnRegistrr.setText("Registrar");
        btnRegistrr.setBorder(null);
        btnRegistrr.setContentAreaFilled(false);
        btnRegistrr.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/registrar 2 icono.png"))); // NOI18N
        btnRegistrr.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnRegistrrActionPerformed(evt);
            }
        });

        btnSalir.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnSalir.setForeground(new java.awt.Color(255, 255, 255));
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/salir.png"))); // NOI18N
        btnSalir.setText("Menú Principal");
        btnSalir.setBorder(null);
        btnSalir.setContentAreaFilled(false);
        btnSalir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/home.png"))); // NOI18N
        btnSalir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnSalirActionPerformed(evt);
            }
        });

        btnActualizar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnActualizar.setForeground(new java.awt.Color(255, 255, 255));
        btnActualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/actualizarr!!.png"))); // NOI18N
        btnActualizar.setText("Actualizar");
        btnActualizar.setBorder(null);
        btnActualizar.setContentAreaFilled(false);
        btnActualizar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/actualizar!.png"))); // NOI18N
        btnActualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnActualizarActionPerformed(evt);
            }
        });

        btnCancelar.setFont(new java.awt.Font("Comfortaa", 1, 18)); // NOI18N
        btnCancelar.setForeground(new java.awt.Color(255, 255, 255));
        btnCancelar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancelar (2).png"))); // NOI18N
        btnCancelar.setText("Cancelar");
        btnCancelar.setBorder(null);
        btnCancelar.setContentAreaFilled(false);
        btnCancelar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cancel.png"))); // NOI18N
        btnCancelar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCancelarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnNuevo, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnRegistrr, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnModificar, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnActualizar, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnEliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(78, 78, 78)
                .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnNuevo)
                .addGap(7, 7, 7)
                .addComponent(jSeparator3, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnRegistrr)
                .addGap(7, 7, 7)
                .addComponent(jSeparator4, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnModificar)
                .addGap(7, 7, 7)
                .addComponent(jSeparator5, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnActualizar)
                .addGap(7, 7, 7)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator6, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(btnEliminar)))
                .addGap(7, 7, 7)
                .addComponent(jSeparator9, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, 0)
                .addComponent(btnCancelar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(10, 10, 10)
                .addComponent(jSeparator10, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 63, Short.MAX_VALUE)
                .addComponent(jSeparator8, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(btnSalir, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(6, 6, 6)
                .addComponent(jSeparator7, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jLabel5.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel5.setText("Año Escolar");

        jLabel6.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel6.setText("Profesor");

        txtAñoEscolar.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        jLabel7.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        jLabel7.setText("Matricula Inscrita:");

        txtMatriculaInscrita.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N

        txtbusqueda.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtbusqueda.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtbusquedaKeyTyped(evt);
            }
        });

        btnbuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/lens.png"))); // NOI18N
        btnbuscar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/buscar icono.png"))); // NOI18N
        btnbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbuscarActionPerformed(evt);
            }
        });

        txtProfesores.setFont(new java.awt.Font("Comfortaa", 1, 16)); // NOI18N
        txtProfesores.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtProfesoresActionPerformed(evt);
            }
        });

        btnCambiar.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        btnCambiar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cambiar.png"))); // NOI18N
        btnCambiar.setText("Cambiar");
        btnCambiar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/cambiar!.png"))); // NOI18N
        btnCambiar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCambiarActionPerformed(evt);
            }
        });

        btnAgregar.setFont(new java.awt.Font("Comfortaa", 1, 12)); // NOI18N
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/add.png"))); // NOI18N
        btnAgregar.setText("Agregar");
        btnAgregar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
        btnAgregar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAgregarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 230, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 824, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel1)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(300, 300, 300)
                                                .addComponent(jLabel2))
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(360, 360, 360)
                                                .addComponent(jLabel3)))
                                        .addGap(153, 153, 153)
                                        .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(230, 230, 230)
                                        .addComponent(jLabel5)
                                        .addGap(6, 6, 6)
                                        .addComponent(txtAñoEscolar, javax.swing.GroupLayout.PREFERRED_SIZE, 71, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblGrado)
                                        .addGap(10, 10, 10)
                                        .addComponent(cmbGrado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(24, 24, 24)
                                        .addComponent(lblSeccion)
                                        .addGap(4, 4, 4)
                                        .addComponent(cmbSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(33, 33, 33)
                                        .addComponent(jLabel6)
                                        .addGap(4, 4, 4)
                                        .addComponent(txtProfesores, javax.swing.GroupLayout.PREFERRED_SIZE, 290, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(5, 5, 5)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addGroup(jPanel1Layout.createSequentialGroup()
                                                .addGap(1, 1, 1)
                                                .addComponent(btnCambiar))))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(lblTurno)
                                        .addGap(10, 10, 10)
                                        .addComponent(cmbTurno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(51, 51, 51)
                                        .addComponent(lblMatricula)
                                        .addGap(10, 10, 10)
                                        .addComponent(cmbMatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(52, 52, 52)
                                        .addComponent(jLabel7)
                                        .addGap(6, 6, 6)
                                        .addComponent(txtMatriculaInscrita, javax.swing.GroupLayout.PREFERRED_SIZE, 47, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addComponent(txtbusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 293, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(btnbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGap(0, 3, Short.MAX_VALUE))
                            .addComponent(jSeparator1))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(14, 14, 14)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(70, 70, 70)
                                .addComponent(jLabel2))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addGap(60, 60, 60)
                                .addComponent(jLabel3))))
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(9, 9, 9)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(5, 5, 5)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel5)
                    .addComponent(txtAñoEscolar, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(19, 19, 19)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(lblGrado))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(cmbGrado, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(lblSeccion))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(cmbSeccion, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel6))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(txtProfesores, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btnAgregar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(btnCambiar, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(3, 3, 3)
                        .addComponent(lblTurno))
                    .addComponent(cmbTurno, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(lblMatricula))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(1, 1, 1)
                        .addComponent(cmbMatricula, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(4, 4, 4)
                        .addComponent(jLabel7))
                    .addComponent(txtMatriculaInscrita, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(6, 6, 6)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txtbusqueda, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btnbuscar))
                .addGap(18, 18, 18)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(11, 11, 11))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void cmbGradoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cmbGradoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cmbGradoActionPerformed

    private void btnRegistrrActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnRegistrrActionPerformed
        // TODO add your handling code here:
if((cmbGrado.getSelectedIndex()==0)|| (cmbSeccion.getSelectedIndex()==0) || (cmbTurno.getSelectedIndex()==0) || (txtProfesores.getText()=="") || (cmbMatricula.getSelectedIndex()==0)){
     dlgDatosVacios dialog = new dlgDatosVacios(new javax.swing.JFrame(), true);
     dialog.setVisible(true);
}
else{
    registrar();
    btnRegistrr.setEnabled(false);
    btnCancelar.setEnabled(false);
    btnNuevo.setEnabled(true);
    btnAgregar.setVisible(false);
    limpiar();
    desactivar();
}

    }//GEN-LAST:event_btnRegistrrActionPerformed

    private void btnSalirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnSalirActionPerformed
        // TODO add your handling code here:
        menuInicio obj=new menuInicio();
        obj.setVisible(true);
        this.dispose();
    }//GEN-LAST:event_btnSalirActionPerformed

    private void txtbusquedaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbusquedaKeyTyped
        // TODO add your handling code here:
        cargarTabla(txtbusqueda.getText());
    }//GEN-LAST:event_txtbusquedaKeyTyped

    private void btnbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbuscarActionPerformed
cargarTabla(txtbusqueda.getText());
// TODO add your handling code here:
    }//GEN-LAST:event_btnbuscarActionPerformed

    private void txtProfesoresActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtProfesoresActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtProfesoresActionPerformed

    private void btnCambiarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCambiarActionPerformed
        // TODO add your handling code here:
        dlgCambiarProfesor dialog = new dlgCambiarProfesor(new javax.swing.JFrame(), true);
        dialog.setVisible(true);
        dialog.txtProfesor.setText(this.txtProfesores.getText());
    }//GEN-LAST:event_btnCambiarActionPerformed

    private void btnNuevoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnNuevoActionPerformed
        // TODO add your handling code here:
        activar();
        btnRegistrr.setEnabled(true);
        btnCancelar.setEnabled(true);
        btnActualizar.setEnabled(false);
        btnModificar.setEnabled(false);
        btnEliminar.setEnabled(false);
        btnAgregar.setVisible(true);
        btnCambiar.setVisible(false);
        btnNuevo.setEnabled(false);
    }//GEN-LAST:event_btnNuevoActionPerformed

    private void btnAgregarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnAgregarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btnAgregarActionPerformed

    private void btnModificarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnModificarActionPerformed
activar();
cmbGrado.setEnabled(false);
cmbSeccion.setEnabled(false);
btnNuevo.setEnabled(false);
btnModificar.setEnabled(false);
btnCancelar.setEnabled(true);
btnEliminar.setEnabled(true);
btnActualizar.setEnabled(true);
    }//GEN-LAST:event_btnModificarActionPerformed

    private void btnActualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnActualizarActionPerformed
        // TODO add your handling code here:
        if((cmbGrado.getSelectedIndex()==0)|| (cmbSeccion.getSelectedIndex()==0) || (cmbTurno.getSelectedIndex()==0) || (txtProfesores.getText()=="") || (cmbMatricula.getSelectedIndex()==0)){
     dlgDatosVacios dialog = new dlgDatosVacios(new javax.swing.JFrame(), true);
     dialog.setVisible(true);
}
        else{
            actualizar();
            btnActualizar.setEnabled(false);
            btnEliminar.setEnabled(false);
            btnCancelar.setEnabled(false);
            btnCambiar.setVisible(false);
            limpiar();
            desactivar();
        }
    }//GEN-LAST:event_btnActualizarActionPerformed

    private void btnEliminarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnEliminarActionPerformed
        // TODO add your handling code here:
        int valor;

        valor=JOptionPane.showConfirmDialog(null,"\t\t¿ESTÁ SEGURO QUE DESEA ELMINAR LA SECCIÓN?\nSe Borraran todos los datos","Confirmar salida",JOptionPane.YES_NO_OPTION,JOptionPane.WARNING_MESSAGE);
        if (valor==JOptionPane.YES_OPTION){
            eliminar();
            limpiar();
           desactivar();
           btnActualizar.setEnabled(false);
           btnCambiar.setVisible(false);
           btnCancelar.setEnabled(false);
           btnEliminar.setEnabled(false);
           btnNuevo.setEnabled(true);
           
            
            
        }
    }//GEN-LAST:event_btnEliminarActionPerformed

    private void btnCancelarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnCancelarActionPerformed
        // TODO add your handling code here:
        desactivar();
        limpiar();
        btnAgregar.setVisible(false);
        btnCambiar.setVisible(false);
        btnNuevo.setEnabled(true);
        btnModificar.setEnabled(false);
        btnActualizar.setEnabled(false);
        btnRegistrr.setEnabled(false);
        btnEliminar.setEnabled(false);
        btnCancelar.setEnabled(false);
    }//GEN-LAST:event_btnCancelarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmAdminSecciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmAdminSecciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmAdminSecciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmAdminSecciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmAdminSecciones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnActualizar;
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnCambiar;
    private javax.swing.JButton btnCancelar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnNuevo;
    private javax.swing.JButton btnRegistrr;
    private javax.swing.JButton btnSalir;
    private javax.swing.JButton btnbuscar;
    private javax.swing.JComboBox cmbGrado;
    private javax.swing.JComboBox cmbMatricula;
    private javax.swing.JComboBox cmbSeccion;
    private javax.swing.JComboBox cmbTurno;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lblGrado;
    private javax.swing.JLabel lblMatricula;
    private javax.swing.JLabel lblSeccion;
    private javax.swing.JLabel lblTurno;
    private javax.swing.JTable tblTablaSeccion;
    private javax.swing.JLabel txtAñoEscolar;
    private javax.swing.JLabel txtMatriculaInscrita;
    public static javax.swing.JTextField txtProfesores;
    private javax.swing.JTextField txtbusqueda;
    // End of variables declaration//GEN-END:variables
}
