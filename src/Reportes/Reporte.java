/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Reportes;

import java.net.URL;
import java.sql.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.swing.JFrame;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.swing.JRViewer;

/**
 *
 * @author Equipo1
 */
public class Reporte {
    
    public void VeReportConnect(String nombReport, Connection Conexion){
        org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
        
        JasperReport jasperReport;
        JasperPrint jasperPrint;
        try{
            URL  in=this.getClass().getResource( nombReport+".jasper" );
            jasperReport=(JasperReport)JRLoader.loadObject(in);
            jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap() , Conexion);
            JFrame jFrame = new JFrame("Reporte");
            jFrame.getContentPane().add(new JRViewer(jasperPrint));
            jFrame.pack();
            //jFrame.setExtendedState(JFrame.MAXIMIZED_BOTH);
            jFrame.setAlwaysOnTop(true);
            jFrame.setSize(700, 500);
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
            
        }catch (JRException ex){
            System.err.println( "Error iReport: " + ex.getMessage() );
        }
    }
    
    public void VeReportParametrs(String nombReport, Map param, List objetos){
        org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
        
        JasperReport jasperReport;
        JasperPrint jasperPrint;
        try{
            URL  in=this.getClass().getResource( nombReport+".jasper" );
            jasperReport=(JasperReport)JRLoader.loadObject(in);
            
            jasperPrint = JasperFillManager.fillReport(jasperReport, param, new JRBeanCollectionDataSource(objetos) );
            
            JFrame jFrame = new JFrame("Reporte");
            jFrame.getContentPane().add(new JRViewer(jasperPrint));
            jFrame.pack();
            //jFrame.setAlwaysOnTop(true);
            jFrame.setSize(700, 500);
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
            
        }catch (JRException ex){
            System.err.println( "Error iReport: " + ex.getMessage() );
        }
    }
    
    public void VeReportParametrs(String nombReport, List objetos){
        org.apache.log4j.Logger.getRootLogger().setLevel(org.apache.log4j.Level.OFF);
        
        JasperReport jasperReport;
        JasperPrint jasperPrint;
        try{
            URL  in=this.getClass().getResource( nombReport+".jasper" );
            jasperReport=(JasperReport)JRLoader.loadObject(in);
            
            jasperPrint = JasperFillManager.fillReport(jasperReport, new HashMap(), new JRBeanCollectionDataSource(objetos) );
            
            JFrame jFrame = new JFrame("Reporte");
            jFrame.getContentPane().add(new JRViewer(jasperPrint));
            jFrame.pack();
            //jFrame.setAlwaysOnTop(true);
            jFrame.setSize(700, 500);
            jFrame.setLocationRelativeTo(null);
            jFrame.setVisible(true);
            
        }catch (JRException ex){
            System.err.println( "Error iReport: " + ex.getMessage() );
        }
    }
}
